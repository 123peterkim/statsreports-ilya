"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_staff = sequelize.define('tbl_staff', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		staff_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_staff_types',
				key: 'id'
			}
		},
		fname: {
			type: DataTypes.STRING
		},
		lname: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_staff.belongsTo(models.tbl_staff_types, {foreignKey: 'staff_type_id', as: 'staff_type'});
			tbl_staff.belongsToMany(models.date_playlists, {as: 'date_playlists', through: models.staff_playlists, foreignKey: 'staff_id', otherKey: 'date_playlist_id'});
		}
	});
	return tbl_staff;
};