<?php
	include("../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$comp_group = mysql_real_escape_string(strtolower($_GET["comp_group"]));
	if($comp_group == "finals")
		$disp_comp_group = "Finals";
	if($comp_group == "vips")
		$disp_comp_group = "VIP";
	if($comp_group == "vips" && $eventid == 14)
		$disp_comp_group = "Best Dancer";
	if($comp_group == "prelims")
		$disp_comp_group = "Prelims";
	$citydata = db_get("city,venue_name","tbl_tour_dates","id=$tourdateid");

	$sql = "SELECT dates,awards FROM `tbl_date_schedule_competition` WHERE tourdateid=$tourdateid LIMIT 1";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			//get all routines in order
			$routines = get_competition_schedule_in_order($tourdateid,$comp_group);
			$dates_raw = json_decode($row["dates"],true);
			$dates = $dates_raw[$comp_group];
			$awards_raw = json_decode($row["awards"],true);
			$awards = $awards_raw[$comp_group];
		}

		foreach($routines as $rkey=>$routine) {
			if($disp_comp_group == "Best Dancer") {
				$routines[$rkey]["routinename"] = $routine["routinename"]." (".$routine["solodancer"].")";
				$routines[$rkey]["perfdivisionname"] = "";
			}
			if(!strlen($routine["time"]) > 0) {
				$routines[$rkey]["time"] = mktime(6,9,0,6,9,1969); //lol
			}
			$routines[$rkey]["teacher"] = db_one("teacher","tbl_routines","id=".$routine["routineid"]);
		}

		if(count($dates) > 0) {
			//clean up dates array
			$newdates = Array();
			foreach($dates as $key=>$value) {
				$expl = explode(" ",$key);
				$datestr = str_replace(array("(",")"),"",$expl[1]);
				list($mm,$dd,$yy) = explode("/",$datestr);
				$datemk = mktime(0,0,0,$mm,$dd,$yy);

				$startexpl = explode(":",$value["start_time"]);
				$hours = $startexpl[0];
				$mins = substr($startexpl[1],0,2);
				$ampm = strtolower(substr($startexpl[1],2,4));
				if($ampm == "pm" && intval($hours) != "12")
					$hours = intval($hours)+12;
				$start_time_mk = mktime($hours,$mins,0,$mm,$dd,$yy);
				$newdates[$datemk] = array("start_time"=>$start_time_mk,"end_routine"=>$value["end_routine"],"dateroutineid"=>$value["dateroutineid"]);
				asort($newdates);
				$dates = $newdates;
				$title_date = "";
 				foreach($dates as $date=>$datedata) {
					$title_date = date('n/d/Y',$date);
					break;
				}
			}
		}

		if(count($awards) > 0) {
			//clean up awards array
			$newawards = Array();
			foreach($awards as $key=>$value) {
				$expl = explode(" ",$value["date"]);
				$datestr = str_replace(array("(",")"),"",$expl[1]);
				list($mm,$dd,$yy) = explode("/",$datestr);
				$datemk = mktime(0,0,0,$mm,$dd,$yy);
				$awards[$key]["desc"] = str_replace("&","&amp;",$awards[$key]["desc"]);
				$durexpl = explode(":",$value["dur"]);
				$mins = ($durexpl[0] * 60) + $durexpl[1];
				$awards[$key]["date"] = $datemk;
				$awards[$key]["dur"] = $mins;
			}
		}
	}

//	$newpage = true;
	$maxrows = 22;
	$rowcount = 4;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Competition Schedule</title>
		<style type="text/css">
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.sched_table {
				margin-top:8px;
			}

			.sched_table tr td {
				border-left: 1px solid #000000;
				border-top: 1px solid #000000;
				font-size: 8pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
			}

			.tline {
				padding: 1px 0;
				text-align: left;
			}
		</style>
	</head>
	<body>
		<div style="width: 952px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-family:Unplug;font-size:20pt;"><?php print($citydata[0]["city"]); ?> <?php print($disp_comp_group); ?> Competition</div>
						<div style="font-size:16px;font-family:Unplug;"><?php print($citydata[0]["venue_name"]); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;font-size:14px;font-family:Unplug;">
						<?php print($title_date); ?>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" class="sched_table">
				<tr style="page-break-before:always;">
					<td class="thead" style="width:40px;">#</td>
					<td class="thead" style="width:60px;">Time</td>
					<td class="thead" style="width:250px;">Studio</td>
					<td class="thead" style="width:260px;">Routine Name</td>
					<td class="thead" style="width:110px;">Age Division</td>
					<td class="thead" style="width:110px;">Category</td>
					<td class="thead" style="width:110px;border-right:1px solid #000000;">Perf. Division</td>
				</tr>
						<?php
							$currdate = date('m/d/y',$routines[0]["time"]);
							for($i=0;$i<count($routines);$i++) {
								if(date('m/d/y',$routines[$i]["time"]) != $currdate) {
									$newpage = true;
									$rowcount = 4;
									$currdate = date('m/d/y',$routines[$i]["time"]);
								}
								if($newpage) {
						?>
							<table cellpadding="0" cellspacing="0" style="width: 100%;page-break-before:always;">
								<tr>
									<td style="vertical-align:top;">
										<div style="font-family:Unplug;font-size:20pt;"><?php print($citydata[0]["city"]); ?> <?php print($disp_comp_group); ?> Competition</div>
										<div style="font-size:16px;font-family:Unplug;"><?php print($citydata[0]["venue_name"]); ?></div>
									</td>
									<td style="vertical-align:top;text-align:right;font-size:14px;font-family:Unplug;">
										<?php print(date('n/d/Y',$routines[$i]["time"])); ?>
									</td>
								</tr>
							</table>
							<table cellpadding="0" cellspacing="0" class="sched_table">
								<tr style="page-break-before:always;">
									<td class="thead" style="width:40px;">#</td>
									<td class="thead" style="width:60px;">Time</td>
									<td class="thead" style="width:250px;">Studio</td>
									<td class="thead" style="width:260px;">Routine Name</td>
									<td class="thead" style="width:110px;">Age Division</td>
									<td class="thead" style="width:110px;">Category</td>
									<td class="thead" style="width:110px;border-right:1px solid #000000;">Perf. Division</td>
								</tr>
							<?php	$newpage = false;
									++$rowcount;
								}	?>
								<tr<?php if($rowcount == $maxrows-1)print(' style="page-break-after:always;"'); ?>>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>text-align:center;"><?php print($routines[$i]["dispnumber"]); ?></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>text-align: center;"><?php print(date('g:i A',$routines[$i]["time"])); ?></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>padding-left:2px;"><div style="width: 243px; overflow: hidden; white-space: nowrap;"><?php print(stripslashes($routines[$i]["studioname"])." (".$routines[$i]["studiocode"].")"); ?></div></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>padding-left:2px;"><div style="width: 253px; overflow: hidden; white-space: nowrap;"><?php if($routines[$i]["routinecategoryname"] == "Solo") print($routines[$i]["solodancer"]." (".$routines[$i]["routinename"].")"); else print(stripslashes($routines[$i]["routinename"])); ?><?="<br/>".$routines[$i]["teacher"];?></div></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>padding-left:2px;"><?php print($routines[$i]["agedivisionnamerange"]); ?></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>padding-left:2px;"><?php if($routines[$i]["routinecategoryname"] == "Extended Time Production") print("Ext. Time Production"); else print($routines[$i]["routinecategoryname"]); ?></td>
									<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<? } ?>padding-left:2px;border-right: 1px solid #000000;"><?php print($routines[$i]["perfdivisionname"]); ?></td>
								</tr>
						<?php
								list($mm,$dd,$yy) = explode("/",date('m/d/Y',$routines[$i]["time"]));
								$date = mktime(0,0,0,$mm,$dd,$yy);
								if(count($awards) > 0) {
									foreach($awards as $award) {
										if(($routines[$i]["dateroutineid"] == $award["dateroutineid"]) && ($date == $award["date"])) {
											++$rowcount; ?>
											<tr><td colspan="7" class="tline" style="padding-left:2px;<?php if(($i == (count($routines)-1))) print("border-bottom:1px solid #000000;"); ?>font-weight:bold;border-right:1px solid #000000;"><?php print(stripslashes($award["desc"])); ?></td></tr>
						<?php			}
									}
								}
								if($rowcount == $maxrows-1) {
//									$rowcount = 4;
									$newpage = true;
								}
								else
									++$rowcount;

							}
						?>
			</table>
		</div>
	</body>
</html>