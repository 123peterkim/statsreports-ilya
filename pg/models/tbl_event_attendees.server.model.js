"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_event_attendees = sequelize.define('tbl_event_attendees', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		event_registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_registrations',
				key: 'id'
			}
		},
		event_reg_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_reg_types',
				key: 'id'
			}
		},
		intensive_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		promo_code_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes',
				key: 'id'
			}
		},
		scholarships: {
			type: DataTypes.JSONB
		},
		note: {
			type: DataTypes.TEXT
		},
		canceled: {
			type: DataTypes.INTEGER
		},
		audition_number: {
			type: DataTypes.INTEGER
		},
		non_commuter: {
			type: DataTypes.STRING
		},
		fee: {
			type: DataTypes.INTEGER
		},
		custom_fee: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_event_attendees.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			tbl_event_attendees.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			tbl_event_attendees.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
			tbl_event_attendees.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
			tbl_event_attendees.belongsTo(models.tbl_event_registrations, {foreignKey: 'event_registration_id', as: 'event_registration'});
			tbl_event_attendees.belongsTo(models.tbl_event_reg_types, {foreignKey: 'event_reg_type_id', as: 'event_reg_type'});
			tbl_event_attendees.belongsTo(models.tbl_events, {foreignKey: 'intensive_id', as: 'intensive'});
			tbl_event_attendees.belongsTo(models.tbl_promo_codes, {foreignKey: 'promo_code_id', as: 'promo_code'});
		}
	});
	return tbl_event_attendees;
};