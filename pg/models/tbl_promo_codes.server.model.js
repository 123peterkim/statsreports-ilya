"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_promo_codes = sequelize.define('tbl_promo_codes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		promo_codes_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes_types',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		intensive_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		description: {
			type: DataTypes.TEXT
		},
		value: {
			type: DataTypes.INTEGER
		},
		charges: {
			type: DataTypes.INTEGER
		},
		uses: {
			type: DataTypes.INTEGER
		},
		active: {
			type: DataTypes.INTEGER
		},
		non_commuter_only: {
			type: DataTypes.INTEGER
		},
		once_per_registration: {
			type: DataTypes.INTEGER
		},
		expires: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_promo_codes.belongsTo(models.tbl_promo_codes_types, {foreignKey: 'promo_codes_type_id', as: 'promo_codes_type'});
			tbl_promo_codes.belongsTo(models.tbl_events, {foreignKey: 'intensive_id', as: 'intensive'});
			tbl_promo_codes.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});

		}
	});
	return tbl_promo_codes;
};