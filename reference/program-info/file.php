<?php
	include_once("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$compgroup = "finals";
	if(!$tourdateid > 1) exit();
	$routines = get_competition_schedule_in_order($tourdateid,$compgroup,true);

	$studios = array();

	if($tourdateid==113)
		$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.city, tbl_studios.state FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid='$tourdateid' AND tbl_studios.name!='[N/A]' ORDER BY tbl_studios.name ASC";
	else
		$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.city, tbl_studios.state FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid='$tourdateid' AND tbl_date_studios.independent=0 ORDER BY tbl_studios.name ASC";

	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);
			$abbr = db_one("abbreviation","tbl_states","name='".$row["state"]."'");
			if(strlen($abbr) > 0)
				$row["state"] = $abbr;

			$studios[] = $row;
		}
	}
//	print_r($studios);
?><html>
	<head>
		<style type="text/css">
		body, html {
				font-family: Calibri, "Calibri", sans-serif;
			}
			.rtable tr td {
				font-size: 8px;
				vertical-align: top;
				font-family: Calibri, 'Calibri', sans-serif;
			}
		</style>
	</head>
	<body>
		Select all studios or routines, copy and paste into Word template.  Select what you just pasted, make sure font is "Calibri" and font size is 9.
		<h1>Studios</h1>
			<?php
			if(count($studios) > 0) {
					foreach($studios as $studio) { ?>
						<table cellpadding="0" cellspacing="0" class="rtable" style="width:465px;" id="studiotable">
							<tr>
								<td style="padding: 1px 0;font-size: 9px;vertical-align: top;border-bottom:1px solid #777777;text-align:left;"><?php print($studio["studioname"]);?></td>
								<td style="font-style:italic;padding: 1px 0;font-size: 9px;vertical-align: top;border-bottom:1px solid #777777;text-align:right;"><?php print($studio["city"].", ".$studio["state"]);?></td>
							</tr>
						</table>
			<?php	}
				}
			?>
			<div style="margin-top:30px;"></div>
			<h1>Routines</h1>
			<?php
				if(count($routines) > 0) {
					foreach($routines as $routine) { ?>
						<table cellpadding="0" cellspacing="0" class="rtable" style="width:460px;">
							<tr>
								<td style="font-size:8px;width:20px;text-align:right;font-weight:bold;padding-right:4px;"><?php print($routine["dispnumber"]);?>.</td>
								<td style="font-size:8px;font-weight:bold;"><?php print($routine["routinename"]);?></td>
								<td style="font-size:8px;text-align:right;text-transform: uppercase;"><?php print($routine["perfdivisionname"]." ".$routine["routinecategoryname"]." (".$routine["agedivisionrange"].")");?></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td style="font-size:8px;font-weight:bold;font-style:italic;"><?php print(str_replace("&amp;","&",$routine["studioname"])." (".$routine["studiocode"].")");?></td>
								<td style="font-size:8px;text-align:right;"><?php print($routine["hmtime2"]);?></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td colspan="2" style="font-size:8px;font-style:italic;"><?php print($routine["dancers_str"]);?></td>
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
							</tr>
						</table>
			<?php	}
				}
			?>

	</body>
</html>