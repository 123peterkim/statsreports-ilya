var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var compOnly = req.query.componly ? true : false;

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                { model: db.tbl_event_cities, as: 'event_city', include: [
                    { model: db.tbl_states, as: 'state' }
                ]},
                { model: db.tbl_events, as: 'event' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if(tourDate) {
                return {
                    city_name: tourDate.event_city.name,
                    state: tourDate.event_city.state.name,
                    disp_date: util.tourDateDisplayDate(tourDate),
                    perf_div_type_id: tourDate.perf_div_type_id,
                    studios: []
                };
            }
            else {
                throw new Error('No results')
            }
        })
        .then(function(studiosResData) {
            // GET STUDIOS
            /* SELECT tbl_date_studios.studioid, tbl_studios.contacts, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname
                FROM `tbl_date_studios`
                LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid
                WHERE tbl_date_studios.tourdateid=$tourdateid
                ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC */
            db.date_studios.findAll({
                where: { tour_date_id: tourDateId },
                include: [
                    { model: db.tbl_studios, as: 'studio' },
                    { model: db.tbl_registrations, as: 'registrations' },
                    { model: db.date_studio_awards, as: 'studio_awards', include: [
                        { model: db.tbl_studio_awards, as: 'studio_award', include: [
                            { model: db.tbl_award_types, as: 'award_type' }
                        ]}
                    ]}
                ]
            })
            .then(function(results) {
                var tasks = [];
                _.forEach(results, function(dateStudio) {
                    var studio = {
                        awarded: false,
                        studio_name: dateStudio.studio.name,
                        studio_code: dateStudio.studio.code,
                        well_rounded: false,
                        has1st_solo: false,
                        has1st_duotrio: false,
                        vip_haswinner: false,
                        vip_hasrunnerup: false,
                        hsp: {
                            Mini: {},
                            Junior: {},
                            Teen: {},
                            Senior: {},
                            "Mini/Junior": {},
                            "Teen/Senior": {}
                        }
                    };
                    
                    tasks.push(function(callback) {
                        var waterfall = [
                            // ADJUDICATED AWARDS
                            function (cb) {
                                var sql = "select routine.name as routine_name, total_score as finals_total_score, t2.name as award_name \
                                    from date_routines \
                                    join date_routines_scoring on date_routines.id = date_routines_scoring.date_routine_id \
                                    join tbl_competition_groups g ON date_routines_scoring.competition_group_id = g.id \
                                    join tbl_competition_awards a ON date_routines_scoring.competition_award_id = a.id \
                                    join tbl_award_types t2 ON a.award_type_id = t2.id \
                                    JOIN tbl_routines routine ON date_routines.routine_id = routine.id \
                                    where g.db_key = :comp_group \
                                    and tour_date_id = :tour_date_id \
                                    and date_routines.studio_id = :studio_id";
                                sequelize.query(sql, {
                                    type: sequelize.QueryTypes.SELECT,
                                    replacements: {
                                        comp_group: Const.FINALS,
                                        tour_date_id: tourDateId,
                                        studio_id: dateStudio.studio_id
                                    }
                                })
                                .then(function (results) {
                                    if(results.length > 0) {
                                        studio.adjudicated = results;
                                        studio.awarded = true;
                                    }
                                    cb(null);
                                })
                                .catch(function (err) {
                                    cb(err);
                                });
                            },
                            // HSA
                            function (cb) {
                                var sql = "select routine.name as routine_name,\
                                    case \
                                        when (place->>'hsa')::int = 1 THEN '1st Place' \
                                        when (place->>'hsa')::int = 2 THEN '2nd Place' \
                                        when (place->>'hsa')::int = 3 THEN '3rd Place' \
                                    end as place_str, \
                                    t.name as age_division_name, \
                                    minimum_age||'-'||minimum_age+d2.range as age_division_range, \
                                    tc.name as routine_category_name \
                                    from date_routines \
                                    JOIN tbl_routines routine ON date_routines.routine_id = routine.id \
                                    join tbl_registrations_routines r ON date_routines.registrations_routine_id = r.id \
                                    join tbl_routine_categories c2 ON r.routine_category_id = c2.id \
                                    join tbl_categories tc ON c2.category_id = tc.id \
                                    join tbl_age_divisions d2 ON r.age_division_id = d2.id \
                                    join tbl_levels t ON d2.level_id = t.id \
                                    where tour_date_id = :tour_date_id \
                                    and routine.studio_id = :studio_id \
                                    and canceled != 1 \
                                    and (place->>'hsa')::int < 4 \
                                    and (place->>'hsa')::int > 0 \
                                    ORDER BY routine.name";
                                sequelize.query(sql, {
                                    type: sequelize.QueryTypes.SELECT,
                                    replacements: {
                                        tour_date_id: tourDateId,
                                        studio_id: dateStudio.studio_id
                                    }
                                })
                                .then(function (results) {
                                    if(results.length > 0) {
                                        studio.awarded = true;
                                        studio.hsa = results;
                                    }
                                    cb(null);
                                })
                                .catch(function (err) {
                                    cb(err);
                                });
                            },
                            // HSP TODO
                            function(cb) {
                                var pdtid = studiosResData.perf_div_type_id ? studiosResData.perf_div_type_id : 1;
                                var options = [
                                    [
                                        {
                                            limit: 3,
                                            where: {
                                                $or: [{age_division_id: 1}, {age_division_id:2}]
                                            },
                                            name: 'Mini/Junior',
                                        },
                                        {
                                            limit: 3,
                                            where: {
                                                $or: [{age_division_id: 3}, {age_division_id:4}]
                                            },
                                            name: 'Teen/Senior',
                                        }
                                    ],
                                    [
                                        {
                                            limit: 1,
                                            where: {
                                                $or: [{ age_division_id: 1 }, { age_division_id: 2 }]
                                            },
                                            name: 'Mini/Junior',
                                        },
                                        {
                                            limit: 1,
                                            where: {
                                                $or: [{ age_division_id: 3 }, { age_division_id: 4 }]
                                            },
                                            name: 'Teen/Senior',
                                        }
                                    ],
                                    [
                                        {
                                            limit: 3,
                                            where: { age_division_id: 1 },
                                            name: 'Mini',
                                        },
                                        {
                                            limit: 3,
                                            where: { age_division_id: 2 },
                                            name: 'Junior',
                                        },
                                        {
                                            limit: 3,
                                            where: { age_division_id: 3 },
                                            name: 'Teen',
                                        },
                                        {
                                            limit: 3,
                                            where: { age_division_id: 4 },
                                            name: 'Senior',
                                        }
                                    ],
                                    [
                                        {
                                            limit: 1,
                                            where: { age_division_id: 1 },
                                            name: 'Mini',
                                        },
                                        {
                                            limit: 1,
                                            where: { age_division_id: 2 },
                                            name: 'Junior',
                                        },
                                        {
                                            limit: 1,
                                            where: { age_division_id: 3 },
                                            name: 'Teen',
                                        },
                                        {
                                            limit: 1,
                                            where: { age_division_id: 4 },
                                            name: 'Senior',
                                        }
                                    ]
                                ];
                                var perfDivTypes = options[pdtid-1];                                
                                var tasks2 = [];
                                _.forEach(perfDivTypes, function(pdt) {
                                    tasks2.push(function(cb2) {
                                        db.date_routines.findAll({
                                            where: {
                                                tour_date_id: tourDateId,
                                                studio_id: dateStudio.studio_id,
                                                "place.hsp": {
                                                    $gt: 0
                                                }
                                            },
                                            limit: pdt.limit,
                                            order: [
                                                [{model: db.tbl_registrations_routines, as: 'registrations_routine'}, {model: db.tbl_performance_divisions, as: 'performance_division'},'order', 'ASC']
                                            ],
                                            include: [
                                                { model: db.tbl_registrations_routines, as: 'registrations_routine', where: pdt.where,
                                                    include: [
                                                        { model: db.tbl_routines, as: 'routine' },
                                                        { model: db.tbl_performance_divisions, as: 'performance_division' },
                                                        { model: db.tbl_age_divisions, as: 'age_division', include: [
                                                            { model: db.tbl_levels, as: 'level' }
                                                        ]}
                                                    ]
                                                }
                                            ]
                                        })
                                        .then(function(results) {
                                            try {
                                                for(var dateRoutine of results) {
                                                    var place = dateRoutine.place.hsp;
                                                    if (place === 1) place = '1st'; else if (place === 2) place = '2nd'; else if (place === 3) place = '3rd';
                                                    var division = studio.hsp[pdt.name][dateRoutine.registrations_routine.performance_division.name]
                                                    if(typeof division === 'object') {
                                                        studio.hsp[pdt.name][dateRoutine.registrations_routine.performance_division.name].push({
                                                            place: place,
                                                            routine_name: dateRoutine.registrations_routine.routine.name,
                                                            studio_name: studio.studio_name
                                                        });
                                                    }
                                                    else if (typeof division === 'undefined') {
                                                        studio.hsp[pdt.name][dateRoutine.registrations_routine.performance_division.name] = [{
                                                            place: place,
                                                            routine_name: dateRoutine.registrations_routine.routine.name,
                                                            studio_name: studio.studio_name
                                                        }];
                                                    }

                                                };
                                                cb2(null);
                                            }
                                            catch(err) {
                                                cb2(err);
                                            }
                                        })
                                        .catch(function(err) {
                                            cb2(err);
                                        });
                                    });
                                });
                                async.parallel(tasks2, function(err, results) {
                                    if(err) cb(err);
                                    else cb(null);
                                });
                            }
                        ];
                        if(!compOnly) {
                            // SCHOLARSHIPS
                            waterfall.push(function (cb) {
                                var sql = "select p.fname, p.lname, t.name as scholarship_name, winner, CASE WHEN winner THEN 'Winner' ELSE 'Runner Up' END as winnerstr \
                                    from date_scholarships \
                                    JOIN date_dancers d3 ON date_scholarships.date_dancer_id = d3.id \
                                    join tbl_scholarships t ON date_scholarships.scholarship_id = t.id \
                                    join tbl_dancers t2 ON d3.dancer_id = t2.id \
                                    join tbl_persons p on t2.person_id = p.id \
                                    join tbl_staff staff ON date_scholarships.staff_id = staff.id \
                                    where date_scholarships.tour_date_id = :tour_date_id \
                                    and t.is_class = 0 \
                                    and studio_id = :studio_id \
                                    ORDER BY report_order, winner DESC, p.lname, p.fname";
                                sequelize.query(sql, {
                                    type: sequelize.QueryTypes.SELECT,
                                    replacements: {
                                        tour_date_id: tourDateId,
                                        studio_id: dateStudio.studio_id
                                    }
                                })
                                .then(function (results) {
                                    if(results.length > 0) {
                                        var hasWinner = _.findIndex(results, function (o) { return o.winner }) >= 0;
                                        if (hasWinner) studio.vip_haswinner = true;
                                        else studio.vip_hasrunnerup = true;
                                        studio.scholarships = results;
                                        studio.awarded = true;
                                    }
                                    cb(null);
                                })
                                .catch(function (err) {
                                    cb(err);
                                });
                            });
                            // CLASS-SCHOLARSHIPS
                            waterfall.push(function (cb) {
                                var sql = "select p.fname, p.lname, t.name as scholarship_name \
                                    from date_scholarships \
                                    JOIN date_dancers d3 ON date_scholarships.date_dancer_id = d3.id \
                                    join tbl_scholarships t ON date_scholarships.scholarship_id = t.id \
                                    join tbl_dancers t2 ON d3.dancer_id = t2.id \
                                    join tbl_persons p on t2.person_id = p.id \
                                    join tbl_staff staff ON date_scholarships.staff_id = staff.id \
                                    where date_scholarships.tour_date_id = :tour_date_id \
                                    and t.is_class = 1 \
                                    and studio_id = :studio_id \
                                    ORDER BY report_order, winner DESC, p.lname, p.fname";
                                sequelize.query(sql, {
                                    type: sequelize.QueryTypes.SELECT,
                                    replacements: {
                                        tour_date_id: tourDateId,
                                        studio_id: dateStudio.studio_id
                                    }
                                })
                                .then(function (results) {
                                    if(results.length > 0) {
                                        studio.awarded = true;
                                        studio.class_scholarships = results;
                                    }
                                    cb(null);
                                })
                                .catch(function (err) {
                                    cb(err);
                                });
                            });
                            // BEST OF JUMP, BEST IN STUDIO, ENTERTAINMENT & CHOREO AWARD
                            waterfall.push(function (cb) {
                                        sql = "select json_object_agg(awards.a_type, awards.receiver) as awards from \
                                            (select \
                                                case \
                                                when special_award_id = 100 then 'enter' \
                                                when special_award_id = 101 then 'choreo' \
                                                when special_award_id = 5 then 'bis'  \
                                                when special_award_id = 110 or special_award_id = 4 or special_award_id = 3 or special_award_id = 2 or special_award_id = 1 then 'boj' \
                                                end as a_type \
                                                , json_agg( (select x from \
                                                    (select t.name as routine_name, at.name as award_name, level2.name as age_division_name, minimum_age || '-' || minimum_age + d2.range as age_division_range) x)) as receiver \
                                                from date_special_awards \
                                                join date_routines routine ON date_special_awards.date_routine_id = routine.id \
                                                join tbl_routines t ON routine.routine_id = t.id \
                                                join tbl_special_awards t2 ON date_special_awards.special_award_id = t2.id \
                                                join tbl_award_types at on t2.award_type_id = at.id \
                                                join tbl_age_divisions d2 ON t2.age_division_id = d2.id \
                                                join tbl_levels level2 ON d2.level_id = level2.id \
                                                where date_special_awards.tour_date_id = :tour_date_id \
                                                and routine.studio_id = :studio_id \
                                                group by special_award_id \
                                                ) awards";

                                sequelize.query(sql, {
                                    type: sequelize.QueryTypes.SELECT,
                                    replacements: {
                                        tour_date_id: tourDateId,
                                        studio_id: dateStudio.studio_id
                                    }
                                })
                                .then(function(results) {
                                    var awards = results[0].awards;
                                    if(!_.isEmpty(awards)) {
                                        _.assignIn(studio, awards);
                                        studio.awarded = true;
                                    }
                                    cb(null);
                                })
                                .catch(function(err) {
                                    console.log('ERROR', err);
                                    cb(err);
                                });
                            });
                            // WELL ROUNDED AWARD
                            waterfall.push(function (cb) {
                                db.date_studio_awards.findAll({
                                    where: {
                                        winner: 1,
                                        studio_award_id: 3
                                    },
                                    include: [
                                        { model: db.date_studios, as: 'date_studio', where: { tour_date_id: tourDateId, studio_id: dateStudio.studio_id} },
                                    ]
                                })
                                .then(function(results) {
                                    if(results.length > 0) {
                                        studio.well_rounded = results.length > 0 ? 1 : 0;
                                        studio.awarded = true;
                                    }
                                    cb(null);
                                })
                                .catch(function(err) {
                                    console.log('ERROR', err);
                                    cb(err);
                                });
                            });
                        }
                        async.waterfall(waterfall,
                        function(err, results) {
                            if(err) callback(err);
                            else {
                                if (studio.awarded) studiosResData.studios.push(studio);
                                callback(null);
                            }
                        });
                    });
                });
                
                async.parallel(tasks, function(err, results) {
                    if(err) {
                        console.log('ERROR', err);
                        res.send(err);
                    }
                    else {
                        console.log('SUCCESS');
                        console.log('DATA', JSON.stringify(studiosResData));
                        res.render('reports/jump-studio-results', studiosResData);
                    }
                })
            })
            .catch(function(err) {
                console.log('ERROR', err);
                res.send(err);
            })
        })
        .catch(function(err) {
            res.send(err);
        });
    }
}