"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_observers = sequelize.define('date_observers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      }
    },
    tour_date_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_tour_dates',
        key: 'id'
      }
    },
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      }
    },
    observer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_observers',
        key: 'id'
      }
    },
    registrations_observer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations_observers',
        key: 'id'
      }
    },
    custom_fee: {
      type: DataTypes.BOOLEAN
    }
  }, {
    timestamps: false,
    associate: function (models) {
      date_observers.belongsTo(models.tbl_observers, {foreignKey: 'observer_id', as: 'observer'});
      date_observers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
      date_observers.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
      date_observers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      date_observers.belongsTo(models.tbl_registrations_observers, {foreignKey: 'registrations_observer_id', as: 'registrations_observer'});
    }
  });
  return date_observers;
};