<?php
	include("../../includes/util.php");
	$cityid = intval($_GET["tourdateid"]);

	$city = Array();
	$wdata = Array();

	if(is_numeric($cityid) && $cityid > 0) {
		$wsn = Array();
		$wsn2 = Array();

		//city name
		$rawcity = db_one("city","tbl_tour_dates","id=$cityid");
		$venuename = stripslashes(db_one("venue_name","tbl_tour_dates","id=$cityid"));

		//is_updated?
		$isupdated = db_one("workshop_updated","tbl_tour_dates","id=$cityid");

		//get workshop room names
		$sql = "SELECT workshop_room_count,workshop_room_1,workshop_room_2,workshop_room_3,workshop_room_4,workshop_room_5,workshop_room_6,workshop_room_7 FROM `tbl_tour_dates` WHERE id=$cityid LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_row($res)) {
			$wsn = $row;
		}

		if(count($wsn) > 0) {
			foreach($wsn as $wkey=>$roomname) {
				list($r1,$r2) = explode("**",$roomname);
				$wsn[$wkey] = $r1;
				$wsn2[$wkey] = $r2;
			}
		}

		$wrc = $wsn[0];

		$sql = "SELECT * FROM `tbl_date_schedule_workshops` WHERE tourdateid=$cityid ORDER BY start_time ";
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			$wdata[] = $row;
		}
	}

	$title = "$rawcity Workshop Schedule";
	$safecity = strtolower(str_replace(array(" ",",","."),"",$rawcity));


	$maxrows = 48;

	//set first page.
	$page = 1;
	$numrows = 0;
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title><?php print($title); ?></title>
		<style>
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
			}
			#sched_table {
			<?php
				if($wrc == 7)
					print("font-size: 7pt;");
				if($wrc == 6)
					print("font-size: 7pt;");
				if($wrc == 5)
					print("font-size: 7pt;");
				if($wrc == 4)
					print("font-size: 7.5pt;");
				if($wrc == 3)
					print("font-size: 10pt;");
				if($wrc == 2)
					print("font-size: 10pt;");
			?>
			}
			.ws_body {
				border-right: 1px solid #000000;
				border-bottom: 1px solid #000000;
				padding: 1px 0px 1px 2px;
			}
		</style>
	</head>
	<body>
		<div style="width: 952px;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;text-align:left;width:282px;">
						<div style="color:#FF1F1F;font-family:Regal box;font-size:50pt;">JUMP</div>
						<div style="font-family:'Festus!';font-size:24px;margin:-10px 0 0 21px;">The Alternative Convention</div>
<!--
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td style="font-size:12px;vertical-align:middle;width:80px;">Sponsored by</td>
								<td><img src="workshop_capezio.png" alt="" style="width:80px;"/></td>
							</tr>
						</table>
-->
					</td>
					<td>
						<?php if($isupdated == "1") { ?>
							<span style="font-family:Blue Highway D Type;color:#111111;font-size:36pt;">&nbsp;&nbsp;&nbsp;&nbsp;UPDATED</span>
						<?php } ?>
					</td>
					<td style="vertical-align:top;text-align:right;">
						<div style="font-family:Blue Highway D Type;color:#FF1F1F;font-size:40pt;">WORKSHOP SCHEDULE</div>
						<!-- <div style="color:#AAAAAA;font-size:18pt;font-family: 'BauhausCTT';margin:-7px 0 0 0;"><?php print($rawcity." - ".$venuename); ?></div> -->
						<div style="color:#000000;font-size:16pt;font-family: Blue Highway D Type;margin:-7px 0 0 0;"><?php print($rawcity." - ".$venuename); ?></div>
					</td>
				</tr>
			</table>
			<table id="sched_table" cellpadding="0" cellspacing="0" style="margin-top:10px;">
			<?php
				$numrows = 5;

				$currentday = "";
				$count = 0;
				foreach($wdata as $wline) {
					$thisdow = date('l',$wline["date"]);
					//if new day
					if($thisdow != $currentday) {
						$currentday = $thisdow;
						$numrows += 4;  ?>
						<?php if($count > 0) { ?>
						<tr><td colspan="<?php print(intval($wrc)+1); ?>" style="height:20px;"></td></tr>
						<?php
							}
							++$count;
						?>
						<tr>
							<td style="border:1px solid #000000;background-color:#EEEEEE;color:#000000;padding:4px 0 3px 2px;width:130px;"><div style="font-style:italic;font-weight:bold;font-size:8pt;"><?php print(date('l M. jS',$wline["date"])); ?></div></td>
							<?php
								if(intval($wrc) == 7)
									$tdw = "115px";
							 	if(intval($wrc) == 6)
									$tdw = "134px";
								if(intval($wrc) == 5)
									$tdw = "161px";
								if(intval($wrc) == 4)
									$tdw = "202px";
								if(intval($wrc) == 3)
									$tdw = "270px";
								if(intval($wrc) == 2)
									$tdw = "420px";

								for($i=0;$i<intval($wrc);$i++) { ?>
									<td style="<?php if($i==(intval($wrc)-1)) print("border-right:1px solid #000000;"); ?>border-right:1px solid #000000;border-top:1px solid #000000;border-bottom:1px solid #000000;background-color:#EEEEEE;font-family:Verdana;font-size:8pt;font-weight:bold;font-style:italic;padding:4px 0 3px 2px;width:<?php print($tdw);?>">
										<?php print($wsn[$i+1]); ?>
										<?php if(isset($wsn2[$i+1])) { ?>
											<div style="font-size:6.5pt;font-weight:normal;"><?php print($wsn2[$i+1]);?></div>
										<?php } ?>
									</td>
							<?php	}
							?>
						</tr>
					<?php	}

					?>

						<tr <?php if($numrows == $maxrows-2) {?> style="page-break-after:always;" <?php } ?>>
							<?php
							   $st = date('g:ia',$wline["start_time"]);
							   $starttime = substr($st,0,strlen($st)-1); ?>
							<td class="ws_body" style="<?php if($numrows == $maxrows-1) print("border-top:1px solid #000000;"); ?>border-left:1px solid #000000;font-weight:bold;font-style:italic;padding:2px 0 2px 2px;"><?php print($starttime); ?>-<?php

								list($hh,$mm,$ss) = explode(":",date('H:i:s',$wline["start_time"]));
								$dur_raw = $wline["duration"];
								list($dhh,$dmm) = explode(":",$dur_raw);

								$end_time = date('g:ia',mktime($hh+(intval($dhh)),$mm+(intval($dmm)),$ss,0,0,0));
								print(substr($end_time,0,strlen($end_time)-1));
							 ?></td>
							<?php
								$remaining = $wrc;

								if(intval($wline["span"]) > $wrc)
									$wline["span"] = $wrc;

								$fuckall = 1;
								for($i=0;$i<intval($wrc);$i++) {
									if($remaining > 0) {
											$str1 = "room".($fuckall)."_bold";
											$str2 = "room".($fuckall)."_highlight";
											$str3 = "room".($fuckall);
										?>
										<td class="ws_body" colspan="<?php print($wline["span"]); ?>" style="padding:2px 0 2px 2px;<?php if($numrows == $maxrows-1) print("border-top:1px solid #000000;"); ?>"><span style="<?php if($numrows == $maxrows-1) print("border-top:1px solid "); ?> <?php if($wline[$str1] == 1) print("font-weight:bold;font-style:italic;"); if($wline[$str2] == 1) print("color:#FF0000;font-style:italic;"); ?>"><?php print(urldecode($wline[$str3])); ?></span></td>
									<?php
										$remaining -= intval($wline["span"]);
										$fuckall += intval($wline["span"]);
										$wline["span"] = 1;
									}
								}
								++$numrows;
								if($numrows == $maxrows)
									$numrows = 0;
							?>
						</tr>
				<?php	}	?>
				</table>
		</div>
	</body>
</html>