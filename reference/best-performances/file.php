<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	//FINALS only!
	$compgroup = "finals";
	$save =  mysql_real_escape_string($_GET["save"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));

	$data = Array();
	$adorder = Array(7,1,2,3,4,8);
	for($i=0;$i<count($adorder);$i++) {
		$adname = "";
		$adrange = "";
		if(intval($adorder[$i]) > 0) {
			$sql7 = "SELECT * FROM `tbl_age_divisions` WHERE id=".$adorder[$i];
			$res7 = mysql_query($sql7) or die(mysql_error());
			if(mysql_num_rows($res7) > 0) {
				while($row7 = mysql_fetch_assoc($res7)) {
					$adname = $row7["name"];
					$adrange = $row7["range"];
				}
			}
		}
		$adname = "$adname ($adrange) : Groups";
		$sql = "SELECT tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_date_routines.number_".$compgroup." AS number, tbl_date_routines.".$compgroup."_has_a AS has_a, tbl_routines.name AS routinename, tbl_studios.name AS studioname, tbl_competition_awards.name AS awardname, tbl_date_routines.".$compgroup."_total_score AS total_score, tbl_date_routines.".$compgroup."_dropped_score AS dropped_score FROM `tbl_date_routines` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_routines.studioid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id = tbl_date_routines.".$compgroup."_awardid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.$compgroup=1 AND tbl_date_routines.routinecategoryid > 2 AND tbl_date_routines.agedivisionid = ".$adorder[$i]." ORDER BY tbl_date_routines.".$compgroup."_total_score DESC, tbl_date_routines.".$compgroup."_dropped_score DESC";
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {

			$num = strlen($row["number"]) > 0 ? $row["number"] : "";
			if($row["has_a"] == "1")
				$num .= ".a";

			$data[$adname][] = $row;
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 7.9pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Georgia;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Best Performances</div>
						<div style="font-family:Georgia;font-size:12pt;"><?php print(ucfirst($compgroup)." Competition"); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Georgia;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<?php
				if(count($data) > 0) {
					foreach($data as $adname=>$routines) {
						if(count($routines) > 0) { ?>
							<div style="margin:10px 0 3px;font-family:Georgia;font-size:16pt;color:#DD1A1A;">
								<?php print($adname); ?>
							</div>
							<table cellpadding="0" cellspacing="0" class="rtable" style="margin:0px 0 20px;page-break-after:always;">
								<tr>
									<td class="thead" style="width:175px;">Award</td>
									<td class="thead" style="width:265px;">Studio</td>
									<td class="thead" style="width:230px;">Routine</td>
									<td class="thead" style="width:175px;border-right:1px solid #000000;">Category</td>
								</tr>
						<?php
							foreach($routines as $routine) { ?>
								<tr>
									<td class="tbody" style="text-align:left;"><?php print($routine["awardname"]." (".$routine["total_score"]." : ".$routine["dropped_score"].")"); ?></td>
									<td class="tbody"><?php print($routine["studioname"]); ?></td>
									<td class="tbody"><?php print($routine["routinename"]); ?></td>
									<td class="tbody" style="border-right:1px solid #000000;"><?php print($routine["routinecategoryname"]); ?></td>
								</tr>
					<?php   }  ?>
							</table>
				<?php 	}
					}
				} ?>
		</div>
	</body>
</html>