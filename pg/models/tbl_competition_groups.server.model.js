"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_competition_groups = sequelize.define('tbl_competition_groups', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		shorter: {
			type: DataTypes.STRING
		},
		shortest: {
			type: DataTypes.STRING
		},
		db_key: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_competition_groups;
};