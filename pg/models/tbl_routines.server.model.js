"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_routines = sequelize.define('tbl_routines', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
    choreographer: {
      type: DataTypes.STRING
    }
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_routines.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
		}
	});
	return tbl_routines;
};