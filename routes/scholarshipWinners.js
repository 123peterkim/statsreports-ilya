var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');
var specialAwards = require('./specialAwards');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var path = req.path;
        var tourDateId = req.query.tourdateid || 746; // placeholder id;

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                { model: db.tbl_event_cities, as: 'event_city', include: [
                    { model: db.tbl_states, as: 'state' }
                ]},
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if(tourDate) {
                return {
                    city: tourDate.event_city.name,
                    venue: tourDate.venue.name,
                    start_date: tourDate.start_date,
                    eventid: tourDate.event_id,
                    scholarships : {}
                };
            }
            else {
                throw new Error('No results')
            }
        })
        .then(function(swData) {
            db.date_scholarships.findAll({
                where: { tour_date_id: 746 },
                include: [
                    { model: db.tbl_scholarships, as: 'scholarship', where: { event_id: swData.eventid} },
                    { model: db.date_dancers, as: 'date_dancer', include: [
                        { model: db.tbl_studios, as: 'studio' }
                    ]},
                    { model: db.tbl_dancers, as: 'dancer', include: [
                        { model: db.tbl_persons, as: 'person' }
                    ]}
                ]
            })
            .then(function(results) {
                _.forEach(results, function(ds) {
                    if(typeof swData.scholarships[ds.scholarship.name] !== 'object') {
                        swData.scholarships[ds.scholarship.name] = {
                            is_class: ds.scholarship.is_class,
                            dancers: [{
                                winner: ds.winner,
                                studio_name: ds.date_dancer.studio.name,
                                fname: ds.dancer.person.fname,
                                lname: ds.dancer.person.lname,
                                scholarship_code: ds.date_dancer.scholarship_code,
                                code: ds.code
                            }]
                        };
                    }
                    else {
                        swData.scholarships[ds.scholarship.name].dancers.push({
                            winner: ds.winner,
                            studio_name: ds.date_dancer.studio.name,
                            fname: ds.dancer.person.fname,
                            lname: ds.dancer.person.lname,
                            scholarship_code: ds.date_dancer.scholarship_code,
                            code: ds.code
                        });
                    }
                });
                if(path.includes('/scholarship-winners-lname')) {
                    res.render('reports/scholarship-winners-lname', swData);
                }
                else {
                    res.render('reports/scholarship-winners', swData);
                }
            })
        });
    }
}