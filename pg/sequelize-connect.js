"use strict";

var path = require('path');
var winston = require('./winston');
var modelPaths = require('./models').models;

winston.info('Initializing Sequelize...');

var orm = require('./sequelize');

var models = [];

var pg = {
  name: process.env.DB_NAME || "ebdb",
  host: process.env.DB_HOST || "aa1kyc1ryotlfnv.cm2ylz91w7p6.us-west-1.rds.amazonaws.com",
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME || "btfpdb",
  password: process.env.DB_PASSWORD || "BTFPHello11",
  dialect: process.env.DB_DIALECT || "postgres", //mysql, postgres, sqlite3,...
  enableSequelizeLog: process.env.DB_LOG || false,
  ssl: process.env.DB_SSL || false,
  sync: process.env.DB_SYNC || false
};

modelPaths.forEach(function(file) {
  models.push(path.resolve(file));
});

orm.discover = models;
orm.connect(pg.name, pg.username, pg.password, {
  host: pg.host,
  port: pg.port,
  dialect: pg.dialect,
  storage: pg.storage,
  logging: pg.enableSequelizeLog ? winston.verbose : false,
  dialectOptions: {
    ssl: pg.ssl ? pg.ssl : false
  },
  define: {
    freezeTableName: true
  }
});