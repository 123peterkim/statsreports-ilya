"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_hearts = sequelize.define('tbl_store_hearts', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		individual_id: {
			type: DataTypes.INTEGER
		},
		is_video: {
			type: DataTypes.INTEGER
		},
		hearts: {
			type: DataTypes.INTEGER
		},
		url: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_hearts;
};