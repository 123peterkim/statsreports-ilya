"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_routines = sequelize.define('date_routines', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_tour_dates',
			  key: 'id'
			}
		},
		routine_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_routines',
			  key: 'id'
			}
		},
		registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      }
		},
		studio_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_studios',
			  key: 'id'
			}
		},
		routine_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routine_types',
				key: 'id'
			}
		},
		canceled: {
		  type: DataTypes.INTEGER
		},
		custom_dancer_count: {
		  type: DataTypes.INTEGER
		},
		duration: {
		  type: DataTypes.INTEGER
		},
		award_name: {
		  type: DataTypes.STRING
		},
		uploaded_duration: {
		  type: DataTypes.STRING
		},
		uploaded: {
		  type: DataTypes.INTEGER
		},
		place: {
		  type: DataTypes.JSONB
		},
		registrations_routine_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations_routines',
        key: 'id'
      }
		},
		custom_fee: {
			type: DataTypes.BOOLEAN
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_routines.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
      date_routines.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      date_routines.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			date_routines.belongsTo(models.tbl_routines, {foreignKey: 'routine_id', as: 'routine'});
			date_routines.belongsTo(models.tbl_routine_types, {foreignKey: 'routine_type_id', as: 'routine_type'});
			date_routines.hasMany(models.date_routines_scoring, {foreignKey: 'date_routine_id', as: 'scores'});
			date_routines.hasMany(models.date_routines_dancers, {foreignKey: 'date_routine_id', as: 'dancers'});
			date_routines.hasMany(models.date_special_awards, {foreignKey: 'date_routine_id', as: 'special_awards'});
      date_routines.belongsTo(models.tbl_registrations_routines, {foreignKey: 'registrations_routine_id', as: 'registrations_routine'});
    }
	});
	return date_routines;
};