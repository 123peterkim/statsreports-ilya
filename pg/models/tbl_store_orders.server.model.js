"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_orders = sequelize.define('tbl_store_orders', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		order_hash: {
			type: DataTypes.TEXT
		},
		submitted: {
			type: DataTypes.DATE
		},
		shipped: {
			type: DataTypes.INTEGER
		},
		stats_user: {
			type: DataTypes.STRING
		},
		digital_only: {
			type: DataTypes.INTEGER
		},
		fees_paid: {
			type: DataTypes.REAL
		},
		tracking: {
			type: DataTypes.STRING
		},
		transaction_id: {
			type: DataTypes.STRING
		},
		label: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_orders;
};