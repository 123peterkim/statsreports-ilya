"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_countries = sequelize.define('tbl_countries', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		abbr: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_countries;
};