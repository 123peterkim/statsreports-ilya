"use strict";

var crypto = require('crypto');

module.exports = function (sequelize, DataTypes) {
  var tbl_mybtf_users = sequelize.define('tbl_mybtf_users', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    person_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_persons',
        key: 'id'
      }
    },
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    password: {
      type: DataTypes.STRING
    },
    active: {
      type: DataTypes.INTEGER
    },
    unregistered: {
      type: DataTypes.INTEGER
    },
    last_login: {
      type: DataTypes.DATE
    },
    salt: DataTypes.STRING,
    reset_password: {
      type: DataTypes.STRING
    },
    provider: {
      type: DataTypes.JSONB
    },
    roles: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      isArray: true
    },
    independent: {
      type: DataTypes.BOOLEAN
    },
    confirmed: {
      type: DataTypes.BOOLEAN
    },
    hints: {
      type: DataTypes.JSONB
    }
  }, {
    timestamps: false,
    associate: function (models) {
      tbl_mybtf_users.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
      tbl_mybtf_users.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
      tbl_mybtf_users.belongsToMany(models.tbl_dancers, {
        as: 'dancers',
        through: models.tbl_mybtf_user_has_dancers,
        foreignKey: 'mybtf_user_id',
        otherKey: 'dancer_id'
      });
      tbl_mybtf_users.belongsToMany(models.tbl_teachers, {
        as: 'teachers',
        through: models.tbl_mybtf_user_has_teachers,
        foreignKey: 'mybtf_user_id',
        otherKey: 'teacher_id'
      });
      tbl_mybtf_users.belongsToMany(models.tbl_observers, {
        as: 'observers',
        through: models.tbl_mybtf_user_has_observers,
        foreignKey: 'mybtf_user_id',
        otherKey: 'observer_id'
      });
    }
  });
  tbl_mybtf_users.prototype.makeSalt = function () {
    return crypto.randomBytes(16).toString('base64');
  };
  tbl_mybtf_users.prototype.authenticate = function (plainText) {
    var encryptedPassword = this.encryptPassword(plainText, this.dataValues.salt);
    return encryptedPassword === this.dataValues.password;
  };
  tbl_mybtf_users.prototype.oldAuthenticate = function (plainText) {
    return plainText === this.dataValues.password;
  };
  tbl_mybtf_users.prototype.encryptPassword = function (password, salt) {
    if (!password || !salt)
      return '';
    salt = new Buffer(salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  };
  return tbl_mybtf_users;
};