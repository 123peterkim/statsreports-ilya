'use strict';

/**
 * Users Model
 */

var crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function (property) {
  if (((this.provider !== 'local' && !this.updated) || property.length !== 0) === false) {
    throw new Error('Local strategy failed');
  }
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function (password) {
  if ((this.provider !== 'local' || (password && password.length > 6)) === false) {
    throw new Error('One field is missing');
  }
};

var isUnique = function (key, value, next) {
  var self = this;
  var obj = {};
  obj[key] = value;
  Users.find({
    where: obj
  })
    .then(function (user) {
      // reject if a different user wants to use the same email
      if (user && self.id !== user.id) {
        return next('Username already exists, please choose another');
      }
      return next();
    })
    .catch(function (err) {
      return next(err);
    });
};

module.exports = function (sequelize, DataTypes) {
  var Users = sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    password: {
      type: DataTypes.STRING,
      default: '',
      validate: {
        isValid: validateLocalStrategyPassword
      }
    },
    lastlogin: DataTypes.STRING,
    firstname: {
      type: DataTypes.STRING,
      defaultValue: '',
      validate: {
        isValid: validateLocalStrategyProperty,
        len: {
          args: [1, 30],
          msg: "First name title must be between 1 and 30 characters in length"
        },
      }
    },
    lastname: {
      type: DataTypes.STRING,
      defaultValue: '',
      validate: {
        isValid: validateLocalStrategyProperty
      }
    },
    displayname: {
      type: DataTypes.STRING,
      defaultValue: ''
    },
    username: {
      type: DataTypes.STRING,
      defaultValue: '',
      allowNull: false,
      unique: true,
      validate: {
        isValid: validateLocalStrategyProperty,
        isUnique: function (value, next) {
          var self = this;
          Users.find({
            where: {
              username: value
            }
          })
            .then(function (user) {
              // reject if a different user wants to use the same email
              if (user && self.id !== user.id) {
                return next('Username already exists, please choose another');
              }
              return next();
            })
            .catch(function (err) {
              return next(err);
            });
        },
        len: {
          args: [1, 30],
          msg: "Username must be between 1 and 30 characters in length"
        },
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isValid: validateLocalStrategyProperty,
        isEmail: {
          msg: 'Please fill a valid email address'
        },
        isUnique: function (value, next) {
          var self = this;
          Users.find({
            where: {
              email: value
            }
          })
            .then(function (user) {
              // reject if a different user wants to use the same email
              if (user && self.id !== user.id) {
                return next('Email already exists, please choose another');
              }
              return next();
            })
            .catch(function (err) {
              return next(err);
            });
        }
      }
    },
    salt: DataTypes.STRING,
    resetpasswordtoken: DataTypes.STRING,
    resetpasswordexpires: DataTypes.BIGINT,
    provider: DataTypes.STRING,
    providerdata: {
      type: DataTypes.JSONB
    },
    additionalprovidersdata: {
      type: DataTypes.JSONB
    },
    profileimageurl: DataTypes.STRING,
    roles: {
      type: DataTypes.JSONB,
      defaultValue: ["user"],
      isArray: true
    }
  }, {
    timestamps: false,
    associate: function (models) {
      if (models.article) {
        Users.hasMany(models.article);
      }
    }
  });

  Users.findUniqueUsername = function (username, suffix, callback) {
    var _this = this;
    var possibleUsername = username + (suffix || '');

    _this.find({
      where: {
        username: possibleUsername
      }
    }).then(function (user) {
      if (!user) {
        callback(possibleUsername);
      } else {
        return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
      }
    });
  };

  Users.prototype.makeSalt = function () {
    return crypto.randomBytes(16).toString('base64');
  };

  Users.prototype.authenticate = function (plainText) {
    return this.encryptPassword(plainText, this.salt) === this.password;
  };

  Users.prototype.encryptPassword = function (password, salt) {
    if (!password || !salt)
      return '';
    salt = new Buffer(salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  };
  return Users;
};