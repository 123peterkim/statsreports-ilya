"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_payment_methods = sequelize.define('tbl_payment_methods', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_payment_methods;
};