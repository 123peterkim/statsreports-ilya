"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_product_types = sequelize.define('tbl_store_product_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_product_types;
};