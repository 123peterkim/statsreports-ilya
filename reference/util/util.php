<?php
/**************************************************************************************
	UTILs.PHP

	- Database connection info [connect using db_connect()]
	- Useful functions


***************************************************************************************/

	session_start();

	require_once('db.php');
	db_connect();

	//cleans evil user input
	function clean($data) {
		$clean = mysql_real_escape_string(strip_tags(stripslashes(rtrim(ltrim(str_replace(array(";","\'","\""),"",$data))))));
		return $clean;
	}

	//useful get one
	function db_one($field,$table,$where = "",$limit = 1) {
		if(strlen($where)>0)
			$where = "WHERE ".$where;
		$base_sql = "SELECT $field FROM $table $where LIMIT $limit";
		$res = mysql_query($base_sql) or die(mysql_error());
		while(list($afield) = mysql_fetch_row($res))
			return $afield;
	}

	//get more than one
	function db_get($fields,$table,$where = "",$order = "") {
		if(strlen($where)>0)
			$where = "WHERE ".$where;

		if(strlen($order)>0)
			$order = "ORDER BY ".$order;

		$base_sql = "SELECT $fields FROM `$table` $where $order";
		$res = mysql_query($base_sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			$data[] = $row;
		}
		return $data;
	}

	//add one to db
	function add_one($table,$data) {
		$columns = array_keys($data);
		$values = array_values($data);

		$sql = "INSERT INTO $table (".implode(",",$columns).") VALUES(".implode(",",$values).")";
		$res = mysql_query($sql) or die(mysql_error());
		return mysql_insert_id();
	}

	//CHECK FOR WAIVER
	function check_waiver($fname="",$lname="",$birth_date="",$profileid=0,$returnwaiverid=false,$savenew=false) {

		$waiverid = 0;

		//try profileid first
		if($profileid > 0) {
			$waiverid = db_one("id","tbl_waivers","profileid='$profileid'");
			if($waiverid > 0) {
				if($returnwaiverid)
					return $waiverid;
				else return true;
			}

			else {
				$fname = strlen($fname) > 0 ? $fname : db_one("fname","tbl_profiles","id=$profileid");
				$lname = strlen($lname) > 0 ? $lname : db_one("lname","tbl_profiles","id=$profileid");
				$birth_date = strlen($birth_date) > 0 ? $birth_date : db_one("birth_date","tbl_profiles","id=$profileid");
			}
		}

		if(!$waiverid > 0) {

			$fname = mysql_real_escape_string($fname);
			$lname = mysql_real_escape_string($lname);
			$birth_date = mysql_real_escape_string($birth_date);

			//then try last name and first name and birth date
			if(strlen($lname) > 0 && strlen($birth_date) > 0) {

				//format birthdate correctly
				list($mm,$dd,$yy) = explode("/", $birth_date);
				$mm = strlen($mm) == 1 ? str_pad($mm,2,'0',STR_PAD_LEFT) : $mm;
				$dd = strlen($dd) == 1 ? str_pad($dd,2,'0',STR_PAD_LEFT) : $dd;
				$birth_date = $mm."/".$dd."/".$yy;

				$acount = db_one("COUNT(id)","tbl_waivers","lname='$lname' AND birth_date='$birth_date'");
				if($acount == 1) {
					$waiverid = db_one("id","tbl_waivers","lname='$lname' AND birth_date='$birth_date'");
					if($returnwaiverid)
						return $waiverid;
					else return true;
				}
				if($acount > 1) {
					$waiverid = db_one("id","tbl_waivers","fname='$fname' AND lname='$lname' AND birth_date='$birth_date'");
					if($waiverid > 0) {
						if($returnwaiverid)
							return $waiverid;
						else return true;
					}
				}
			}
		}

		if($savenew) {
			if($waiverid > 0) {
				//do nothing, already exists
			} else {
				$sql = "INSERT INTO `tbl_waivers` (fname,lname,birth_date,profileid) VALUES('$fname','$lname','$birth_date','$profileid')";
				$res = mysql_query($sql) or die(mysql_error());
				$waiverid = mysql_insert_id();
				if($returnwaiverid)
					return $waiverid;
				else return true;
			}
		}

	}

	//total fees for a studio in a tourdate
	function calc_total_studio_fees($studioid,$tourdateid,$save = false) {
		$dancer_fee_sum = db_one("SUM(fee)","tbl_date_dancers","tourdateid=$tourdateid AND studioid=$studioid");
		$competition_fee_sum = db_one("SUM(fee)","tbl_date_routines","tourdateid=$tourdateid AND studioid=$studioid");
		$total_fees = number_format((doubleval(str_replace(",","",$dancer_fee_sum)) + doubleval(str_replace(",","",$competition_fee_sum))),2,'.','');
		$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
		$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");

		//get free teacher count & value (PAID ONLY)
		$ftcftv = calc_ftv($studioid,$tourdateid,true);
		$ftc = $ftcftv["ftc"];
		$ftv = $ftcftv["ftv"];

		if($save) {
			$sql = "UPDATE `tbl_date_studios` SET total_fees='$total_fees', free_teacher_count='$ftc', free_teacher_value='$ftv' WHERE studioid=$studioid AND tourdateid=$tourdateid LIMIT 1";
			$res = mysql_query($sql);
		}

		return $total_fees;
	}


	function calc_ftv($studioid=0,$tourdateid=0,$save = false) {
		if($studioid > 0 && $tourdateid > 0) {
			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");

			//no teachers or observers or young observers
			$dc = db_one("COUNT(id)","tbl_date_dancers","studioid='$studioid' AND tourdateid='$tourdateid' AND (workshoplevelid != 1 && workshoplevelid != 8 && workshoplevelid != 12) AND fee > 0");
			$ftc = 0;
			$ftv = 0;
			if($dc > 4 && $dc < 21)
				$ftc = 1;
			if($dc > 20 && $dc < 40)
				$ftc = 2;
			if($dc > 39 && $dc < 60)
				$ftc = 3;
			if($dc > 59 && $dc < 80)
				$ftc = 4;
			if($dc > 79) {
				$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
				$ftc = $eventid == 14 ? 6 : 4;
			}

			$fullrates = db_one("full_rates","tbl_date_studios","tourdateid='$tourdateid' AND studioid='$studioid'");
			$isfinale = db_one("is_finals","tbl_tour_dates","id='$tourdateid'");

			if($isfinale == "1")
				$teacherfee = $fullrates == "1" ? db_one("finale_full_fee","tbl_workshop_levels_$seasonid","name='Teacher'") : db_one("finale_discount_fee","tbl_workshop_levels_$seasonid","name='Teacher'");
			else
				$teacherfee = $fullrates == "1" ? db_one("full_fee","tbl_workshop_levels_$seasonid","name='Teacher'") : db_one("discount_fee","tbl_workshop_levels_$seasonid","name='Teacher'");

			//teacher count
			$tc = db_one("COUNT(id)","tbl_date_dancers","tourdateid=$tourdateid AND studioid=$studioid AND workshoplevelid=1 AND has_scholarship=0");

			//oneday
			$odtc = db_one("COUNT(id)","tbl_date_dancers","studioid='$studioid' AND tourdateid='$tourdateid' AND one_day=1 AND workshoplevelid=1");
			if($odtc > 0)
				$odtf = $fullrates == "1" ? db_one("one_day_full_fee","tbl_workshop_levels_$seasonid","name='Teacher'") : db_one("one_day_discount_fee","tbl_workshop_levels_$seasonid","name='Teacher'");

			//attr
			$artc = db_one("COUNT(id)","tbl_date_dancers","studioid='$studioid' AND tourdateid='$tourdateid' AND workshoplevelid=1 AND (attended_reg=1 OR attended_reg_both=1)");
			if($artc > 0)
				$artf = number_format(($teacherfee / 2),2,'.','');

			$ftc = ($ftc > $tc) ? $tc : $ftc;

			$fullpriceteachercount = ($tc - $odtc - $artc);
			$fullpriceteachercount = $fullpriceteachercount > 0 ? $fullpriceteachercount : 0;
			if($fullpriceteachercount >= $ftc)
				$ftv = $ftc * intval($teacherfee);
			else {
				$ftv = $fullpriceteachercount * intval($teacherfee);
				$remainderfreeteachers = $ftc - $fullpriceteachercount;

				if($remainderfreeteachers > 0) {

					if($odtc > 0) {
						if($remainderfreeteachers > $odtc) {
							$ftv += ($odtc * $odtf);
							$remainderfreeteachers = $remainderfreeteachers - $odtc;
						}
						if($remainderfreeteachers <= $odtc) {
							$ftv += ($remainderfreeteachers * $odtf);
							$remainderfreeteachers = 0;
						}
					}

				}

				if($remainderfreeteachers > 0) {

					if($artc > 0) {
						if($remainderfreeteachers > $artc) {
							$ftv += ($artc * $artf);
							$remainderfreeteachers = $remainderfreeteachers - $artc;
						}
						if($remainderfreeteachers <= $artc) {
							$ftv += ($remainderfreeteachers * $artf);
							$remainderfreeteachers = 0;
						}
					}

				}

			}

			$return["ftc"] = $ftc;
			$return["ftv"] = $ftv;
			return $return;

		}
	}

	//calc a studio's routine fee
	function calc_date_studio_routine_fee($routineid,$tourdateid,$routinecategoryid,$dateroutineid,$save = false,$inprelimsonly = false) {

		if($tourdateid > 0 && $routineid > 0) {
			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$fees = 0.00;

			//IF STUDIO IS CHECKED AS FULL_RATES, use full rates.
			$studioid = db_one("studioid","tbl_date_routines","id='$dateroutineid'");
			$extra_time = intval(db_one("extra_time","tbl_date_routines","id='$dateroutineid'"));
			$full_rates = db_one("full_rates","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$is_finale = db_one("is_finals","tbl_tour_dates","id='$tourdateid'");
			$in_prelims = db_one("prelims","tbl_date_routines","id='$dateroutineid'");
			$in_finals = 0;
			if(!$inprelimsonly)
				$in_finals = db_one("finals","tbl_date_routines","id='$dateroutineid'");

			$whichfee = ($full_rates == 1) ? "full_fee_per_dancer" : "discount_fee_per_dancer";
			//if city set as nationals - which fee = prelim or finals nationals fee, overwrites discount vs full rate on line above
			if($is_finale == 1) {
				//split shit into prelims or finals or vips for nationals
				if($in_finals == 1) {
					$whichfee = "finale_fee_per_dancer";
					$feeperdancer += intval(db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'"));
				}
				if($in_prelims == 1) {
					$whichfee = "finale_prelim_fee_per_dancer";
					$feeperdancer += intval(db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'"));
				}
			}
			else {
				$feeperdancer = db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'");
			}

			$custom_fee = db_one("custom_fee","tbl_date_routines","id='$dateroutineid'");

			if(!$custom_fee == 1) {

				//discount total if dancers have attended_reg_both checked
				$attended_reg_count = 0;
				$attended_reg_value = 0;
				$sql3 = "SELECT profileid FROM `tbl_date_routine_dancers` WHERE routineid='$routineid' AND tourdateid='$tourdateid'";
				$res3 = mysql_query($sql3) or die(mysql_error());
				while($row = mysql_fetch_assoc($res3)) {
					$sql4 = "SELECT attended_reg_both FROM `tbl_date_dancers` WHERE profileid='".$row["profileid"]."' AND tourdateid='$tourdateid'";
					$res4 = mysql_query($sql4) or die(mysql_error());
					while(list($hasreg) = mysql_fetch_row($res4)) {
						$attended_reg_count += intval($hasreg);
					}
				}

				//multiply dancers by fee_per_dancer
				$count = db_one("COUNT(id)","tbl_date_routine_dancers","tourdateid='$tourdateid' AND routineid='$routineid'");

				$fees = number_format(($count*$feeperdancer),2,'.','');

				//subtract 50%
				if($attended_reg_count > 0) {
					$attended_reg_value = ($feeperdancer * .5) * $attended_reg_count;
					$fees -= $attended_reg_value;
				}

				//add on extra minutes ($5 per dancer per minute)
				$extra_time_addon = ($extra_time * $count * 5);

				$fees = number_format(($fees+$extra_time_addon),2,'.','');

				if($in_prelims == 0 && $in_finals == 0) {
					//TDA: if tourdate eventid is 14 [TDA], and routineid is in best dancer tbl, shit's free anyway.
					if($eventid == 14) {
						$testid = db_one("id","tbl_tda_bestdancer_data","tourdateid='$tourdateid' AND routineid='$routineid'");
						if($testid > 0)
							$fees = 0; //(lol)
					}
				}

				if($save) {
					$sql = "UPDATE `tbl_date_routines` SET fee='$fees' WHERE id='$dateroutineid' LIMIT 1";
					$res = mysql_query($sql) or die(mysql_error());
				}

				return($fees);

			}
		}
	}

	//MyBTF API call for routine fee [has to be separate because other function uses info already in the db.  api call info isn't in the db yet]
	function calc_date_reg_routine_fee_api($tourdateid,$fullrates,$routinecategoryid,$in_finals=0,$in_prelims=0,$dcount=0,$is_exttime=0,$is_vips=0,$extra_time=0) {
		if($tourdateid > 0 && $routinecategoryid > 0) {

			$fees = 0.00;
			$is_finale = db_one("is_finals","tbl_tour_dates","id=$tourdateid");
			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$whichfee = ($fullrates == 1) ? "full_fee_per_dancer" : "discount_fee_per_dancer";

			//if city set as nationals - which fee = 'finale_fee_per_dancer', overwrites discount vs full rate
			if($is_finale == 1) {
				if($is_exttime == 1)
					$routinecategoryid = 8;

				$feeperdancer = 0;

				if($in_finals == 1) {
					$whichfee = "finale_fee_per_dancer";
					$feeperdancer += intval(db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'"));
				}
				if($in_prelims == 1) {
					$whichfee = "finale_prelim_fee_per_dancer";
					$feeperdancer += intval(db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'"));
				}

			}
			else {
				$feeperdancer = db_one($whichfee,"tbl_routine_categories_$seasonid","id='$routinecategoryid'");
			}

			//add on extra minutes ($5 per dancer per minute)
			$extra_time_addon = ($extra_time * $dcount * 5);

			//multiply dancers by fee_per_dancer
			$fees = number_format(($dcount*$feeperdancer)+$extra_time_addon,2,'.','');

			if($in_prelims == 1 && $in_finals == 1 && $is_finale != 1) {
				$fees = ($fees+$fees);
			}



			return(number_format($fees,2,'.',''));
		}
	}

	//single dancer fee calculation for a tourdate
	function calc_date_dancer_workshop_fee($level,$attendedreg,$full_rates = 0,$tourdateid,$oneday = 0,$attendedreg_both = 0) {
		$which_fee = ($full_rates == 1) ? "full_fee" : "discount_fee";
		$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");


		//if one day only
		if($oneday > 0)
			$which_fee = "one_day_".$which_fee;

		//if finale [tda], CHARGE THOSE FUCKERS MORE $$
		if(db_one("is_finals","tbl_tour_dates","id='$tourdateid'") == 1)
			$which_fee = "finale_".$which_fee;

		$wfee = db_one($which_fee,"tbl_workshop_levels_$seasonid","id='$level'");


		//if attended_reg or attended_reg_both box is checked apply 50% discount
		// TO WORKSHOP ONLY
		if($attendedreg == 1 || $attendedreg_both == 1) {
			//teachers are free
			if($level == 1)
				$wfee = 0;
			else $wfee = $wfee * .5;
		}

		return number_format(doubleval($wfee),2,'.','');
	}

	//recalc ALL FEES for a studio [dancers, routines, discounts, blabla]
	function recalc_all_fees($tourdateid,$studioid,$datestudioid,$fullrates) {

		$sql = "UPDATE `tbl_date_studios` SET full_rates='$fullrates' WHERE studioid='$studioid' AND tourdateid='$tourdateid' LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());

		$sql = "SELECT tbl_date_dancers.id AS datedancerid, tbl_date_dancers.profileid, tbl_date_dancers.custom_fee, tbl_date_dancers.fee, tbl_date_dancers.workshoplevelid, tbl_date_dancers.attended_reg, tbl_date_dancers.attended_reg_both, tbl_date_dancers.one_day, tbl_date_dancers.has_scholarship FROM `tbl_date_dancers` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res = mysql_query($sql) or die(mysql_error());
		$datedancers = Array();
		while($row = mysql_fetch_assoc($res)) {
			$datedancers[] = $row;
		}

		$check = "";
		foreach($datedancers as $datedancer) {
			if(intval($datedancer["custom_fee"]) == 0 && $datedancer["has_scholarship"] == 0) {
				$check = calc_date_dancer_workshop_fee($datedancer["workshoplevelid"],$datedancer["attended_reg"],$fullrates,$tourdateid,$datedancer["one_day"],$datedancer["attended_reg_both"]);
				$sql = "UPDATE `tbl_date_dancers` SET fee=$check WHERE id=".$datedancer["datedancerid"]." LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());
			}
			if(intval($datedancer["custom_fee"]) == 0 && $datedancer["has_scholarship"] == 1) {
				$sql = "UPDATE `tbl_date_dancers` SET fee='0' WHERE id=".$datedancer["datedancerid"]." LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());
			}
		}

		$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.routinecategoryid, tbl_date_routines.routineid FROM `tbl_date_routines` WHERE tbl_date_routines.tourdateid='$tourdateid' AND tbl_date_routines.studioid='$studioid'";
		$res = mysql_query($sql) or die(mysql_error());
		$dateroutines = Array();
		while($row = mysql_fetch_assoc($res)) {
			$dateroutines[] = $row;
		}

		foreach($dateroutines as $dateroutine) {
			$check = calc_date_studio_routine_fee($dateroutine["routineid"],$tourdateid,$dateroutine["routinecategoryid"],$dateroutine["dateroutineid"],true);
		}

		calc_total_studio_fees($studioid,$tourdateid,true);
	}


	function delete_tour_studio($tourdateid=0,$studioid=0) {
		//get all studio's dateroutineids
		$sdateroutineids = Array();
		$sql4 = "SELECT id FROM `tbl_date_routines` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res4 = mysql_query($sql4) or die(mysql_error());
		if(mysql_num_rows($res4) > 0) {
			while($row = mysql_fetch_assoc($res4)) {
				$sdateroutineids[] = $row["id"];
			}
		}

		//get all studio's datedancerids & profileids
		$sdatedancerids = Array();
		$sprofileids = Array();
		$sql5 = "SELECT id,profileid FROM `tbl_date_dancers` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res5 = mysql_query($sql5) or die(mysql_error());
		if(mysql_num_rows($res5) > 0) {
			while($row = mysql_fetch_assoc($res5)) {
				$sdatedancerids[] = $row["id"];
				$sprofileids[] = $row["profileid"];
			}
		}

		//remove studios routines from special_awards
		if(count($sdateroutineids) > 0) {
			foreach($sdateroutineids as $sdateroutineid) {
				$sql6 = "DELETE FROM `tbl_date_special_awards` WHERE tourdateid=$tourdateid AND dateroutineid=$sdateroutineid";
				$res6 = mysql_query($sql6) or die(mysql_error());
			}
		}

		//remove studio from studio_awards
		$sql6 = "DELETE FROM `tbl_date_studio_awards` WHERE tourdateid=$tourdateid AND studioid=$studioid";
		$res6 = mysql_query($sql6) or die(mysql_error());

		//remove studios dancers from date_scholarships
		if(count($sdatedancerids) > 0) {
			foreach($sdatedancerids as $sdatedancerid) {
				$sql7 = "DELETE FROM `tbl_date_scholarships` WHERE tourdateid=$tourdateid AND datedancerid=$sdatedancerid";
				$res7 = mysql_query($sql7) or die(mysql_error());
			}
		}

		//remove studios dancers from tbl_date_routine_dancers
		if(count($sprofileids) > 0) {
			foreach($sprofileids as $sprofileid) {
				$sql8 = "DELETE FROM `tbl_date_routine_dancers` WHERE tourdateid=$tourdateid AND profileid=$sprofileid";
				$res8 = mysql_query($sql8) or die(mysql_error());
			}
		}

		//remove studio's dancers from date_dancers
		$sql = "DELETE FROM `tbl_date_dancers` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res = mysql_query($sql);

		//remove studio's routines from date_routines
		$sql2 = "DELETE FROM `tbl_date_routines` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res2 = mysql_query($sql2);

		//remove studio from date_studios
		$sql3 = "DELETE FROM `tbl_date_studios` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res3 = mysql_query($sql3);

		//remove studio's best dancers
		$sql9 = "DELETE FROM `tbl_tda_bestdancer_data` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res9 = mysql_query($sql9) or die(mysql_error());

		//remove studio from tda award nominations
		$sql10 = "DELETE FROM `tbl_tda_award_nominations` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res10 = mysql_query($sql10) or die(mysql_error());



		//check if studio is assigned to any other tour dates - if not return notify str
		$stillexists = db_one("id","tbl_date_studios","studioid=$studioid");
		if(!$stillexists)
			return("noexists");
	}


	//get all dancers in a specific date routine
	function get_date_routine_dancers($tourdateid,$routineid=0,$dateroutineid=0) {
		if(($routineid > 0 && $tourdateid > 0) || ($dateroutineid > 0 && $tourdateid > 0)) {
			if($routineid > 0) {
				$sql = "SELECT tbl_date_routine_dancers.profileid AS profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_date_dancers.age FROM `tbl_date_routine_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_routine_dancers.profileid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_date_routine_dancers.profileid WHERE tbl_date_routine_dancers.tourdateid='$tourdateid' AND tbl_date_routine_dancers.routineid='$routineid' AND tbl_date_dancers.tourdateid='$tourdateid' ORDER BY tbl_profiles.lname ASC";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0) {
					while($row = mysql_fetch_assoc($res)) {
						$dancers[] = $row;
					}
					return $dancers;
				}
			}
			if($dateroutineid > 0) {
				$routineid = db_one("routineid","tbl_date_routines","id='$dateroutineid' AND tourdateid='$tourdateid'");
				if($routineid > 0) {
					$sql = "SELECT tbl_date_routine_dancers.profileid AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_routine_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_routine_dancers.profileid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_date_routine_dancers.profileid WHERE tbl_date_routine_dancers.tourdateid='$tourdateid' AND tbl_date_routine_dancers.routineid='$routineid' AND tbl_date_dancers.tourdateid='$tourdateid' ORDER BY tbl_profiles.lname ASC";
					$res = mysql_query($sql) or die(mysql_error());
					if(mysql_num_rows($res) > 0) {
						while($row = mysql_fetch_assoc($res)) {
							$dancers[] = $row;
						}
						return $dancers;
					}
				}
			}
		}
	}

	//specific routine details for a certain tourdate
	function get_date_routine_info($tourdateid,$routineid) {
		if($routineid > 0 && $tourdateid > 0) {
			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$sql = "SELECT tbl_routines.name AS routinename, tbl_studios.name AS studioname, tbl_date_routines.studioid, tbl_age_divisions.name AS agedivision, tbl_routine_categories_$seasonid.name AS routinecategory, tbl_date_routines.routinecategoryid, tbl_date_routines.agedivisionid, tbl_performance_divisions.name AS perfdivision, tbl_date_routines.fee FROM `tbl_date_routines` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_routines.studioid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE tbl_date_routines.tourdateid='$tourdateid' AND tbl_date_routines.routineid='$routineid'";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					if($row["studioid"] > 1)
						$row["studiocode"] = db_one("studiocode","tbl_date_studios","tourdateid='$tourdateid' AND studioid='".$row["studioid"]."'");
					return $row;
				}
			}
		}
	}

	//update time for competition schedule.  not really used
	function competition_last_update($tourdateid,$comp_group){
		$newtime = time();
		$all_previous = json_decode(db_one("last_update","tbl_date_schedule_competition","tourdateid='$tourdateid'"),true);
		$all_previous[$comp_group] = $newtime;
		$sql = "UPDATE `tbl_date_schedule_competition` SET last_update='".mysql_real_escape_string(json_encode($all_previous))."' WHERE tourdateid='$tourdateid' LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());
		return $newtime;
	}

	//resets the competition schedule
	function reset_competition_schedule($tourdateid,$comp_group) {
		//get order
		$order = json_decode(db_one("default_category_order","tbl_tour_dates","id='$tourdateid'"),true);
		$count = 1;

		if($order[$comp_group]) {
			$sql2 = "UPDATE tbl_date_routines SET number_".$comp_group."=0, ".$comp_group."_has_a=0, ".$comp_group."_time='' WHERE tourdateid='".$tourdateid."'";
			$res2 = mysql_query($sql2) or die(mysql_error());
		}
		competition_last_update($tourdateid,$comp_group);
		return(json_encode(get_competition_schedule_in_order($tourdateid,$comp_group)));
	}

	//re-numbers all competition routines in order.  will remove any '.a'
	function competition_schedule_renumber_all($tourdateid) {
		$cga = array("vips","prelims","finals");
		$pos = 1;
		foreach($cga as $compgroup) {
			if($tourdateid > 1 && strlen($compgroup) > 0) {
				$routines = array();
				$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.routineid FROM `tbl_date_routines` WHERE tbl_date_routines.$compgroup=1 AND tbl_date_routines.tourdateid = $tourdateid AND tbl_date_routines.agedivisionid > 0 ORDER BY tbl_date_routines.number_".$compgroup." ASC, tbl_date_routines.".$compgroup."_has_a ASC";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0)
					while($row = mysql_fetch_assoc($res))
						$routines[] = $row;

				//$routines = get_competition_schedule_in_order($tourdateid,$compgroup);
				if(count($routines) > 0) {
					foreach($routines as $routine) {
						$sql = "UPDATE `tbl_date_routines` SET number_$compgroup='$pos', ".$compgroup."_has_a=0 WHERE routineid='".$routine["routineid"]."' AND tourdateid='$tourdateid' LIMIT 1";
						$res = mysql_query($sql) or die(mysql_error());
						++$pos;
					}
					$pos += 30;
				}
			}
		}
	}

	//updates award-after-routine assignments and date-end-routine assignments if the routine number it was assigned to has changed
	function fix_dates_awards_end_routines($tourdateid,$compgroup) {
		$alldates = json_decode(db_one("dates","tbl_date_schedule_competition","tourdateid='$tourdateid'"),true);
		if(count($alldates) > 0)
			$dates = $alldates[$compgroup];

		if(count($dates) > 0) {
			foreach($dates as $dayname=>$date) {
				if(strlen($date["end_routine"]) > 0) {
					$hasac = explode(".",$date["end_routine"]);
					$endroutine = $hasac[0];
					$hasa = $hasac[1] == "a" ? 1 : 0;
					//get dateroutineid for  dateendroutine (dispnumber)
					if(intval($endroutine) > 0) {
						$newdrid = db_one("id","tbl_date_routines","number_".$compgroup."='$endroutine' AND ".$compgroup."_has_a='$hasa' AND tourdateid='$tourdateid'");
						if(!$newdrid > 1)
							$newdrid = db_one("id","tbl_date_routines","number_$compgroup='$endroutine' AND tourdateid='$tourdateid'");
						$newdrid = $newdrid > 0 ? $newdrid : 0;
						$date["dateroutineid"] = $newdrid;
						$alldates[$compgroup][$dayname] = $date;
					}
				}
			}
			$sql = "UPDATE `tbl_date_schedule_competition` SET dates='".mysql_real_escape_string(json_encode($alldates))."' WHERE tourdateid='$tourdateid'";
			$res = mysql_query($sql) or die(mysql_error());
		}

		$allawards = json_decode(db_one("awards","tbl_date_schedule_competition","tourdateid='$tourdateid'"),true);
		if(count($allawards) > 0)
			$awards = $allawards[$compgroup];
		if(count($awards) > 0) {
			foreach($awards as $key=>$award) {
				if(strlen($award["end_routine"]) > 0) {
					$hasac = explode(".",$award["end_routine"]);
					$endroutine = $hasac[0];
					$hasa = $hasac[1] == "a" ? 1 : 0;
					if($endroutine > 0) {
						//get dateroutineid for awardendroutine (dispnumber)
						$newdrid = db_one("id","tbl_date_routines","number_".$compgroup."='$endroutine' AND ".$compgroup."_has_a='$hasa' AND tourdateid='$tourdateid'");
						if(!$newdrid > 1)
							$newdrid = db_one("id","tbl_date_routines","number_$compgroup='$endroutine' AND tourdateid='$tourdateid'");
						$award["dateroutineid"] = $newdrid;
						$allawards[$compgroup][$key] = $award;
					}
				}
			}
			$sql = "UPDATE `tbl_date_schedule_competition` SET awards='".mysql_real_escape_string(json_encode($allawards))."' WHERE tourdateid='$tourdateid'";
			$res = mysql_query($sql) or die(mysql_error());
		}

	}

	//restore default routine duration (in minutes) for each routine category
	function reset_routine_durations($tourdateid,$comp_group) {
		$default_times = '{"finals":{"1":"3","2":"3","3":"3","4":"3","5":"4","6":"5","7":"8","8":"8","9":"6"},"prelims":{"1":"3","2":"3","3":"3","4":"3","5":"4","6":"5","7":"8","8":"8","9":"6"},"vips":{"1":"3","2":"3","3":"3","4":"3","5":"4","6":"5","7":"8","8":"8","9":"6"}}';
		$alldurations = json_decode($default_times,true);
		$durations = $alldurations[$comp_group];
		$sql = "UPDATE `tbl_tour_dates` SET custom_routine_durations='$default_times' WHERE id=$tourdateid LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());

		//update all routines in comp_group
		$sql = "SELECT id AS dateroutineid, routinecategoryid FROM `tbl_date_routines` WHERE $comp_group=1 AND tourdateid=$tourdateid";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				if($row["routinecategoryid"] == 8)
					$duration = $durations[7];
				else $duration = $durations[$row["routinecategoryid"]];

				$sql2 = "UPDATE `tbl_date_routines` SET duration='$duration' WHERE id=".$row["dateroutineid"]." LIMIT 1";
				$res2 = mysql_query($sql2) or die(mysql_error());
			}
		}
		return($sql);
	}

	//default routine category order in bottom-right table
	function reset_routine_group_order($tourdateid,$comp_group) {
		//set default order field
		$default_order = '{"finals":[[11,1],[11,2],[1,1],[1,2],[2,1],[2,2],[3,1],[3,2],[4,1],[4,2],[8,1],[8,2],[11,3],[11,4],[11,5],[11,6],[1,3],[1,4],[1,5],[1,6],[2,3],[2,4],[2,5],[2,6],[3,3],[3,4],[3,5],[3,6],[7,1],[7,2],[7,3],[7,4],[7,5],[7,6],[4,3],[4,4],[4,5],[4,6],[8,3],[8,4],[8,5],[8,6],[8,1]],"vips":[[1,1,"f"],[1,1,"m"],[2,1,"f"],[2,1,"m"],[3,1,"f"],[3,1,"m"],[4,1,"f"],[4,1,"m"]],"prelims":[[11,1],[11,2],[7,1],[7,2],[1,1],[1,2],[2,1],[2,2],[3,1],[3,2],[4,1],[4,2],[8,1],[8,2],[7,3],[7,4],[7,5],[7,6],[6,3],[6,4],[6,5],[6,6],[11,3],[11,4],[11,5],[1,3],[1,4],[1,5],[1,6],[2,3],[2,4],[2,5],[2,6],[3,3],[3,4],[3,5],[3,6],[4,3],[4,4],[4,5],[4,6],[8,3],[8,4],[8,5],[8,6]]}';
		$sql = "UPDATE `tbl_tour_dates` SET default_category_order='$default_order' WHERE id=$tourdateid LIMIT 1";
		$res = mysql_query($sql) or die(mysql_error());
	}


	//returns competition schedule for a tourdate.  can also return dancer names for each routine, and/or specify the desired room number (1 or 2)
	function get_competition_schedule_in_order($tourdateid,$compgroup,$withdancers=false,$room=0) {
		$rooma = "";
		if($room > 0) {
			$rooma = " AND tbl_date_routines.room_".$compgroup."=$room";
		}
		$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
		$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");

		//get all solo dancers
		$sql = "
			SELECT
				tbl_date_routines.routineid,
				tbl_date_routine_dancers.profileid,
				tbl_profiles.gender,
				CONCAT(tbl_profiles.fname,' ', tbl_profiles.lname) AS solodancer,
				tbl_tour_dates.eventid,
				tbl_tour_dates.seasonid
			FROM `tbl_date_routines`
			LEFT JOIN tbl_date_routine_dancers
				ON tbl_date_routine_dancers.routineid = tbl_date_routines.routineid
			LEFT JOIN tbl_profiles
				ON tbl_profiles.id=tbl_date_routine_dancers.profileid
			LEFT JOIN tbl_tour_dates
				ON tbl_tour_dates.id = tbl_date_routines.tourdateid
			WHERE tbl_date_routines.tourdateid='$tourdateid'
				AND tbl_date_routine_dancers.tourdateid='$tourdateid'
				AND tbl_tour_dates.id = $tourdateid";

		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$rid = $row["routineid"];
				unset($row["routineid"]);
				unset($row["profileid"]);
				$soloists[$rid] = $row;
			}
		}

		//get all routines
		$sql = "SELECT
		  tbl_date_routines.routineid,
		  tbl_date_routines.".$compgroup."_time   AS time,
		  tbl_date_routines.extra_time,
		  tbl_date_routines.room_$compgroup       AS room,
		  tbl_date_routines.studioid,
		  tbl_date_routines.duration,
		  tbl_date_routines.id               	  AS dateroutineid,
		  tbl_date_routines.number_$compgroup     AS number,
		  tbl_date_routines.routinecategoryid,
		  tbl_date_routines.agedivisionid,
		  tbl_date_routines.".$compgroup."_has_a  AS has_a,
		  tbl_performance_divisions.name      	  AS perfdivisionname,
		  tbl_routines.name                   	  AS routinename,
		  tbl_studios.name                    	  AS studioname,
		  tbl_age_divisions.name             	  AS agedivisionname,
		  tbl_age_divisions.range             	  AS agedivisionrange,
		  tbl_routine_categories_$seasonid.name   AS routinecategoryname,
		  tbl_date_studios.studiocode
		FROM `tbl_date_routines`
		  LEFT JOIN tbl_performance_divisions
		    ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid
		  LEFT JOIN tbl_age_divisions
		    ON tbl_age_divisions.id = tbl_date_routines.agedivisionid
		  LEFT JOIN tbl_routines
		    ON tbl_routines.id = tbl_date_routines.routineid
		  LEFT JOIN tbl_studios
		    ON tbl_studios.id = tbl_date_routines.studioid
		  LEFT JOIN tbl_routine_categories_$seasonid
		    ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid
		  LEFT JOIN tbl_date_studios
		    ON tbl_date_studios.studioid = tbl_date_routines.studioid
		WHERE tbl_date_routines.$compgroup = 1
		    AND tbl_date_routines.tourdateid = $tourdateid
		    AND tbl_date_studios.tourdateid = $tourdateid
		    $rooma
		ORDER BY tbl_date_routines.number_$compgroup ASC, tbl_date_routines.".$compgroup."_has_a ASC";

		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			$count = 0;
			while($row = mysql_fetch_assoc($res)) {

				$row["studioname"] = strlen($row["studioname"]) > 0 ? stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"]))) : "-";

				$row["hmtime"] = strlen($row["time"]) > 0 ? substr(date('g:ia',$row["time"]),0,-1) : "--";
				$row["hmtime2"] = strlen($row["time"]) > 0 ? strtoupper(date('g:i a',$row["time"])) : "--";
				$row["duration"] = strlen($row["duration"]) > 0 ? $row["duration"] : "";

				//SEP 2014 -- add in EXTRA TIME to duration
				$row["duration"] = intval($row["duration"]) + intval($row["extra_time"]);

				$row["agedivisionnamerange"] = $row["agedivisionname"]. "(".$row["agedivisionrange"].")";
				$row["dispnumber"] = ($row["has_a"] == 1) ? $row["number"].".a" : $row["number"];

				//merge in soloist data
				if($row["routinecategoryname"] == "Solo") {
					$row = array_merge($row,$soloists[$row["routineid"]]);
				}

				//for NATIONALS VIPS SOLOS only - append gender to routinecategory label
				if($compgroup == "vips" && $row["routinecategoryname"] == "Solo" && isset($row["gender"]))
					$row["routinecategoryname"] = $row["routinecategoryname"]." (".substr(ucfirst($row["gender"]),0,1).")";


				//add dancers if flagged true!
				if($withdancers) {
					$dancers = array();
					$dancers = get_date_routine_dancers($tourdateid,$row["routineid"]);
					$row["dancers"] = $dancers;
					$row["dancers_str"] = "";
					$dstrs = array();
					if(count($dancers) > 0) {
						foreach($dancers as $dancer) {
							$dstrs[] = $dancer["fname"]." ".$dancer["lname"];

						}
						$row["dancers_str"] = join(", ",$dstrs);
					}
				}
				$routines[$count] = $row;
				++$count;

			}

		}
		return $routines;
	}

	//calculates the start time for each routine based off of the day start time, the last routine's duration, and any award duration(s)
	function competition_schedule_set_times($tourdateid,$comp_group){
		$sql2 = "SELECT dates,awards FROM `tbl_date_schedule_competition` WHERE tourdateid=$tourdateid LIMIT 1";
		$res2 = mysql_query($sql2) or die(mysql_error());
		if(mysql_num_rows($res2) > 0) {
			while($row = mysql_fetch_assoc($res2)) {
			//get dates for comp group (date, start time, final routine)
			//[date [Dayname (m/d/Y)] =>("end_routine"=>,["start_time"]=>,dateroutineid_end=>)
				$dates_raw = json_decode($row["dates"],true);
				$dates = $dates_raw[$comp_group];
			//get awards/breaks for comp group (date, final routine, duration)
			//[id(0a) => (["date"]=>[format as date key],"dur"=>h:mm,"end_routine"=>,["desc"]=>"","dateroutineid_end"=>)
				$awards_raw = json_decode($row["awards"],true);
				$awards = $awards_raw[$comp_group];
			//get routines in order
			//()=>([0++]=>routineid,duration(3),dateroutineid
				$routines = get_competition_schedule_in_order($tourdateid,$comp_group);

				if(count($dates) > 0) {
					//clean up dates array
					$newdates = Array();
					foreach($dates as $key=>$value) { //key = date , value = endroutine#
						$expl = explode(" ",$key);
						$datestr = str_replace(array("(",")"),"",$expl[1]);
						list($mm,$dd,$yy) = explode("/",$datestr);
						$datemk = mktime(0,0,0,$mm,$dd,$yy);

						$value["start_time"] = str_replace(" ","",$value["start_time"]); //4:30<space>pm becomes 4:30pm
						$startexpl = explode(":",$value["start_time"]);
						$hours = $startexpl[0];
						$mins = substr($startexpl[1],0,2);
						$ampm = strtolower(substr($startexpl[1],2,4));
						if($ampm == "pm" && intval($hours) != "12")
							$hours = intval($hours)+12;
						$start_time_mk = mktime($hours,$mins,0,$mm,$dd,$yy);

						$newdates[$datemk] = array("start_time"=>$start_time_mk,"end_routine"=>$value["end_routine"],"dateroutineid"=>$value["dateroutineid"]);
						asort($newdates);
						$dates = $newdates;
					}
				}

				if(count($awards) > 0) {
					//clean up awards array
					$newawards = Array();
					foreach($awards as $key=>$value) {
						$expl = explode(" ",$value["date"]);
						$datestr = str_replace(array("(",")"),"",$expl[1]);
						list($mm,$dd,$yy) = explode("/",$datestr);
						$datemk = mktime(0,0,0,$mm,$dd,$yy);

						$durexpl = explode(":",$value["dur"]);
						$mins = ($durexpl[0] * 60) + $durexpl[1];
						$awards[$key]["date"] = $datemk;
						$awards[$key]["dur"] = $mins;
						$awards[$key]["nicetime"] = date('h:i:s m/d/y',$datemk);
					}
				}

				$pos = 0; //routine array pos
				$curtime = 0;
				$times = Array(); //final, [dateroutineid]=>(mktime)
				if(count($dates) > 0) {
					foreach($dates as $date=>$datedata) {
						$times[$date] = Array();
						//set start time based on day start_time
						$currtime = $datedata["start_time"];
						$dayisover = false;

						while(!$dayisover && $pos <= sizeof($routines)) {
							$curr = $routines[$pos];

							if($curr["agedivisionid"] > 0) {
								//curr(routine)=>([0++]=>routineid,duration(3),dateroutineid
								//get routine duration in minutes
								$dur = intval($curr["duration"]);
								//assign currtime to routine
								$times[$date][$curr["dateroutineid"]] = array($currtime,date('h:i:sa m/d/y',$currtime));

								//create new time (old time + item duration minutes)
								/* NOTE: EXTRA TIME IS ALREADY FACTORED INTO ROUTINE DURATION BECAUSE ARRAY HERE COMES FROM GET_SCHED_IN_ORDER, WHICH DOES IT ALREADY */
								list($mm,$dd,$yy,$hh,$ii,$ss) = explode("/",date('m/d/Y/H/i/s',$currtime));
								$currtime = mktime($hh,$ii+$dur,$ss,$mm,$dd,$yy);

								//check end of day
								if($curr["dateroutineid"] == $datedata["dateroutineid"])
									$dayisover = true;

								//check if routine triggers award
								if(count($awards) > 0) {
									foreach($awards as $award) {
										if(($curr["dateroutineid"] == $award["dateroutineid"]) && ($date == $award["date"])) {
											//$times[$date]['a'] = "AWARD!";
											$award_dur = intval($award["dur"]);
											list($mm,$dd,$yy,$hh,$ii,$ss) = explode("/",date('m/d/Y/H/i/s',$currtime));
											$currtime = mktime($hh,$ii+$award_dur,$ss,$mm,$dd,$yy);
										}
									}
								}
							}

							++$pos;
						}

					}
					foreach($times as $day) {
						foreach($day as $dateroutineid=>$val) {
							$sql3 = "UPDATE `tbl_date_routines` SET ".$comp_group."_time='".$val[0]."' WHERE id='".$dateroutineid."' LIMIT 1";
							$res3 = mysql_query($sql3) or die(mysql_error());
						}
					}
				}

			}
		}
	}

	//grab a session[system] var
	function get_system_session_var($which) {
		$which = clean($which);
		return($_SESSION["System"][$which]);
	}
	//grab a session[system] var
	function get_user_session_var($which) {
		$which = clean($which);
		return($_SESSION["User"][$which]);
	}


	//cutoff date used to decide MyBreakTheFloor early rates or full rates (not always the day that is advertised)
	function within_cutoff_date2($tourdateid) {
		if($tourdateid > 0) {
			$today_all_mk = time();
			list($mm,$dd,$yy) = explode("/",date('m/d/Y',$today_all_mk));
			$today_date_mk = mktime(0,0,0,$mm,$dd,$yy);
			$cutoff_date = db_one("cutoff_date2","tbl_tour_dates","id='$tourdateid'");
			list($yy2,$mm2,$dd2) = explode("-",$cutoff_date);
			if(is_numeric($mm2) && is_numeric($dd2) && is_numeric($yy2))
				$cutoff_mk = mktime(23,59,59,$mm2,$dd2,$yy2);
		//	return ($today_date_mk." | ".$cutoff_mk);

			if($today_date_mk >= $cutoff_mk)
				return true;
			else
				return false;
		}
	}

	//gets day 27 days before cutoff day (not really used anymore)
	function within_27_days($tourdateid) {
		if($tourdateid > 0) {
			$today_all_mk = time();
			list($mm,$dd,$yy) = explode("/",date('m/d/Y',$today_all_mk));
			$today_date_mk = mktime(0,0,0,$mm,$dd,$yy);
			$start_date = db_one("start_date","tbl_tour_dates","id=$tourdateid");
			list($yy2,$mm2,$dd2) = explode("-",$start_date);
			$within_30_mk = mktime(0,0,0,$mm2,$dd2-27,$yy2);
			if($today_date_mk >= $within_30_mk)
				return true;
			else
				return false;
		}
	}

	//gets day 30 days before cutoff day (used for website cutoff dates)
	function within_30_days($tourdateid) {
		if($tourdateid > 0) {
			$today_all_mk = time();
			list($mm,$dd,$yy) = explode("/",date('m/d/Y',$today_all_mk));
			$today_date_mk = mktime(0,0,0,$mm,$dd,$yy);
			$start_date = db_one("start_date","tbl_tour_dates","id=$tourdateid");
			list($yy2,$mm2,$dd2) = explode("-",$start_date);
			$within_30_mk = mktime(0,0,0,$mm2,$dd2-30,$yy2);
			if($today_date_mk >= $within_30_mk)
				return true;
			else
				return false;
		}
	}

	//get all dancers in tourdate (option to show only best dancers, and teachers/observers)
	function get_all_date_dancers_list($tourdateid,$showteachobs = false,$bdonly = false) {
		if($bdonly) {
			$sql = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid='$tourdateid' AND tbl_date_dancers.workshoplevelid!=1 AND tbl_date_dancers.workshoplevelid!=8 AND tbl_date_dancers.vip=1 ORDER BY tbl_profiles.lname ASC";
		}
		else {
			if($showteachobs)
				$sql = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid='$tourdateid' ORDER BY tbl_profiles.lname ASC";
			else
				$sql = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid='$tourdateid' AND tbl_date_dancers.workshoplevelid!=1 AND tbl_date_dancers.workshoplevelid!=8 ORDER BY tbl_profiles.lname ASC";
		}
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$dancers[] = $row;
			}
		}
		return $dancers;
	}

	//get all routines in tourdate (specified competition group (prelims|finals|vip), option to show groups only)
	function get_all_date_routines($tourdateid,$comp_group,$groupsonly=false) {
		if($groupsonly)
			$sql = "SELECT perfcategoryid,agedivisionid,routinecategoryid,id,routinetypeid,number_".$comp_group." AS number,".$comp_group."_has_a AS has_a, routineid,studioid,".$comp_group."_awardid AS awardid,".$comp_group."_total_score AS total_score,".$comp_group."_dropped_score AS dropped_score, ".$comp_group."_dropped_score2 AS dropped_score2,room_".$comp_group." AS room FROM `tbl_date_routines` WHERE tourdateid=$tourdateid AND $comp_group=1 AND routinecategoryid > 2 ORDER BY number ASC";
		else
			$sql = "SELECT perfcategoryid,agedivisionid,routinecategoryid,id,routinetypeid,number_".$comp_group." AS number,".$comp_group."_has_a AS has_a, routineid,studioid,".$comp_group."_awardid AS awardid,".$comp_group."_total_score AS total_score,".$comp_group."_dropped_score AS dropped_score, ".$comp_group."_dropped_score2 AS dropped_score2,room_".$comp_group." AS room FROM `tbl_date_routines` WHERE tourdateid=$tourdateid AND $comp_group=1 ORDER BY number ASC";
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			if($row["awardid"] > 0) {
				if($row["dropped_score2"] > 0)
					$award_str = db_one("name","tbl_competition_awards","id=".$row["awardid"])." (".$row["total_score"]." : ".$row["dropped_score2"]." / ".$row["dropped_score"].")";
				else
					$award_str = db_one("name","tbl_competition_awards","id=".$row["awardid"])." (".$row["total_score"]." : ".$row["dropped_score"].")";
			}
			else $award_str = "";

			$num = strlen($row["number"]) > 0 ? $row["number"] : "";
			if($row["has_a"] == "1")
				$num .= ".a";

			$data[] = array("room"=>$row["room"],"routinetypeid"=>$row["routinetypeid"],"perfcategoryid"=>$row["perfcategoryid"],"id"=>$row["id"],"basenum"=>$row["number"],"has_a"=>$row["has_a"],"number"=>$num,"routinename"=>stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",db_one("name","tbl_routines","id=".$row["routineid"])))),"studioname"=>stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",db_one("name","tbl_studios","id=".$row["studioid"])))),"award"=>$award_str);
		}

		for($i=0;$i<count($data);$i++) {
			$firstnum = $data[$i]["basenum"];
			$first_hasa = $data[$i]["has_a"];
			if(isset($data[$i+1])) {
				if(($data[$i+1]["basenum"] == $firstnum) && $first_hasa == "1") {
					$tmp = $data[$i];
					$data[$i] = $data[$i+1];
					$data[$i+1] = $tmp;
				}
			}
		}

		return($data);
	}

	//array of studio info for specific tourdate
	function get_date_studio_stats($studioid,$tourdateid) {
		if($studioid > 0 && $tourdateid > 0) {

			//which event...
			$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
			$fullrates = db_one("full_rates","tbl_date_studios","tourdateid=$tourdateid AND studioid=$studioid");
			$isfinale = db_one("is_finals","tbl_tour_dates","id=$tourdateid");
			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$data["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
			$data["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
			$data["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
			$data["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
			$data["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
			$data["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
			$data["nubie_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=7 AND tourdateid=$tourdateid");
			$data["peewee_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=9 AND tourdateid=$tourdateid");
			$data["sidekick_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=10 AND tourdateid=$tourdateid");
			$data["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
			$data["solo_count"] = 0;
			$data["duotrio_count"] = 0;
			$data["group_count"] = 0;
 			$data["routine_count"] = db_one("COUNT(id)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid");
			$data["allgrouproutinecount"] = 0;
			$data["prelims_count"] = 0;
			$data["finals_count"] = 0;
			$data["vips_count"] = 0;

			$routines = get_date_studio_routines($tourdateid,$studioid);
			if(count($routines) > 0) {
				foreach($routines as $routine) {
					$asum = $routine["prelims"] + $routine["finals"] + $routine["vips"];
					if($routine["prelims"] > 0)
						$data["prelims_count"] += 1;
					if($routine["finals"] > 0)
						$data["finals_count"] += 1;
					if($routine["vips"] > 0)
						$data["vips_count"] += 1;

					$data["allgrouproutinecount"] += $asum;
					//routinecategoryids > 1=solo, 2=duo/trio, 3+=group
					if($routine["routinecategoryid"] == 1) {
						$data["solo_count"] += $asum;
					}
					if($routine["routinecategoryid"] == 2) {
						$data["duotrio_count"] += $asum;
					}
					if($routine["routinecategoryid"] > 2) {
						$data["group_count"] += $asum;
					}
				}
			}

			$data["workshop_fees"] = db_one("SUM(fee)","tbl_date_dancers","studioid=$studioid AND tourdateid=$tourdateid");
			$data["competition_fees"] = db_one("SUM(fee)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid");
			$staffnotes = json_decode(db_one("notes","tbl_studios","id=$studioid"));
			$data["staff_note_count"] = count($staffnotes);

			$data["dancer_count"] = $data["senior_count"] + $data["teen_count"] + $data["junior_count"] + $data["mini_count"] + $data["jumpstart_count"] + $data["nubie_count"] + $data["peewee_count"] + $data["sidekick_count"];

			//subtract best dancers... son of a...
			$sql = "SELECT tbl_tda_bestdancer_data.id FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_date_dancers.studioid=$studioid";
			$res = mysql_query($sql) or die(mysql_error());
			$bdc = mysql_num_rows($res);

			//free students.... fml... on schol, or fee=0.  but NOT for teachers or observers
			if($eventid == 14 || ($eventid == 18 && $isfinale)) {
				$data["free_student_count"] = intval(db_one("COUNT(id)","tbl_date_dancers","(has_scholarship=1 AND !fee>0 AND vip!=0) AND (tbl_date_dancers.workshoplevelid!=8 AND tbl_date_dancers.workshoplevelid!=1) AND studioid=$studioid AND tourdateid=$tourdateid"));
			}
			else {
				$data["free_student_count"] = intval(db_one("COUNT(id)","tbl_date_dancers","(has_scholarship=1 AND !fee>0) AND (tbl_date_dancers.workshoplevelid!=8 AND tbl_date_dancers.workshoplevelid!=1) AND studioid=$studioid AND tourdateid=$tourdateid"));
			}

			//BEST DANCERS FOR NATIONALS DOOOOO COUNT FOR FREE TEACHERS, OTHERWISE SUBTRACT FREE STUDENTS
			if($eventid != 14 || ($eventid == 18 && !$isfinale))
				$data["dancer_count"] = (intval($data["dancer_count"]) - intval($data["free_student_count"]));

			//for TDA, DO subtract NON-PAYING best dancers
			$data["bd_nonpaying_count"] = intval(db_one("COUNT(id)","tbl_date_dancers","(has_scholarship=1 AND !fee>0) AND (tbl_date_dancers.workshoplevelid!=8 AND tbl_date_dancers.workshoplevelid!=1) AND studioid=$studioid AND tourdateid=$tourdateid"));

			if($eventid == 14 || ($eventid == 18 && $isfinale))
				$dc = $data["dancer_count"] - $data["bd_nonpaying_count"];
			else $dc = $data["dancer_count"];

			$ftcftv = calc_ftv($studioid,$tourdateid);
			$data["free_teacher_count"] = $ftcftv["ftc"];
			$data["free_teacher_value"] = $ftcftv["ftv"];

			$sql = "UPDATE `tbl_date_studios` SET free_teacher_value='".$data["free_teacher_value"]."',free_teacher_count='".$data["free_teacher_count"]."' WHERE studioid='$studioid' AND tourdateid=$tourdateid LIMIT 1";
			$res =  mysql_query($sql) or die(mysql_error());


			$data["dancer_count"] = $dc;


			//auto-subtract free teachers value
			$data["workshop_fees"] = ($data["workshop_fees"] - $data["free_teacher_value"]);

			$data["total_fees"] = number_format(doubleval(str_replace(",","",$data["competition_fees"])) + doubleval(str_replace(",","",$data["workshop_fees"])),2,'.','');
			$data["fees_paid"] = db_one("fees_paid","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$data["credit"] = db_one("credit","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$data["balance_due"] = number_format((doubleval(str_replace(",","",$data["total_fees"])) - doubleval(str_replace(",","",$data["fees_paid"])) - doubleval(str_replace(",","",$data["credit"]))),2,'.','');
			if($data["balance_due"] < 0)
				$data["balance_due"] = "(".number_format((-1 * $data["balance_due"]),2).")";


			if(intval($data["dancer_count"]) == 1)
				$data["students_tag"] = "Student";
			else $data["students_tag"] = "Students";

			if(intval($data["routine_count"]) == 1)
				$data["routines_tag"] = "Routine";
			else $data["routines_tag"] = "Routines";

			if(intval($data["free_teacher_count"]) == 1)
				$data["teacher_tag"] = "Teacher";
			else $data["teacher_tag"] = "Teachers";

			return $data;
		}
	}

	//single-attendee fee calc for DTS regs
	function calc_dts_attendee_fee($regtypeid,$promocodeid = 0,$onlineregid=0) {
		if(is_numeric($regtypeid) && $regtypeid > 0) {

			/* 2017 2-cutoff promo */

			//if theres a reg submit time, use that.  otherwise new and now
			if($onlineregid > 0) {
				$checktime = intval(db_one("registrations.date","registrations","id='$onlineregid"));
			}

			$checktime = $checktime > 0 ? $checktime : time();

			$cutoff1 = mktime(23,59,59,5,4,2018); //end of may 4
			$cutoff2 = mktime(23,59,59,7,2,2018); //end of jul 2

			if($checktime <= $cutoff1) {
				$basefee = 475;
			} elseif(($checktime > $cutoff1) && ($checktime <= $cutoff2)) {
				$basefee = 525;
			} elseif($checktime > $cutoff2) {
				$basefee = 575;
			}

			//$basefee = db_one("fee","tbl_dts_reg_types","id='$regtypeid'");

			$newfee = 0;
			if(is_numeric($promocodeid) && $promocodeid > 0) {
				$pcdatas = db_get("*","promo_codes","id='$promocodeid'");
				$pcdata = $pcdatas[0];
				$pctype = $pcdata["type"];
				$pcvalue = $pcdata["value"];
				if($pctype == "percent") {
					$newfee = $basefee - (($pcvalue/100) * $basefee);
				}
				if($pctype == "value") {
					$newfee = ($basefee - $pcvalue);
				}
				$newfee = ($newfee >= 0 ? $newfee : 0);
				return (number_format($newfee,2));
			}
			else
				return (number_format($basefee,2));
		}
	}

	//entire registration fee calc for summer events
	function calc_event_registration_fees($registrationid,$save = false) {
		if($registrationid > 0) {
			$data = Array();
			$totalfees = 0;

			//get SUM attendee fees
			$sql = "SELECT SUM(fee) FROM `tbl_event_attendees` WHERE registrationid='$registrationid'";
			$res = mysql_query($sql) or die(mysql_error());
			while($row = mysql_fetch_row($res)) {
				$totalfees += $row[0];
			}

			$data["oldtotal"] = db_one("totalfees","tbl_event_registrations","id='$registrationid'");
			$data["regid"] = $registrationid;
			$data["totalfees"] = $totalfees;

			//get fees_paid
			$feespaid = db_one("feespaid","tbl_event_registrations","id='$registrationid'");
			$feespaid = strlen($feespaid) > 0 ? $feespaid : 0;
			$data["feespaid"] = str_replace(",","",(number_format($feespaid,2,'.','').""));

			//balance due
			$data["balancedue"] = number_format(($totalfees - $data["feespaid"]),2,'.','');

			if($save) {
				$sql = "UPDATE `tbl_event_registrations` SET feespaid='".$data["feespaid"]."',totalfees='".$totalfees."',balancedue='".$data["balancedue"]."' WHERE id='$registrationid' LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());
			}

			return(json_encode($data,true));
		}
	}

	//not used yet
	function get_promo_code_from_name($eventid,$promocode) {
		if(strlen($promocode) > 0 && $eventid > 0) {
			$promocode = mysql_real_escape_string(strtoupper($promocode));
			$sql = "SELECT * FROM `promo_codes` WHERE name='$promocode' AND eventid='$eventid' LIMIT 1";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					return ($row);
				}
			}
		}
	}

	//add staff note [SUMMER EVENT REG]
	function add_event_reg_staff_note($regid=0,$name="",$note="") {
		if($regid > 1 && strlen($note) > 0) {
			$name = strlen($name) > 0 ? $name : "Anonymous";
			$date = time();
			$notes_data = db_one("notes","tbl_event_registrations","id='$regid'");
			$notes_data = json_decode($notes_data, true);
			$notes_data[] = array("name"=>$name,"date"=>$date,"note"=>$note);
			$count = count($notes_data);
			$notes_data = json_encode($notes_data);
			$sql = "UPDATE `tbl_event_registrations` SET notes='$notes_data' WHERE id='$regid' LIMIT 1";
			$res = mysql_query($sql) or die(mysql_error());

			return($name."|".date('M d, Y h:ia')."|".$count);
		}
	}

	// update fees paid for single summer event registration (also used via api for paying balances on mybtf)
	function event_update_fees_paid($regid=0,$feespaid=0,$tourdateid=0) {
		$regid = intval($regid);
		if($regid > 1 && $feespaid > 0) {
			//get total fees
			$total_fees = db_one("totalfees","tbl_event_registrations","id=$regid");
			$balance_due = number_format($total_fees - $feespaid,2,".","");
			$tourdate = $tourdateid > 0 ? " AND tourdateid='".intval($tourdateid)."' " : "" ;
			$sql = "UPDATE `tbl_event_registrations` SET balancedue='$balance_due',feespaid='$feespaid' WHERE id=$regid $tourdate LIMIT 1";
			$res = mysql_query($sql) or die(mysql_error());
			return ($balance_due);
		}
	}

// DP Promo Code -- Validate ONLY, for MyBTF. Unused in stats....
	function validate_dp_promo_code($promocodeid, $regtypeid, $intensiveid) {

		if($promocodeid > 0) {

			if($regtypeid == 1)
				$add = " AND intensiveid='$intensiveid' ";

			// Check for promocode
			$sql = "SELECT active, description FROM promo_codes WHERE promo_codes.id='$promocodeid' AND promo_codes.event_regtypeid=$regtypeid $add AND promo_codes.eventid=26 LIMIT 1";
			$res = mysql_query($sql) or die(mysql_error());
			$row = mysql_fetch_array($res);

			if($row['active'] == 1) {
				return($row["description"]);
			} else {
				return false;
			}
		} else {
			return false;
		}

	}

	//single attendee fee calc for dp
	function calc_dp_attendee_fee($tourdateid,$regtypeid,$hasscholarship,$intensiveid,$scholarshipamt,$usediscountfee = false,$promocodeid=0) {

		if($tourdateid > 0) {
 			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$eventid = 26;

			$fee = 0;
			$regtypename = "";

			//regtypeid 1 = intensive, use iid and parse scholarship.  regtype 2 = crash course only.  regtype 3 = beat street only.   regtypeid 4 = observer
			if($regtypeid == 1) {
				if($intensiveid > 0) {
					$regtypename = db_one("name","tbl_event_reg_types_$seasonid","eventid=$intensiveid");

					if($usediscountfee)
						$fee = db_one("discount_fee","tbl_event_reg_types_$seasonid","eventid=$intensiveid");
					else
						$fee = db_one("fee","tbl_event_reg_types_$seasonid","eventid=$intensiveid");

					if(strlen($scholarshipamt) > 0 && $hasscholarship == 1) {
						$scholarshipamt = intval($scholarshipamt);
						$fee = $fee - $scholarshipamt;
					}

				}
				else {
					$regtypename = "";
					$fee = 0;
				}
			}
			if($regtypeid == 2) {
				$regtypename = "Crash Course";

				if($usediscountfee)
					$fee = db_one("discount_fee","tbl_event_reg_types_$seasonid","name='Crash Course' AND eventid=$eventid");
				else
					$fee = db_one("fee","tbl_event_reg_types_$seasonid","name='Crash Course' AND eventid=$eventid");
			}

			//APPLY PROMO CODE
			if($regtypeid == 1 || $regtypeid == 2) {

				if($promocodeid > 0) {

					//promo code regtypeid
					$pc_regtypeid = db_one("event_regtypeid","promo_codes","id='$promocodeid'");

					//intensive
					if($pc_regtypeid == 1) {

						$pc_iid = db_one("intensiveid","promo_codes","id='$promocodeid'");
						if($pc_iid == $intensiveid) {

							$pcdatas = db_get("*","promo_codes","id='$promocodeid'");
							$pcdata = $pcdatas[0];
							$pctype = $pcdata["type"];
							$pcvalue = $pcdata["value"];
							if($pctype == "percent") {
								$newfee = $fee - (($pcvalue/100) * $fee);
							}
							if($pctype == "value") {
								$newfee = ($fee - $pcvalue);
							}

							$fee = $newfee >= 0 ? $newfee : 0;

						}

					}

					//crash course
					if($pc_regtypeid == 2) {

						if($pc_regtypeid == $regtypeid) {

							$pcdatas = db_get("*","promo_codes","id='$promocodeid'");
							$pcdata = $pcdatas[0];
							$pctype = $pcdata["type"];
							$pcvalue = $pcdata["value"];
							if($pctype == "percent") {
								$newfee = $fee - (($pcvalue/100) * $fee);
							}
							if($pctype == "value") {
								$newfee = ($fee - $pcvalue);
							}

							$fee = $newfee >= 0 ? $newfee : 0;

						}

					}

				}
			}


			if($regtypeid == 3) {
				$regtypename = "Beat Street";
				$fee = 0;
			}
			if($regtypeid == 4) {
				$regtypename = "Observer";
				$fee = within_cutoff_date2($tourdateid) ? db_one("fee","tbl_event_reg_types_$seasonid","name='Observer'") : $fee = db_one("discount_fee","tbl_event_reg_types_$seasonid","name='Observer'");
			}

			return ($fee >= 0 ? number_format($fee,2,'.','') : 0);
		}
	}

	//single attendee fee calc for a single summer event
	function calc_event_attendee_fee($tourdateid,$commuter,$hasscholarship,$classesonly,$promocode,$scholarshipamt,$usediscountfee = false) {
		if($tourdateid > 0) {
 			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			$basefee = 0;
			$regtypeid = 0;
			$regtypename = "";

			// IF EVENTID == 23, WORKSHOP ONLY
			if($eventid == 23) {
				$regtypename = "Workshop";
				$basefee = db_one("fee","tbl_event_reg_types_$seasonid","eventid='$eventid'");
				$regtypeid = db_one("id","tbl_event_reg_types_$seasonid","eventid='$eventid'");
			}
			else {
				$regtypename = "Non-Commuter";
				if($commuter == 1)
					$regtypename = "Commuter";

				if($classesonly == 1)
					$regtypename = "Classes Only";

				// IF SHAPING SOUND
				if($eventid == 22) {
					$regtypename = "Workshop";
				}

				$regtypeid = 0;

				$newfee = 0;

				$sql = "SELECT id,fee,discountfee FROM `tbl_event_reg_types_$seasonid` WHERE name='$regtypename' AND eventid='$eventid' LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0) {
					while($row = mysql_fetch_assoc($res)) {
						$basefee = $usediscountfee ? $row["discountfee"] : $row["fee"];
						$regtypeid = $row["id"];
					}
				}


			}


			$feedata["fee"] = $basefee;
			$feedata["hs"] = $hasscholarship;

			if(strlen($scholarshipamt) > 0) {
				if(strtolower($scholarshipamt) == "full" || strtolower($scholarshipamt) == "half") {
					if(strtolower($scholarshipamt) == "full")
						$feedata["fee"] = 0;
					if(strtolower($scholarshipamt) == "half")
						$feedata["fee"] = $basefee/2;
				}
				else {
					$scholarshipamt = intval($scholarshipamt);
					$feedata["fee"] = $basefee - $scholarshipamt;
				}
			}
			else {
			/*	//get promo code id (try)
				$promocodedata = get_promo_code_from_name($eventid,$promocode);
				$promocodeid = $promocodedata["id"];
				if(intval($promocodeid) > 0) {
					$pcdatas = db_get("*","promo_codes","id=$promocodeid AND eventid=$eventid");
					$pcdata = $pcdatas[0];
					$pctype = $pcdata["type"];
					$pcvalue = $pcdata["value"];
					$noncommuteronly = $pcdata["noncommuteronly"] == 1 ? true : false;

					if(($noncommuteronly && $commuter==0) || (!$noncommuteronly)) {
						if($pctype == "percent") {
							$newfee = $basefee - (($pcvalue/100) * $basefee);
						}
						if($pctype == "value") {
							$newfee = ($basefee - $pcvalue);
						}
						$newfee = ($newfee >= 0 ? $newfee : 0);
						$feedata["promocodedata"] = $promocodedata;
						$feedata["fee"] = $newfee;
					}
				}
			 */
			}

			$feedata["fee"] = $feedata["fee"] >= 0 ? number_format($feedata["fee"],2,'.','') : 0;

			return ($feedata);
		}
	}

	//entire registration fee calc for DTS
	function calc_dts_registration_fees($registrationid,$save = false) {
		if($registrationid > 0) {
			$data = Array();
			$totalfees = 0;

			$sql = "SELECT extra_ff,extra_gala,extra_ace,extra_dvdpre FROM `tbl_dts_registrations` WHERE id=$registrationid LIMIT 1";
			$res = mysql_query($sql) or die(mysql_error());
			while($row = mysql_fetch_assoc($res)) {
				if($row["extra_ff"] > 0)
					$totalfees += 35*$row["extra_ff"];
				if($row["extra_ace"] > 0)
					$totalfees += 35*$row["extra_ace"];
				if($row["extra_dvdpre"] > 0)
					$totalfees += 150*$row["extra_dvdpre"];
//				if($row["extra_gala"] > 0)
//					$totalfees += 50*$row["extra_gala"];
//				if($row["boxsets_2009"] > 0)
//					$totalfees += 797*$row["boxsets_2009"];
//				if($row["boxsets_2010"] > 0)
//					$totalfees += 797*$row["boxsets_2010"];
//				if($row["boxsets_0910_combo"] > 0)
//					$totalfees += 1297*$row["boxsets_0910_combo"];
			}


			//get SUM attendee fees
			$sql = "SELECT SUM(fee) FROM `tbl_dts_attendees` WHERE registrationid=$registrationid";
			$res = mysql_query($sql) or die(mysql_error());
			while($row = mysql_fetch_row($res)) {
				$totalfees += $row[0];
			}

			$data["oldtotal"] = db_one("totalfees","tbl_dts_registrations","id=$registrationid");
			$data["regid"] = $registrationid;
			$data["totalfees"] = $totalfees;

			//get fees_paid
			$data["feespaid"] = str_replace(",","",(number_format(doubleval(db_one("feespaid","tbl_dts_registrations","id=$registrationid")),2,'.','').""));

			//balance due
			$data["balancedue"] = str_replace(",","",(number_format(($totalfees - $data["feespaid"]),2).""));

			if($save) {
				$sql = "UPDATE `tbl_dts_registrations` SET feespaid='".$data["feespaid"]."',totalfees='".$totalfees."',balancedue='".$data["balancedue"]."' WHERE id=$registrationid LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());
			}

			return(json_encode($data,true));
		}
	}

	//another function with other tourdate studio info.  I think this is slow but whatever. it's here.
	function get_date_studio_details($tourdateid,$studioid) {
		$datestudioid = db_one("id","tbl_date_studios","studioid=$studioid AND tourdateid=$tourdateid");
		$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");

		//get base studio info
		$sdata = Array();
		$sql = "SELECT * FROM `tbl_studios` WHERE id=$studioid";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$sdata = $row;
				$sdata["country"] = db_one("name","tbl_countries","id=".$row["countryid"]);
				$sdata["contacts"] = json_decode($sdata["contacts"],true);
			}
		}

		//get date studio info
		$dsdata = Array();
		$sql = "SELECT * FROM `tbl_date_studios` WHERE studioid=$studioid AND tourdateid=$tourdateid";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$dsdata = $row;
				$dsdata["balancedue"] = intval($dsdata["total_fees"])-intval($dsdata["fees_paid"]);
			}
		}

		//get all registrants
		$adata = Array();
		$sql = "SELECT tbl_date_dancers.id, tbl_date_dancers.profileid, tbl_date_dancers.fee, tbl_date_dancers.one_day, tbl_date_dancers.has_scholarship, tbl_date_dancers.custom_fee, tbl_date_dancers.workshoplevelid, tbl_date_dancers.vip, tbl_profiles.fname, tbl_profiles.lname, tbl_date_dancers.age, tbl_workshop_levels_$seasonid.name AS workshoplevelname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid LEFT JOIN tbl_workshop_levels_$seasonid ON tbl_workshop_levels_$seasonid.id = tbl_date_dancers.workshoplevelid WHERE tbl_date_dancers.studioid=$studioid AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$adata[] = $row;
			}
		}

		//get routines
		$rdata = Array();
		$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_age_divisions.name AS agedivision, tbl_routine_categories_$seasonid.name AS routinecategory, tbl_performance_divisions.name AS perfdivision, tbl_routine_types.name AS routinetype, tbl_routines.name AS routinename, tbl_routines.id AS routineid, tbl_date_routines.fee FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid LEFT JOIN tbl_routine_types ON tbl_routine_types.id = tbl_date_routines.routinetypeid WHERE tbl_date_routines.studioid=$studioid AND tbl_date_routines.tourdateid=$tourdateid ORDER BY tbl_routines.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				//routine dancers
				$sql2 = "SELECT tbl_date_routine_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_routine_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_routine_dancers.profileid WHERE tbl_date_routine_dancers.tourdateid=$tourdateid AND tbl_date_routine_dancers.routineid=".$row["routineid"]." ORDER BY tbl_profiles.lname ASC";
				$res2 = mysql_query($sql2) or die(mysql_error());
				if(mysql_num_rows($res2) > 0) {
					while($row2 = mysql_fetch_assoc($res2)) {
						$row["routinedancers"][] = $row2;
					}
				}
				$rdata[] = $row;
			}
		}

		$data["sdata"] = $sdata;
		$data["dsdata"] = $dsdata;
		$data["adata"] = $adata;
		$data["rdata"] = $rdata;
		return $data;
	}

	//get all routines for a specific studio in tourdate
	function get_date_studio_routines($tourdateid,$studioid,$basic = false) {

		if($tourdateid > 0 && $studioid > 0) {
			$data = Array();
			if($basic)
				$sql = "SELECT tbl_date_routines.routineid, tbl_date_routines.prelims, tbl_date_routines.finals, tbl_date_routines.vips, tbl_routines.name AS routinename, tbl_date_routines.tourdateid, tbl_date_routines.studioid FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid WHERE tbl_date_routines.tourdateid='$tourdateid' AND tbl_date_routines.studioid='$studioid' ORDER BY tbl_routines.name ASC";
			else
				$sql = "SELECT * FROM `tbl_date_routines` WHERE tourdateid='$tourdateid' AND studioid='$studioid'";

			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					if(!$basic)
						$row["routinename"] = stripslashes(db_one("name","tbl_routines","id=".$row["routineid"]));
					else $row["routinename"] = stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$row["routinename"])));

					$row["seasonid"] = db_one("seasonid","tbl_tour_dates","id='".$row["tourdateid"]."'");
					$row["eventid"] = db_one("eventid","tbl_tour_dates","id='".$row["tourdateid"]."'");

					$data[] = $row;
				}
			}
			return($data);

		}
	}

	function get_date_studio_routines_compgroup($tourdateid,$studioid,$basic=false,$compgroup="finals") {
		if($tourdateid > 0 && $studioid > 0) {
			$data = Array();
			if($basic)
				$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.routineid, tbl_routines.name AS routinename, tbl_date_routines.tourdateid, tbl_date_routines.studioid, tbl_date_routines.uploaded, tbl_date_routines.uploaded_duration FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid WHERE tbl_date_routines.tourdateid='$tourdateid' AND tbl_date_routines.studioid='$studioid' AND tbl_date_routines.$compgroup=1 ORDER BY tbl_routines.name ASC";
			else
				$sql = "SELECT * FROM `tbl_date_routines` WHERE tourdateid='$tourdateid' AND studioid='$studioid' AND $compgroup=1";

			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {

					$row["routinename"] = stripslashes(db_one("name","tbl_routines","id=".$row["routineid"]));

					if(!$basic) {
						$row["dateroutineid"] = $row["id"];
						$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
						$row["routinename"] = stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$row["routinename"])));
						$row["agedivision"] = db_one("name","tbl_age_divisions","id='".$row["agedivisionid"]."'");
						$row["routinecategory"] = db_one("name","tbl_routine_categories_".$seasonid,"id='".$row["routinecategoryid"]."'");
						$row["perfcategory"] = db_one("name","tbl_performance_divisions","id='".$row["perfcategoryid"]."'");
					}

					$row["seasonid"] = db_one("seasonid","tbl_tour_dates","id='".$row["tourdateid"]."'");
					$row["eventid"] = db_one("eventid","tbl_tour_dates","id='".$row["tourdateid"]."'");

					$data[] = $row;
				}
			}
			return($data);
		}
	}

	//get all routines for video critique sheet
	function get_date_critique_sheet($tourdateid,$studioid) {
		if($tourdateid > 0 && $studioid > 0) {
			$data = Array();
			$fdata = Array();
			$sql = "SELECT * FROM `tbl_date_routines` WHERE tourdateid=$tourdateid AND studioid=$studioid";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$data[] = $row;
				}

				$compgroups = array("vips","prelims","finals");
				foreach($data as $routine) {
					foreach($compgroups as $compgroup) {
						if($routine[$compgroup] > 0) {
							$routine["routinename"] = db_one("name","tbl_routines","id=".$routine["routineid"]);
							$routine["mktime"] = $routine[$compgroup."_time"];
							$routine["dispnum"] = $routine[$compgroup."_has_a"] == 1 ? $routine["number_".$compgroup].".a" : $routine["number_".$compgroup];
							$fdata[$compgroup][$routine["mktime"]] = $routine;
						}
					}
				}
			}
			return($fdata);
		}
	}

	//finds conflicts in workshop schedule.
	function date_competition_schedule_find_conflicts($tourdateid,$compgroup) {
		if($tourdateid && $compgroup) {
			$distance = intval(db_one("routine_dist","tbl_tour_dates","id=$tourdateid"));
			$conflicts = Array();
			$routines = get_competition_schedule_in_order($tourdateid,$compgroup);

		//get all awards
			$allawards = json_decode(db_one("awards","tbl_date_schedule_competition","tourdateid=$tourdateid"),true);
			$awards = $allawards[$compgroup];

			$clusters = array();
			$newclusters = array();
			$cnt = 0; //cluster key pos
			$pos = 0; //routines i pos
			if(count($awards) > 0) {
				foreach($awards as $award) {
					$isend = false;
					for($i=$pos;$i<count($routines);$i++) {
						if(!$isend) {
							$clusters[$cnt][] = $routines[$i];
							if($routines[$i]["dateroutineid"] == $award["dateroutineid"]) {
								$isend = true;
								++$cnt;
							}
							++$pos;
						}
					}
				}
			}

			//for each cluster
			$routines = array();
			foreach($clusters as $clusterkey=>$routines) {
				for($i=0;$i<count($routines);$i++) {
					//get dancers in routine
					$this_dancers = Array();
					$routdancers_sql = "
						SELECT
							tbl_date_routine_dancers.profileid,
							tbl_profiles.studioid,
							tbl_profiles.fname,
							tbl_profiles.lname,
							tbl_studios.name AS studioname,
							tbl_date_dancers.id AS datedancerid
						FROM `tbl_date_routine_dancers`
						LEFT JOIN tbl_profiles
							ON tbl_profiles.id = tbl_date_routine_dancers.profileid
						LEFT JOIN tbl_studios
							ON tbl_studios.id = tbl_profiles.studioid
						LEFT JOIN tbl_date_dancers
							ON tbl_date_dancers.profileid=tbl_date_routine_dancers.profileid
						WHERE tbl_date_routine_dancers.tourdateid=$tourdateid
							AND tbl_date_routine_dancers.routineid=".$routines[$i]["routineid"]."
							AND tbl_date_dancers.tourdateid=$tourdateid";

					$routdancers_res = mysql_query($routdancers_sql) or die(mysql_error());
					while($row = mysql_fetch_assoc($routdancers_res)) {
						$this_dancers[] = $row;
					}

					//forward
					for($j=0;$j<$distance;$j++) {
						$next_dancers = Array();
						$next_routine = $routines[$i+($j+1)];

						if($next_routine) {
							$n_rid = $next_routine["routineid"];
							$n_routdancers_sql = "SELECT profileid FROM `tbl_date_routine_dancers` WHERE tourdateid=$tourdateid AND routineid=".$n_rid;
							$n_routdancers_res = mysql_query($n_routdancers_sql) or die(mysql_error());

							while($row = mysql_fetch_assoc($n_routdancers_res)) {
								$next_dancers[] = array("profileid"=>$row["profileid"]);
							}

							for($od=0;$od<count($this_dancers);$od++) {
								for($nd=0;$nd<count($next_dancers);$nd++) {
									if($this_dancers[$od]["profileid"] == $next_dancers[$nd]["profileid"]) {
										$routinenum = $routines[$i]["number"];
										$routinehasa = $routines[$i]["has_a"];
										if($routinehasa == "1")
											$number = $routinenum.".a";
										else $number = $routinenum;

										$nextdispnumber = ($next_routine["has_a"] == 1 ? $next_routine["number"].".a" : $next_routine["number"]);
										$conflicts[$this_dancers[$od]["profileid"]]["routines"][] = Array("number"=>$number,"dateroutineid"=>$routines[$i]["dateroutineid"],"routineid"=>$routines[$i]["routineid"],"routinename"=>db_one("name","tbl_routines","id=".$routines[$i]["routineid"]),"number2"=>$nextdispnumber,"dateroutineid2"=>$next_routine["dateroutineid"],"routinename2"=>db_one("name","tbl_routines","id=".$next_routine["routineid"]),"distance"=>$j);
										//print("This routineid: ".$routines[$i]["routineid"]." Next routineid(dist:$j): ".$next_routine["routineid"]." || Thisdancer: ".$this_dancers[$od]["profileid"]." Nextdancer: ".$next_dancers[$nd]["profileid"]."<br/>");
									}
								}	//each next routine dancer
							}	//each this routine dancer
						}	//if routine exists
					}	//end forward distance loop

				}	//end each routines in cluster
			}	//end each cluster

			//unique-ify conflicts (group per person)

			foreach($conflicts as $key=>$dancer) {
				$tmp = Array();
				for($i=0;$i<count($dancer["routines"]);$i++) {
					if(!in_array($dancer["routines"][$i],$tmp))
						$tmp[] = $dancer["routines"][$i];
				}
				$dancer["routines"] = $tmp;
				$dancer["info"] = Array();
				$sql = "
					SELECT
						tbl_date_dancers.studioid,
						tbl_profiles.fname,
						tbl_profiles.lname,
						tbl_studios.name AS studioname
					FROM tbl_date_dancers
					LEFT JOIN tbl_profiles
						ON tbl_profiles.id = tbl_date_dancers.profileid
					LEFT JOIN tbl_studios
						ON tbl_studios.id = tbl_profiles.studioid
					WHERE tbl_date_dancers.profileid = ".$key."
						AND tbl_date_dancers.tourdateid = $tourdateid";
				$res = mysql_query($sql) or die(mysql_error());
				while($row = mysql_fetch_assoc($res)) {
					//$sid = db_one("studioid","tbl_date_dancers","profileid='$key' AND tourdateid='$tourdateid'");
					//$row["studioname"] = stripslashes(db_one("name","tbl_studios","id='$sid'"));
					$dancer["info"] = $row;
				}

				$conflicts[$key] = $dancer;
			}

			return $conflicts;
		}
	}

	//order observers if numberic   2] if alphanumeric, last name asc
	function get_studio_observers_real_order($tourdateid,$studioid){
		if($tourdateid > 0 && $studioid > 0) {
			$sql = "SELECT tbl_date_dancers.id, tbl_date_dancers.fee, tbl_date_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.birth_date, tbl_date_dancers.age FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid=$studioid AND tbl_date_dancers.workshoplevelid=8 ORDER BY tbl_profiles.lname ASC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$row["birth_date"] = (strlen($row["birth_date"]) > 0 ? $row["birth_date"] : "-");
					$obs[] = $row;
				}
				if(count($obs) > 0) {
					$nobs = array();
					foreach($obs as $obk=>$obv) {
						if(is_numeric($obv["lname"])) {
							$nobs[intval($obv["lname"])]=array("id"=>$obv["id"],"workshoplevelname"=>"Observer","fee"=>$obv["fee"],"birth_date"=>$obv["birth_date"],"age"=>$obv["age"],"fname"=>$obv["fname"],"lname"=>$obv["lname"]);
						}
						else {
							$tmp[] = array("id"=>$obv["id"],"workshoplevelname"=>"Observer","fee"=>$obv["fee"],"birth_date"=>$obv["birth_date"],"age"=>$obv["age"],"fname"=>$obv["fname"],"lname"=>$obv["lname"]);
						}
						unset($obs[$obk]);
					}
					ksort($nobs);
					if(count($tmp) > 0) {
						foreach($tmp as $atmp) {
							$nobs[] = $atmp;
						}
					}
					return($nobs);
				}

			}
		}
	}

	//order young observers if numberic   2] if alphanumeric, last name asc
	function get_young_studio_observers_real_order($tourdateid,$studioid){
		if($tourdateid > 0 && $studioid > 0) {

			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			$youngstr = "";
			switch($eventid) {
				case 7:
					$youngstr = "JUMPstart";
					break;
				case 8:
					$youngstr = "Nubie";
					break;
				case 18:
					$youngstr = "Sidekick";
					break;
			}

			$sql = "SELECT tbl_date_dancers.id, tbl_date_dancers.fee, tbl_date_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.birth_date, tbl_date_dancers.age FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid=$studioid AND tbl_date_dancers.workshoplevelid=12 ORDER BY tbl_profiles.lname ASC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$row["birth_date"] = (strlen($row["birth_date"]) > 0 ? $row["birth_date"] : "-");
					$obs[] = $row;
				}
				if(count($obs) > 0) {
					$nobs = array();
					foreach($obs as $obk=>$obv) {
						if(is_numeric($obv["lname"])) {
							$nobs[intval($obv["lname"])]=array("id"=>$obv["id"],"workshoplevelname"=>"$youngstr Observer","fee"=>$obv["fee"],"birth_date"=>$obv["birth_date"],"age"=>$obv["age"],"fname"=>$obv["fname"],"lname"=>$obv["lname"]);
						}
						else {
							$tmp[] = array("id"=>$obv["id"],"workshoplevelname"=>"$youngstr Observer","fee"=>$obv["fee"],"birth_date"=>$obv["birth_date"],"age"=>$obv["age"],"fname"=>$obv["fname"],"lname"=>$obv["lname"]);
						}
						unset($obs[$obk]);
					}
					ksort($nobs);
					if(count($tmp) > 0) {
						foreach($tmp as $atmp) {
							$nobs[] = $atmp;
						}
					}
					return($nobs);
				}

			}
		}
	}

	//get individual event's age-as-of-year
	function get_ageasofyear($tourdateid = 0) {
		if($tourdateid > 0) {
			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			if($eventid > 0) {
				return db_one("ageasofyear","events","id='$eventid'");
			}
		}
	}

	//determine one or more dancers' ages based off of tourdate's current age-as-of-year.  save optional.
	//fromapi = during mybtf reg, nobody in table_date_dancers yet, where date age is stored.  skip.
	function calc_current_age($profileids = array(),$tourdateid = 0,$save = false,$fromapi=false) {
		//send in array of profileids, one at a time or all at a time...
		if(count($profileids) > 0 && $tourdateid > 0) {

			//get age as of year
			$asofyear = get_ageasofyear($tourdateid);

			foreach($profileids as $profileid) {

				$basebd = db_one("birth_date","tbl_profiles","id='$profileid'");
				$datedancerid = $fromapi ? 0 : db_one("id","tbl_date_dancers","profileid='$profileid' AND tourdateid='$tourdateid'");

				/*
				$sql = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.birth_date FROM tbl_date_dancers LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid WHERE tbl_date_dancers.profileid=$profileid AND tbl_date_dancers.tourdateid=$tourdateid";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0) {
					while($row = mysql_fetch_assoc($res)) {
						//print_r($row);exit();
						$basebd = $row["birth_date"]; //db_one("birth_date","tbl_profiles","id='$profileid'");
						$datedancerid = $fromapi ? 0 : $row["datedancerid"];
						//db_one("id","tbl_date_dancers","profileid='$profileid' AND tourdateid='$tourdateid'");
					}
				}
				 */

				if(strlen($basebd) > 0) {
					$mdy = explode("/",$basebd);
					if(count($mdy) == 3) {

						//idiot-proof... set to current year.
						if(!$asofyear > 0) $asofyear = date("Y",time());

						$age = $asofyear - $mdy[2];
						if($mdy[1] == 1 && $mdy[0] == 1)
							$age = $age;
						else
							$age = $age-1;

						//save if flagged true
						if($save) {
							if($datedancerid > 0)
								$res = mysql_query("UPDATE `tbl_date_dancers` SET age='$age' WHERE id='$datedancerid'");
							else
								$res = mysql_query("UPDATE `tbl_profiles` SET age='$age' WHERE id='$profileid' LIMIT 1");
						}

						if(count($profileids) == 1)
							return $age;
					}
				}
			}
		}
	}

	//get real age as of right meow
	function calc_current_age_by_birthdate_real($mm = 0,$dd = 0,$yy = 0) {
		if($mm > 0 && $dd > 0 && $yy > 1900) {
			list($cYear, $cMonth, $cDay) = explode("-", date("Y-m-d"));
  			return ( ($cMonth >= $mm && $cDay >= $dd) || ($cMonth > $mm) ) ? $cYear - $yy : $cYear - $yy - 1;
		}
	}

	//pre-determine the workshop level for a dancer based on their current age
	function get_level_based_on_age($tourdateid,$age = 0) {
		if($age > 0 && $tourdateid > 0) {
			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			if($eventid > 0) {
				$wlid = 0;
				if($age < 8) {
					if($eventid == 7)
						$wlid = 6;
					if($eventid == 8)
						$wlid = 7;
					if($eventid == 14)
						$wlid = 9;
					if($eventid == 18)
						$wlid = 10;
				}
				if($age > 7 && $age < 11)
					$wlid = 5;
				if($age > 10 && $age < 13)
					$wlid = 4;
				if($age > 12 && $age < 16)
					$wlid = 3;
				if($age > 15)
					$wlid = 2;
				return $wlid;
			}
		}
	}

	function get_age_division_based_on_age($tourdateid=0,$age=0) {
		if($age > 0 && $tourdateid > 0) {
			$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
			if($age < 8) {
				if($eventid == 7)
					$adid = 5;
				if($eventid == 8)
					$adid = 6;
				if($eventid == 14)
					$adid = 7;
				if($eventid == 18)
					$adid = 10;
			}
			if($age > 7 && $age < 11)
				$adid = 1;
			if($age > 10 && $age < 13)
				$adid = 2;
			if($age > 12 && $age < 16)
				$adid = 3;
			if($age > 15 && $age < 19)
				$adid = 4;
			if($age > 18)
				$adid = 8;
		}
		return $adid;
	}

	//returns nicely-formatted tourdate date string (ex: January 4-6, 2013  or  February 28 - March 2, 2013  or  May 28, 2013)
	function get_tourdate_dispdate($tourdateid = 0) {
		if($tourdateid > 1) {
			$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
			if(strlen($start_date_a) > 2) {
				list($yy,$mm,$dd) = explode("-",$start_date_a);
				$start_date = date('F j',mktime(0,0,0,$mm,$dd,$yy));
				$start_month = date('F',mktime(0,0,0,$mm,$dd,$yy));
				$end_date_a = db_one("end_date","tbl_tour_dates","id=$tourdateid");
				list($eyy,$emm,$edd) = explode("-",$end_date_a);
				$end_date = date('F j',mktime(0,0,0,$emm,$edd,$eyy));
				$end_month = date('F',mktime(0,0,0,$emm,$edd,$eyy));
				$end_day = date('j',mktime(0,0,0,$emm,$edd,$eyy));

				//if one day
				if($start_date == $end_date) {
					$dispdate = $start_date.", $yy";
				}
				//if not one day
				else {
					if($start_month != $end_month)
						$dispdate = $start_date."-".$end_date.", $eyy";
					else $dispdate = $start_date."-".$end_day.", $eyy";
				}
			}
			return $dispdate;
		}
	}

	//get stuff from MyBTF's db
	function mybtf_get($fields,$table,$where,$order = "",$limit = 0) {
		$ip = $_SERVER["SERVER_ADDR"];
		if(strlen($fields) > 0) {
			$ord = strlen($order) > 0 ? "ORDER BY $order" : "";
			$limit = $limit > 0 ? "LIMIT $limit" : "";
			$datastr = "ip=$ip&query=".urlencode("SELECT $fields FROM `$table` WHERE $where $ord $limit");
			$url = "http://www.mybreakthefloor.com/api/get/";

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		    curl_setopt($ch, CURLOPT_POST, 2);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $datastr);

		    $res = curl_exec($ch);
		    curl_close($ch);

		    return $res;
		}
	}

	//update stuff in MyBTF's db
	function mybtf_update($data,$table,$where,$limit = 0) {
		$ip = $_SERVER["SERVER_ADDR"];
		if(count($data) > 0) {
			$datastr = "";
			foreach($data as $dak=>$dav)
				$datastr .= "$dak='$dav',";
			$datastr = rtrim($datastr,",");

			$limit = $limit > 0 ? "LIMIT $limit" : "";

			$datastr = "ip=$ip&query=".urlencode("UPDATE `$table` SET $datastr WHERE $where $limit");
			$url = "http://www.mybreakthefloor.com/api/update/";

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		    curl_setopt($ch, CURLOPT_POST, 2);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $datastr);

		    $res = curl_exec($ch);
		    curl_close($ch);

		    return $res;
		}
	}

	//get mybtf store product types
	function mybtf_store_product_types() {
		$ip = $_SERVER["SERVER_ADDR"];
		if(strlen($query) > 1) {
			$datastr = "ip=$ip&query=".urlencode($query);
			$url = "http://www.mybreakthefloor.com/api/producttypes/";

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		    curl_setopt($ch, CURLOPT_POST, 2);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $datastr);

		    $res = curl_exec($ch);
		    curl_close($ch);

		    return $res;
		}
	}

	//run any query on MyBTF
	function mybtf($query) {
		$ip = $_SERVER["SERVER_ADDR"];
		if(strlen($query) > 1) {
			$datastr = "ip=$ip&query=".urlencode($query);
			$url = "http://www.mybreakthefloor.com/api/get/";

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		    curl_setopt($ch, CURLOPT_POST, 2);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $datastr);

		    $res = curl_exec($ch);
		    curl_close($ch);

		    return $res;
		}
	}

	function date_prettify($time) {
		$date = date("m-d-Y",$time);
		return $date;
	}



	function get_dispseason($seasonid=0) {
		$seasonid = intval($seasonid);
		if($seasonid > 0) {
			$y1 = db_one("year1","tbl_seasons","id='$seasonid'");
			$y2 = db_one("year2","tbl_seasons","id='$seasonid'");
			return strlen($y2) > 0 ? $y1."-".$y2 : $y1;
		}
	}

	function acl() {
		// 1 = dev/admin
		// 2 = edit users, show financials, all access
		// 3 = cannot edit users, hide financials, all access
		// 4 = on-site, nearly everything disabled
		$userid = intval($_SESSION["User"]["id"]);
		$acl = db_one("accesslevel","users","id='$userid'");
		if($acl < 1) exit();
		return $acl;
	}

?>