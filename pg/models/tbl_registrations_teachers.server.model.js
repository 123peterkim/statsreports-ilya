"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_registrations_teachers = sequelize.define('tbl_registrations_teachers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      }
    },
    teacher_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_teachers',
        key: 'id'
      }
    },
    event_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_events',
        key: 'id'
      }
    },
    event_reg_type_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_event_reg_types',
        key: 'id'
      }
    },
    workshop_level_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_workshop_levels',
        key: 'id'
      }
    },
    promo_code_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_promo_codes',
        key: 'id'
      }
    },
    one_day: {
      type: DataTypes.BOOLEAN
    },
    attended_reg: {
      type: DataTypes.BOOLEAN
    },
    fees: {
      type: DataTypes.JSONB
    },
    date_teacher_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_teachers',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_registrations_teachers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      tbl_registrations_teachers.belongsTo(models.tbl_teachers, {foreignKey: 'teacher_id', as: 'teacher'});
      tbl_registrations_teachers.belongsTo(models.date_teachers, {foreignKey: 'date_teacher_id', as: 'date_teacher'});
      tbl_registrations_teachers.belongsTo(models.tbl_workshop_levels, {foreignKey: 'workshop_level_id', as: 'workshop_level'});
    }
  });
  return tbl_registrations_teachers;
};