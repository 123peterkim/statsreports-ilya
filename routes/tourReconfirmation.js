var _ = require('lodash');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var Op = require('sequelize').Op;
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var registrationId = req.query.registrationid || 87;

        var DateStudios = db.date_studios;
        var DateDancers = db.date_dancers;
        var TourDates = db.tbl_tour_dates;
        var DateStudiosReg = db.date_studios_registrations;

        TourDates.findAll({
            where: {
                id: tourDateId
            },
            limit: 1,
            include: [
                {model: db.tbl_event_cities, as: 'event_city', include: [
                    {model: db.tbl_states, as: 'state'}
                ]},
                { model: db.tbl_events, as: 'event' }
            ]
        })
        .then(function(results) {
            var tourDate = results[0];
            async.waterfall([
            function(cb) {
                var tourRecData = {};
                tourRecData.city = tourDate.event_city.name;
                tourRecData.state = tourDate.event_city.state.abbr;
                tourRecData.age_as_of_year = tourDate.event.age_as_of_year;
                tourRecData.current_year = new Date().getFullYear();
                tourRecData.studios = [];
                cb(null, tourRecData);
            },
            function (tourRecData, cb) {
                var studio = {};
                util.getTourdateDispdate(tourDateId)
                .then(function (dispdate) {
                    tourRecData.disp_date = dispdate;
                    cb(null, tourRecData);
                })
                .catch(function (err) {
                    console.log('ERROR', err);
                    cb(err);
                });
            },
            function (tourRecData, cb) {
                /* SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.invoice_note, tbl_date_studios.studioid AS studioid, tbl_studios.name AS studioname, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_states.abbreviation, tbl_studios.zip,  tbl_studios.cemailontacts 
                    FROM `tbl_date_studios` 
                    LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid 
                    LEFT JOIN tbl_states ON tbl_states.name = tbl_studios.state 
                    WHERE tbl_date_studios.tourdateid=$tourdateid 
                    AND tbl_date_studios.id=$datestudioid */                
                DateStudiosReg.findAll({
                    where: {
                        registration_id: registrationId,
                        tour_date_id: tourDateId
                    },
                    limit: 1,
                    include: [
                        { model: DateStudios, as: 'date_studio', include: [
                            {
                                model: db.tbl_studios, as: 'studio', include: [
                                    {
                                        model: db.tbl_addresses, as: 'address', include: [
                                            { model: db.tbl_states, as: 'state' }
                                        ]
                                    },
                                    { model: db.tbl_contact_info, as: 'phone' },
                                    { model: db.tbl_contact_info, as: 'email' },
                                    {
                                        model: db.tbl_mybtf_users, as: 'users', include: [
                                            { model: db.tbl_persons, as: 'person' }
                                        ]
                                    }
                                ]
                            },
                        ]},
                        { model: db.tbl_registrations, as: 'registration', include:[
                            { model: db.tbl_registrations_dancers, as: 'dancers', include: [
                                { model: db.tbl_dancers, as: 'dancer', include: [
                                    { model: db.tbl_persons, as: 'person' }
                                ]},
                                { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
                                    { model: db.tbl_levels, as: 'level' }
                                ]}
                            ]},
                            { model: db.tbl_registrations_routines, as: 'routines' },
                            { model: db.tbl_registrations_teachers, as: 'teachers', include: [
                                { model: db.tbl_teachers, as: 'teacher', include: [
                                    { model:db.tbl_persons, as: 'person' }
                                ]},
                                { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
                                    { model: db.tbl_levels, as: 'level' }
                                ]}
                            ] },
                            { model: db.tbl_registrations_observers, as: 'observers' },
                            { model: db.tbl_payments, as: 'payments' }
                        ]}
                    ]
                })
                .then(function(results) {
                    dateStudioReg = results[0];
                    var dateStudio = dateStudioReg.date_studio;
                    tourRecData.isind = dateStudioReg.registration.independent;

                    var studio = {
                        studio_name: dateStudio.studio.name,
                        contact_full: dateStudio.studio.users[0].person.fname + ' ' + dateStudio.studio.users[0].person.lname,
                        address: dateStudio.studio.address.address,
                        city: dateStudio.studio.address.city,
                        abbreviation: dateStudio.studio.address.state.abbr,
                        zip: dateStudio.studio.address.zip,
                        contact: dateStudio.studio.users[0].person.fname,
                        workshop_fees: 0,
                        competition_fees: 0,
                        total_fees: 0,
                        fees_paid: 0,
                        credit: 0,
                        invoice_note: null,
                        balance_due: 0,
                        teacher_count: 0,
                        senior_count: 0,
                        teen_count: 0,
                        junior_count: 0,
                        mini_count: 0,
                        jumpstart_count: 0,
                        observer_count: 0,
                        observer2_count: 0,
                        free_teacher_value: 0,
                        free_teacher_count: 0,
                        workshop_total_fees: 0,
                        workshop_registrants: [],
                        obs: [],
                        competition_routines: [],
                        oitems_count: 0,
                        oitems: {
                            balance: 0,
                            tbas: [],
                            noperfcat: [],
                            noagediv: [],
                            waivers: [],
                            nobirthday: []
                        }
                    };

                    async.parallel([
                        function(callback) {
                            var registration = dateStudioReg.registration;
                            var dancers = [];
                            // FREE TEACHER COUNT/VALUE
                            studio.free_teacher_count += registration.free_teacher.count;
                            studio.free_teacher_value += registration.free_teacher.value;

                            // FEES N SHIT
                            studio.workshop_fees = registration.fees.workshop - studio.free_teacher_value;
                            studio.competition_fees = registration.fees.competition;
                            studio.total_fees = registration.fees.total;
                            studio.credit = registration.credit.amount;
                            _.forEach(registration.payments, function(p) {
                                studio.fees_paid += p.amount;
                            });
                            studio.balance_due = studio.total_fees - studio.fees_paid - studio.credit;
                            studio.oitems.balance = studio.balance_due > 0 ? studio.balance_due : studio.oitems.balance;
                            studio.workshop_total_fees = studio.free_teacher_value + studio.workshop_fees;

                            studio.invoice_note = dateStudioReg.invoice_note; // invoice note
                            
                            _.forEach(registration.teachers, function (regTeacher) {
                                var level = regTeacher.workshop_level.level.name;
                                var birthdate = regTeacher.teacher.person.birthdate;
                                studio.workshop_registrants.push({
                                    workshop_level_name: level,
                                    fname: regTeacher.teacher.person.fname,
                                    lname: regTeacher.teacher.person.lname,
                                    birthdate: birthdate? birthdate : '-',
                                    age: util.getAge(regTeacher.teacher.person.birthdate),
                                    one_day: regTeacher.one_day,
                                    fee: regTeacher.fees.workshop
                                });
                                studio.teacher_count++;
                            });
                            _.forEach(registration.dancers, function (regDancer) {
                                var level = regDancer.workshop_level.level;
                                var birthdate = regDancer.dancer.person.birthdate;
                                dancers.push({
                                    workshop_level_name: level.name,
                                    fname: regDancer.dancer.person.fname,
                                    lname: regDancer.dancer.person.lname,
                                    birthdate: birthdate? birthdate : '-',
                                    age: util.getAge(regDancer.dancer.person.birthdate),
                                    has_scholarship: regDancer.has_scholarship,
                                    one_day: regDancer.one_day,
                                    fee: regDancer.fees.workshop,
                                    order: level.order
                                });
                                if (level === Const.SENIOR) studio.senior_count++;
                                else if (level === Const.TEEN) studio.teen_count++;
                                else if (level === Const.JUNIOR) studio.junior_count++;
                                else if (level === Const.MINI) studio.mini_count++;
                                else if (level === Const.JUMPSTART) studio.jumpstart_count++;
                                else if (level === Const.OBSERVER) studio.observer_count++;
                                else if (level === Const.OBSERVER2) studio.observer2_count++;
                            });
                            
                            dancers = _.sortBy(dancers, ['order']);
                            studio.workshop_registrants = _.concat(studio.workshop_registrants, dancers);

                            studio.workshop_registrants_count = studio.workshop_registrants.length;
                            callback(null);
                        },
                        // STUDIO OBSERVERS REAL ORDER
                        function(callback) {
                            util.getStudioObserverRealOrder(tourDateId, studioId)
                            .then(function(dancers) {
                                studio.obs = dancers;
                                callback(null);
                            })
                            .catch(function(err) {
                                callback(err);
                            });
                        },
                        // COPETITION ROUTINES
                        function(callback) {
                            /* SELECT tbl_date_routines.routineid AS routineid, tbl_date_routines.extra_time, tbl_routines.name AS routinename, tbl_age_divisions.name AS agedivisionname, tbl_age_divisions.range, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_performance_divisions.name AS performancedivisionname, tbl_date_routines.fee 
                                FROM `tbl_date_routines` 
                                LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid 
                                LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid 
                                LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid 
                                LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid 
                                WHERE tbl_date_routines.tourdateid=$tourdateid 
                                AND tbl_date_routines.studioid=$studioid ORDER BY tbl_routines.name ASC */
                            db.date_routines.findAll({
                                where: {
                                    tour_date_id: tourDateId,
                                    studio_id: studioId
                                },
                                include: [
                                    { model: db.tbl_routines, as: 'routine' },
                                    { model: db.tbl_registrations_routines, as: 'registrations_routine', include: [
                                        { model: db.tbl_age_divisions, as: 'age_division', include: [
                                            { model: db.tbl_levels, as: 'level' }
                                        ]},
                                        { model: db.tbl_routine_categories, as: 'routine_category', include:[
                                            { model: db.tbl_categories, as: 'category' }
                                        ]},
                                        { model: db.tbl_performance_divisions, as: 'performance_division' }
                                    ]},
                                    { model: db.tbl_registrations, as: 'registration' },
                                    { model: db.date_routines_dancers, as: 'dancers', include: [
                                        { model: db.date_dancers, as: 'date_dancer', include: [
                                            { model: db.tbl_dancers, as: 'dancer', include: [
                                                { model: db.tbl_persons, as: 'person' }
                                            ]}
                                        ]}
                                    ]}
                                ]
                            })
                            .then(function(results) {
                                studio.competition_routines_count = results.length;
                                _.forEach(results, function(dateRoutine) {
                                    var ageDivision = dateRoutine.registrations_routine.age_division;
                                    var range = '' + ageDivision.minimum_age + '-'+(ageDivision.range+ageDivision.minimum_age);
                                    var routineCategory = dateRoutine.registrations_routine.routine_category;
                                    var routineDancers = [];
                                    _.forEach(dateRoutine.dancers, function(routDancer) {
                                        routineDancers.push({
                                            fname: routDancer.date_dancer.dancer.person.fname,
                                            lname: routDancer.date_dancer.dancer.person.lname
                                        });
                                    });
                                    studio.competition_routines.push({
                                        routine_name: dateRoutine.routine.name,
                                        age_division_name: ageDivision.level.name,
                                        range: range,
                                        routine_category_name: routineCategory.category.name,
                                        routine_dancers: routineDancers,
                                        routine_dancers_count: routineDancers.length,
                                        performance_division_name: dateRoutine.registrations_routine.performance_division.name,
                                        fee: dateRoutine.registration.fees.workshop,
                                        extra_time: dateRoutine.registrations_routine.extra_time
                                    });
                                });

                                callback(null);
                            })
                            .catch(function(err) {
                                callback(err);
                            });
                        },
                        // OITEMS - BEGIN
                        function(callback) {
                            // check for missing waivers (for dancers only)
                            /* SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.fname, tbl_profiles.lname
                                FROM `tbl_date_dancers` 
                                LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid 
                                WHERE tbl_date_dancers.tourdateid=$tourdateid 
                                AND tbl_date_dancers.workshoplevelid > 1 
                                AND tbl_date_dancers.workshoplevelid < 7 
                                AND tbl_date_dancers.studioid=".$studioid." 
                                AND (tbl_date_dancers.waiverid IS NULL OR tbl_date_dancers.waiverid < 1) 
                                ORDER BY tbl_profiles.lname ASC */
                            db.date_dancers.findAll({
                                where: {
                                    tour_date_id: tourDateId,
                                    studio_id: studioId
                                },
                                include: [
                                    { model: db.tbl_registrations_dancers, as: 'registrations_dancer', include: [
                                        { model: db.tbl_workshop_levels, as: 'workshop_level', where: { level_id: { $gt: 1, $lt: 7 } }}
                                    ]},
                                    { model: db.tbl_dancers, as: 'dancer', include: [{ model: db.tbl_persons, as: 'person' }],
                                        where: { 
                                            $or: [
                                                { waiver: null },
                                                { waiver: { $eq: '' } }
                                            ]
                                        }
                                    }
                                ],
                                order: [
                                    [{model: db.tbl_dancers, as: 'dancer'}, {model: db.tbl_persons, as: 'person'}, 'lname', 'ASC']
                                ]
                            })
                            .then(function(results) {
                                _.forEach(results, function(dateDancer) {
                                    studio.oitems.waivers.push({
                                        fname: dateDancer.dancer.person.fname,
                                        lname: dateDancer.dancer.person.lname
                                    });

                                    studio.oitems_count++;
                                });
                                callback(null);
                            })
                            .catch(function(err) {
                                console.error(err);
                                callback(err);
                            });
                        },
                        function(callback) {
                            // check for blank/tba routine names
                            /*SELECT tbl_routines.name AS routinename, tbl_routines.id AS routineid, tbl_date_routines.id AS dateroutineid, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname 
                                FROM `tbl_date_routines`
                                LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid 
                                LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid
                                LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid
                                WHERE tbl_date_routines.tourdateid=$tourdateid
                                AND tbl_date_routines.studioid=".$studioid." 
                                AND (tbl_routines.name='TBA' OR tbl_routines.name='') */
                            db.date_routines.findAll({
                                where: {
                                    tour_date_id: tourDateId,
                                    studio_id: studioId
                                },
                                include: [
                                    { model: db.tbl_routines, as: 'routine', where: { $or: [{name: 'TBA'}, {name: ''}]} },
                                    { model: db.tbl_registrations_routines, as: 'registrations_routine', include: [
                                        { model: db.tbl_age_divisions, as: 'age_division', include: [
                                            { model: db.tbl_levels, as: 'level' }
                                        ]},
                                        { model: db.tbl_routine_categories, as: 'routine_category', include: [
                                            { model: db.tbl_categories, as: 'category' }
                                        ]}
                                    ]}
                                ]
                            })
                            .then(function(results) {
                                _.forEach(results, function(dateRoutine) {
                                    var regRoutine = dateRoutine.registrations_routine;
                                    studio.oitems.tbas.push({
                                        age_division_name: regRoutine.age_division.level.name,
                                        routine_category_name: regRoutine.routine_category.category.name
                                    });

                                    studio.oitems_count++;
                                });

                                callback(null);
                            })
                            .catch(function(err) {
                                callback(err);
                            });
                        },
                        function(callback) {
                            // check for missing routine categories
                            /* SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid 
                                FROM `tbl_date_routines` 
                                LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid 
                                WHERE !tbl_date_routines.perfcategoryid > 0 
                                AND tbl_date_routines.tourdateid=$tourdateid 
                                AND tbl_date_routines.studioid=".$row["studioid"] */
                            db.date_routines.findAll({
                                where: {
                                    tour_date_id: tourDateId,
                                    studio_id: studioId
                                },
                                include: [
                                    { model: db.tbl_registrations_routines, as: 'registrations_routine', 
                                        include: [ { model: db.tbl_performance_divisions, as: 'performance_division' } ],
                                        where: { performance_division_id: null }
                                    },
                                    { model: db.tbl_routines, as: 'routine' }
                                ]
                            })
                            .then(function(results) {
                                _.forEach(results, function(dateRoutine) {
                                    studio.oitems.noperfcat.push({
                                        routine_name: dateRoutine.routine.name
                                    });
                                    studio.oitems_count++;
                                });

                                callback(null);
                            })
                            .catch(function(err) {
                                callback(err);
                            });
                        },
                        function(callback) {
                            // check for missing birth dates
                            /* SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid,tbl_profiles.fname,  tbl_profiles.lname 
                                FROM `tbl_date_dancers` 
                                LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid 
                                WHERE tbl_date_dancers.tourdateid=$tourdateid 
                                AND tbl_date_dancers.studioid=".$row["studioid"]." 
                                AND tbl_profiles.birth_date='' 
                                AND (tbl_date_dancers.workshoplevelid > 1 AND tbl_date_dancers.workshoplevelid < 7) */
                            db.date_dancers.findAll({
                                where: {
                                    tour_date_id: tourDateId,
                                    studio_id: studioId
                                },
                                include: [
                                    { model: db.tbl_dancers, as: 'dancer', include: [
                                        { model: db.tbl_persons, as: 'person', where: { birthdate: null } 
                                    }]
                                    },
                                    { model: db.tbl_registrations_dancers, as: 'registrations_dancer', 
                                        include: [
                                            {
                                                model: db.tbl_workshop_levels, as: 'workshop_level', where: {
                                                    level_id: { [Op.gt]: 1 },
                                                    level_id: { [Op.lt]: 7 },
                                                }
                                            }
                                        ]
                                    }
                                ]
                            })
                            .then(function(results) {
                                _.forEach(results, function(dateDancer) {
                                    if(dateDancer.dancer) {
                                        studio.oitems.nobirthday.push({
                                            fname: dateDancer.dancer.person.fname,
                                            lname: dateDancer.dancer.person.lname
                                        });
                                        studio.oitems_count++;
                                    }
                                });

                                callback(null);
                            })
                            .catch(function(err) {
                                callback(err);
                            });
                        },
                        function(callback) {
                            // check for missing routine age divisions
                            /* SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid 
                                FROM `tbl_date_routines` 
                                LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid 
                                WHERE !tbl_date_routines.agedivisionid > 0 
                                AND tbl_date_routines.tourdateid=$tourdateid 
                                AND tbl_date_routines.studioid=".$row["studioid"] */
                            db.date_routines.findAll({
                                where: {
                                    studio_id: studioId,
                                    tour_date_id: tourDateId
                                },
                                include: [
                                    { model: db.tbl_registrations_routines, as: 'registrations_routine', 
                                        where: { age_division_id: null },
                                        include: [
                                            { model: db.tbl_routines, as: 'routine' }
                                        ]
                                    }
                                ]

                            })
                            .then(function(results) {
                                _.forEach(results, function(dateRoutine) {
                                    studio.oitems.noagediv.push({
                                        routine_name: dateRoutine.registrations.routine.routine.name
                                    });
                                    studio.oitems_count++;
                                });
                                callback(null);
                            })
                            .catch(function(err) {
                                console.error(err);
                                res.send(err);
                            });

                        }
                        // OITEMS - END
                    ],
                    function(err, results) {
                        if(err) cb(err);
                        else {
                            tourRecData.studios = [studio];
                            cb(null, tourRecData);
                        }
                    });
                    /* 
                    Unsed data
                    $row["pd_senior_count"] + $row["pd_teen_count"] + $row["pd_junior_count"] + $row["pd_mini_count"] + $row["pd_jumpstart_count"];
                    $row["pd_senior_count"] = db_one("COUNT(id)", "tbl_date_dancers", "studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid AND fee > 0");
                    $row["pd_teen_count"] = db_one("COUNT(id)", "tbl_date_dancers", "studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid AND fee > 0");
                    $row["pd_junior_count"] = db_one("COUNT(id)", "tbl_date_dancers", "studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid AND fee > 0");
                    $row["pd_mini_count"] = db_one("COUNT(id)", "tbl_date_dancers", "studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid AND fee > 0");
                    $row["pd_jumpstart_count"] = db_one("COUNT(id)", "tbl_date_dancers", "studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid AND fee > 0");
                    $row["pd_dancer_count"] = $row["pd_senior_count"] + $row["pd_teen_count"] + $row["pd_junior_count"] + $row["pd_mini_count"] + $row["pd_jumpstart_count"]; 
                    */
                })
                .catch(function(err) {
                    console.log('ERROR', err);
                    cb(err);
                });                
            }
            ],
            function(err, tourRecData) {
                if(err) {
                    console.log(err);
                    res.send(err);
                }
                else {
                    res.render('reports/tour-reconfirmation', tourRecData);
                }
            });
        });
        






    }
}