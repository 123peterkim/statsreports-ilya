<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$excelonly = $_GET["xls"] == 1 ? true: false;
	$xlsadd = $_GET["xls"] == 1 ? "_xls" : "";

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));

	$sql = "SELECT id,name$xlsadd FROM `tbl_special_awards` WHERE eventid=$eventid ORDER BY report_order ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$baseawards = Array();

	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$baseawards[] = $row;
		}

		$awards = Array();
		foreach($baseawards as $baseaward) {//tbl_routines.name AS routinename, tbl_routines.studioid, tbl_studios.name AS studioname
			$sql2 = "SELECT tbl_date_special_awards.dateroutineid FROM `tbl_date_special_awards` WHERE tbl_date_special_awards.tourdateid = $tourdateid AND tbl_date_special_awards.awardid = ".$baseaward["id"];
			$res2 = mysql_query($sql2) or die(mysql_error());
			if(mysql_num_rows($res2) > 0) {
				while($row = mysql_fetch_assoc($res2)) {
					$studioid = db_one("studioid","tbl_date_routines","id=".$row["dateroutineid"]);
					$row["studioname"] = db_one("name","tbl_studios","id=$studioid");
					$routineid = db_one("routineid","tbl_date_routines","id=".$row["dateroutineid"]);
					$row["routinename"] = db_one("name","tbl_routines","id=$routineid");
					$awards[$baseaward["name"]][] = $row;
				}
			}
		}
	}

	if($excelonly) {
		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("Routine","Studio","Special Award");
		foreach($awards as $awardname=>$winners) {
			foreach($winners as $winner) {
				$dataArray[] = array(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$winner["routinename"]))),stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$winner["studioname"]))), $baseaward["name_xls"]); // FIX FOR THE EMPTY COLUMN PROBLEM
			}
		}
		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "JUMP_specialawardwinners_$tourdateid".substr($mk,6,5);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);
		exit();
	}


?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 11px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
			//window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Special Awards</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print($venue); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<?php
				if(count($awards) > 0) {
					foreach($awards as $key=>$group) {
						if(count($group) > 0) {?>
					<div style="margin:10px 0 7px;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;"><?php
							print($key);
					?></div>
					<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:30px;">
						<tr>
							<td class="thead" style="width:400px;">Studio</td><td class="thead" style="width:500px;border-right:1px solid #000000;">Routine</td>
						</tr>
					<?php

							foreach($group as $line) { ?>
						<tr>
							<td class="tbody"><?php print($line["studioname"]); ?></td>
							<td class="tbody" style="border-right:1px solid #000000;"><?php print($line["routinename"]); ?></td>
						</tr>
					<?php } ?>
					</table>
			<?php  	}
				}
			} ?>
		</div>
	</body>
</html>