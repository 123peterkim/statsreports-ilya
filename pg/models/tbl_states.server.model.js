"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_states = sequelize.define('tbl_states', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		country_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_countries',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		abbr: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_states.belongsTo(models.tbl_countries, {foreignKey: 'country_id', as: 'country'});
		}
	});
	return tbl_states;
};