"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_venues = sequelize.define('tbl_venues', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		address_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_addresses',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		website: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_venues.belongsTo(models.tbl_addresses, {foreignKey: 'address_id', as: 'address'});
      tbl_venues.belongsToMany(models.tbl_contact_info, {foreignKey: 'venue_id', otherKey: 'contact_info_id', as: 'phone', through: models.contact_info_venue});
      tbl_venues.belongsToMany(models.tbl_contact_info, {foreignKey: 'venue_id', otherKey: 'contact_info_id', as: 'email', through: models.contact_info_venue});
		}
	});
	return tbl_venues;
};