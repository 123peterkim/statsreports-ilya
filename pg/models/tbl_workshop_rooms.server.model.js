"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_workshop_rooms = sequelize.define('tbl_workshop_rooms', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_workshop_rooms.belongsToMany(models.tbl_tour_dates, {as: 'tour_dates', through: models.tour_dates_workshop_rooms, foreignKey: 'workshop_room_id'});
		}
	});
	return tbl_workshop_rooms;
};