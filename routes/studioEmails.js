var Excel = require('exceljs');
var _ = require('lodash');
var fs = require('fs');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var util = require('../reference/util/util');

module.exports = {
    studioEmailsSpreadsheet: function(req, res) {
        // query params
        var tourDateId = req.query.tourdateid;
        var DateStudios = db.date_studios;

        DateStudios.findAll({
            where: {
                tour_date_id: tourDateId
            },
            include: [
                { model: db.tbl_studios, as: 'studio', include: [
                    { model: db.tbl_mybtf_users, as: 'users', include: [
                        { model: db.tbl_persons, as: 'person' }
                    ]},
                ]},
                { model: db.tbl_registrations, as: 'registrations' }
            ]
        })
        .then(function(result) {
            var wb = new Excel.Workbook();
            var ws = wb.addWorksheet('Worksheet');
            var studioEmailsData = [];
            var contact, email;

            _.forEach(result, function (data) {
                var studio = data.studio;
                var registration = data.registrations[0];

                try { contact = studio.users[0].person.fname + " " + studio.users[0].person.lname }
                catch (err) { contact = ""; }

                try { email = studio.users[0].email; }
                catch(err) { email = ""; }

                studioEmailsData.push({
                    name: studio.name,
                    contact: contact,
                    email: email,
                    independent: registration.independent? 'Yes' : 'No'
                });
            });

            ws.columns = [
                { header: 'Studio Name', key: 'name', width: util.setWidthAuto(studioEmailsData, 'name', 'Studio Name'), style: { font: { name: 'Arial' } } },
                { header: 'Contact', key: 'contact', width: util.setWidthAuto(studioEmailsData, 'contact', 'Contact'), style: { font: { name: 'Arial' } } },
                { header: 'Email', key: 'email', width: util.setWidthAuto(studioEmailsData, 'email', 'Email'), style: { font: { name: 'Arial' } } },
                { header: 'Independent', key: 'independent', width: util.setWidthAuto(studioEmailsData, 'independent', 'Independent'), style: { font: { name: 'Arial' } } }
            ];

            ws.addRows(studioEmailsData);

            var filename = 'public/' + Date.now();

            wb.xlsx.writeFile(filename)
                .then(function () {
                    res.download(filename, "Studio emails.xlsx", function (err) {
                        if (!err) fs.unlink(filename);
                    });
                });
        })
    }
}