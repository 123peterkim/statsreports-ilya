"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_studios = sequelize.define('date_studios', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		combine: {
			type: DataTypes.BOOLEAN
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_studios.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			date_studios.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			date_studios.hasMany(models.date_studio_awards, {foreignKey: 'date_studio_id', as: 'studio_awards'});
      date_studios.hasMany(models.date_studios_registrations, {foreignKey: 'date_studio_id', as: 'studio_registrations'});
      date_studios.belongsToMany(models.tbl_registrations, {through: 'date_studios_registrations', foreignKey: 'date_studio_id', otherKey: 'registration_id', as: 'registrations'});
    }
	});
	return date_studios;
};