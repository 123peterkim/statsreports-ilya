<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");

	$adids = array(1,2,3,4);
	$genders = array("female","male");
	$allshit = array();

	foreach($adids as $adid) {
		$adname = db_one("name","tbl_age_divisions","id=$adid");
		foreach($genders as $gender) {

			$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.routineid, tbl_studios.name AS studioname, tbl_date_routines.number_vips AS number, tbl_date_routines.vips_has_a AS has_a, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender, tbl_date_dancers.scholarship_code FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_routines ON tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.iscompeting=1 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_routines.agedivisionid=$adid AND tbl_profiles.gender='$gender' AND tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid ORDER BY tbl_profiles.lname DESC, tbl_profiles.fname DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);
					$allshit[$adname][ucfirst($gender)][] = $row;
				}
			}

		}//each goddam gender
	}// each piece of shit age division

	$tmp = array();
	if(count($allshit) > 0) {
		foreach($allshit as $adname=>$genders) {
			foreach($genders as $gender=>$dancers) {
				$spot = 1;
				$group = 1;
				foreach($dancers as $dancerkey=>$dancer) {
					if($spot == 7) {
						$spot = 1;
						$group = $group == 1 ? 2 : 1;
					}
					$dancer["ac_group"] = $group;
					$dancer["ac_spot"] = $spot;
					++$spot;
					$tmp[$adname][$gender][] = $dancer;
				}
			}
		}

		$allshit = $tmp;
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Audition Class</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}
			.dtable thead tr th {
				background-color: #000000;
				color: #FFFFFF;
				font-family: 'Trajan Pro';
				padding: 6px 0 2px;
				text-align: center;
				font-size: 15px;
				font-weight: normal;
			}
			.dtable tbody tr td {
				border-left: 1px solid #CCCCCC;
				border-bottom: 1px solid #CCCCCC;
				padding: 8px 0;
			}
		</style>
	</head>
	<body>
	<?php
		foreach($allshit as $adname=>$genders) {
			foreach($genders as $gender=>$dancers) {
	?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align: top;">
					<?php
						if($eventid == 14) { ?>
							<img src="tda_logo.jpg" alt="" style="height:100px;width:auto;" />
					<?php } ?>
					<?php
						if($eventid == 18) { ?>
							<img src="../../24seven_logo.png" alt="" style="height:100px;width:auto;" />
					<?php } ?>
					</td>
					<td style="text-align:right;">
						<div style="text-align:right;">
							<table style="float:right;">
								<tr><td style="padding-right:10px;"><?php if($eventid == 14) print("Judge's Pick"); if($eventid == 18) print("Stop the Clock"); ?></td><td>97-100</td></tr>
								<tr><td style="padding-right:10px;">High Gold</td><td>92-96</td></tr>
								<tr><td style="padding-right:10px;">Gold</td><td>87-91</td></tr>
								<tr><td style="padding-right:10px;">High Silver</td><td>82-86</td></tr>
								<tr><td style="padding-right:10px;">Silver</td><td>77-81</td></tr>
							</table>
						</div>
						<div style="clear:both;"></div>
						<div style="font-family:'Trajan Pro';font-size:25px;"><?php print(strtoupper($adname." ".$gender));?> <?php print($eventid == 14 ? "BEST DANCERS" : "NON-STOP DANCERS"); ?></div>
						<div style="font-family:'Trajan Pro';font-size:20px;">AUDITION CLASS</div>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" style="width:100%;margin-top:15px;" class="dtable">
				<thead>
					<tr>
						<th style="width: 55px;">AUD.#</th><th style="width:55px;">SPOT</th><th style="width:130px;">DANCER</th><th style="width:235px;">STUDIO</th><th style="width:115px;">BALLET SCORE</th><th>JAZZ<br/>SCORE</th>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($dancers as $dancer) {
				?>
					<tr>
						<td style="text-align:center;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>"><?php print($dancer["scholarship_code"]);?></td>
						<td style="text-align:center;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>"><?php print($dancer["ac_spot"]);?></td>
						<td style="padding-left:3px;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>"><?php print($dancer["fname"]." ".$dancer["lname"]);?></td>
						<td style="padding-left:3px;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>"><?php print($dancer["studioname"]);?></td>
						<td style="padding-left:3px;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>">&nbsp;</td>
						<td style="border-right: 1px solid #CCCCCC;<?php if($dancer["ac_group"]==2) print("background-color:#DDDDDD;");?>">&nbsp;</td>
					</tr>
				<?php
					 }	?>
				</tbody>
			</table>
		</div>
	<?php	  }
	  } ?>
	</body>
</html>