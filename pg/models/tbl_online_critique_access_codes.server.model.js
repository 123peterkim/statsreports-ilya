"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_online_critique_access_codes = sequelize.define('tbl_online_critique_access_codes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		code: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_online_critique_access_codes;
};