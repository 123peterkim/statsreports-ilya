"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_teachers = sequelize.define('date_teachers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      }
    },
    tour_date_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_tour_dates',
        key: 'id'
      }
    },
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      }
    },
    teacher_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_teachers',
        key: 'id'
      }
    },
    registrations_teacher_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations_teachers',
        key: 'id'
      }
    },
    custom_fee: {
      type: DataTypes.BOOLEAN
    }
  }, {
    timestamps: false,
    associate: function (models) {
      date_teachers.belongsTo(models.tbl_teachers, {foreignKey: 'teacher_id', as: 'teacher'});
      date_teachers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
      date_teachers.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
      date_teachers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      date_teachers.belongsTo(models.tbl_registrations_teachers, {foreignKey: 'registrations_teacher_id', as: 'registrations_teacher'});
    }
  });
  return date_teachers;
};