<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);

	$contacts = Array();
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.contacts, tbl_studios.address, tbl_studios.city, tbl_studios.zip, tbl_studios.state, tbl_countries.name AS countryname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid LEFT JOIN tbl_countries ON tbl_countries.id = tbl_studios.countryid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			if(strlen($row["contacts"]) > 0) {
				$scontacts = json_decode($row["contacts"],true);
				$row["contact"] = $scontacts[0]["fname"]." ".$scontacts[0]["lname"];
				$row["studioname"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"])));
				$abbr = db_one("abbreviation","tbl_states","name='".$row["state"]."'");
				if(strlen($abbr) > 0)
					$row["state"] = $abbr;
			}
			unset($row["contacts"]);
			unset($row["datestudioid"]);
			$contacts[] = $row;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Mailing Labels</title>
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,700|Nothing+You+Could+Do:400' rel='stylesheet' type='text/css'>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.sl_table {
				/*margin-top: 50px;*/
			}

			.sl_table tr td {
				width: 360px;
				/*background-color: rgba(100,0,0,0.2);
			border: 1px solid #AA0000;*/
				padding-top: 65px;
			}

			.labelbg {
				background: url(mailing_label_bg.jpg) no-repeat top center;
				height:260px;
				margin: 0 0 0 0;
				width: 100%;

			}
			.labeltext {
				font-family: 'Oswald', sans-serif;
				font-size:20px;
				padding: 60px 0 0px 65px;
			}
		</style>

	</head>
	<body>
	<?php
		$count = 0;
		for($i=0;$i<count($contacts);$i+=2) { ?>
		<table cellpadding="0" cellspacing="0" class="sl_table" <?php if(($count+1) % 3 == 0) { print("style='page-break-after:always;'"); }?>>
			<tr>
				<td style="vertical-align: top;">
					<?php if($contacts[$i]) { ?>
						<div class="labelbg">
							<div class="labeltext">
							<?php
								if(strlen($contacts[$i]["contact"]) > 0)
									print($contacts[$i]["contact"]."<br/>");
								if($contacts[$i]["studioname"] != "[N/A]")
									print($contacts[$i]["studioname"]."<br/>");
								print($contacts[$i]["address"]."<br/>");
								print($contacts[$i]["city"].", ".$contacts[$i]["state"]." ".$contacts[$i]["zip"]."<br/>");
								if($contacts[$i]["countryname"] != "United States")
									print($contacts[$i]["countryname"]);
							?>
							</div>
						</div>
					<?php } ?>
				</td>
				<td style="vertical-align: top;padding-left:40px;">
					<?php if($contacts[$i+1]) { ?>
						<div class="labelbg">
							<div class="labeltext">
							<?php
								if(strlen($contacts[$i+1]["contact"]) > 0)
									print($contacts[$i+1]["contact"]."<br/>");
								if($contacts[$i+1]["studioname"] != "[N/A]")
									print($contacts[$i+1]["studioname"]."<br/>");
								print($contacts[$i+1]["address"]."<br/>");
								print($contacts[$i+1]["city"].", ".$contacts[$i+1]["state"]." ".$contacts[$i+1]["zip"]."<br/>");
								if($contacts[$i+1]["countryname"] != "United States")
									print($contacts[$i+1]["countryname"]);
							?>
							</div>
						</div>
					<?php } ?>
				</td>
			</tr>
		</table>
	<?php
		++$count;
	} ?>
</body>