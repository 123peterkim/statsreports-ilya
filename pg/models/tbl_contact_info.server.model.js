"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_contact_info = sequelize.define('tbl_contact_info', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    contact_type_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_contact_types',
        key: 'id'
      }
    },
    value: {
      type: DataTypes.STRING
    },
    primary: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_contact_info.belongsTo(models.tbl_contact_types, {foreignKey: 'contact_type_id', as: 'contact_type'});
    }
  });
  return tbl_contact_info;
};