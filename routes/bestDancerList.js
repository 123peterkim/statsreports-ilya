var _ = require('lodash');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var registrationId = req.query.registrationid || 87;

        db.tbl_tour_dates.findAll({
            where: {
                id: tourDateId
            },
            limit: 1,
            include: [
                { model: db.tbl_event_cities, as: 'event_city', include: [
                    { model: db.tbl_states, as: 'state' }
                ]},
                { model: db.tbl_events, as: 'event' }
            ]
        })
        .then(function(results) {
            var tourDate = results[0];
            return {
                city: tourDate.event_city.name,
                disp_date: util.tourDateDisplayDate(tourDate),
                data: {}
            };
        })
        .then(function(bestDancerData) {
            /* SELECT tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.iscompeting, tbl_date_dancers.age, 
                tbl_profiles.lname, tbl_profiles.fname, tbl_profiles.gender, tbl_studios.name AS studioname
                FROM`tbl_tda_bestdancer_data`
                LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_tda_bestdancer_data.profileid
                LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_tda_bestdancer_data.profileid
                LEFT JOIN tbl_studios ON tbl_studios.id = tbl_tda_bestdancer_data.studioid
                WHERE tbl_tda_bestdancer_data.tourdateid = $tourdateid
                AND tbl_date_dancers.tourdateid = '$tourdateid' minimalismo
                ORDER BY tbl_profiles.gender ASC, tbl_profiles.lname ASC */
            db.tbl_tda_best_dancer_data.findAll({
                where: {
                    tour_date_id: tourDateId,
                },
                include: [
                    { model: db.tbl_dancers, as: 'dancer', include: [
                        { model: db.tbl_persons, as: 'person', include: [
                            { model: db.tbl_gender, as: 'gender' }
                        ]}
                    ]},
                    { model: db.tbl_routines, as: 'routine', include: [
                        { model: db.tbl_studios, as: 'studio' }
                    ]},
                    { model: db.tbl_tour_dates, as: 'tour_date' }
                ]
            })
            .then(function(results) {
                _.forEach(results, function(bestDancer) {
                    // TODO Get the workshop level for the dancers
                    bestDancerData.push({
                        is_competing: bestDancer.is_competing,
                        fname: bestDancer.dancer.person.fname,
                        lname: bestDancer.dancer.person.lname,
                        studio_name: bestDancer.routine.studio.name,
                        age: util.getAge(bestDancer.dancer.person.birthdate)
                    })
                })
                
                res.render('best-dancer-list', bestDancerData);
            });
        });
    }
}