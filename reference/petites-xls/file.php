<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$cityname = strtolower(str_replace(array(" ","-",","),"",db_one("city","tbl_tour_dates","id=$tourdateid")));
	$dancers = array();

	if($tourdateid > 0) {
		$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
		if($eventid > 0) {
			$adid = 0;
			$astr = "";
			if($eventid == 7) {
				$adid = 6;
				$astr = "jumpstarts";
			}
			if($eventid == 8) {
				$adid = 7;
				$astr = "nubies";
			}
			if($eventid == 14) {
				$adid = 9;
				$astr = "peewees";
			}
			if($eventid == 18) {
				$adid = 10;
				$astr = "sidekicks";
			}
			if($eventid == 28) {
				$adid = 11;
				$astr = "rookies";
			}

			if($adid > 0) {
				$sql = "SELECT tbl_date_dancers.id AS datedancerid,tbl_profiles.fname, tbl_profiles.lname, tbl_date_dancers.age, tbl_date_dancers.one_day, tbl_studios.name AS studioname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_dancers.studioid WHERE tbl_date_dancers.workshoplevelid=$adid AND tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0) {
					while($row = mysql_fetch_assoc($res)) {
						$row["studioname"] = stripslashes(str_replace("&amp;","&",$row["studioname"]));
						$row["days"] = $row["one_day"] == 1 ? 1 : 2;
						$dancers[] = $row;
					}

				}

				if(count($dancers) > 0) {
					//create excel shit
					$workbook = new PHPExcel();
					$workbook->setActiveSheetIndex(0);

					//set global font & font size
//					$workbook->getDefaultStyle()->getFont()->setName('Arial');

					//col widths
					$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

					$dataArray = array();
					$dataArray[] = array("FIRST","LAST","AGE","DAYS #","STUDIO");
					foreach($dancers as $dancer) {
						$dataArray[] = array($dancer["fname"],$dancer["lname"],$dancer["age"],$dancer["days"],$dancer["studioname"]);
					}
					$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

					$outputFileType = 'Excel5';
					$mk = time();
					$somekindofrandomstr = $astr."_".$cityname.substr($mk,6,4);
					$outputFileName = "../../temp/$somekindofrandomstr.xls";
					$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
					$objWriter->save($outputFileName);
					chmod($outputFileName,777);
					unset($workbook);
					unset($objWriter);

					header("Cache-Control: public");
					header("Content-Description: File Transfer");
					header("Content-Disposition: attachment; filename=".basename($outputFileName));
					header("Content-type: application/octet-stream");
					header("Content-Transfer-Encoding: binary");
					readfile($outputFileName);
				}
			}
		}
	}



	exit();
?>