"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_hotels = sequelize.define('tbl_hotels', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		address_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_addresses',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		website: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_hotels.belongsTo(models.tbl_addresses, {foreignKey: 'address_id', as: 'address'});
      tbl_hotels.belongsToMany(models.tbl_contact_info, {foreignKey: 'hotel_id', otherKey: 'contact_info_id', as: 'phone', through: models.contact_info_hotel});
      tbl_hotels.belongsToMany(models.tbl_contact_info, {foreignKey: 'hotel_id', otherKey: 'contact_info_id', as: 'email', through: models.contact_info_hotel});
		}
	});
	return tbl_hotels;
};