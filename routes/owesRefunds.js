var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var compOnly = req.query.componly ? true : false;

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                {
                    model: db.tbl_event_cities, as: 'event_city', include: [
                        { model: db.tbl_states, as: 'state' }
                    ]
                },
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if (tourDate) {
                return {
                    city: tourDate.event_city.name,
                    state: tourDate.event_city.state.name,
                    venue: tourDate.venue.name,
                    start_date: tourDate.start_date,
                    end_date: tourDate.end_date,
                    studios: []
                };
            }
            else {
                throw new Error('No Tour Date with this id')
            }
        })
        .then(function(owesRefundsData) {
            db.date_studios_registrations.findAll({
                where: {
                    tour_date_id: tourDateId
                },
                include: [
                    { model: db.date_studios, as: 'date_studio', include: [
                        { model: db.tbl_studios, as: 'studio' }
                    ]},
                    { model: db.tbl_registrations, as: 'registration', include: [
                        { model: db.tbl_payments, as: 'payments' }
                    ]}
                ],
                order: [
                    [{model: db.tbl_registrations, as: 'registration'}, 'independent', 'ASC'],
                    [{model: db.date_studios, as: 'date_studio'}, {model: db.tbl_studios, as: 'studio'}, 'name', 'ASC']
                ]
            })
            .then(function(results) {
                _.forEach(results, function(dsr) {
                    var studio = {
                        independent: dsr.registration.independent,
                        studio_name: dsr.date_studio.studio.name,
                        studio_code: dsr.studio_code,
                        total_fees: dsr.registration.fees.total,
                        fees_paid: 0,
                        credit: dsr.registration.credit.amount,
                        balance_due: 0
                    };
                    _.forEach(dsr.registration.payments, function (p) {
                        studio.fees_paid += p.amount;
                    });
                    studio.balance_due = studio.total_fees - studio.fees_paid - studio.credit;
                    owesRefundsData.studios.push(studio);
                });
                res.render('reports/owes-refunds', owesRefundsData);
            })
        })
        .catch(function(err) {
            console.log('ERROR', err);
            res.send(err);            
        })
    }
}