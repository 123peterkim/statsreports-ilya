"use strict";

module.exports = function(sequelize, DataTypes) {
  var contact_info_hotel = sequelize.define('contact_info_hotel', {
    contact_info_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_contact_info',
        key: 'id'
      },
      primaryKey: true
    },
    hotel_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_hotels',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {
    }
  });
  return contact_info_hotel;
};