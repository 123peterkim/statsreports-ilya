"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_promo_codes = sequelize.define('tbl_store_promo_codes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		promo_codes_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes_types',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		descsription: {
			type: DataTypes.TEXT
		},
		value: {
			type: DataTypes.INTEGER
		},
		charges: {
			type: DataTypes.INTEGER
		},
		uses: {
			type: DataTypes.INTEGER
		},
		active: {
			type: DataTypes.INTEGER
		},
		expires: {
			type: DataTypes.DATEONLY
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_promo_codes;
};