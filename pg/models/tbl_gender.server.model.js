"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_gender = sequelize.define('tbl_gender', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		value: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_gender;
};