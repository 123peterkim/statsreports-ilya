"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_giftcards = sequelize.define('tbl_store_giftcards', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		code: {
			type: DataTypes.STRING
		},
		initial_balance: {
			type: DataTypes.REAL
		},
		balance: {
			type: DataTypes.REAL
		},
		created: {
			type: DataTypes.DATEONLY
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_giftcards;
};