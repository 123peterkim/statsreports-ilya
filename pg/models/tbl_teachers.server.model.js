"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_teachers = sequelize.define('tbl_teachers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    person_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_persons',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_teachers.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
      tbl_teachers.belongsToMany(models.tbl_studios, {through: 'studios_teachers', foreignKey: 'teacher_id', otherKey: 'studio_id', as: 'studios'});
      tbl_teachers.hasMany(models.studios_teachers, {foreignKey: 'teacher_id', as: 'has_many_studios_teachers'});
    }
  });
  return tbl_teachers;
};