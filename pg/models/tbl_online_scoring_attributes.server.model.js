"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_online_scoring_attributes = sequelize.define('tbl_online_scoring_attributes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
		}
	});
	return tbl_online_scoring_attributes;
};