"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_dts_fees = sequelize.define('tbl_dts_fees', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		value: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_dts_fees;
};