var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var start = req.query.start || 0; // placeholder start
        var end = req.query.end || 9999; // placeholder end

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                {
                    model: db.tbl_event_cities, as: 'event_city', include: [
                        { model: db.tbl_states, as: 'state' }
                    ]
                },
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if (tourDate) {
                return {
                    city: tourDate.event_city.name,
                    eventid: tourDate.event_id,
                    start: start,
                    end: end,
                    venue: tourDate.venue.name,
                    disp_date: util.tourDateDisplayDate(tourDate),
                    studios: []
                };
            }
            else {
                throw new Error('No Tour Date with this id')
            }
        })
        .then(function(studioPaidDancersData) {
            db.date_studios_registrations.findAll({
                where: {
                    tour_date_id: tourDateId
                },
                include: [
                    { model: db.date_studios, as: 'date_studio', include: [
                        { model: db.tbl_studios, as: 'studio', include: [
                            { model: db.tbl_mybtf_users, as: 'primary_mybtf_user', include: [
                                { model: db.tbl_persons, as: 'person' }
                            ]}
                        ]},
                    ]},
                    { model: db.tbl_registrations, as: 'registration', include: [
                        { model: db.tbl_payments, as: 'payments' },
                        { model: db.tbl_registrations_dancers, as: 'dancers', include: [
                            { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
                                { model: db.tbl_levels, as: 'level' }
                            ]}
                        ], where: { "fees.workshop": { $gt: 0 } } }
                    ]}
                ],
                order: [
                    [{model: db.tbl_registrations, as: 'registration'}, 'independent', 'ASC'],
                    [{model: db.date_studios, as: 'date_studio'}, {model: db.tbl_studios, as: 'studio'}, 'name', 'ASC']
                ]
            })
            .then(function(results) {
                _.forEach(results, function(dsr) {
                    var person = dsr.date_studio.studio.primary_mybtf_user.person;
                    var studio = {
                        studio_name: dsr.date_studio.studio.name,
                        studio_code: dsr.studio_code,
                        contact: person.fname + ' ' + person.lname,
                        independent: dsr.registration.independent ? 'Yes' : 'No',
                        sum_dancers: 0,
                        senior_count: 0,
                        teen_count: 0,
                        junior_count: 0,
                        mini_count: 0,
                        jumpstart_count: 0,
                        nubie_count: 0,
                        peewee_count: 0,
                        sidekick_count: 0,
                    };
                    _.forEach(dsr.registration.payments, function (p) {
                        studio.fees_paid += p.amount;
                    });
                    studio.balance_due = studio.total_fees - studio.fees_paid - studio.credit;

                    _.forEach(dsr.registration.dancers, function (regDancer) {
                        var level = regDancer.workshop_level.level.name;
                        studio.sum_dancers++;
                        if (level === Const.SENIOR) studio.senior_count++;
                        else if (level === Const.TEEN) studio.teen_count++;
                        else if (level === Const.JUNIOR) studio.junior_count++;
                        else if (level === Const.MINI) studio.mini_count++;
                        else if (level === Const.JUMPSTART) studio.jumpstart_count++;
                        else if (level === Const.NUBIE) studio.nubie_count++;
                        else if (level === Const.PEEWEE) studio.peewee_count++;
                        else if (level === Const.SIDEKICK) studio.sidekick_count++;
                    });

                    if(studio.sum_dancers >= start && studio.sum_dancers <= end) {
                        studioPaidDancersData.studios.push(studio);
                    }
                });

                res.render('reports/studio-paid-dancers', studioPaidDancersData);
            })
        })
        .catch(function(err) {
            console.log('ERROR', err);
            res.send(err);            
        })
    }
}