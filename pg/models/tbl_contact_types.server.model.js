"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_contact_types = sequelize.define('tbl_contact_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_contact_types;
};