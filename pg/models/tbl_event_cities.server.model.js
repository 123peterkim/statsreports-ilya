"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_event_cities = sequelize.define('tbl_event_cities', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		state_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_states',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_event_cities.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});
			tbl_event_cities.belongsTo(models.tbl_states, {foreignKey: 'state_id', as: 'state'});
		}
	});
	return tbl_event_cities;
};