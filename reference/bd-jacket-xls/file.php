<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$city = db_one("city","tbl_tour_dates","id='$tourdateid'");
	$safecity = strtolower(str_replace(array(" ",",","-"),"",$city));
	$tmp = array();
	$sql = "SELECT tbl_tda_bestdancer_data.profileid, tbl_date_dancers.age, tbl_date_dancers.workshoplevelid, tbl_profiles.studioid, tbl_profiles.fname, tbl_profiles.lname, tbl_tda_bestdancer_data.jacketname, tbl_tda_bestdancer_data.jacketsize, tbl_profiles.studioid FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.tourdateid='$tourdateid' AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["regwl"] = $row["workshoplevelid"] > 0 ? db_one("name","tbl_workshop_levels_$seasonid","id='".$row["workshoplevelid"]."'") : "-";
			$row["agewl"] = $row["age"] > 0 ? db_one("name","tbl_workshop_levels_$seasonid","id='".get_level_based_on_age($tourdateid,$row["age"])."'") : "-";
			$row["studioname"] = db_one("name","tbl_studios","id='".$row["studioid"]."'");
			$dancers[] = $row;
		}

		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("First Name","Last Name","Age","Reg Workshop Lvl","Real Age Workshop Lvl","Studio","Jacket Size","Jacket Name");
		foreach($dancers as $studio) {
			$dataArray[] = array(stripslashes($studio["fname"]),stripslashes($studio["lname"]),$studio["age"],$studio["regwl"],$studio["agewl"],stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["studioname"]))),stripslashes($studio["jacketsize"]),stripslashes($studio["jacketname"]));
		}
		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "TDA_Jackets_".substr($mk,6,5);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);
	}
?>