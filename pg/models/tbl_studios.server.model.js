"use strict";

module.exports = function (sequelize, DataTypes) {
  var tbl_studios = sequelize.define('tbl_studios', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    address_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_addresses',
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING
    },
    notes: {
      type: DataTypes.JSONB
    },
    verified: {
      type: DataTypes.BOOLEAN
    },
    primary_mybtf_user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_mybtf_users',
        key: 'id'
      }
    },
    code: {
      type: DataTypes.STRING
    },
    waiver_link: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false,
    associate: function (models) {
      tbl_studios.belongsTo(models.tbl_mybtf_users, {foreignKey: 'primary_mybtf_user_id', as: 'primary_mybtf_user'});
      tbl_studios.belongsTo(models.tbl_addresses, {foreignKey: 'address_id', as: 'address'});
      tbl_studios.belongsToMany(models.tbl_mybtf_users, {
        through: 'studios_users',
        foreignKey: 'studio_id',
        otherKey: 'mybtf_user_id',
        as: 'users'
      });
      tbl_studios.belongsToMany(models.tbl_dancers, {
        through: 'studios_dancers',
        foreignKey: 'studio_id',
        otherKey: 'dancer_id',
        as: 'dancers'
      });
      tbl_studios.belongsToMany(models.tbl_contact_info, {
        through: 'contact_info_studio',
        foreignKey: 'studio_id',
        otherKey: 'contact_info_id',
        as: 'phone'
      });
      tbl_studios.belongsToMany(models.tbl_contact_info, {
        through: 'contact_info_studio',
        foreignKey: 'studio_id',
        otherKey: 'contact_info_id',
        as: 'email'
      });
    }
  });
  return tbl_studios;
};