var _ = require('lodash');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');
var util = require('../reference/util/util');

module.exports = {
    route: function(req, res) {
        /* 
        SELECT 
            tbl_date_studios.id AS datestudioid, 
            tbl_date_studios.studioid, 
            tbl_date_studios.independent, 
            tbl_date_studios.studiocode, 
            tbl_studios.name 
        FROM 
            `tbl_date_studios` 
        LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid 
        WHERE tbl_date_studios.tourdateid=$tourdateid 
        AND tbl_date_studios.studiocode !='' 
        ORDER BY tbl_date_studios.independent ASC, tbl_date_studios.studiocode ASC */
        // query params
        var tourDateid = req.query.tourdateid;
        var DateStudios = db.date_studios;
        var TourDates = db.tbl_tour_dates;
        var critiqueSheetData = {};

        async.waterfall([
            function(cb) {
                TourDates.findOne({
                    where: {
                        id: tourDateid
                    },
                    include: [
                        { model: db.tbl_event_cities, as: 'event_city' },
                        { model: db.tbl_venues, as: 'venue'}
                    ]
                })
                .then(function(tourDate) {
                    cb(null, {event_id: tourDate.event_id, city_data: {city: tourDate.event_city.name, venue_name: tourDate.venue.name}});
                })
                .catch(function(err) {
                    cb(err);
                });
            },
            function(data, cb) {
                DateStudios.findAll({
                    where: {
                        tour_date_id: tourDateid
                    },
                    include: [
                        { model: db.tbl_studios, as: 'studio' },
                        { model: db.tbl_registrations, as: 'registrations', include: [
                            { model: db.tbl_registrations_routines, as: 'routines', include: [
                                { model: db.tbl_routines, as: 'routine' }
                            ] }
                        ]}
                    ]
                })
                .then(function(dateStudios) {
                    cb(null, data, dateStudios);
                })
                .catch(function(err) {
                    cb(err);
                });
            },
            function (data, dateStudios, cb) {
                var compGroupRoutines = {};
                var tasks = [];

                _.forEach(dateStudios, function (dateStudio) {
                    tasks.push(function (cbParallel) {
                        util.getDateCritiqueSheet(tourDateid, dateStudio.studio.id)
                            .then(function (result) {
                                cbParallel(null, {
                                    studioId: dateStudio.studio.id,
                                    routines: result
                                });
                            })
                            .catch(function (err) {
                                cbParallel(err);
                            })
                        });
                });
                
                async.parallel(tasks, function (err, result) {
                    if(err) cb(err);
                    else cb(null, data, dateStudios, result);
                });
            }
        ],
        function(err, results) {
            if(err) {
                console.log('ERRRO', err);
                res.send('ERROR')
            }
            else {
                var routines = results[2];
                _.assign(critiqueSheetData, results[0]);
                critiqueSheetData.studios = [];

                // missing function
                // get_date_critique_sheet($tourdateid,$row["studioid"]);

                _.forEach(results[1], function(dateStudio) {
                    var total = 0;

                    var studio = {
                        name: dateStudio.studio.name,
                        studio_code: dateStudio.studio.code,
                        studio_id: dateStudio.studio.id,
                        total: total,
                        routines: routines[dateStudio.studio.id]
                    }

                    critiqueSheetData.studios.push(studio);
                });

                res.render('reports/critique-sheet', critiqueSheetData);
            }
        });
    }
}