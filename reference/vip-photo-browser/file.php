<?php
error_reporting(E_ALL);
	include("../../../includes/util.php");
	require_once("../../../includes/s3/sdk.class.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$bucket = "mybtf_bestdancer_photos";

	if($tourdateid > 1) {
		$s3 = new AmazonS3();
		$photo_base = "$tourdateid/";
		$photofilelist = $s3->get_object_list($bucket,array(
			'prefix' => $photo_base
		));

		if(count($photofilelist) > 0) {

			foreach($photofilelist as $file) {
				$expl = explode("/",$file);
				$filename = $expl[sizeof($expl)-1];
				if(!(strpos($filename,"tn_") > -1) && ($filename != "missing_person.jpg") && strlen($filename) > 0) {
					$pids = explode(".",$filename);
					$profileid = intval($pids[0]);
					$fname = $profileid > 0 ? db_one("fname","tbl_profiles","id='$profileid'") : "" ;
					$lname = $profileid > 0 ? db_one("lname","tbl_profiles","id='$profileid'") : "" ;
					$add = array("filename"=>$filename,"url"=>"http://s3.amazonaws.com/$bucket/$tourdateid/$filename","profileid"=>$profileid,"fname"=>$fname,"lname"=>$lname);
					$photos[$lname][] = $add;
				}
			}
		}
		ksort($photos);
	}
?><!DOCTYPE html>
<html>
	<head>
		<title>Photo Browser</title>
	</head>
	<body>
		<?php
			if(count($photos) > 0) {
				$tmp = array();
				foreach($photos as $aname=>$ppl) {
					foreach($ppl as $person) {
						$tmps[] = $person;
					}
				}

				foreach($tmps as $tmp) { ?>
					<a style="display:block;" target="_blank" href="<?=$tmp["url"];?>"><?=$tmp["fname"]." ".$tmp["lname"];?></a>
		<?php	}

			}
		?>
	</body>
</html>