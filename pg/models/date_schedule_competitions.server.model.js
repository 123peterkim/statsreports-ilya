"use strict";

module.exports = function(sequelize, DataTypes) {
	var dateScheduleCompetitions = sequelize.define('date_schedule_competitions', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_tour_dates',
			  key: 'id'
			}
		},
		last_update: {
		  type: DataTypes.JSONB
		},
		dates: {
		  type: DataTypes.JSONB
		},
		awards: {
		  type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return dateScheduleCompetitions;
};