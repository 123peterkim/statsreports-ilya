<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");

	$dispdate = get_tourdate_dispdate($tourdateid);

	$sql = "SELECT tbl_studios.name AS studioname, tbl_date_studios.total_fees, tbl_date_studios.studioid, tbl_date_studios.id AS datestudioid, tbl_date_studios.fees_paid, tbl_studios.email, tbl_studios.phone, tbl_studios.phone2, tbl_studios.fax, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
//			print_r($row);
			$row["studioname"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"])));


			//check for waivers before saying they're missing
			$sql1 = "SELECT tbl_date_dancers.profileid FROM `tbl_date_dancers` WHERE studioid='".$row["studioid"]."' AND tourdateid='$tourdateid' AND (waiverid IS NULL OR waiverid < 1)";
			$res1 = mysql_query($sql1) or die(mysql_error());
			if(mysql_num_rows($res1) > 0) {
				while($row1 = mysql_fetch_assoc($res1)) {
					$waiverid = check_waiver("","","",$row1["profileid"],true);
					if($waiverid > 0) {
						$sql1a = "UPDATE `tbl_date_dancers` SET waiverid='$waiverid' WHERE profileid='".$row1["profileid"]."' AND tourdateid='$tourdateid' AND studioid='".$row["studioid"]."'";
						$res1a = mysql_query($sql1a) or die(mysql_error());
					}
				}
			}

			//check for missing waivers
			$sql2 = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.workshoplevelid > 1 AND tbl_date_dancers.workshoplevelid < 7 AND tbl_date_dancers.studioid=".$row["studioid"]." AND (tbl_date_dancers.waiverid IS NULL OR tbl_date_dancers.waiverid < 1) ORDER BY tbl_profiles.lname ASC";
			$res2 = mysql_query($sql2) or die(mysql_error());
			if(mysql_num_rows($res2) > 0) {
				while($row2 = mysql_fetch_assoc($res2)) {
					$row["waivers"][] = $row2;
				}
			}

			//check for outstanding balance due
			$studioid = $row["studioid"];
			$dancer_fee_sum = db_one("SUM(fee)","tbl_date_dancers","tourdateid=$tourdateid AND studioid=$studioid");
			$competition_fee_sum = db_one("SUM(fee)","tbl_date_routines","tourdateid=$tourdateid AND studioid=$studioid");
			$total_fees = number_format(doubleval(str_replace(",","",$competition_fee_sum)) + doubleval(str_replace(",","",$dancer_fee_sum)),2,'.','');
			$fees_paid = db_one("fees_paid","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$ftv = db_one("free_teacher_value","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$credit = db_one("credit","tbl_date_studios","studioid='$studioid' AND tourdateid='$tourdateid'");
			$balance = number_format((doubleval(str_replace(",","",$total_fees)) - doubleval(str_replace(",","",$ftv)) - doubleval(str_replace(",","",$fees_paid)) - doubleval(str_replace(",","",$credit))),2,'.','');
			if($balance < 0)
				$row["balance"] = "$$balance";

			//check for blank/tba routine names
			$sql3 = "SELECT tbl_routines.name AS routinename, tbl_routines.id AS routineid, tbl_date_routines.id AS dateroutineid, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid='$studioid' AND (tbl_routines.name LIKE 'TBA%' OR tbl_routines.name='')";
			$res3 = mysql_query($sql3) or die(mysql_error());
			if(mysql_num_rows($res3) > 0) {
				while($row3 = mysql_fetch_assoc($res3)) {
					if($row3["routinecategoryname"] == "Solo") {
						$pid = db_one("profileid","tbl_date_routine_dancers","routineid=".$row3["routineid"]." AND tourdateid=$tourdateid");
						$name = db_one("fname","tbl_profiles","id=$pid")." ".db_one("lname","tbl_profiles","id=$pid");
						$row3["dname"] = $name;
					}
					$row["tbas"][] = $row3;
				}
			}

			//check for missing routine categories
			$sql4 = "SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE !tbl_date_routines.perfcategoryid > 0 AND tbl_date_routines.tourdateid=$tourdateid AND (tbl_date_routines.finals=1 OR tbl_date_routines.prelims=1) AND tbl_date_routines.studioid=".$row["studioid"];
			$res4 = mysql_query($sql4) or die(mysql_error());
			if(mysql_num_rows($res4) > 0) {
				while($row4 = mysql_fetch_assoc($res4)) {
					$row["noperfcat"][] = $row4;
				}
			}

			//check for missing birth dates
			$sql5 = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid=".$row["studioid"]." AND tbl_profiles.birth_date='' AND (tbl_date_dancers.workshoplevelid > 1 AND tbl_date_dancers.workshoplevelid < 7)";
			$res5 = mysql_query($sql5) or die(mysql_error());
			if(mysql_num_rows($res5) > 0) {
				while($row5 = mysql_fetch_assoc($res5)) {
					$row["nobirthday"][] = $row5;
				}
			}

			//check for missing routine age divisions
			$sql12 = "SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE !tbl_date_routines.agedivisionid > 0 AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$row["studioid"];
			$res12 = mysql_query($sql12) or die(mysql_error());
			if(mysql_num_rows($res12) > 0) {
				while($row12 = mysql_fetch_assoc($res12)) {
					$row["noagediv"][] = $row12;
					++$oicount;
				}
			}

			//check for missing best dancer photos
			$sql13 = "SELECT tbl_tda_bestdancer_data.id AS bdid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.tourdateid='$tourdateid' AND tbl_tda_bestdancer_data.hasphoto = 0 AND tbl_profiles.studioid='".$row["studioid"]."' ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
			$res13 = mysql_query($sql13) or die(mysql_error());
			if(mysql_num_rows($res13) > 0) {
				while($row13 = mysql_fetch_assoc($res13)) {
					$row["nobdphoto"][] = $row13;
					++$oicount;
				}
			}


			//check for missing bd jacket name
			$sql14 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_tda_bestdancer_data.profileid, tbl_date_dancers.studioid FROM `tbl_profiles` LEFT JOIN tbl_tda_bestdancer_data ON tbl_tda_bestdancer_data.profileid = tbl_profiles.id LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_profiles.id WHERE (tbl_tda_bestdancer_data.jacketname = '' OR tbl_tda_bestdancer_data.jacketname = 'Name on Jacket') AND tbl_date_dancers.studioid = ".$row["studioid"]." AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC";
			$res14 = mysql_query($sql14) or die(mysql_error());
			if(mysql_num_rows($res14) > 0) {
				while($row14 = mysql_fetch_assoc($res14)) {
					$row["nobdjacketname"][] = $row14;
					++$oicount;
				}
			}

			//check for missing bd jacket size
			$sql15 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_tda_bestdancer_data.profileid, tbl_date_dancers.studioid FROM `tbl_profiles` LEFT JOIN tbl_tda_bestdancer_data ON tbl_tda_bestdancer_data.profileid = tbl_profiles.id LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_profiles.id WHERE tbl_tda_bestdancer_data.jacketsize = '' AND tbl_date_dancers.studioid = ".$row["studioid"]." AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC";
			$res15 = mysql_query($sql15) or die(mysql_error());
			if(mysql_num_rows($res15) > 0) {
				while($row15 = mysql_fetch_assoc($res15)) {
					$row["nobdjacketsize"][] = $row15;
					++$oicount;
				}
			}

			//if either exist for row, add to studios array
			if(count($row["nobdjacketname"]) > 0 || count($row["nobdjacketsize"]) > 0 || count($row["nobdphoto"]) > 0 || count($row["noagediv"]) > 0 || count($row["waivers"]) > 0 || strlen($row["balance"]) > 0 || count($row["tbas"]) > 0 || count($row["noperfcat"]) > 0) {
				$row["contacts"] = json_decode($row["contacts"],true);
				$studios[] = $row;
			}

		}
	}

	//print_r($studios);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}
		</style>
		<script type="text/javascript">
			//window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta charset="utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Unplug;font-size:20pt;margin: 5px 0 0 0;"><?php print($city); ?> Studio Outstand. Items</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:5px;"><?php print($venue." / ".$dispdate); ?></div>
		<?php
			if(count($studios) > 0) {
				foreach($studios as $studio) { ?>
					<div style="margin:10px 0;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;">
					<?php print($studio["studioname"]); ?>
					</div>
					<div style="font-style:italic;font-family:Verdana,Arial,sans-serif;font-size:12px;padding-bottom:25px;border-bottom:1px dotted #000000;">
						The following items need to be fixed to complete this studio's registration:
						<br/>
						<br/>
					<?php if(count($studio["contacts"]) > 0) {
							for($i=0;$i<count($studio["contacts"]);$i++) {
								if(strlen($studio["contacts"][$i]["fname"]) > 0) {
									print("Contact ".($i+1).": ".$studio["contacts"][$i]["fname"]." ".$studio["contacts"][$i]["lname"]);
									if(strlen($studio["contacts"][$i]["type"]) > 0)
										print(" (".$studio["contacts"][$i]["type"].")");
									print("<br/>");
								}
							}
						  } ?>
						<br/>
						Phone 1: <?php print(strlen($studio["phone"]) > 0 ? $studio["phone"] : "-"); ?>
						<br/>
						Phone 2: <?php print(strlen($studio["phone2"]) > 0 ? $studio["phone2"] : "-"); ?>
						<br/>
						Fax: <?php print(strlen($studio["fax"]) > 0 ? $studio["fax"] : "-"); ?>
						<br/>
						Email: <?php print(strlen($studio["email"]) > 0 ? $studio["email"] : "-"); ?>
						<br/>
						<div style="font-style:normal;line-height:18px;padding-left:10px;margin-top:5px;">
					<?php
						$pos = 1;
						  if(count($studio["waivers"]) > 0) {
							for($i=0;$i<count($studio["waivers"]);$i++) {
								print($pos.".&nbsp;&nbsp;&nbsp;Missing participant waiver for ".strtoupper($studio["waivers"][$i]["fname"]." ".$studio["waivers"][$i]["lname"])."<br/>");
								++$pos;
							}
						  }
						  if(count($studio["tbas"]) > 0) {
						  	for($i=0;$i<count($studio["tbas"]);$i++) {
						  		if(strlen($studio["tbas"][$i]["dname"]) > 0)
							  		print($pos.".&nbsp;&nbsp;&nbsp;TBA ".$studio["tbas"][$i]["agedivisionname"]." ".$studio["tbas"][$i]["routinecategoryname"]." (".$studio["tbas"][$i]["dname"].") routine needs a Title.<br/>");
						  		else
							  		print($pos.".&nbsp;&nbsp;&nbsp;TBA ".$studio["tbas"][$i]["agedivisionname"]." ".$studio["tbas"][$i]["routinecategoryname"]." routine needs a Title.<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["noperfcat"]) > 0) {
						  	for($i=0;$i<count($studio["noperfcat"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Routine '".$studio["noperfcat"][$i]["routinename"]."' has no assigned PERFORMANCE CATEGORY (e.g. Jazz, Lyrical, Tap)<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["noagediv"]) > 0) {
						  	for($i=0;$i<count($studio["nopagediv"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Routine '".$studio["noagediv"][$i]["routinename"]."' has no assigned AGE DIVISION<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["nobirthday"]) > 0) {
						  	for($i=0;$i<count($studio["nobirthday"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;".$studio["nobirthday"][$i]["fname"]." ".$studio["nobirthday"][$i]["lname"]." has no birth date set.<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["nobdphoto"]) > 0) {
						  	for($i=0;$i<count($studio["nobdphoto"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Best Dancer ".strtoupper($studio["nobdphoto"][$i]["fname"]." ".$studio["nobdphoto"][$i]["lname"])." has not submitted a photo<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["nobdjacketname"]) > 0) {
						  	for($i=0;$i<count($studio["nobdjacketname"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Best Dancer ".strtoupper($studio["nobdjacketname"][$i]["fname"]." ".$studio["nobdjacketname"][$i]["lname"])." has not selected a JACKET NAME<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["nobdjacketsize"]) > 0) {
						  	for($i=0;$i<count($studio["nobdjacketsize"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Best Dancer ".strtoupper($studio["nobdjacketsize"][$i]["fname"]." ".$studio["nobdjacketsize"][$i]["lname"])." has not selected a JACKET SIZE<br/>");
						  		++$pos;
						  	}
						  }
						  if(strlen($studio["balance"]) > 0)
							print(($pos.".&nbsp;&nbsp;&nbsp;".strtoupper($studio["studioname"])." has a BALANCE DUE of ".$studio["balance"])."<br/>");
						 ?>
						  </div>
					</div>
		<?php	}
			} ?>
		</div>
	</body>