"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_playlists = sequelize.define('date_playlists', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
		  type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		song_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_songs',
			  key: 'id'
			}
		},
		level_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_levels',
			  key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_playlists.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			date_playlists.belongsTo(models.tbl_songs, {foreignKey: 'song_id', as: 'song'});
			date_playlists.belongsTo(models.tbl_levels, {foreignKey: 'level_id', as: 'level'});
			date_playlists.belongsToMany(models.tbl_staff, {as: 'staff', through: models.staff_playlists, foreignKey: 'date_playlist_id', otherKey: 'staff_id'});
			date_playlists.hasMany(models.staff_playlists, {foreignKey: 'date_playlist_id', as: 'has_many_staff_playlists'})
		}
	});
	return date_playlists;
};