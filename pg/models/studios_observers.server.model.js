"use strict";

module.exports = function(sequelize, DataTypes) {
  var studios_observers = sequelize.define('studios_observers', {
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      },
      primaryKey: true
    },
    observer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_observers',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {
      studios_observers.belongsTo(models.tbl_observers, {foreignKey: 'observer_id', as: 'observer'});
      studios_observers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
    }
  });
  return studios_observers;
};