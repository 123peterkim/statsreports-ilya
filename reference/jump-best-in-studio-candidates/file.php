<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	//FINALS only!
	$compgroup = "finals";
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));

	$data = Array();

			//for each studio with routines (unique/distinct studioid // date_routines), count total routines, if > 15, count routine types, if >7 are line+, they are eligible
			$estudios = Array();
			$finalstudios = Array();
			$sql9 = "SELECT DISTINCT(studioid) FROM `tbl_date_routines` WHERE tourdateid=$tourdateid";
			$res9 = mysql_query($sql9) or die(mysql_error());
			if(mysql_num_rows($res9) > 0) {
				while($row = mysql_fetch_assoc($res9))
					$estudios[] = $row;
			}
			if(count($estudios) > 0) {
				foreach($estudios as $es) {
					$sql8 = "SELECT COUNT(id) AS acount FROM `tbl_date_routines` WHERE studioid=".$es["studioid"]." AND tourdateid=$tourdateid";
					$res8 = mysql_query($sql8) or die(mysql_error());
					while($row = mysql_fetch_assoc($res8)) {
						if(intval($row["acount"]) >= 15) {
							$sql7 = "SELECT id FROM `tbl_date_routines` WHERE studioid=".$es["studioid"]." AND tourdateid=$tourdateid AND routinecategoryid > 2";
							$res7 = mysql_query($sql7) or die(mysql_error());
							if(mysql_num_rows($res7) >= 7)
								$bisstudios[] = $es;
						}
					}
				}
			}

			if(count($bisstudios) > 0) {
				foreach($bisstudios as $fs) {
					$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.perfcategoryid, tbl_performance_divisions.name AS perfdivisionname, tbl_date_routines.finals_total_score AS total_score, tbl_date_routines.number_finals AS number, tbl_date_routines.finals_has_a AS has_a, tbl_studios.name as studioname, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_competition_awards.name AS awardname FROM `tbl_date_routines` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_routines.studioid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id=tbl_date_routines.perfcategoryid LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id = tbl_date_routines.finals_awardid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE tbl_date_routines.studioid=".$fs["studioid"]." AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.routinecategoryid > 2 ORDER BY tbl_date_routines.finals_total_score DESC";
						$res = mysql_query($sql) or die(mysql_error());
						if(mysql_num_rows($res) > 0) {
							while($row = mysql_fetch_assoc($res)) {
								$num = $row["number"];
								if($row["has_a"] == "1")
									$num .= ".a";
								$row["number"] = $num;
								$row["total_score"] = (strlen($row["total_score"]) > 0 ? $row["total_score"] : "0");
								$row["awardname"] = (strlen($row["awardname"]) > 0 ? $row["awardname"] : "N/A");
								$row["studioname"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"])));

								//highlight if already BoJ.  special award id 1,2,3,4 [best of jump]
								$bojcheck = db_one("id","tbl_date_special_awards","tourdateid='$tourdateid' AND dateroutineid='".$row["dateroutineid"]."' AND (awardid=1 OR awardid=2 OR awardid=3 OR awardid=4)");
								if($bojcheck > 0)
									$row["isboj"] = true;

								$routines["eligible"][] = $row;
							}
						}
				}
			}
$data = $routines["eligible"];


?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 7.9pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
			.isboj {
				color: #FF0000 !important;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family: 'Script of Sheep';font-size:17pt;margin: 5px 0 0 0;">JUMP <?php print($city); ?> Best In Studio Candidates</div>
						<div style="font-family: 'Script of Sheep';font-size:12pt;"><?php print(ucfirst($compgroup)." Competition"); ?></div>
					</td>
					<td style="vertical-align:bottom;text-align:right;padding-top:25px;"><div style="font-family: 'Script of Sheep';font-size:10pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>

							<table cellpadding="0" cellspacing="0" class="rtable" style="margin:0px 0 20px;page-break-after:always;">
								<tr>
									<td class="thead" style="width:45px;">#</td>
									<td class="thead" style="width:225px;">Routine</td>
									<td class="thead" style="width:225px;">Studio</td>
									<td class="thead" style="width:220px;">Category</td>
									<td class="thead" style="width:120px;border-right:1px solid #000000;">Award</td>
								</tr>
			<?php
				if(count($data) > 0) {
					foreach($data as $dkey=>$routine) { ?>
								<tr>
									<td class="tbody <?php if($routine["isboj"]) print("isboj"); ?>" style="text-align:left;"><?php print($routine["number"]); ?></td>
									<td class="tbody <?php if($routine["isboj"]) print("isboj"); ?>"><?php print($routine["routinename"]); ?></td>
									<td class="tbody <?php if($routine["isboj"]) print("isboj"); ?>"><?php print($routine["studioname"]); ?></td>
									<td class="tbody <?php if($routine["isboj"]) print("isboj"); ?>"><?php print($routine["agedivisionname"]." ".$routine["perfdivisionname"]." ".$routine["routinecategoryname"]); ?></td>
									<td class="tbody <?php if($routine["isboj"]) print("isboj"); ?>" style="border-right:1px solid #000000;"><?php print($routine["awardname"]." (".$routine["total_score"].")"); ?></td>
								</tr>

				<?php 	}
				} ?>
			</table>
		</div>
	</body>
</html>