<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$cr = db_one("city","tbl_tour_dates","id=$tourdateid");
	$cityname = strtolower(str_replace(array(" ","-",","),"",$cr));
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);

	$dancers = array();

	if($tourdateid > 0) {
		$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
		if($eventid > 0) {
			$wlid = 0;
			$astr = "";
			if($eventid == 7) {
				$wlid = 6;
				$astr = "jumpstarts";
				$astr2 = "JUMPstart";
			}
			if($eventid == 8) {
				$wlid = 7;
				$astr = "nubies";
				$astr2 = "Nubie";
			}
			if($eventid == 18) {
				$wlid = 10;
				$astr = "sidekicks";
				$astr2 = "Sidekick";
			}
			if($eventid == 14) {
				$wlid = 9;
				$astr = "peewees";
				$astr2 = "PeeWee";
			}
			if($eventid == 28) {
				$wlid = 11;
				$astr = "rookies";
				$astr2 = "Rookie";
			}

			if($wlid > 0) {
				$sql = "SELECT tbl_date_dancers.id AS datedancerid,tbl_profiles.fname, tbl_profiles.lname, tbl_date_dancers.age, tbl_date_dancers.one_day, tbl_studios.name AS studioname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_dancers.studioid WHERE tbl_date_dancers.workshoplevelid=$wlid AND tourdateid='$tourdateid' ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
				$res = mysql_query($sql) or die(mysql_error());
				if(mysql_num_rows($res) > 0) {
					while($row = mysql_fetch_assoc($res)) {
						$row["studioname"] = stripslashes(str_replace("&amp;","&",$row["studioname"]));
						$dancers[] = $row;
					}

				}
			}
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php print($cr." ".$astr2." List"); ?></title>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Unplug;font-size:20px;"><?php print("$cr $astr2 List"); ?></div>
			<span style="font-family:Unplug;font-size:14px;"><?php print($venue); ?> / <?php print($dispdate); ?></span>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
				<tr>
					<td class="thead" style="width:165px;">Dancer</td>
					<td class="thead" style="width:300px;">Studio</td>
					<td class="thead" style="width:100px;">Day #</td>
					<td class="thead" style="border-right: 1px solid #000000;width:70px;">Age</td>
				</tr>
		<?php	if(count($dancers) > 0) {
					foreach($dancers as $dancer) {	?>
				<tr>
					<td class="tbody"><?php print(stripslashes($dancer["fname"]." ".$dancer["lname"]));?></td>
					<td class="tbody"><?php print($dancer["studioname"]); ?></td>
					<td class="tbody" style="text-align:center;"><?php print($dancer["one_day"] == 1 ? "1" : "2"); ?></td>
					<td class="tbody" style="text-align:center;border-right: 1px solid #000000;"><?php print($dancer["age"]); ?></td>
				</tr>
			<?php   }
				} ?>
			</table>
		</div>
	</body>
</html>