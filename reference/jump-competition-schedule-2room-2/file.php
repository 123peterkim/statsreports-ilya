<?php
	include("../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);

	if(!$tourdateid > 0) exit();

	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");


	$comp_group = mysql_real_escape_string(strtolower($_GET["comp_group"]));
	if($comp_group == "finals")
		$disp_comp_group = "Finals";
	if($comp_group == "vips")
		$disp_comp_group = "Best Dancer";
	if($comp_group == "prelims")
		$disp_comp_group = "Prelims";
	$citydata = db_get("city,venue_name","tbl_tour_dates","id='$tourdateid'");
	$sql = "SELECT dates,awards FROM `tbl_date_schedule_competition` WHERE tourdateid='$tourdateid' LIMIT 1";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			//get all routines in order
			$routines = get_competition_schedule_in_order($tourdateid,$comp_group);
			$dates_raw = json_decode($row["dates"],true);
			$dates = $dates_raw[$comp_group];
			$awards_raw = json_decode($row["awards"],true);
			$awards = $awards_raw[$comp_group];

			if(count($awards) > 0) {
				foreach($awards as $akey=>$award) {
					$rfmk = db_one("finals_time","tbl_date_routines","id=".$award["dateroutineid"]);
					$rfdu = db_one("duration","tbl_date_routines","id=".$award["dateroutineid"]);
					list($hh,$mm,$ss,$mo,$dd,$yy) = explode(":",date('G:i:s:m:d:Y',$rfmk));
					$atmk = mktime($hh,($mm+$rfdu),$ss,$mo,$dd,$yy);
					$awards[$akey]["timemk"] = $atmk;
					$awards[$akey]["time"] = strtoupper(date('g:i a',$atmk));
				}
			}

		}
		if(count($routines) > 0) {
			foreach($routines as $routine) {
				$aa[$routine["time"]]["room".$routine["room"]] = $routine;
			}
			//$routines = array_values($aa);
			$routines = $aa;
			ksort($routines);
			foreach($routines as $rkey=>$routine) {
				if(!isset($routine["room1"]))
					$routines[$rkey]["room1"] = array();
			}
		}

		//print_r($routines);exit();
		if(count($dates) > 0) {
			//clean up dates array
			$newdates = Array();
			foreach($dates as $key=>$value) {
				$expl = explode(" ",$key);
				$datestr = str_replace(array("(",")"),"",$expl[1]);
				list($mm,$dd,$yy) = explode("/",$datestr);
				$datemk = mktime(0,0,0,$mm,$dd,$yy);

				$startexpl = explode(":",$value["start_time"]);
				$hours = $startexpl[0];
				$mins = substr($startexpl[1],0,2);
				$ampm = strtolower(substr($startexpl[1],2,4));
				if($ampm == "pm" && intval($hours) != "12")
					$hours = intval($hours)+12;
				$start_time_mk = mktime($hours,$mins,0,$mm,$dd,$yy);
				$newdates[$datemk] = array("start_time"=>$start_time_mk,"end_routine"=>$value["end_routine"],"dateroutineid"=>$value["dateroutineid"]);
				asort($newdates);
				$dates = $newdates;
				$title_date = "";
 				foreach($dates as $date=>$datedata) {
					$title_date = date('n/d/Y',$date);
					break;
				}
			}
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
			//window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Competition Schedule</title>
		<style type="text/css">
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			#sched_table {
/*  				width: 100%; */
				margin-top:8px;
			}

			#sched_table tr td {
				border-left: 1px solid #000000;
				border-top: 1px solid #000000;
				font-size: 8pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
			}

			.tline {
				padding: 1px 0;
				text-align: left;
			}
		</style>
	</head>
	<body>
<?php
		$newpage = true;
		$maxrows = 24;
		$rowcount = 7;	?>
		<div style="width: 952px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="vertical-align:top;">
						<img src="../logos/email/<?php print($eventid); ?>.jpg" style="width:210px;display:block;margin-bottom:10px;" alt="" />
						<div style="font-family: 'Script of Sheep';font-size:20pt;"><?php print($citydata[0]["city"]); ?> <?php print($disp_comp_group); ?> Competition</div>
						<div style="font-size:16px;font-family: 'Script of Sheep';"><?php print($citydata[0]["venue_name"]); ?></div>
					</td>
					<td style="vertical-align:bottom;text-align:right;font-size:14px;font-family: 'Script of Sheep';">
						<?php print($title_date); ?>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" id="sched_table">
				<?php
					foreach($routines as $routine) {
						if($newpage) {
				?>
						<tr><td colspan="4" style="padding-left:15px;font-size:12px;border:none;">ROOM 1</td><td colspan="4" style="padding-left:15px;font-size:12px;border:none;">ROOM 2</td></tr>
						<tr>
							<td class="thead" style="width:40px;">#</td>
							<td class="thead" style="width:100px;">Time</td>
							<td class="thead" style="width:215px;">Routine/Studio</td>
							<td class="thead" style="width:330px;">Cat./Div.</td>
							<td class="thead" style="width:40px;">#</td>
							<td class="thead" style="width:100px;">Time</td>
							<td class="thead" style="width:215px;">Routine/Studio</td>
							<td class="thead" style="width:330px;border-right:1px solid #000000;">Cat./Div.</td>
						</tr>
					<?php
							$newpage = false;
							++$rowcount;
						}	?>
						<tr<?php if($rowcount == $maxrows-1) print(' style="page-break-after:always;"'); ?>>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>text-align:center;">
								<?php print($routine["room1"]["dispnumber"]); ?>
							</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>text-align: center;">
								<?php if(strlen($routine["room1"]["time"]) > 0) print(date('g:i A',$routine["room1"]["time"])); ?>
							</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>padding-left:2px;">
								<div style="width: 253px; overflow: hidden; white-space: nowrap;"><?php if($routine["room1"]["routinecategoryname"] == "Solo") print($routine["room1"]["solodancer"]." (".$routine["room1"]["routinename"].")"); else print(stripslashes($routine["room1"]["routinename"])); ?></div>
								<div style="width: 243px; overflow: hidden; white-space: nowrap;"><?php print(stripslashes($routine["room1"]["studioname"])); if(strlen($routine["room1"]["studiocode"]) > 0) { print(" (".$routine["room1"]["studiocode"].")"); } ?></div>
								</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>padding-left:2px;">
								<?php print($routine["room1"]["agedivisionnamerange"]); ?><br/><?php print($routine["room1"]["routinecategoryname"]); ?> <?php print($routine["room1"]["perfdivisionname"]); ?>
							</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>text-align:center;">
								<?php print($routine["room2"]["dispnumber"]); ?>
							</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>text-align: center;">
								<?php if(strlen($routine["room2"]["time"]) > 0) print(date('g:i A',$routine["room2"]["time"])); ?>
							</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>padding-left:2px;">
								<div style="width: 253px; overflow: hidden; white-space: nowrap;"><?php if($routine["room2"]["routinecategoryname"] == "Solo") print($routine["room2"]["solodancer"]." (".$routine["room2"]["routinename"].")"); else print(stripslashes($routine["room2"]["routinename"])); ?></div>
								<div style="width: 243px; overflow: hidden; white-space: nowrap;"><?php print(stripslashes($routine["room2"]["studioname"])); if(strlen($routine["room2"]["studiocode"]) > 0) { print(" (".$routine["room2"]["studiocode"].")"); } ?></div>
								</td>
							<td class="tline" style="<?php if($rowcount == $maxrows-1) { ?>border-bottom:1px solid #000000;<?php } ?>padding-left:2px;border-right:1px solid #000000;">
								<?php print($routine["room2"]["agedivisionnamerange"]); ?><br/><?php print($routine["room2"]["routinecategoryname"]); ?> <?php print($routine["room2"]["perfdivisionname"]); ?>
							</td>
						</tr>


<?php
		list($mm,$dd,$yy) = explode("/",date('m/d/Y',$routine["room1"]["time"]));
		list($mmr2,$ddr2,$yyr2) = explode("/",date('m/d/Y',$routine["room2"]["time"]));
		$date = date("l (n/d/Y)",mktime(0,0,0,$mm,$dd,$yy));
		$date2 = date("l (n/d/Y)",mktime(0,0,0,$mmr2,$ddr2,$yyr2));
		$hasrow = false;
		$alreadysplat = false;
		$alreadysplat2 = false;
		foreach($awards as $award) {


			if(($routine["room1"]["dateroutineid"] == $award["dateroutineid"]) && ($date == $award["date"]) || ($routine["room2"]["dateroutineid"] == $award["dateroutineid"]) && ($date2 == $award["date"])) {
				if(!$hasrow) {
					print("<tr/>");
					$hasrow = true;
				}
			  if(($routine["room1"]["dateroutineid"] == $award["dateroutineid"]) && ($date == $award["date"])) { ?>
					<td colspan="4" class="tline" style="padding: 3px 0 3px 2px;<?php if($i == count($routines)-1) print("border-bottom:1px solid #000000;"); ?>font-weight:bold;border-right:none;">
						<?php print($award["time"]." - ".stripslashes($award["desc"])); ?>
					</td>
		<?php 	$alreadysplat = true;
			  } else {
					if(!$alreadysplat && (($routine["room2"]["dateroutineid"] == $award["dateroutineid"]) && ($date2 == $award["date"]))) { ?>
					<td colspan="4" class="tline">&nbsp;</td>
		<?php   		$alreadysplat = true;
					}
				} ?>
		<?php if(($routine["room2"]["dateroutineid"] == $award["dateroutineid"]) && ($date2 == $award["date"])) { ?>
					<td colspan="4" class="tline" style="padding: 3px 0 3px 2px;<?php if($i == count($routines)-1) print("border-bottom:1px solid #000000;"); ?>font-weight:bold;border-right:1px solid #000000;">
						<?php print($award["time"]." - ".stripslashes($award["desc"])); ?>
					</td>
		<?php 	$alreadysplat = true;
			  } else {
					if(!$alreadysplat && (($routine["room1"]["dateroutineid"] == $award["dateroutineid"]) && ($date == $award["date"]))) { ?>
					<td colspan="4" class="tline" style="border-right:1px solid #000000;">&nbsp;</td>
		<?php   		$alreadysplat = true;
					}
				}
			}
		}  //foreach award
		if($hasrow) print("</tr>");

	} //each routine ?>
			</table>
		</div>
	</body>
</html>