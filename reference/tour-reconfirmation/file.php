<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$ageasofyear = db_one("ageasofyear","events","id='$eventid'");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$stateid = db_one("stateid","tbl_tour_dates","id=$tourdateid");
	$state = db_one("abbreviation","tbl_states","id=$stateid");
	$studioid = intval($_GET["studioid"]);
	$datestudioid = intval(db_one("id","tbl_date_studios","tourdateid=$tourdateid AND studioid=$studioid"));

	$dispdate = get_tourdate_dispdate($tourdateid);

	//get all datestudioids <> studioid
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.invoice_note, tbl_date_studios.studioid AS studioid, tbl_studios.name AS studioname, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_states.abbreviation, tbl_studios.zip, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid LEFT JOIN tbl_states ON tbl_states.name = tbl_studios.state WHERE tbl_date_studios.tourdateid=$tourdateid AND tbl_date_studios.id=$datestudioid";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$studioid = $row["studioid"];
			$fullrates = db_one("full_rates","tbl_date_studios","tourdateid=$tourdateid AND studioid=$studioid");
			$isfinale = db_one("is_finals","tbl_tour_dates","id=$tourdateid");
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"];
			$row["contact_full"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$isind = db_one("independent","tbl_date_studios","studioid=$studioid AND tourdateid=$tourdateid");

			//workshop registrants
			$row["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
			$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
			$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
			$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
			$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
			$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
			$row["jumpstart_count_oneday"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid AND one_day=1");
			$row["jumpstart_count_twoday"] = $row["jumpstart_count"] - $row["jumpstart_count_oneday"];

			$row["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
			$row["observer2_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=12 AND tourdateid=$tourdateid");

			$row["dancer_count"] = $row["senior_count"] + $row["teen_count"] + $row["junior_count"] + $row["mini_count"] + $row["jumpstart_count"];

			$row["obs"] = get_studio_observers_real_order($tourdateid,$studioid);

			//goddamn piece of shit PAID registrants because now they want PAID ppl to count for free teachers only.
			$row["pd_senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid AND fee > 0");
			$row["pd_teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid AND fee > 0");
			$row["pd_junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid AND fee > 0");
			$row["pd_mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid AND fee > 0");
			$row["pd_jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid AND fee > 0");

			$row["pd_dancer_count"] = $row["pd_senior_count"] + $row["pd_teen_count"] + $row["pd_junior_count"] + $row["pd_mini_count"] + $row["pd_jumpstart_count"];

			$ftcftv = calc_ftv($studioid,$tourdateid);
			$row["free_teacher_count"] = $ftcftv["ftc"];
			$row["free_teacher_value"] = $ftcftv["ftv"];

		//workshop registrants
			$ao = implode("', '",array(1,2,3,4,5,6,8,12));
			$sql2 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.birth_date, tbl_date_dancers.age, tbl_date_dancers.workshoplevelid, tbl_workshop_levels_$seasonid.name AS workshoplevelname, tbl_date_dancers.fee, tbl_date_dancers.has_scholarship , tbl_date_dancers.one_day FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid LEFT JOIN tbl_workshop_levels_$seasonid ON tbl_workshop_levels_$seasonid.id = tbl_date_dancers.workshoplevelid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid = $studioid AND tbl_date_dancers.workshoplevelid IN ('$ao') ORDER BY FIELD (tbl_date_dancers.workshoplevelid,'$ao'), tbl_profiles.lname ASC, tbl_profiles.fname ASC";
			$res2 = mysql_query($sql2) or die(mysql_error());
			$workshopregistrants = Array();
			if(mysql_num_rows($res2) > 0) {
				while($row2 = mysql_fetch_assoc($res2)) {
					$row2["birth_date"] = (strlen($row2["birth_date"]) > 0 ? $row2["birth_date"] : "-");
					$workshopregistrants[] = $row2;
				}
			}
			$row["workshopregistrants"] = $workshopregistrants;

		//FEES N SHIT
			$row["workshop_fees"] = number_format((db_one("SUM(fee)","tbl_date_dancers","studioid=$studioid AND tourdateid=$tourdateid")),2,'.','');
			$row["workshop_fees"] -= $row["free_teacher_value"];

			$row["competition_fees"] = number_format(db_one("SUM(fee)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid"),2,'.','');

			$row["total_fees"] = number_format(doubleval(str_replace(",","",$row["competition_fees"])) + doubleval($row["workshop_fees"]),2,'.','');
			$row["fees_paid"] = number_format(doubleval(db_one("fees_paid","tbl_date_studios","studioid=$studioid AND tourdateid=$tourdateid")),2,'.','');
			$row["credit"] = number_format(doubleval(db_one("credit","tbl_date_studios","studioid=$studioid AND tourdateid=$tourdateid")),2,'.','');
			$row["balance_due"] = number_format($row["total_fees"]-$row["fees_paid"]-$row["credit"],2,'.','');

			if($row["balance_due"] < 0)
				$row["balance_due"] = "(".number_format((-1 * $row["balance_due"]),2).")";
			else
				$row["balance_due"] = number_format($row["balance_due"],2);

			//COMPETITION ROUTINES
			$row["competition_routine_count"] = db_one("COUNT(id)","tbl_date_routines","tourdateid=$tourdateid AND studioid=$studioid");
			$sql3 = "SELECT tbl_date_routines.routineid AS routineid, tbl_date_routines.extra_time, tbl_routines.name AS routinename, tbl_age_divisions.name AS agedivisionname, tbl_age_divisions.range, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_performance_divisions.name AS performancedivisionname, tbl_date_routines.fee FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=$studioid ORDER BY tbl_routines.name ASC";
			$res3 = mysql_query($sql3) or die(mysql_error());
			$competitionroutines = Array();
			while($row3 = mysql_fetch_assoc($res3)) {
				$routineid = $row3["routineid"];
				$sql4 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_date_routine_dancers.id AS dateroutinedancerid, tbl_date_routine_dancers.profileid FROM `tbl_date_routine_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_routine_dancers.profileid WHERE tbl_date_routine_dancers.tourdateid=$tourdateid AND tbl_date_routine_dancers.routineid=$routineid ORDER BY tbl_profiles.lname ASC";
				$res4 = mysql_query($sql4) or die(mysql_error());
				$dancersbleh = Array();
				while($row4 = mysql_fetch_assoc($res4)) {
					$dancersbleh[] = $row4;
				}
				$row3["routinedancers"] = $dancersbleh;
				$competitionroutines[] = $row3;
			}
			$row["competitionroutines"] = $competitionroutines;


			//check for waivers before saying they're missing
			$sql1 = "SELECT tbl_date_dancers.profileid FROM `tbl_date_dancers` WHERE studioid='$studioid' AND tourdateid='$tourdateid' AND (waiverid IS NULL OR waiverid < 1)";
			$res1 = mysql_query($sql1) or die(mysql_error());
			if(mysql_num_rows($res1) > 0) {
				while($row1 = mysql_fetch_assoc($res1)) {
					$waiverid = check_waiver("","","",$row1["profileid"],true);
					if($waiverid > 0) {
						$sql1a = "UPDATE `tbl_date_dancers` SET waiverid='$waiverid' WHERE profileid='".$row1["profileid"]."' AND tourdateid='$tourdateid' AND studioid='$studioid'";
						$res1a = mysql_query($sql1a) or die(mysql_error());
					}
				}
			}

			//check for missing waivers (for dancers only)
			$sql8 = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.workshoplevelid > 1 AND tbl_date_dancers.workshoplevelid < 7 AND tbl_date_dancers.studioid=".$studioid." AND (tbl_date_dancers.waiverid IS NULL OR tbl_date_dancers.waiverid < 1) ORDER BY tbl_profiles.lname ASC";
			$res8 = mysql_query($sql8) or die(mysql_error());
			$oicount = 0;
			if(mysql_num_rows($res8) > 0) {
				while($row8 = mysql_fetch_assoc($res8)) {
					$rowoi["waivers"][] = $row8;
					++$oicount;
				}
			}

			//check for outstanding balance due
			if($row["balance_due"] > 0) {
				$rowoi["balance"] = "$".$row["balance_due"];
				++$oicount;
			}

			//check for blank/tba routine names
			$sql9 = "SELECT tbl_routines.name AS routinename, tbl_routines.id AS routineid, tbl_date_routines.id AS dateroutineid, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$studioid." AND (tbl_routines.name='TBA' OR tbl_routines.name='')";
			$res9 = mysql_query($sql9) or die(mysql_error());
			while($row9 = mysql_fetch_assoc($res9)) {
				$rowoi["tbas"][] = $row9;
				++$oicount;
			}

			//check for missing routine categories
			$sql10 = "SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE !tbl_date_routines.perfcategoryid > 0 AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$row["studioid"];
			$res10 = mysql_query($sql10) or die(mysql_error());
			if(mysql_num_rows($res10) > 0) {
				while($row10 = mysql_fetch_assoc($res10)) {
					$rowoi["noperfcat"][] = $row10;
					++$oicount;
				}
			}

			//check for missing birth dates
			$sql11 = "SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid=".$row["studioid"]." AND tbl_profiles.birth_date='' AND (tbl_date_dancers.workshoplevelid > 1 AND tbl_date_dancers.workshoplevelid < 7)";
			$res11 = mysql_query($sql11) or die(mysql_error());
			if(mysql_num_rows($res11) > 0) {
				while($row11 = mysql_fetch_assoc($res11)) {
					$rowoi["nobirthday"][] = $row11;
					++$oicount;
				}
			}

			//check for missing routine age divisions
			$sql12 = "SELECT tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_date_routines.routineid FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid WHERE !tbl_date_routines.agedivisionid > 0 AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$row["studioid"];
			$res12 = mysql_query($sql12) or die(mysql_error());
			if(mysql_num_rows($res12) > 0) {
				while($row12 = mysql_fetch_assoc($res12)) {
					$rowoi["noagediv"][] = $row12;
					++$oicount;
				}
			}

			//if either exist for row, add to studios array
			if(count($rowoi["noagediv"]) > 0 || count($rowoi["waivers"]) > 0 || strlen($rowoi["balance"]) > 0 || count($rowoi["tbas"]) > 0 || count($rowoi["noperfcat"]) > 0 || count($rowoi["nobirthday"]) > 0) {
				$row["oitems"] = $rowoi;
			}

			$data[$studioid] = $row;
		}
	}

//	print_r($data);exit();

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Confirmation</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}

			#sum_table tr td {
				font-size:11px;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
		<?php if(count($data) > 0) {
			foreach($data as $studio) { ?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;">
						<span style="font-family:Regal Box;font-size:52px;color:#FF0000;text-shadow:0 1px 1px rgba(0, 0, 0, 0);">JUMP</span><br/>
						<div style="font-family:'festus!';font-size:23px;margin-bottom:2px;line-height:31px;">the alternative convention</div>
					</td>
					<td style="text-align:right;vertical-align:top;">
						<div style="margin-top:5px;color:#0000FF;font-size:17px;">
						<?php
							if($isind == 0)
								print(stripslashes($studio["studioname"]));
							else
								print(stripslashes($studio["contact_full"]));
						?></div>
						<div style="font-size:11px;text-align:right;">
							<?php print($studio["address"]); ?>
							<br/>
							<?php print($studio["city"].", ".$studio["abbreviation"]." ".$studio["zip"]); ?>
						</div>
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top;padding-top:15px;">
						<span style="font-size:10px;line-height:10px;">
							5446 Satsuma Ave.<br/>
							North Hollywood, CA 91601<br/>
							Tel: 818.432.4395<br/>
							Fax: 888.873.0795<br/>
							info@jumptour.com<br/>
							www.jumptour.com
						</span>
					</td>
					<td style="text-align:right;vertical-align:bottom;padding-top:15px;font-size:16px;">
						<span style="font-weight:bold;font-size:20px;"><?php print($city); ?></span>
						<br/>
						<?php print($dispdate); ?>
					</td>
				</tr>
				<tr>
					<td colspan="2"><div style="width:100%;height:1px;border-bottom: 1px solid #777777;margin-top:10px;margin-bottom:20px;"></div></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;width:460px;">
						<div style="">
							Dear <?php print($studio["contact"]); ?>,
							<br/><br/>
							<?php if($isind == 0) { ?>
								Hey!  I am so glad you are joining us in <?php print("$city, $state"); ?> this year for JUMP!  I know you and your kids will have a great time!  Please review this attached statement and make sure it matches your records.
								<br/><br/>
								The workshop and competition schedules, as well as the downloadable personal PDFs for each dancer, will be posted on our website within one week of the event.  Each personal packet will include a workshop schedule, personalized competition schedule, as well as all other info for the event.
								<br/><br/>
								I really appreciate you supporting JUMP.  The faculty and I are determined to make this event a fantastic experience for you and your students!  If at any time you have questions regarding the event, please email us at info@jumptour.com or call us at (818) 432-4395; we are happy to help! I look forward to seeing you at JUMP!
								<br/><br/>
								Rock On,<br/>
								<img src="gilsig.png" alt="" />
						<?php } else { ?>
								Hey!  I am so glad you are joining us in <?php print("$city, $state"); ?> this year for JUMP!  I know you will have a great time!  Please review this attached statement and make sure it matches your records.
								<br/><br/>
								The workshop and competition schedules, as well as the downloadable personal PDFs for each dancer, will be posted on our website within one week of the event.  Each personal packet will include a workshop schedule, personalized competition schedule, as well as all other info for the event.
								<br/><br/>
								I really appreciate you supporting JUMP.  The faculty and I are determined to make this event a fantastic experience for you!  If at any time you have questions regarding the event, please email us at info@jumptour.com or call us at (818) 432-4395; we are happy to help! I look forward to seeing you at JUMP!
								<br/><br/>
								Rock On,<br/>
								<img src="gilsig.png" alt="" />
						<?php   } ?>
						</div>
					</td>
					<td style="vertical-align:top;text-align:right;">
						<div style="font-weight:bold;font-size:17px;margin-left:64px;text-align:left;margin-bottom:5px;">Registration Summary</div>
						<div style="margin-left:55px;background-color:#CCCCCC;width:200px;padding: 15px 5px 0;min-height:375px;text-align:left;">
							<table cellpadding="0" cellspacing="0" style="text-align:left;margin-left:10px;" id="sum_table">
								<tr>
									<td style="color:#0000FF;text-align:right;width: 100px;">WORKSHOP<br/>FEES:</td>
									<td style="color:#0000FF;vertical-align:top;text-align:left;padding-left:10px;width:80px;">$<?php print(number_format($studio["workshop_fees"],2)); ?></td>
								</tr>
								<tr>
									<td style="padding-top:5px;color:#0000FF;text-align:right;">COMPETITION<br/>FEES:</td>
									<td style="padding-top:5px;color:#0000FF;text-align:left;padding-left:10px;vertical-align:top;">$<?php print(number_format($studio["competition_fees"],2)); ?></td>
								</tr>
								<tr>
									<td style="padding-top:5px;text-align:right;">TOTAL:</td>
									<td style="padding-top:5px;text-align:left;padding-left:10px;">$<?php print(number_format($studio["total_fees"],2)); ?></td>
								</tr>
								<tr>
									<td style="padding-top:15px;text-align:right;">FEES PAID AS OF<br/><?php print(date('m/d/Y',time()));?>:</td>
									<td style="vertical-align:top;padding-top:15px;text-align:left;padding-left:10px;">$<?php print(number_format($studio["fees_paid"],2)); ?></td>
								</tr>
							<?php if($studio["credit"] > 0) { ?>
								<tr>
									<td style="padding-top:15px;text-align:right;">CREDIT:<br/></td>
									<td style="vertical-align:top;padding-top:15px;text-align:left;padding-left:10px;">$<?php print(number_format($studio["credit"],2)); ?></td>
								</tr>
							<?php } ?>
								<tr>
									<td style="padding-top:15px;color:#FF0000;text-align:right;text-align:right;">BALANCE DUE:</td>
									<td style="padding-top:15px;color:#FF0000;text-align:left;padding-left:10px;">$<?php print($studio["balance_due"]); ?></td>
								</tr>
							</table>

							<div style="margin-top:15px;font-weight:bold;font-style:italic;">NOTES:</div>
							<div style="">
								<?php print($studio["invoice_note"]); ?>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<!-- END PAGE 1 -->


		<?php if(count($studio["workshopregistrants"]) > 0) { ?>
		<div style="width:720px;page-break-after:always;">
			<div style="padding: 0 0 5px 20px;margin-bottom:10px;border-bottom:1px solid #CCCCCC;color:#0000FF;font-size:20px;">Workshop Registrants (<?php print(count($studio["workshopregistrants"])); ?>)</div>
			<div style="font-style:italic;padding-left:20px;margin-bottom:20px;">Teachers (<?php print($studio["teacher_count"]); ?>), Seniors (<?php print($studio["senior_count"]); ?>), Teens (<?php print($studio["teen_count"]); ?>), Juniors (<?php print($studio["junior_count"]); ?>), Minis (<?php print($studio["mini_count"]); ?>), JUMPstarts (<?php print($studio["jumpstart_count"]); ?>), Observers (<?php print($studio["observer_count"]); ?>), JUMPstarts Observers(<?php print($studio["observer2_count"]); ?>)</div>
			<?php $curryear = date('Y',time()); ?>
			<table cellpadding="0" cellspacing="0" style="margin-left:20px;">
				<tr>
					<td style="width:190px;padding-bottom:10px;">Name</td><td style="width:120px;padding-bottom:10px;">Birthday</td><td style="width:140px;padding-bottom:10px;">Age as of 1/1/<?php print($ageasofyear); ?></td><td style="width:120px;padding-bottom:10px;">Workshop Level</td><td style="text-align:right;width:120px;padding-bottom:10px;">Fee</td>
				</tr>
			<?php foreach($studio["workshopregistrants"] as $dancer) {
					if($dancer["workshoplevelname"] != "Observer") { ?>
				<tr>
					<td style="padding-bottom:6px;"><?php print($dancer["fname"]." ".$dancer["lname"]); ?></td>
					<td style="padding-bottom:6px;"><?php print($dancer["birth_date"]); ?></td>
					<td style="padding-bottom:6px;"><?php print($dancer["age"]); ?></td>
					<td style="padding-bottom:6px;"><?php print(str_replace("2","",$dancer["workshoplevelname"])); ?></td>
					<td style="padding-bottom:6px;text-align:right;"><?php if($dancer["has_scholarship"] == 1) print("<span style='font-size:11px;font-style:italic;'>(Scholarship)&nbsp;&nbsp;&nbsp;</span> "); if($dancer["one_day"] == 1) print("<span style='font-size:11px;font-style:italic;'>(One Day)&nbsp;&nbsp;&nbsp;</span>"); if(strlen($dancer["fee"]) > 0) print("$".number_format($dancer["fee"],2,'.','')); else print("$0.00"); ?></td>
				</tr>
			<?php 	}
				}?>
			<?php
				if(count($studio["obs"]) > 0) {
					foreach($studio["obs"] as $dancer) { ?>
						<tr>
							<td style="padding-bottom:6px;"><?php print($dancer["fname"]." ".$dancer["lname"]); ?></td>
							<td style="padding-bottom:6px;"><?php print($dancer["birth_date"]); ?></td>
							<td style="padding-bottom:6px;"><?php print($dancer["age"]); ?></td>
							<td style="padding-bottom:6px;"><?php print($dancer["workshoplevelname"]); ?></td>
							<td style="padding-bottom:6px;text-align:right;"><?php print("$".number_format($dancer["fee"],2,'.','')); ?></td>
						</tr>
			<?php		}
				}
			?>
			<tr><td colspan="3">&nbsp;</td><td colspan="2" style="padding-top:10px;text-align:right;"><span style="font-weight:bold;">Workshop Fees</span><br/>$<?php print(number_format(($studio["free_teacher_value"]+$studio["workshop_fees"]),2)); ?></td></tr>
			<?php
				if($studio["free_teacher_count"] > 0) { ?>
			<tr><td colspan="3">&nbsp;</td><td colspan="2" style="padding-top:10px;text-align:right;"><span style="font-weight:bold;">Free Teacher Discount</span><br/><?php print($studio["free_teacher_count"] > 0 ? $studio["free_teacher_count"] : 0); ?> Free ($<?php print($studio["free_teacher_value"]); ?>)</td></tr>
			<?php  } ?>
			<tr><td colspan="3">&nbsp;</td><td colspan="2" style="padding-top:10px;text-align:right;"><span style="color:#FF0000;font-weight:bold;">Total Workshop Fees</span><br/>$<?php print(number_format($studio["workshop_fees"],2)); ?></td></tr>
			</table>
		</div>
		<?php } ?>
		<!-- END PAGE 2 -->


		<?php if(count($studio["competitionroutines"]) > 0) { ?>
		<div style="width:720px;page-break-after:always;">
			<div style="border-bottom:1px solid #CCCCCC;color:#358F39;font-size:20px;padding:0px 0 5px 20px;margin-bottom:10px;">Competition Routines (<?php print($studio["competition_routine_count"]); ?>)</div>
			<table cellpadding="0" cellspacing="0" style="margin-left:20px;">
				<tr>
					<td style="width:250px;padding-bottom:10px;">Routine Name</td><td style="width:130px;padding-bottom:10px;">Age Division</td><td style="width:110px;padding-bottom:10px;">Category</td><td style="width:100px;padding-bottom:10px;">Perf. Division</td><td style="text-align:right;width:90px;padding-bottom:10px;">Fee</td>
				</tr>
			<?php foreach($studio["competitionroutines"] as $routine) { ?>
				<tr>
					<td style="vertical-align:top;"><span style="font-weight:bold;"><?php print($routine["routinename"]); ?></span></td>
					<td style="vertical-align:top;"><?php print($routine["agedivisionname"]." (".$routine["range"].")"); ?></td>
					<td style="vertical-align:top;"><?php print($routine["routinecategoryname"]." (".count($routine["routinedancers"]).")"); ?></td>
					<td style="vertical-align:top;"><?php print($routine["performancedivisionname"]); ?></td>
					<td style="vertical-align:top;text-align:right;"><?php print("$".$routine["fee"]); ?></td>
				</tr>
				<tr>
					<td colspan="5" style="padding-bottom:10px;font-size:11px;">
						Performers: <?php
							$dstr = "";
							foreach($routine["routinedancers"] as $dancer) {
								$dstr .= $dancer["fname"]." ".$dancer["lname"].", ";
							}
							print(substr($dstr,0,-2));
						?>
						<?php
							if($routine["extra_time"] > 0) {
								print("<div style='font-weight:bold;font-style:italic;'>Extra time: ".$routine["extra_time"]."minute(s)</div>");
							}
						?>
					</td>
				</tr>
			<?php } ?>
			<tr><td colspan="3">&nbsp;</td><td colspan="2" style="padding-top:10px;text-align:right;"><span style="color:#FF0000;font-weight:bold;">Total Competition Fees</span><br/>$<?php print($studio["competition_fees"]); ?></td></tr>
			</table>
		</div>
		<?php } ?>


		<!-- END PAGE 3 -->


		<?php if(count($studio["oitems"]) > 0) { ?>
		<div style="width:720px;page-break-after:always;">
			<div style="padding:0px 0 5px 20px;border-bottom:1px solid #CCCCCC;color:#DFB953;font-size:20px;margin-bottom:10px;">Outstanding Items (<?php print($oicount); ?>)</div>
						<div style="padding-left:20px;">
			Please Note:<br/><br/>The following items are needed to complete your registration.  Please get this information to the JUMP office ASAP.
			<br/><br/>
					<?php
						$pos = 1;
						  if(strlen($studio["oitems"]["balance"]) > 0) {
							print(($pos.".&nbsp;&nbsp;&nbsp;".strtoupper($studio["studioname"])." has a BALANCE DUE of ".$studio["oitems"]["balance"])."<br/>");
							++$pos;
						  }

						  if(count($studio["oitems"]["tbas"]) > 0) {
						  	for($i=0;$i<count($studio["oitems"]["tbas"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;TBA ".$studio["oitems"]["tbas"][$i]["agedivisionname"]." ".$studio["oitems"]["tbas"][$i]["routinecategoryname"]." routine needs a Title.<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["oitems"]["noperfcat"]) > 0) {
						  	for($i=0;$i<count($studio["oitems"]["noperfcat"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Routine '".$studio["oitems"]["noperfcat"][$i]["routinename"]."' has no assigned PERFORMANCE CATEGORY (e.g. Jazz, Lyrical, Tap)<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["oitems"]["nobirthday"]) > 0) {
						  	for($i=0;$i<count($studio["oitems"]["nobirthday"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;".$studio["oitems"]["nobirthday"][$i]["fname"]." ".$studio["oitems"]["nobirthday"][$i]["lname"]." has no birth date set.<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["oitems"]["noagediv"]) > 0) {
						  	for($i=0;$i<count($studio["oitems"]["noagediv"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Routine '".$studio["oitems"]["noagediv"][$i]["routinename"]."' has no assigned AGE DIVISION<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["oitems"]["waivers"]) > 0) {
							for($i=0;$i<count($studio["oitems"]["waivers"]);$i++) {
								print($pos.".&nbsp;&nbsp;&nbsp;Missing participant waiver for ".strtoupper($studio["oitems"]["waivers"][$i]["fname"]." ".$studio["oitems"]["waivers"][$i]["lname"])."<br/>");
								++$pos;
							}
						  }
					?>
				</div>

		</div>
		<?php } ?>

		<?php
			}
		} ?>
	</body>
</html>