<?php
	include("../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = intval($_GET["eventid"]);

	if ($eventid == 7) $tour = 'jump';
	if ($eventid == 8) $tour = 'nuvo';
	if ($eventid == 18) $tour = '24';

	//select scholarship_code, dancer name, studio name.  order by studio name AND scholarship_code
	$sql = "SELECT tbl_date_dancers.scholarship_code,
			tbl_date_dancers.age,
			tbl_date_dancers.id as datedancerid,
			tbl_date_dancers.profileid, tbl_profiles.fname,
			tbl_profiles.lname,
			tbl_studios.name as studioname
			FROM `tbl_date_dancers`
			LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid
			LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid WHERE tbl_date_dancers.tourdateid = $tourdateid
			AND tbl_date_dancers.scholarship_code > 0
			ORDER BY tbl_studios.name, tbl_date_dancers.scholarship_code";

	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			/* if ($row['age'] <= 7) $row['age_div'] = 'Mini'; */
			if ($row['age'] <= intval(10)) { $row['age_div'] = 'Mini'; }
			if ($row['age'] >= intval(11) && $row['age'] <= intval(12)) { $row['age_div'] = 'Junior'; }
			if ($row['age'] >= intval(13) && $row['age'] <= intval(15)) { $row['age_div'] = 'Teen'; }
			if ($row['age'] >= intval(16)) { $row['age_div'] = 'Senior'; }

			if (strlen($row['fname']) <= 8 ) { $row['length'] = 'short'; }
			if (strlen($row['fname']) > 8 && strlen($row['fname']) <= 12 ) { $row['length'] = 'long'; }
			if (strlen($row['fname']) > 12) { $row['length'] = 'very-long'; }

			$dancers[] = $row;
		}
	}

/*
	$dancers = array(
		array(
			"scholarship_code" => "888",
			"fname" => "MaryBethJane",
			"lname" => "Simmons",
			"studioname" => "THE DANCE STUDIO"
		)
	);
*/


	$pageWidth = 11;
	$pageHeight = 8.5;
	$pageMargin = 0.375;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Scholarship Labels</title>
		<link href="https://fonts.googleapis.com/css?family=Archivo+Black|Oswald|Roboto:900" rel="stylesheet">
		<style type="text/css">
			html {
				margin: 0;
				padding: 0;
			}

			body {
				text-align: left;
				margin: 0;
				padding: 0;
				color: #000000;
			}

			@page {
				margin: 0.3in !important;
			}

			.page {
				width: 10.25in;
				max-height: 100%;
				padding: 0;
				overflow: hidden;
				position: relative;
				background-size: 10.25in 7.75in;
				background-position: center center;
				box-sizing: border-box;
				page-break-after: always !important;
			}
			.sl_table {
				padding: 0;
				margin: 0;
				border: none;
				border-collapse: collapse;
				box-sizing: border-box;
			}
			.label {
				width: 5in;
				height: 3.5in;
				overflow: hidden;
				padding: 0;
				margin: 0;
				text-align: center;
				vertical-align: middle;
				font-family: 'Archivo Black', sans-serif;
				font-weight: normal;
			}

			.label-inner {
				width: 100%;
				height: 100%;
				padding: 0;
				margin: 0;
				display: block;
				position: relative;
			}

			.logo {
				position: absolute;
				top: 0;
				left: .25in;
				margin: 0;
			}
			.logo img {
				display: block;
				position: relative;
				height: 1in;
				width: auto;
				margin: 0;
			}
			.number {
				font-family: 'Roboto';
				font-weight: normal;
				font-size: 1.7in;
				line-height: 1in;
				border-radius: .125in;
				letter-spacing: -.07in;
				text-align: right;
				position: absolute;
				top: 47%;
				left: 50%;
				transform: translate(-50%,-50%);
				color: #000;
/*

				top: 0.125in;
				right: 0.125in;
				border: 0.05in solid #000;
				padding: 0in .235in;

*/
			}
			.age-div {
				font-weight: 900;
				font-size: .5in;
				line-height: .5in;
				text-transform: uppercase;
				letter-spacing: -.025in;
				text-align: right;
				position: absolute;
				bottom: 0;
				left: 50%;
				transform: translateX(-50%);
				color: #ea148c;
				padding: .125in .25in;
			}
			.first-name {
				font-family: 'Oswald', sans-serif;
				font-weight: bold;
				padding: .25em;
				width: 100%;
				letter-spacing: -.025in;
				text-transform: uppercase;
				text-align: center;
				position: absolute;
				bottom: 0;
				left: 50%;
				transform: translateX(-50%);
			}

			.short { font-size: 1in; line-height: 1in; }
			.long { font-size: .9in; line-height: .9in; }
			.very-long { font-size: .8in; line-height: .8in; }

			.info {
				font-size: 10pt;
				position: absolute;
/*

				bottom: .25in;
				left: .25in;
				text-align: left;

*/
				top: .25in;
				right: .25in;
				text-align: right;
				color: #fff;
			}
			.label-background {
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				overflow: hidden;
			}
			.label-background img {
				width: 100%;
				height: 100%;
			}

			.Mini {
				color: #7a3da5;
			}
			.Junior {
				color: #f20591;
			}

			.Teen {
				color: #298119;
			}

			.Senior {
				color: #0a66ff;
			}

			.vertical-spacer {
				height: 0.125in;
				font-size: 0;
				line-height: 0;
			}
			.horizontal-spacer {
				width: 0.125in;
				font-size: 0;
				line-height: 0;
			}
			img {
				margin: 0;
			}
			.custom-box {
				position: fixed;
				z-index: 100;
				top: 0;
				right: 0;
				background: #fff;
			}
			.custom-box-inner {
				border: 1px solid #888;
				padding: 1rem;
			}

			.custom-box input {
				display: block;
				width: 100%;
				margin-bottom: 5px;
				border: 1px solid #444;
			}
			@media print {
				.custom-box { display: none; }
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
		<div id="custom-box" class="custom-box">
			<div class="custom-box-inner">
				<a id="toggle-hide">Hide</a>
				<h1>Create custom tag</h1>
				<input type="text" id="fname" name="fname" placeholder="First Name">
				<input type="text" id="lname" name="lname" placeholder="Last Name">
				<input type="number" id="age" name="age" placeholder="Age">
				<input type="text" id="studio" name="studio" placeholder="Studio">
				<input type="number" id="num" name="num" placeholder="#">
				<a id="go">GO</a>
			</div>
		</div>
		<?php
			for($i=0; $i < count($dancers); $i++) { ?>

		<div class="page" style="background-image: url('./images/<?php print($tour); ?>/<?php print(strtolower($dancers[$i]["age_div"])); ?>.jpg');">
			<table cellpadding="0" cellspacing="0" border="0" class="sl_table">
				<tr>
					<td class="vertical-spacer" colspan="4">&nbsp;</td>
				</tr>
				<?php
					for($j=0; $j<2; $j++) { ?>
				<tr>
					<td class="horizontal-spacer">&nbsp;</td>
					<?php for($k=0; $k<2; $k++) { ?>
					<td class="label">
						<div class="label-inner">
							<div class="logo">
								<img src="http://breakthefloor.com/images/logos-white/<?php print($tour); ?>.png" />
							</div>
							<div class="number"><?php print($dancers[$i]["scholarship_code"]); ?></div>
							<div class="first-name <?php print($dancers[$i]["length"]); ?>"><?php print($dancers[$i]["fname"]); ?></div>
							<div class="info">
								<div><?php print($dancers[$i]["age_div"]." (".$dancers[$i]["age"].")"); ?></div>
								<div><?php print($dancers[$i]["fname"]." ".$dancers[$i]["lname"]); ?></div>
								<div><?php print(stripslashes($dancers[$i]["studioname"])); ?></div>
							</div>
							<!-- <div class="age-div <?php print($dancers[$i]["age_div"]); ?>"><?php print($dancers[$i]["age_div"]); ?></div> -->
						</div>
					</td>
					<?php } ?>
					<td class="horizontal-spacer">&nbsp;</td>
				</tr>

				<?php if($j == 0) { ?>
					<tr>
						<td class="vertical-spacer" style="height: 0.5in;" colspan="4">&nbsp;</td>
					</tr>
				<?php } ?>

				<?php } ?>
<!--
				<tr>
					<td class="vertical-spacer" style="height: 0.in;" colspan="4">&nbsp;</td>
				</tr>
-->
			</table>
		</div>
		<?php  } ?>

		<script>
			var tourdateid = <?php print($tourdateid); ?>;
			var eventid = <?php print($eventid); ?>;
			var hidden = false;

			document.getElementById("go").onclick = function () {
				var fname = document.getElementById("fname").value;
				var lname = document.getElementById("lname").value;
				var age = document.getElementById("age").value;
				var studio = document.getElementById("studio").value;
				var num = document.getElementById("num").value;

				var link = "custom.php?fname="+fname+"&lname="+lname+"&age="+age+"&studio="+studio+"&num="+num+"&eventid="+eventid+"&tourdateid="+tourdateid;
		        location.href = link;
		    };

		    document.getElementById("toggle-hide").onclick = function () {
			    hidden = !hidden;
			    var customBox = document.getElementById("custom-box");
			    var toggle = document.getElementById("toggle-hide");

			    if (hidden) {
			    	customBox.style.right = '-200px';
			    	customBox.style.opacity = '0.5';
			    	toggle.innerHTML = 'Show';
			    } else {
				    customBox.style.right = '0';
				    customBox.style.opacity = '1';
				    toggle.innerHTML = 'Hide';
			    }
		    };
		</script>
	</body>