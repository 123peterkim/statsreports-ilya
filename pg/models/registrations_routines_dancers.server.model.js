"use strict";

module.exports = function(sequelize, DataTypes) {
	var registrations_routines_dancers = sequelize.define('registrations_routines_dancers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		registrations_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		},
		workshop_skip: {
			type: DataTypes.INTEGER
		},
		date_routines_dancer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_routines_dancers',
        key: 'id'
      }
		}
	}, {
		timestamps: false,
		associate: function(models) {
      registrations_routines_dancers.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
      registrations_routines_dancers.belongsTo(models.date_routines_dancers, {foreignKey: 'date_routines_dancer_id', as: 'date_routines_dancer'});
		}
	});
	return registrations_routines_dancers;
};