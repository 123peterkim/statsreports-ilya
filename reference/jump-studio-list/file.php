<?php
//phpinfo();exit();
//ini_set('display_errors',1);
//ini_set ('max_execution_time',  -1);
//error_reporting(E_ALL);

	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('F d',mktime(0,0,0,$mm,$dd,$yy));
	$end_date_a = db_one("end_date","tbl_tour_dates","id=$tourdateid");
	list($yy2,$mm2,$dd2) = explode("-",$end_date_a);
	$end_date = date('d',mktime(0,0,0,$mm2,$dd2,$yy2));
	$tpc = 0;

	//NON-IND'T
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tourdateid=$tourdateid AND tbl_date_studios.independent=0 AND tbl_studios.name!='[N/A]' ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$studioid = $row["studioid"];
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["studioname"] = stripslashes($row["studioname"]);
			unset($row["contacts"]);
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			$row["studiocode"] = strlen($row["studiocode"]) > 0 ? $row["studiocode"] : "-";
			$row["routine_count"] = db_one("COUNT(id)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid");
			$row["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
			$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
			$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
			$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
			$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
			$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
			$row["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
			$row["observer2_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=12 AND tourdateid=$tourdateid");
			$row["packet_count"] = $row["teacher_count"] + $row["senior_count"] + $row["junior_count"] + $row["teen_count"] + $row["mini_count"] + $row["jumpstart_count"] + 1;
			$tpc += $row["packet_count"];
			$studios[] = $row;
		}
	}
	//IND'T NA
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.independent=1 AND tourdateid=$tourdateid AND tbl_studios.name='[N/A]' ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$studioid = $row["studioid"];
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["studioname"] = stripslashes($row["studioname"]);
			unset($row["contacts"]);
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			$row["studiocode"] = strlen($row["studiocode"]) > 0 ? $row["studiocode"] : "-";
			$row["routine_count"] = db_one("COUNT(id)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid");
			$row["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
			$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
			$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
			$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
			$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
			$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
			$row["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
			$row["observer2_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=12 AND tourdateid=$tourdateid");
			$row["packet_count"] = $row["teacher_count"] + $row["senior_count"] + $row["junior_count"] + $row["teen_count"] + $row["mini_count"] + $row["jumpstart_count"];
			$tpc += $row["packet_count"];
			$studios[] = $row;
		}
	}
	//IND'T NON-NA
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tourdateid=$tourdateid AND tbl_date_studios.independent=1 AND tbl_studios.name!='[N/A]' ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$studioid = $row["studioid"];
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["studioname"] = stripslashes($row["studioname"]);
			unset($row["contacts"]);
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			$row["studiocode"] = strlen($row["studiocode"]) > 0 ? $row["studiocode"] : "-";
			$row["routine_count"] = db_one("COUNT(id)","tbl_date_routines","studioid=$studioid AND tourdateid=$tourdateid");
			$row["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
			$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
			$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
			$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
			$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
			$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
			$row["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
			$row["observer2_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=12 AND tourdateid=$tourdateid");
			$row["packet_count"] = $row["teacher_count"] + $row["senior_count"] + $row["junior_count"] + $row["teen_count"] + $row["mini_count"] + $row["jumpstart_count"];
			$tpc += $row["packet_count"];
			$studios[] = $row;
		}
	}

	if($tpc > 0) {
		$tsec = $tpc * 43;
		$tmin = ceil($tsec/60);
		$thour = floor($tmin / 60);
		$trem = str_pad($tmin - ($thour * 60),2,"0",STR_PAD_LEFT);
		$total = "$thour:$trem";
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Unplug;font-size:20px;"><?php print($city); ?> Studio List</div>
			<span style="font-family:Unplug;font-size:14px;"><?php print($venue); ?> / <?php print($start_date."-".$end_date); ?></span>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
				<tr>
					<td class="thead" style="width:235px;">Studio</td>
					<td class="thead" style="width:115px;">Contact</td>
					<td class="thead" style="width:35px;">Indp.</td>
					<td class="thead" style="width:45px;">Rou. #</td>
					<td class="thead" style="width:245px;">Dancer #</td>
					<td class="thead" style="border-right: 1px solid #000000;width:50px;">Pkt. #</td>
				</tr>
			<?php foreach($studios as $studio) { ?>
				<tr>
					<td class="tbody"><?php print($studio["studioname"]." (".$studio["studiocode"].")"); ?></td>
					<td class="tbody"><?php print($studio["contact"]); ?></td>
					<td class="tbody"><?php print($studio["independent"]); ?></td>
					<td class="tbody"><?php print($studio["routine_count"]); ?></td>
					<td class="tbody"><?php print("Tc(".$studio["teacher_count"].") Sr(".$studio["senior_count"].") Tn(".$studio["teen_count"].") Jr(".$studio["junior_count"].") Mi(".$studio["mini_count"].") Js(".$studio["jumpstart_count"].") Ob(".$studio["observer_count"].") Ob2(".$studio["observer2_count"].")"); ?></td>
					<td class="tbody" style="text-align:center;border-right: 1px solid #000000;"><?php print($studio["packet_count"]); ?></td>
				</tr>
			<?php } ?>
				<tr>
					<td colspan="6" style="text-align:right;font-size:11px;font-weight:bold;padding-right:10px;">Total Packets: <?php print($tpc); ?> @ ~43 sec/each = approx <?php print($total); ?></td>
				</tr>
			</table>
		</div>
	</body>
</html>