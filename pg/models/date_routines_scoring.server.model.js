"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_routines_scoring = sequelize.define('date_routines_scoring', {
		date_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_routines',
				key: 'id'
			},
			primaryKey: true
		},
		competition_group_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_competition_groups',
				key: 'id'
			},
			primaryKey: true
		},
		competition_award_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_competition_awards',
				key: 'id'
			}
		},
		room: {
			type: DataTypes.INTEGER
		},
		time: {
			type: DataTypes.DATE
		},
		has_a: {
			type: DataTypes.BOOLEAN
		},
		score: {
			type: DataTypes.JSONB
		},
		number: {
			type: DataTypes.INTEGER
		},
		total_score: {
			type: DataTypes.INTEGER
		},
		dropped_score: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_routines_scoring.belongsTo(models.date_routines, {foreignKey: 'date_routine_id', as: 'date_routine'});
			date_routines_scoring.belongsTo(models.tbl_competition_groups, {foreignKey: 'competition_group_id', as: 'competition_group'});
			date_routines_scoring.belongsTo(models.tbl_competition_awards, {foreignKey: 'competition_award_id', as: 'competition_award'});
		}
	});
	return date_routines_scoring;
};