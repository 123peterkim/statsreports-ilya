var Excel = require('exceljs');
var _ = require('lodash');
var fs = require('fs');
var awardLabelData = require('../reference/award-label-xls/data.json');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');

module.exports = {
    awardLabelSpreadsheet: function (req, res) {
        // query params
        var DateRoutines = db.date_routines;
        var tourDateId = req.query.tourdateid || 746;

        // async.parallel([
        //     /* 
        //         SELECT 
        //         tbl_date_routines.id AS dateroutineid, tbl_date_routines.routineid, tbl_date_routines.number_".$cg." AS number, tbl_date_routines.".$cg."_has_a AS has_a, tbl_date_studios.id AS datestudioid, tbl_date_studios.studiocode, tbl_date_studios.studioid, tbl_routines.name AS routinename, tbl_studios.name AS studioname 
        //         FROM 
        //             `tbl_date_routines` 
        //         LEFT JOIN tbl_date_studios ON tbl_date_studios.studioid=tbl_date_routines.studioid 
        //         LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid 
        //         LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid 
        //         WHERE 
        //             tbl_date_routines.tourdateid=$tourdateid 
        //             AND tbl_date_studios.tourdateid=$tourdateid 
        //             AND tbl_date_routines.$cg=1 
        //         ORDER BY tbl_date_routines.number_$cg ASC, tbl_date_routines.".$cg."_has_a ASC
        //     */

        //     function(cb) {
        //         DateRoutines.findAll({
        //             where: {
        //                 tour_date_id: tourDateId,

        //             }
        //         })
        //     }
        // ])

        var wb = new Excel.Workbook();
        var ws = wb.addWorksheet('Worksheet');
        var city = awardLabelData.city;
        city = city.toLowerCase().replace(/(\s|\,|\-)/g, "");

        // TODO: setup auto width
        // https://stackoverflow.com/a/16762003
        ws.columns = [
            { header: 'Number', key: 'number', width: 10, style: { font: {name: 'Arial' } } },
            { header: 'Routine Name', key: 'name', width: 10, style: { font: { name: 'Arial' } } },
            { header: 'Studio Name', key: 'studio_name', width: 10, style: { font: { name: 'Arial' } } },
            { header: 'Studio Code', key: 'studio_code', width: 10, style: { font: { name: 'Arial' } } },
            { header: 'Dancer #', key: 'dancer_count', width: 10, style: { font: { name: 'Arial' } } }
        ];

        _.forEach(awardLabelData.routines, function (routine) {
            ws.addRow(routine);
        });

        var filename = 'public/' + Date.now();

        wb.xlsx.writeFile(filename)
            .then(function () {
                res.download(filename, "awardslabels_"+city+".xlsx", function (err) {
                    if (!err) fs.unlink(filename);
                });
            });
    }
}