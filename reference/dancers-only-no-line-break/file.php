<?php
	include("../../../includes/util.php");
	/* FOR VIDEO DEPT. & PRODUCTION MANAGER */
	$tourdateid = clean($_GET["tourdateid"]);
	$showall = intval($_GET["showall"] == 1) ? true : false;
	$bdonly = intval($_GET["bdonly"] == 1) ? true : false;
	if($tourdateid > 0) {
		$dancers = get_all_date_dancers_list($tourdateid,$showall,$bdonly);  //set false to hide teachers & obs
		if(count($dancers) > 0) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Dancer List [no link break -- 3 spaces between]</title>
	</head>
	<body>
	<?php
			foreach($dancers as $dancer) {
				//if bestdancerlist
				if($bdonly)
					print(stripslashes($dancer["fname"])." ".stripslashes($dancer["lname"])."&nbsp;&nbsp;&nbsp;");
				else {
					if(!(strpos($dancer["fname"],"Observer") > -1) && $dancer["fname"] != "Teacher")
						print(stripslashes($dancer["fname"])." ".stripslashes($dancer["lname"])."&nbsp;&nbsp;&nbsp;");
				}
			}
?>
	</body>
</html><?php
		}
	}
	exit();
?>