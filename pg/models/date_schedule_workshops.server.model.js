"use strict";

module.exports = function(sequelize, DataTypes) {
	var dateScheduleWorkshops = sequelize.define('date_schedule_workshops', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		span: {
			type: DataTypes.INTEGER
		},
		duration: {
			type: DataTypes.INTEGER
		},
		start_time: {
			type: DataTypes.DATE
		},
		date: {
			type: DataTypes.DATEONLY
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return dateScheduleWorkshops;
};