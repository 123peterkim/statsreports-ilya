"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_event_reg_types = sequelize.define('tbl_event_reg_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		event_reg_type_name_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_reg_type_names',
				key: 'id'
			}
		},
		season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			}
		},
		old_id: {
			type: DataTypes.INTEGER
		},
		fees: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_event_reg_types;
};