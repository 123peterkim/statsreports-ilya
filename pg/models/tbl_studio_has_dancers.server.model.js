"use strict";

module.exports = function(sequelize, DataTypes) {
	var studios_dancers = sequelize.define('studios_dancers', {
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			},
			primaryKey: true
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			},
			primaryKey: true
		}
	}, {
		timestamps: false,
		associate: function(models) {
			studios_dancers.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
			studios_dancers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
		}
	});
	return studios_dancers;
};