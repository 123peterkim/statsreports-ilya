var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');
var specialAwards = require('./specialAwards');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var compGroup = req.query.comp_group || 'finals'; // placeholder comp_group
        var dispCompGroup = '';
        if(compGroup == 'fianls') dispCompGroup = 'Finals';
        else if(compGroup == 'vips') dispCompGroup = 'VIP';
        else if(compGroup == 'prelims') dispCompGroup = 'Prelims';

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                { model: db.tbl_event_cities, as: 'event_city', include: [
                    { model: db.tbl_states, as: 'state' }
                ]},
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if(tourDate) {
                return {
                    city_data: {
                        city: tourDate.event_city.name,
                        venue: tourDate.venue.name
                    },
                    disp_comp_group: dispCompGroup,
                    new_page: "",
                    room: 0,
                    maxrows: 0,
                    rowcount: 0,
                    title_date: null,
                    routines: [],
                    awards: []
                };
            }
            else {
                throw new Error('No results')
            }
        })
        .then(function(jroData) {
            db.date_special_awards.findAll({
                where: { tour_date_id: tourDateId },
                include: [
                    { model: db.date_routines, as: 'date_routine', include: [
                        { model: db.tbl_routines, as: 'routine', include: [
                            { model: db.tbl_studios, as: 'studio' }
                        ]}
                    ]},
                    { model: db.tbl_special_awards, as: 'special_award', include: [
                        { model: db.tbl_award_types, as: 'award_type' }
                    ]}
                ]
            })
            .then(function(results) {

                _.forEach(results, function(dsa) {
                    if(typeof jroData.awards[dsa.special_award.award_type.name] === 'undefined') {
                        jroData.awards[dsa.special_award.award_type.name] = [{
                            studio_name: dsa.date_routine.routine.studio.name,
                            routine_name: dsa.date_routine.routine.name
                        }];
                    }
                    else {
                        jroData.awards[dsa.special_award.award_type.name].push({
                            studio_name: dsa.date_routine.routine.studio.name,
                            routine_name: dsa.date_routine.routine.name
                        });
                    }
                });

                if (xls) {
                    specialAwards.specialAwardsSpreadsheet(req, res, jroData);
                }
                else {
                    res.render('reports/jump-special-awards', jroData);
                }
            })
            .catch(function(err) {
                console.log('ERROR', err);
                res.send(err);
            });
        });

    }
}