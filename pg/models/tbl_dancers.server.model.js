"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_dancers = sequelize.define('tbl_dancers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		parent_guardian: {
			type: DataTypes.STRING
		},
		email_parents: {
			type: DataTypes.STRING
		},
		waiver: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_dancers.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
			tbl_dancers.belongsToMany(models.tbl_studios, {through: 'studios_dancers', foreignKey: 'dancer_id', otherKey: 'studio_id', as: 'studios'});
			tbl_dancers.hasMany(models.studios_dancers, {foreignKey: 'dancer_id', as: 'has_many_studios_dancers'})
		}
	});
	return tbl_dancers;
};