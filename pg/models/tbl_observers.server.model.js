"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_observers = sequelize.define('tbl_observers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    person_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_persons',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_observers.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
      tbl_observers.belongsToMany(models.tbl_studios, {through: 'studios_observers', foreignKey: 'observer_id', otherKey: 'studio_id', as: 'studios'});
      tbl_observers.hasMany(models.studios_observers, {foreignKey: 'observer_id', as: 'has_many_studios_observers'});
    }
  });
  return tbl_observers;
};