<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	//$safecity = strtolower(str_replace(array(" ",",","-"),"",$city));
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);
	$tmp = array();
	$sql = "SELECT tbl_date_studios.studioid, tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_date_studios.independent, tbl_date_studios.free_teacher_value, tbl_date_studios.total_fees, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.total_fees DESC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["sumdancers"] = db_one("COUNT(id)","tbl_date_dancers","tourdateid=$tourdateid AND studioid='".$row["studioid"]."' AND (workshoplevelid!=8 AND workshoplevelid!=1 AND workshoplevelid!=12)");
			$row["sumroutines"] = db_one("COUNT(id)","tbl_date_routines","tourdateid=$tourdateid AND studioid='".$row["studioid"]."'");
			$row["independent"] = $row["independent"] == "1" ? "Yes" : "No";
			$row["freeteachervalue"] = $row["free_teacher_value"];
			$row["dispfee"] = number_format(($row["total_fees"] - $row["free_teacher_value"]),2,'.','');

			$tmp[$row["studioid"]] = $row["dispfee"];
			$studios[] = $row;
		}
		$tmp2 = array();
		if(count($tmp) > 0) {
			arsort($tmp);
			foreach($tmp as $atk => $atk) {
				foreach($studios as $sk => $sv)
				if($atk == $sv["studioid"])
					$tmp2[] = $studios[$sk];
			}
			$studios = $tmp2;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Georgia;font-size:20px;"><?php print($city); ?> Hotel Comps</div>
			<span style="font-family:Georgia;font-size:14px;"><?php print($venue); ?> / <?php print($dispdate); ?></span>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
				<tr>
					<td class="thead" style="width:225px;">Studio</td>
					<td class="thead" style="width:135px;">Contact</td>
					<td class="thead" style="width:60px;">Indep.</td>
					<td class="thead" style="width:60px;">Dancers</td>
					<td class="thead" style="width:60px;">Routines</td>
					<td class="thead" style="border-right: 1px solid #000000;width:60px;">Total Fee</td>
				</tr>
			<?php
				if(count($studios) > 0) {
					foreach($studios as $studio) { ?>
						<tr>
							<td class="tbody"><?php print($studio["studioname"]." (".$studio["studiocode"].")"); ?></td>
							<td class="tbody"><?php print($studio["contact"]); ?></td>
							<td class="tbody"><?php print($studio["independent"]); ?></td>
							<td class="tbody"><?php print($studio["sumdancers"]); ?></td>
							<td class="tbody"><?php print($studio["sumroutines"]); ?></td>
							<td class="tbody" style="border-right:1px solid #000000;"><?php print($studio["dispfee"]); ?></td>
						</tr>
			<?php 		}
				} ?>
			</table>
		</div>
	</body>
</html>