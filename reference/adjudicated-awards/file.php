<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$compgroup = mysql_real_escape_string($_GET["compgroup"]);
	$showscores = $_GET["showscores"] == "true" ? true : false;
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);
	$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.canceled, tbl_date_routines.number_$compgroup AS number, tbl_date_routines.".$compgroup."_has_a AS has_a, tbl_date_routines.routineid, tbl_date_routines.".$compgroup."_total_score AS total_score, tbl_date_routines.".$compgroup."_awardid AS awardid, tbl_competition_awards.name AS awardname, tbl_date_routines.agedivisionid, tbl_date_routines.routinecategoryid, tbl_date_routines.perfcategoryid, tbl_routines.name AS routinename, tbl_date_routines.studioid, tbl_studios.name AS studioname, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_performance_divisions.name AS perfdivisionname FROM `tbl_date_routines` LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id=tbl_date_routines.".$compgroup."_awardid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_routines.studioid WHERE (tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.$compgroup=1 AND tbl_date_routines.routinetypeid < 4) ORDER BY tbl_date_routines.number_$compgroup ASC, tbl_date_routines.".$compgroup."_has_a ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$routines = Array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$routines[] = $row;
		}
	}
	//print_r($routines);exit();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 8pt;
				text-transform: uppercase;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}

			#thetable tr:nth-child(odd) {
				background-color: #EEEEEE;
			}

			.strike td {
				text-decoration: line-through;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Adjudicated Awards</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print(ucfirst($compgroup)." Competition"); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($dispdate); ?></div></td>
				</tr>
			</table>

			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:30px;" id="thetable">
				<tr>
					<td class="thead" style="width:37px;">#</td><td class="thead" style="width:195px;">Studio</td><td class="thead" style="width:205px;">Routine Name</td><td class="thead" style="width:145px;">Award</td><td class="thead" style="width:80px;">Age Division</td><td class="thead" style="width:90px;">Category</td><td class="thead" style="width:90px;border-right:1px solid #000000;">Perf. Division</td>
				</tr>
			<?php
				if(count($routines) > 0) {
					foreach($routines as $routine) {

						$dispaward = ($showscores ? $routine["awardname"]." (".$routine["total_score"].")" : $routine["awardname"]);

						$dispnumber = $routine["has_a"] == 1 ? $routine["number"].".a" : $routine["number"];
						if($routine["routinecategoryname"] == "Solo") {
							$pid = db_one("profileid","tbl_date_routine_dancers","routineid=".$routine["routineid"]." AND tourdateid=$tourdateid");
							if($pid > 0) {
								$dname = db_one("fname","tbl_profiles","id=$pid")." ".db_one("lname","tbl_profiles","id=$pid");
								$trn = $routine["routinename"];
								$routine["routinename"] = $dname." (".$routine["routinename"].")";
							}
						}
						?>
						<tr <? if($routine["canceled"]) print("class='strike'"); ?>>
							<td class="tbody" style="text-align:center;<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($dispnumber); ?></td>
 							<td class="tbody" style="<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($routine["studioname"]); ?></td>
							<td class="tbody" style="<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($routine["routinename"]); ?></td>
							<td class="tbody" style="color:#FF0000;"><?php print($dispaward); ?></td>
							<td class="tbody" style="<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($routine["agedivisionname"]); ?></td>
							<td class="tbody" style="<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($routine["routinecategoryname"]); ?></td>
							<td class="tbody" style="border-right:1px solid #000000;<?php if(!$routine["total_score"] > 0) { print("color:#AAAAAA;");} ?>"><?php print($routine["perfdivisionname"]); ?></td>
						</tr>
			<?php  	}
				} ?>
			</table>
		</div>
	</body>
</html>