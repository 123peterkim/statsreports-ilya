<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);

	$contacts = Array();
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.contacts, tbl_studios.address, tbl_studios.city, tbl_studios.zip, tbl_studios.state, tbl_countries.name AS countryname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid LEFT JOIN tbl_countries ON tbl_countries.id = tbl_studios.countryid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			if(strlen($row["contacts"]) > 0) {
				$scontacts = json_decode($row["contacts"],true);
				$row["contact"] = $scontacts[0]["fname"]." ".$scontacts[0]["lname"];
				$row["studioname"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"])));
				$abbr = db_one("abbreviation","tbl_states","name='".$row["state"]."'");
				if(strlen($abbr) > 0)
					$row["state"] = $abbr;
			}
			unset($row["contacts"]);
			unset($row["datestudioid"]);
			$contacts[] = $row;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Mailing Labels</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-family: 'OfficinaSansCTT', Tahoma, sans-serif;
			}
			.sl_table {
/* 				margin-left:30px; */
			}
			.sl_table tr td {
				padding: 0 0 0 0px;
				width: 340px;
				text-align: left;
			}

			.labelbg {
				background: url(mailing_label_bg.png) no-repeat top center;width:300px;height:149px;
				font-family: 'Blue Highway D Type', Arial, sans-serif;
				font-size:16px;
				padding-top:30px;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
		<?php
			$count = 0;
			for($i=0;$i<count($contacts);$i+=2) { ?>
		<div style="width:720px;padding-top:35px;">
			<table cellpadding="0" cellspacing="0" class="sl_table" <?php if($count+1 == 3) { print("style='page-break-after:always;padding-bottom:0;'"); }?>>
				<tr>
					<td style="vertical-align: top;">
						<?php if($contacts[$i]) { ?>
							<span style="font-family:Regal Box;font-size:52px;color:#FF0000;">JUMP</span><br/> <!-- text-shadow:0 1px 1px rgba(0, 0, 0, 1); -->
							<div style="font-family:'festus!';font-size:18px;margin-bottom:2px;line-height:16px;">the alternative convention</div>
							<span style="font-size:10px;line-height:9px;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601<br/>
								818.432.4395
							</span>
							<div class="labelbg">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="width:45px;vertical-align:top;">To:</td>
										<td style="vertical-align:top;">
											<?php
												if(strlen($contacts[$i]["contact"]) > 0)
													print($contacts[$i]["contact"]."<br/>");
												if($contacts[$i]["studioname"] != "[N/A]")
													print($contacts[$i]["studioname"]."<br/>");
												print($contacts[$i]["address"]."<br/>");
												print($contacts[$i]["city"].", ".$contacts[$i]["state"]." ".$contacts[$i]["zip"]."<br/>");
												if($contacts[$i]["countryname"] != "United States")
													print($contacts[$i]["countryname"]);
											?>
										</td>
									</tr>
								</table>
							</div>
						<?php } ?>
					</td>
					<td style="vertical-align: top;padding-left:90px;">
						<?php if($contacts[$i+1]) { ?>
							<span style="font-family:Regal Box;font-size:52px;color:#FF0000;">JUMP</span><br/> <!-- text-shadow:0 1px 1px rgba(0, 0, 0, 1); -->
							<div style="font-family:'festus!';font-size:18px;margin-bottom:2px;line-height:16px;">the alternative convention</div>
							<span style="font-size:10px;line-height:9px;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601<br/>
								818.432.4395
							</span>
							<div class="labelbg">
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td style="width:45px;vertical-align:top;">To:</td>
										<td style="vertical-align:top;">
											<?php
												if(strlen($contacts[$i+1]["contact"]) > 0)
													print($contacts[$i+1]["contact"]."<br/>");
												if($contacts[$i+1]["studioname"] != "[N/A]")
													print($contacts[$i+1]["studioname"]."<br/>");
												print($contacts[$i+1]["address"]."<br/>");
												print($contacts[$i+1]["city"].", ".$contacts[$i+1]["state"]." ".$contacts[$i+1]["zip"]."<br/>");
												if($contacts[$i+1]["countryname"] != "United States")
													print($contacts[$i+1]["countryname"]);
											?>
										</td>
									</tr>
								</table>
							</div>
						<?php } ?>
					</td>
				</tr>
			</table>
		</div>
		<?php
			++$count;
		} ?>
	</body>