<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_REQUEST["tourdateid"]);
	if($tourdateid > 0) {
		$tourdate = db_one("city","tbl_tour_dates","id=$tourdateid");
		$safecity = strtolower(str_replace(array(" ",",","-"),"",$tourdate));
		$studios = array();
		$sql = "SELECT tbl_date_studios.studioid, tbl_date_studios.independent,tbl_studios.email, tbl_studios.contacts, tbl_studios.name AS studioname, tbl_studios.phone, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_studios.countryid, tbl_studios.zip, tbl_states.abbreviation FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid LEFT JOIN tbl_states ON tbl_states.name=tbl_studios.state WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			$cns = json_decode($row["contacts"],true);
			$row["contact"] = $cns[0]["fname"]." ".$cns[0]["lname"];
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			unset($row["contacts"]);
			unset($row["studioid"]);
			unset($row["state"]);
			foreach($row as $rkey=>$rval) {
				$row[$rkey] = str_replace(",","\,",$rval);
			}
			$studios[] = $row;
		}

		if(count($studios) > 0) {
			//create excel shit
			$workbook = new PHPExcel();
			$workbook->setActiveSheetIndex(0);

			//set global font & font size
			$workbook->getDefaultStyle()->getFont()->setName('Arial');

			//col widths
			$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

			$dataArray = array();
			foreach($studios as $studio) {
				$dataArray[] = array($studio["studioname"],$studio["contact"],$studio["email"],$studio["independent"]);
			}
			$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

			$outputFileType = 'Excel5';
			$mk = time();
			$somekindofrandomstr = "$safecity"."_emails_$tourdateid".substr($mk,6,5);
			$outputFileName = "../../temp/$somekindofrandomstr.xls";
			$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
			$objWriter->save($outputFileName);
			chmod($outputFileName,777);
			unset($workbook);
			unset($objWriter);

			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=".basename($outputFileName));
			header("Content-type: application/octet-stream");
			header("Content-Transfer-Encoding: binary");
			readfile($outputFileName);
		}
	}
	exit();
?>
