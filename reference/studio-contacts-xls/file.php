<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = intval(db_one("eventid","tbl_tour_dates","id=$tourdateid"));
	$cityname = strtolower(str_replace(array(" ","-",","),"",db_one("city","tbl_tour_dates","id=$tourdateid")));

	//get all studios & main contact
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.independent, tbl_studios.name AS studioname, tbl_studios.email, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_studios.contacts, tbl_studios.phone, tbl_studios.fax, tbl_studios.zip, tbl_countries.name AS country FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid LEFT JOIN tbl_countries ON tbl_countries.id=tbl_studios.countryid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = str_replace("&#44;",",",stripslashes(str_replace("&amp;","&",$row["studioname"])));
			$row["state_abbr"] = db_one("abbreviation","tbl_states","name='".$row["state"]."' OR abbreviation='".$row["state"]."'");
			$row["address"] = str_replace("&#44;",",",str_replace("&amp;","&",$row["address"]));
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			$contacts = json_decode($row["contacts"],true);
			$nc = array();
			if(count($contacts) > 0) {
				$row["contactf"] = $contacts[0]["fname"];
				$row["contactl"] = $contacts[0]["lname"];
			}
			$studios[] = $row;
		}
	}


	if(count($studios) > 0) {
		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("First Name","Last Name","Studio","Independent?","Full Name","Address","City","State","Zip","Phone","Fax","Email","Country");
		foreach($studios as $studio) {
			$dataArray[] = array($studio["contactf"],$studio["contactl"],$studio["studioname"],$studio["independent"],$studio["contactf"]." ".$studio["contactl"],$studio["address"],$studio["city"],$studio["state_abbr"],$studio["zip"],$studio["phone"],$studio["fax"],$studio["email"],$studio["country"]);
		}

		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "$cityname"."_studio_contacts".substr($mk,6,5);
		if(file_exists($outputFileName))
			unlink($outputFileName);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);
	}

	exit();
?>