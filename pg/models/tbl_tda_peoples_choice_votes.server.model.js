"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_tda_peoples_choice_votes = sequelize.define('tbl_tda_peoples_choice_votes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		tda_award_nomination_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tda_award_nominations',
				key: 'id'
			}
		},
		ip: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_tda_peoples_choice_votes;
};