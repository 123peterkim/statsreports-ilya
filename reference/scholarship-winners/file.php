<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$sql = "SELECT id,name,isclass FROM `tbl_scholarships` WHERE eventid=$eventid AND active = 1 ORDER BY report_order ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$dancers = Array();
			$dsql = "SELECT tbl_date_scholarships.profileid, tbl_date_scholarships.code, tbl_date_scholarships.scholarshipid, tbl_scholarships.name AS scholarshipname, tbl_profiles.fname, tbl_profiles.lname, tbl_date_scholarships.winner, tbl_date_dancers.scholarship_code, tbl_date_scholarships.datedancerid, tbl_studios.name AS studioname FROM `tbl_date_scholarships` LEFT JOIN tbl_scholarships ON tbl_scholarships.id = tbl_date_scholarships.scholarshipid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.id = tbl_date_scholarships.datedancerid LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_scholarships.profileid LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid WHERE tbl_date_scholarships.tourdateid=$tourdateid AND tbl_date_scholarships.scholarshipid=".$row["id"]." ORDER BY tbl_scholarships.report_order ASC, tbl_date_scholarships.winner ASC, tbl_profiles.lname ASC";
			$dres = mysql_query($dsql) or die(mysql_error());
			while($row2 = mysql_fetch_assoc($dres)) {
				$dancers[] = $row2;
			}
			$scholarships[] = Array("id"=>$row["id"],"name"=>$row["name"],"isclass"=>$row["isclass"],"dancers"=>$dancers);
		}
	}
	$current_sn = "";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 11px;
				text-transform: uppercase;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 2px 0 2px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}


			.rtable.isclass tr:nth-child(odd) {
				background-color: #EEEEEE !important;
			}

		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:952px;margin: 10px auto 0;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><div style="font-size:20pt;font-family:Unplug;"><?php print($city); ?> <?php if($eventid == 7) print("VIP"); if($eventid == 8) print("Breakout"); if($eventid == 14) print("Best Dancer"); ?> List</div></td>
					<td style="vertical-align:top;padding-right:75px;"><div style="text-align:right;font-size:10pt;font-family:Unplug;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<div style="font-size:12pt;font-family:Unplug;"><?php print($venue); ?></div>

			<?php foreach($scholarships as $scholarship) {
					if(count($scholarship["dancers"]) > 0) { ?>
			<div style="font-family:Unplug;color:#3366FF;margin: 20px 0 7px 0;font-style:italic;font-weight:bold;font-size:18px;"><?php print($scholarship["name"]); ?></div>

			<table cellpadding="0" cellspacing="0" class="rtable <? if($scholarship["isclass"]) print('isclass'); ?>">
				<tr>
					<td class="thead" style="width:150px;">Placement</td>
					<td class="thead" style="width:380px;">Studio</td>
					<td class="thead" style="width:225px;">Dancer</td>
					<td class="thead" style="width:115px;border-right:1px solid #000000;">Code</td>
				</tr>
					<?php for($i=0;$i<count($scholarship["dancers"]);$i++) { ?>
				<tr>
					<td class="tbody" <?php if($scholarship["dancers"][$i]["winner"] == "1" && !$scholarship["isclass"]) print("style='background-color:#EEEEEE;'"); ?>><?php print($scholarship["dancers"][$i]["winner"] == "1" ? "Winner" : "Runner-Up");?></td>
					<td class="tbody" <?php if($scholarship["dancers"][$i]["winner"] == "1" && !$scholarship["isclass"]) print("style='background-color:#EEEEEE;'"); ?>><?php print($scholarship["dancers"][$i]["studioname"]);?></td>
					<td class="tbody" <?php if($scholarship["dancers"][$i]["winner"] == "1" && !$scholarship["isclass"]) print("style='background-color:#EEEEEE;'"); ?>><?php print($scholarship["dancers"][$i]["fname"]." ".$scholarship["dancers"][$i]["lname"]." (".$scholarship["dancers"][$i]["scholarship_code"].")")?></td>
					<td class="tbody" style="<?php if($scholarship["dancers"][$i]["winner"] == "1" && !$scholarship["isclass"]) print("background-color:#EEEEEE;"); ?>border-right:1px solid #000000;text-align:center;"><?php print($scholarship["dancers"][$i]["code"]); ?></td>
				</tr>
					<?php }
				    } ?>
			</table>
			<?php } ?>
		</div>
	</body>
</html>