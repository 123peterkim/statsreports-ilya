<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$compgroup = mysql_real_escape_string($_GET["compgroup"]);
	$compgroup = "finals";
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");

	//get dateroutineid, routine #, has a, studio code, studio name, age division, routinecategory, performance division, time
	$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.studioid, tbl_date_routines.number_$compgroup AS number, tbl_date_routines.".$compgroup."_time, tbl_date_routines.".$compgroup."_has_a AS has_a, tbl_date_routines.perfcategoryid, tbl_date_routines.agedivisionid, tbl_date_routines.routinecategoryid, tbl_routines.name AS routinename, tbl_age_divisions.name AS agedivisionname, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_performance_divisions.name AS perfdivisionname FROM tbl_date_routines LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid WHERE tbl_date_routines.tourdateid = $tourdateid AND $compgroup = 1 ORDER BY tbl_date_routines.number_$compgroup ASC, tbl_date_routines.".$compgroup."_has_a ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["date"] = date('n/d/Y',$row["time"]);
			$sql3 = "SELECT * FROM `tbl_age_divisions` WHERE id=".$row["agedivisionid"];
			$res3 = mysql_query($sql3) or die(mysql_error());
			while($row3 = mysql_fetch_assoc($res3)) {
				$row["agedivisionrange"] = "(".$row3["range"].")";
			}
			if($row["studioid"] > 0) {
				$sc = db_one("studiocode","tbl_date_studios","studioid=".$row["studioid"]." AND tourdateid=$tourdateid");
				$row["studiocode"] = ($sc!="") ? $sc : "-";
			}

			$row["dispnum"] = $row["has_a"] == "1" ? $row["number"].".a" : $row["number"];
			$routines[] = $row;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title><?php print($city); ?> Score Sheets</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.tr_table tr td{
				font-family: Verdana, Arial, sans-serif;
				color: #555555;
				font-size: 10px;
				padding-right:3px;
			}

			.rtable tr td {
				font-size: 10px;
				text-align: right;
			}

			.inner_table tr td{
				font-size: 11px;
				width: 133px;
				height: 30px;
				padding-left:15px;
				padding-top:15px;
				font-family: Verdana, Arial, sans-serif;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
	<?php
		if(count($routines) > 0) {
		foreach($routines as $routine) { ?>
		<div style="page-break-after:always;width:720px;height:970px;">
			<table cellpadding="0" cellspacing="0" style="width:100%;margin-top:10px;">
				<tr>
					<td style="width:445px;vertical-align:top;">
						<img src="jump_logo_black.png" style="width:250px;height:99px;" alt="" />
						<br/>
						<div style="font-family:impact;font-size:37pt;margin: 25px 0 0 0;">OFFICIAL SCORE SHEET</div>
					</td>
					<td>
						<table cellpadding="0" cellspacing="0" style="float:right;border-right: 3px dotted #000000;" class="tr_table">
							<tr>
								<td style="text-align:right;padding-bottom:6px;">
									Routine:<br/>
									<span style="font-weight:bold;font-size: 18px;color:#000000;"><?php print("#".$routine["dispnum"]." ".strtoupper($routine["routinename"])." (".$routine["studiocode"].")"); ?></span>
								</td>
							</tr>
							<tr>
								<td style="text-align:right;padding-bottom:6px;">
									Age Division:<br/>
									<span style="font-size: 14px;font-weight:bold;color:#000000;"><?php print(strtoupper($routine["agedivisionname"])); ?> <?=$routine["agedivisionrange"];?></span>
								</td>
							</tr>
							<tr>
								<td style="text-align:right;padding-bottom:6px;">
									Category:<br/>
									<span style="font-size: 14px;font-weight:bold;color:#000000;"><?php print(strtoupper($routine["routinecategoryname"])); ?></span>
								</td>
							</tr>
							<tr>
								<td style="text-align:right;padding-bottom:6px;">
									Perf. Division:<br/>
									<span style="font-size: 14px;font-weight:bold;color:#000000;"><?php print(strtoupper($routine["perfdivisionname"])); ?></span>
								</td>
							</tr>
							<tr>
								<td style="text-align:right;"><div style="height:2px;border-bottom:3px dotted #000000;width:140px;float:right;"></div></td>
							</tr>
							<tr>
								<td style="clear:both;padding-top:10px;text-align:right;color:#000000;font-size:12px;padding-bottom:10px;">
									O = Needs Work<br/>
									X = Great Work
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div style="width:720px;height:520px;border:1px solid #000000;margin:15px 0 0 0;">
				<div style="width:100%;border-bottom:1px solid #000000;height:110px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:190px;vertical-align:top;font-family: GF Ordner Inverted;font-size:20pt;font-style:italic;">TECHNIQUE</td>
							<td style="width:352px;vertical-align:top;"><div style="width:347px;height:20px;border-bottom:4px dotted #000000;"></div></td>
							<td style="width:100px;vertical-align:top;padding-left:4px;font-weight:bold;font-size:15pt;font-style:italic;">1-50 pts.</td>
							<td style="vertical-align:top;padding-left:4px;"><div style="width:64px;height:23px;border-bottom:2px solid #000000;"></div></td>
						</tr>
						<tr>
							<td colspan="4">
								<table cellpadding="0" cellspacing="0" class="inner_table">
									<tr><td>Control/Balance</td><td>Elevation</td><td>Turns</td><td>Arms</td><td>Hands</td></tr>
									<tr><td>Extensions</td><td>Head/Shoulders</td><td>Body Lines</td><td>Feet</td><td>Legs</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:100%;border-bottom:1px solid #000000;height:110px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:210px;vertical-align:top;font-family: GF Ordner Inverted;font-size:20pt;font-style:italic;">PERFORMANCE</td>
							<td style="width:333px;vertical-align:top;"><div style="width:330px;height:20px;border-bottom:4px dotted #000000;"></div></td>
							<td style="width:100px;vertical-align:top;padding-left:4px;font-weight:bold;font-size:15pt;font-style:italic;">1-20 pts.</td>
							<td style="vertical-align:top;padding-left:4px;"><div style="width:64px;height:23px;border-bottom:2px solid #000000;"></div></td>
						</tr>
						<tr>
							<td colspan="4">
								<table cellpadding="0" cellspacing="0" class="inner_table">
									<tr><td>Personality</td><td>Stage Presence</td><td>Smile</td><td>&nbsp;</td><td>&nbsp;</td></tr>
									<tr><td>Intensity</td><td>Emotion</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:100%;border-bottom:1px solid #000000;height:110px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:440px;vertical-align:top;font-family: GF Ordner Inverted;font-size:20pt;font-style:italic;">CHOREO.&nbsp;AND&nbsp;MUSICALITY</td>
							<td style="width:105px;vertical-align:top;"><div style="width:105px;height:20px;border-bottom:4px dotted #000000;"></div></td>
							<td style="width:100px;vertical-align:top;padding-left:4px;font-weight:bold;font-size:15pt;font-style:italic;">1-20 pts.</td>
							<td style="vertical-align:top;padding-left:4px;"><div style="width:64px;height:23px;border-bottom:2px solid #000000;"></div></td>
						</tr>
						<tr>
							<td colspan="4">
								<table cellpadding="0" cellspacing="0" class="inner_table">
									<tr><td>Choreography</td><td>Precision</td><td>Creativeness</td><td>Timing</td><td>&nbsp;</td></tr>
									<tr><td>Difficulty</td><td>Use of Space</td><td>Continuity</td><td>&nbsp;</td><td>&nbsp;</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:100%;border-bottom:1px solid #000000;height:110px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:345px;vertical-align:top;font-family: GF Ordner Inverted;font-size:20pt;font-style:italic;">OVERALL APPEARANCE</td>
							<td style="width:197px;vertical-align:top;"><div style="width:187px;height:20px;border-bottom:4px dotted #000000;"></div></td>
							<td style="width:100px;vertical-align:top;padding-left:4px;font-weight:bold;font-size:15pt;font-style:italic;">1-10 pts.</td>
							<td style="vertical-align:top;padding-left:4px;"><div style="width:64px;height:23px;border-bottom:2px solid #000000;"></div></td>
						</tr>
						<tr>
							<td colspan="4">
								<table cellpadding="0" cellspacing="0" class="inner_table">
									<tr><td>Costume</td><td>Confidence</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
									<tr><td>Appearance</td><td>Appropriateness</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<!-- <div style="width:100%;height:60px;">
					<div style="font-weight:bold;margin:20px 0 0 40px;font-size:12px;font-style:italic;font-family:Verdana,Arial,sans-serif;">
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td><div style="width:20px;height:20px;border:1px solid #CCCCCC;margin:-3px 0 0 -30px;"></div></td>
								<td><div style="">This routine might not be "JUMP Friendly"</div></td>
								<td><img src="sadface.png" style="width:30px;height:30px;margin:-13px 0 0 13px;" alt="" /></td>
							</tr>
						</table>
					</div>
				</div> -->
				<img src="jumpfriendly.jpg" alt="" style="display:block;" />
			</div>

			<div style="width:720px;margin-top:25px;">
				<table cellpadding="0" cellspacing="0">
					<tr><td style="font-weight:bold;font-size:12px;width:450px;font-family:Verdana,Arial,sans-serif;padding-left:15px;">&nbsp;ADDITIONAL COMMENTS:</td>
						<td style="font-weight:bold;font-size:12px;font-family:Verdana,Arial,sans-serif;">TOTAL SCORE:<?php if($routine["perfcategoryid"] == 6) print("<br/><span style='font-weight:normal;font-size:18px;'> THIS ROUTINE IS SHOWCASE ONLY. DO NOT SCORE.</span>"); ?></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-left:15px;font-family: Verdana,Arial,sans-serif;font-size:10px;">
							<div style="border-bottom:1px solid #777777;width: 325px;height:120px;"></div>
							JUDGE'S SIGNATURE
						</td>
					</tr>
				</table>
			</div>
			<div style="color:#555555;width:720px;margin-top:5px;border-top:1px solid #777777;font-family:Tahoma,Arial,sans-serif;font-size:10px;">
				<span style="font-weight:bold;">&nbsp;&nbsp;&nbsp;JUMP</span> / The Alternative Convention
			</div>
		</div>
		<div style="clear:both;"></div>
	<?php  }
	} ?>
	</body>
</html>