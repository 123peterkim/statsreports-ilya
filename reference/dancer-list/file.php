<?php
	include("../../../includes/util.php");
	/* FOR VIDEO DEPT. & PRODUCTION MANAGER */
	$tourdateid = clean($_GET["tourdateid"]);
	$showall = intval($_GET["showall"] == 1) ? true : false;
	if($tourdateid > 0) {
		$dancers = get_all_date_dancers_list($tourdateid,$showall);  //set false to hide teachers & obs
		if(count($dancers) > 0) {
			foreach($dancers as $dancer) {
				if(!(strpos($dancer["fname"],"Observer") > -1) && $dancer["fname"] != "Teacher")
					print($dancer["fname"]." ".$dancer["lname"]."<br/>");
			}
		}
	}
	exit();
?>