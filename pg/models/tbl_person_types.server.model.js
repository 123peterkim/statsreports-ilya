"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_person_types = sequelize.define('tbl_person_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		signup: {
			type: DataTypes.BOOLEAN
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_person_types;
};