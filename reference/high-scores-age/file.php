<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$compgroup = mysql_real_escape_string($_GET["compgroup"]);
	$save =  mysql_real_escape_string($_GET["save"]) == "true" ? true : false;
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));
	$maxplaces = 3;

	$adorder = Array(5,6,7,10,11,1,2,3,4,8);
	$rcorder = Array(1,2,3,4,5,6,8,10);

	if($save) {
		//reset
		$sql2 = "UPDATE `tbl_date_routines` SET place_hsa='0' WHERE tourdateid=$tourdateid AND $compgroup=1";
		$res2 = mysql_query($sql2) or die(mysql_error());
	}

	$awards = Array();
	for($i=0;$i<count($adorder);$i++) {
		$sql = "SELECT * FROM `tbl_age_divisions` WHERE id=".$adorder[$i];
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			$adnames = $row["name"];
			$adranges = $row["range"];
		}
		$adname = "$adnames ($adranges)";
		for($j=0;$j<count($rcorder);$j++) {
			$routines = array();
			$place = 0;
			$lastscore = 0;
			$lastdropped = 0;
			$tiebreaker = 0;
			$rcname = db_one("name","tbl_routine_categories_$seasonid","id=$rcorder[$j]");
			//make sure to NOT include ADJUDICATED ONLY, SHOWCASE ONLY (perfdivision AND routinetype) or PERFORMANCE ONLY (so routinetypeid has to == 1)
			$sql = "SELECT tbl_date_routines.routineid,tbl_date_routines.perfcategoryid, tbl_performance_divisions.name AS perfdivname, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_date_routines.agedivisionid, tbl_age_divisions.name AS agedivisionname, tbl_routines.name AS routinename, tbl_date_routines.".$compgroup."_total_score AS total_score, tbl_date_routines.".$compgroup."_dropped_score AS dropped_score, tbl_date_routines.".$compgroup."_awardid AS awardid, tbl_date_routines.".$compgroup."_dropped_score2 AS dropped_score2, tbl_studios.name AS studioname FROM `tbl_date_routines` LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id=tbl_date_routines.routinecategoryid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id=tbl_date_routines.perfcategoryid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_routines.studioid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id=tbl_date_routines.agedivisionid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.$compgroup=1 AND tbl_date_routines.routinecategoryid=".$rcorder[$j]." AND tbl_date_routines.agedivisionid=".$adorder[$i]." AND tbl_date_routines.routinetypeid=1 ORDER BY tbl_date_routines.".$compgroup."_total_score DESC, tbl_date_routines.".$compgroup."_dropped_score DESC, tbl_date_routines.".$compgroup."_dropped_score2 DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$row["place"] = "-";
					$row["rcname"] = $rcname;
					if($row["awardid"] > 0)
						$row["awardname"] = db_one("name","tbl_competition_awards","id=".$row["awardid"]);
					else
						$row["awardname"] = "-";

					if($row["dropped_score2"] > 0)
						$row["dispnumbers"] = "(".$row["total_score"]." : ".$row["dropped_score"]."/".$row["dropped_score2"].")";
					else
						$row["dispnumbers"] = "(".$row["total_score"]." : ".$row["dropped_score"].")";

					//if solo dispname
					if($rcorder[$j] == 1) {
						$profileid = db_one("profileid","tbl_date_routine_dancers","tourdateid=$tourdateid AND routineid=".$row["routineid"]);
						if($profileid > 0) {
							$aname = db_one("fname","tbl_profiles","id=$profileid")." ".db_one("lname","tbl_profiles","id=$profileid");
							$row["dispname"] = $row["routinename"]." ($aname)";
						}
					}
					else
						$row["dispname"] = $row["routinename"];

					if(db_one("SUM(".$compgroup."_total_score)","tbl_date_routines","tourdateid=$tourdateid") > 0) {
						if($row["total_score"] > 0) {

							//if two dropped scores get the avg. as tiebreaker.. otherwise first dropped score
							if($row["dropped_score2"] > 0)
								$tiebreaker = ceil(($row["dropped_score"] + $row["dropped_score2"])/2);
							else
								$tiebreaker = $row["dropped_score"];

							//if not the same score as last routine... ignore tiebreaker
							if($lastscore != $row["total_score"]) {
								++$place;
							}
							else {
								if($tiebreaker == $lastdropped) {

								}
								else {
									++$place;
								}
							}

							if($place == 1)
								$row["place"] = "1st";
							if($place == 2)
								$row["place"] = "2nd";
							if($place == 3)
								$row["place"] = "3rd";

							if($rcorder[$j] == 1) { //for solos, show top 10 not top 3
								if($place == 4)
									$row["place"] = "4th";
								if($place == 5)
									$row["place"] = "5th";
								if($place == 6)
									$row["place"] = "6th";
								if($place == 7)
									$row["place"] = "7th";
								if($place == 8)
									$row["place"] = "8th";
								if($place == 9)
									$row["place"] = "9th";
								if($place == 10)
									$row["place"] = "10th";
							}

							$lastscore = $row["total_score"];
							$lastdropped = $tiebreaker;

							if($row["place"] != "-")
								$routines[] = $row;
						} //END disp max places only
					} //END if scores exist
					else {
						$routines[] = $row;
					}
				} //END each routine
				if(count($routines) > 0)
					$awards[$adname][$rcname] = array_reverse($routines);

				//save if flagged
				if(strlen($save) > 0) {
					if(count($routines) > 0) {
						//update each
						foreach($routines as $routine) {
							$place = intval(str_replace(array("st","nd","rd","th"),"",$routine["place"]));
							$sql3 = "UPDATE `tbl_date_routines` SET place_hsa='$place' WHERE routineid=".$routine["routineid"]." AND tourdateid=$tourdateid LIMIT 1";
							$res3 = mysql_query($sql3) or die(mysql_error());
						}
					}
				}

			}
		}
	}
//error_reporting(E_ALL);
//print_r($awards);exit();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 7.9pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Placement Awards (by Age)</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print(ucfirst($compgroup)." Competition"); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<?php
				if(count($awards) > 0) {
					foreach($awards as $agedivision=>$routinecategories) {
						if(count($routinecategories) > 0) {
							foreach($routinecategories as $rcname=>$routines) { ?>
								<div style="margin:10px 0 3px;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;">
									<?php print($agedivision." : ".$rcname); ?>
								</div>
								<table cellpadding="0" cellspacing="0" class="rtable" style="margin:0px 0 20px;">
									<tr>
										<td class="thead" style="width:60px;">Place</td>
										<td class="thead" style="width:165px;">Award</td>
										<td class="thead" style="width:305px;">Routine/Dancer</td>
										<td class="thead" style="width:240px;">Studio</td>
										<td class="thead" style="width:115px;border-right:1px solid #000000;">Perf. Div</td>
									</tr>
								<?php
									foreach($routines as $routine) {
								?>
										<tr>
											<td class="tbody" style="text-align:center;"><?php print($routine["place"]); ?></td>
											<td class="tbody" style="text-align:center;"><?php print($routine["awardname"]." ".$routine["dispnumbers"]); ?></td>
											<td class="tbody"><?php print($routine["dispname"]); ?></td>
											<td class="tbody"><?php print($routine["studioname"]); ?></td>
											<td class="tbody" style="border-right:1px solid #000000;"><?php print($routine["perfdivname"]);?></td>
										</tr>
								<?php } ?>
								</table>
					<?php } ?>
				<?php	}
				}
			} ?>
		</div>
	</body>
</html>