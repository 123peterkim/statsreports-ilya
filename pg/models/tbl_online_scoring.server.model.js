"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_online_scoring = sequelize.define('tbl_online_scoring', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		competition_group_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_competition_groups',
				key: 'id'
			}
		},
		date_routine_id: {
			type: DataTypes.INTEGER,
      references: {
        model: 'date_routines',
        key: 'id'
      }
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_online_scoring.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
      tbl_online_scoring.belongsTo(models.date_routines, {foreignKey: 'date_routine_id', as: 'date_routine'});
			tbl_online_scoring.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			tbl_online_scoring.belongsTo(models.tbl_routines, {foreignKey: 'routine_id', as: 'routine'});
			tbl_online_scoring.belongsTo(models.tbl_competition_groups, {foreignKey: 'competition_group_id', as: 'competition_groups'});
			tbl_online_scoring.hasMany(models.tbl_online_scoring_data, {foreignKey: 'online_scoring_id', as: 'online_scoring_data'});
		}
	});
	return tbl_online_scoring;
};