"use strict";

module.exports = function(sequelize, DataTypes) {
	var store_products_sizes = sequelize.define('store_products_sizes', {
		store_size_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_sizes',
				key: 'id'
			}
		},
		store_product_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_products',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return store_products_sizes;
};