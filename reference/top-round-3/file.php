<?php
	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$adids = array(1,2,3,4);
	$genders = array("female","male");
	$allshit = array();

	//danceoff judge count
	$jcount = db_one("tda_danceoff_judge_count","tbl_tour_dates","id=$tourdateid");
	$dcounts = json_decode(db_one("tda_danceoff_bestdancer_counts","tbl_tour_dates","id=$tourdateid"),true);
	$groupid = 1;

	foreach($adids as $adid) {
		$adname = db_one("name","tbl_age_divisions","id=$adid");
		foreach($genders as $gender) {
			$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_tda_bestdancer_data.perc_round2, tbl_tda_bestdancer_data.danceoff, tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.routineid, tbl_studios.name AS studioname, tbl_date_routines.number_vips AS number, tbl_date_routines.vips_has_a AS has_a, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_routines ON tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid WHERE tbl_tda_bestdancer_data.iscompeting=1 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.agedivisionid=$adid AND tbl_profiles.gender='$gender' AND tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid ORDER BY tbl_tda_bestdancer_data.perc_round2 DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {

					$dcpos = strtolower(substr($adname,0,1).substr($gender,0,1));
					$dcount = $dcounts[$dcpos];
					if($dcount > 0) { // && $row["danceoff"] > 0) {
						$maxtotal = $dcount * $jcount;
						$danceoff = $row["danceoff"];
//						print($danceoff." | ");
						$perc_danceoff = 0;
						if($danceoff > 0) {
							$nlm = ((1 - (($danceoff-$jcount) / $maxtotal))) * 100;
							$perc_danceoff = $nlm * .3;
						}
				//		print(("NLM $nlm DO:".$danceoff."-danceoff $jcount"."-jcount"." $maxtotal - maxtotal ".$perc_danceoff)."<br/>");
					}
					$perc_total = number_format($row["perc_round2"] + $perc_danceoff,2);

					//update perc_danceoff & perc_total
					$sql2 = "UPDATE `tbl_tda_bestdancer_data` SET perc_danceoff='$perc_danceoff', perc_total='$perc_total',danceoff_max='$maxtotal',groupid='$groupid' WHERE id=".$row["tdabdid"]." LIMIT 1";
					$res2 = mysql_query($sql2) or die(mysql_error());
				}
			}
			++$groupid;
		}//each gender
	}// each ad
//exit();
	foreach($adids as $adid) {
		$adname = db_one("name","tbl_age_divisions","id=$adid");
		foreach($genders as $gender) {
			$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.perc_total, tbl_tda_bestdancer_data.routineid, tbl_studios.name AS studioname, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_routines ON tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid WHERE tbl_tda_bestdancer_data.iscompeting=1 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.agedivisionid=$adid AND tbl_profiles.gender='$gender' AND tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid ORDER BY tbl_tda_bestdancer_data.perc_total DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);
					$allshit[$adname][ucfirst($gender)][] = $row;
				}
			}
		}//each gender
	}// each ad

	//calculate some shit
	foreach($allshit as $agediv=>$cggenders) {
		foreach($cggenders as $cggender=>$routines) {
			$place = 1;
			$lastperc = 0;
			foreach($routines as $rkey=>$routine) {
				if($routine["perc_total"] != $lasttotal) {
					$allshit[$agediv][$cggender][$rkey]["place"] = $place;
					$lastperc = $routine["perc_total"];
					++$place;
				}
				else {
					//if percs match give same score
					if($routine["perc_total"] == $lastperc) {
						$allshit[$agediv][$cggender][$rkey]["place"] = $place-1;
						$lastperc = $routine["perc_total"];
					}
					//otherwise 2nd place gets next place#
					else {
						$allshit[$agediv][$cggender][$rkey]["place"] = $place;
						++$place;
						$lastpercl = $routine["perc_total"];
					}
				}
				//update round3_place
				$sql2 = "UPDATE `tbl_tda_bestdancer_data` SET round3_place='".$allshit[$agediv][$cggender][$rkey]["place"]."' WHERE id=".$routine["tdabdid"]." LIMIT 1";
				$res2 = mysql_query($sql2) or die(mysql_error());
			}
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>TDA Round 3</title>
		<style type="text/css">
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.sched_table {
/*  				width: 100%; */
				margin-top:8px;
			}

			.sched_table tr td {
				border-left: 1px solid #000000;
				font-size: 8pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
			}

			.tline {
				padding: 1px 0;
				text-align: left;
			}
		</style>
	</head>
	<body>
		<div style="width: 952px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-family:'Trajan Pro';font-size:20pt;"><?php print($eventid == 14) ? "TDA Best Dancers" : "24SEVEN Non-Stop Dancers"; ?> : Round 3 (Solo + Audition + Dance-Off)</div>
						<div style="font-size:16px;font-family:Unplug;"><?php print($citydata[0]["venue_name"]); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;font-size:14px;font-family:Unplug;">
						<?php print($title_date); ?>
					</td>
				</tr>
			</table>
		<?php
			foreach($allshit as $agediv=>$cggenders) {
				foreach($cggenders as $cggender=>$routines) {
		?>

			<div style="color:#AA0000;font-size:17px;font-style:italic;margin-top:15px;"><?=$agediv." &bull; ".$cggender;?></div>
			<table cellpadding="0" cellspacing="0" class="sched_table">
				<tr>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:80px;">Place</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:200px;">TOTAL SCORE</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:330px;">Studio</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:350px;border-right:1px solid #000000;">Dancer</td>
				</tr>

				<?php	foreach($routines as $routine) { ?>

				<tr>
					<td class="tline" style="border-bottom:1px solid #000000;text-align:center;"><?php print($routine["place"]);?></td>
					<td class="tline" style="border-bottom:1px solid #000000;text-align: center;"><?php print($routine["perc_total"]."%");?></td>
					<td class="tline" style="border-bottom:1px solid #000000;padding-left:2px;"><?php print($routine["studioname"]);?></td>
					<td class="tline" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:2px;"><?php print($routine["fname"]." ".$routine["lname"]);?></td>
				</tr>

				<?php	} ?>
			</table>
		<?		}
			}
		?>

		</div>
	</body>
</html>