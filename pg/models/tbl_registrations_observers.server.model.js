"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_registrations_observers = sequelize.define('tbl_registrations_observers', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      }
    },
    observer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_observers',
        key: 'id'
      }
    },
    workshop_level_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_workshop_levels',
        key: 'id'
      }
    },
    fees: {
      type: DataTypes.JSONB
    },
    date_observer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_observers',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_registrations_observers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      tbl_registrations_observers.belongsTo(models.tbl_observers, {foreignKey: 'observer_id', as: 'observer'});
      tbl_registrations_observers.belongsTo(models.date_observers, {foreignKey: 'date_observer_id', as: 'date_observer'});
      tbl_registrations_observers.belongsTo(models.tbl_workshop_levels, {foreignKey: 'workshop_level_id', as: 'workshop_level'});
    }
  });
  return tbl_registrations_observers;
};