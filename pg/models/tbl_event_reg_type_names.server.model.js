"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_event_reg_type_names = sequelize.define('tbl_event_reg_type_names', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_event_reg_type_names;
};