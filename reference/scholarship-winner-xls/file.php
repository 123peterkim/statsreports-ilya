<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$cityname = strtolower(str_replace(array(" ","-",","),"",db_one("city","tbl_tour_dates","id=$tourdateid")));
	$dancers = array();

	$sql = "SELECT tbl_date_scholarships.id AS scholid, tbl_date_scholarships.winner, tbl_date_scholarships.facultyid, tbl_date_scholarships.profileid, tbl_date_scholarships.code, tbl_date_dancers.scholarship_code, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.studioid, tbl_studios.name AS studioname, tbl_scholarships.name AS scholname, CONCAT(tbl_staff.fname, ' ', tbl_staff.lname) AS facultyname FROM `tbl_date_scholarships` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_scholarships.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_profiles.studioid LEFT JOIN tbl_scholarships ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid LEFT JOIN tbl_staff ON tbl_staff.id=tbl_date_scholarships.facultyid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_date_scholarships.profileid WHERE tbl_date_scholarships.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid='$tourdateid' ORDER BY tbl_scholarships.report_order ASC, tbl_date_scholarships.winner ASC, tbl_profiles.lname ASC";

	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
		//	$row["studioid"] = db_one("studioid","tbl_date_dancers","profileid='".$row["profileid"]."' AND tourdateid='$tourdateid'");
		//	$row["studioname"] = str_replace(",","",stripslashes(str_replace("&amp;","&",db_one("name","tbl_studios","id='".$row["studioid"]."'"))));
			$row["studioname"] = str_replace(",","",stripslashes(str_replace("&amp;","&",$row["studioname"])));
			$dancers[] = $row;
		}
	}
	//	print_r($dancers);exit();
	if(count($dancers) > 0) {

			//create excel shit
			$workbook = new PHPExcel();
			$workbook->setActiveSheetIndex(0);

			//set global font & font size
			$workbook->getDefaultStyle()->getFont()->setName('Arial');

			//col widths
			$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

			$dataArray = array();
			$dataArray[] = array("FIRST","LAST","STUDIO","SCHOL. NUM","TYPE","WINNER","CRITIQUE CODE","FACULTY");
			foreach($dancers as $dancer) {
				$winner = $dancer["winner"] == 1 ? "Winner" : "Runner-Up";
				$dataArray[] = array($dancer["fname"],$dancer["lname"],$dancer["studioname"],$dancer["scholarship_code"],$dancer["scholname"],$winner,$dancer["code"],$dancer["facultyname"]);
			}
			$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

			$outputFileType = 'Excel5';
			$mk = time();
			$somekindofrandomstr = "$cityname"."_schol_winners_$tourdateid".substr($mk,6,5);
			$outputFileName = "../../temp/$somekindofrandomstr.xls";
			$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
			$objWriter->save($outputFileName);
			chmod($outputFileName,777);
			unset($workbook);
			unset($objWriter);

			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=".basename($outputFileName));
			header("Content-type: application/octet-stream");
			header("Content-Transfer-Encoding: binary");
			readfile($outputFileName);
	}

	exit();
?>