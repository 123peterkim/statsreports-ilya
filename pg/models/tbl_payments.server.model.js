"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_payments = sequelize.define('tbl_payments', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    mybtf_user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_mybtf_users',
        key: 'id'
      }
    },
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      }
    },
    payment_method_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_payment_methods',
        key: 'id'
      }
    },
    amount: {
      type: DataTypes.REAL
    },
    transaction_id: {
      type: DataTypes.STRING
    },
    ip_address: {
      type: DataTypes.STRING
    },
    timestamp: {
      type: DataTypes.DATE
    },
    discount: {
      type: DataTypes.REAL
    },
    last_four: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false,
    associate: function(models) {
      tbl_payments.belongsTo(models.tbl_mybtf_users, {foreignKey: 'mybtf_user_id', as: 'mybtf_user'});
      tbl_payments.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
      tbl_payments.belongsTo(models.tbl_payment_methods, {foreignKey: 'payment_method_id', as: 'payment_method'});
    }
  });
  return tbl_payments;
};