"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_events = sequelize.define('tbl_events', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_types',
				key: 'id'
			}
		},
		current_season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		link: {
			type: DataTypes.STRING
		},
		web_home_webcast_banner: {
			type: DataTypes.INTEGER
		},
		facebook_link: {
			type: DataTypes.STRING
		},
		age_as_of_year: {
			type: DataTypes.INTEGER
		},
		in_top_menu: {
			type: DataTypes.INTEGER
		},
		workshop_only: {
			type: DataTypes.INTEGER
		},
    mybtf_order: {
      type: DataTypes.INTEGER
    }
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_events.belongsTo(models.tbl_seasons, {foreignKey: 'current_season_id', as: 'current_season'});
			tbl_events.belongsTo(models.tbl_event_types, {foreignKey: 'event_type_id', as: 'event_type'});
		}
	});
	return tbl_events;
};