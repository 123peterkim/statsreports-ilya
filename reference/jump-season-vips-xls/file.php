<?php
	ini_set("memory_limit","1024M");
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");
	$tourdateid = intval($_REQUEST["tourdateid"]);

	if(!$tourdateid > 0)
		exit();

	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$safeevent = strtolower(str_replace(array(" ",",","\'"),"",db_one("name","events","id='$eventid'")));
	$seasontourdateidsa = db_get("id","tbl_tour_dates","seasonid='$seasonid' AND eventid='$eventid'");
	foreach($seasontourdateidsa as $stdid) {
		$seasontourdateids[] = $stdid["id"];
	}


	$data = array();

	/*
	$sids = "";
	if($eventid == 7)
		$sids = join("', '",array(1,2,3,4,5,6,7,8));
	if($eventid == 8)
		$sids = join("', '",array(15,16,17,18,19,20,21,22));
	if($eventid == 18)
		$sids = join("', '",array(39,40,41,42,43,44,45,46));
	*/
	foreach($seasontourdateids as $tdid) {
	//	$sql = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.age, tbl_workshop_levels_$seasonid.name AS workshoplevelname, tbl_studios.name AS studioname, tbl_profiles.address, tbl_profiles.city, tbl_profiles.state, tbl_profiles.zip, tbl_countries.name AS country, tbl_profiles.phone, tbl_profiles.email, tbl_scholarships.name AS scholarshipname, tbl_date_scholarships.winner, tbl_date_dancers.tourdateid FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid LEFT JOIN tbl_workshop_levels_$seasonid ON tbl_workshop_levels_$seasonid.id = tbl_date_dancers.workshoplevelid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_profiles.studioid LEFT JOIN tbl_date_scholarships ON tbl_date_scholarships.profileid=tbl_date_dancers.profileid LEFT JOIN tbl_scholarships ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid LEFT JOIN tbl_countries ON tbl_countries.id=tbl_profiles.countryid WHERE tbl_date_dancers.tourdateid='$tdid' AND tbl_date_scholarships.scholarshipid IN ('$sids') ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";

		/*$sql = "SELECT
			tbl_date_scholarships.id AS dsid,
			tbl_date_scholarships.profileid,
			tbl_date_scholarships.scholarshipid,
			tbl_date_scholarships.datedancerid,
			tbl_date_scholarships.winner,
			tbl_scholarships.name AS scholarshipname,
			tbl_profiles.fname,
			tbl_profiles.lname
		FROM `tbl_date_scholarships`
			LEFT JOIN tbl_scholarships
				ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid
			LEFT JOIN tbl_profiles
				ON tbl_profiles.id=tbl_date_scholarships.profileid
		WHERE tbl_date_scholarships.tourdateid='$tdid'
		ORDER BY tbl_date_scholarships.scholarshipid ASC, tbl_profiles.lname ASC, tbl_profiles.fname ASC";
		*/

		$sql = "SELECT
			tbl_date_scholarships.id AS dsid,
			tbl_date_scholarships.profileid,
			tbl_date_scholarships.scholarshipid,
			tbl_date_scholarships.datedancerid,
			tbl_date_scholarships.winner,
			tbl_scholarships.name AS scholarshipname,
			tbl_date_dancers.workshoplevelid,
			tbl_workshop_levels_$seasonid.name AS workshoplevelname,
			tbl_profiles.fname,
			tbl_profiles.lname,
			tbl_studios.name AS studioname,
			tbl_studios.email AS studioemail,
			tbl_tour_dates.city
		FROM `tbl_date_scholarships`
			LEFT JOIN tbl_scholarships
				ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid
			LEFT JOIN tbl_date_dancers
				ON tbl_date_dancers.profileid=tbl_date_scholarships.profileid
			LEFT JOIN tbl_profiles
				ON tbl_profiles.id=tbl_date_scholarships.profileid
			LEFT JOIN tbl_studios
				ON tbl_studios.id=tbl_date_dancers.studioid
			LEFT JOIN tbl_tour_dates
				ON tbl_tour_dates.id=tbl_date_scholarships.tourdateid
			LEFT JOIN tbl_workshop_levels_$seasonid
				ON tbl_workshop_levels_$seasonid.id=tbl_date_dancers.workshoplevelid
		WHERE tbl_date_scholarships.tourdateid='$tdid'
			AND tbl_date_dancers.tourdateid='$tdid'
		ORDER BY tbl_date_scholarships.scholarshipid ASC, tbl_profiles.lname ASC, tbl_profiles.fname ASC";

	//	print($sql);exit();

		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$profileshit = db_get("*","tbl_profiles","id='".$row["profileid"]."'");
				unset($profileshit[0]["fname"]);
				unset($profileshit[0]["lname"]);
				$row = array_merge($profileshit[0],$row);
				$row["state_abbr"] = db_one("abbreviation","tbl_states","name='".$row["state"]."' OR abbreviation='".$row["state"]."'");
				$row["country"] = db_one("name","tbl_countries","id='".$row["countryid"]."'");
				$row["winner"] = $row["winner"] == 1 ? "Yes" : "No";
				$data[] = $row;
			}
		}
	}

	//create excel shit
	$workbook = new PHPExcel();
	$workbook->setActiveSheetIndex(0);

	//set global font & font size
	$workbook->getDefaultStyle()->getFont()->setName('Arial');

	//col widths
	$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$workbook->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

	$dataArray = array();
	$dataArray[] = array("First Name","Last Name","Tour City","Workshop Lvl","Studio","Scholarshp Type","Winner","Address","City","State","Zip","Country","Phone","Email","Studio E-Mail");
	foreach($data as $studio) {
		$dataArray[] = array($studio["fname"],$studio["lname"],$studio["tourdate"],$studio["workshoplevelname"],stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["studioname"]))),$studio["scholarshipname"],$studio["winner"],$studio["address"],$studio["city"],$studio["state_abbr"],$studio["zip"],$studio["country"],$studio["phone"],$studio["email"],$studio["studioemail"]);
	}
	$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

	$outputFileType = 'Excel5';
	$mk = time();
	if($eventid == 7)
		$somekindofrandomstr = $safeevent."_VIP_Season_ToDate_$seasonid_".substr($mk,6,5);
	if($eventid == 8)
		$somekindofrandomstr = $safeevent."_BO_Season_ToDate_$seasonid_".substr($mk,6,5);
	if($eventid == 18)
		$somekindofrandomstr = $safeevent."_NSD_Season_ToDate_$seasonid_".substr($mk,6,5);
	$outputFileName = "../../temp/$somekindofrandomstr.xls";
	$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
	$objWriter->save($outputFileName);
	chmod($outputFileName,777);
	unset($workbook);
	unset($objWriter);

	header("Cache-Control: public");
	header("Content-Description: File Transfer");
	header("Content-Disposition: attachment; filename=".basename($outputFileName));
	header("Content-type: application/octet-stream");
	header("Content-Transfer-Encoding: binary");
	readfile($outputFileName);
?>
