"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_mybtf_user_has_teachers = sequelize.define('tbl_mybtf_user_has_teachers', {
    teacher_id: {
      type: DataTypes.STRING,
      references: {
        model: 'tbl_teachers',
        key: 'id'
      },
      primaryKey: true
    },
    mybtf_user_id: {
      type: DataTypes.STRING,
      references: {
        model: 'tbl_mybtf_users',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {

    }
  });
  return tbl_mybtf_user_has_teachers;
};