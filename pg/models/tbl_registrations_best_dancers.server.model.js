"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_best_dancers = sequelize.define('tbl_registrations_best_dancers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		competing: {
			type: DataTypes.INTEGER
		},
		jacket: {
			type: DataTypes.REAL
		},
		routine: {
			type: DataTypes.STRING
		},
		choreographer: {
			type: DataTypes.STRING
		},
		photo_file: {
			type: DataTypes.STRING
		},
		jacket_only: {
			type: DataTypes.INTEGER
		},
		fee: {
			type: DataTypes.REAL
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_registrations_best_dancers;
};