"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_dancers = sequelize.define('tbl_registrations_dancers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		event_reg_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_reg_types',
				key: 'id'
			}
		},
		workshop_level_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_workshop_levels',
				key: 'id'
			}
		},
		promo_code_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes',
				key: 'id'
			}
		},
		one_day: {
			type: DataTypes.BOOLEAN
		},
		attended_reg: {
			type: DataTypes.BOOLEAN
		},
		is_commuter: {
			type: DataTypes.INTEGER
		},
		non_commuter: {
			type: DataTypes.STRING
		},
		classes_only: {
			type: DataTypes.INTEGER
		},
		fees: {
			type: DataTypes.JSONB
		},
		scholarship: {
			type: DataTypes.JSONB
		},
		best_dancer: {
			type: DataTypes.INTEGER
		},
		event_teacher: {
			type: DataTypes.INTEGER
		},
    has_scholarship: {
      type: DataTypes.BOOLEAN
    },
		date_dancer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_dancers',
        key: 'id'
      }
		},
    vip_type_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_vip_types',
        key: 'id'
      }
    }
	}, {
		timestamps: false,
		associate: function(models) {
      tbl_registrations_dancers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      tbl_registrations_dancers.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
      tbl_registrations_dancers.belongsTo(models.date_dancers, {foreignKey: 'date_dancer_id', as: 'date_dancer'});
      tbl_registrations_dancers.belongsTo(models.tbl_workshop_levels, {foreignKey: 'workshop_level_id', as: 'workshop_level'});
      tbl_registrations_dancers.belongsTo(models.tbl_vip_types, {foreignKey: 'vip_type_id', as: 'vip_type'});
    }
	});
	return tbl_registrations_dancers;
};