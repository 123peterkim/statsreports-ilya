"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_tda_award_types = sequelize.define('tbl_tda_award_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		has_places: {
			type: DataTypes.INTEGER
		},
		type: {
			type: DataTypes.STRING
		},
		report_order: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_tda_award_types;
};