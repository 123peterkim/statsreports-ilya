"use strict";

module.exports = function(sequelize, DataTypes) {
	var contact_info_studio = sequelize.define('contact_info_studio', {
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			},
      primaryKey: true
		},
    contact_info_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_contact_info',
        key: 'id'
      },
      primaryKey: true
    }
	}, {
		timestamps: false,
		associate: function(models) {
		}
	});
	return contact_info_studio;
};