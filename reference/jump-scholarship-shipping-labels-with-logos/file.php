<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = intval(db_one("eventid","tbl_tour_dates","id=$tourdateid"));

	$dancers = Array();
	$sql = "SELECT tbl_date_scholarships.id AS datescholarshipid, tbl_date_scholarships.profileid, tbl_scholarships.name AS scholarshipname, tbl_date_scholarships.winner, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_scholarships` LEFT JOIN tbl_scholarships ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_scholarships.profileid WHERE tbl_date_scholarships.tourdateid=$tourdateid ORDER BY tbl_scholarships.report_order ASC, tbl_date_scholarships.winner ASC, tbl_profiles.lname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",db_one("name","tbl_studios","id='".db_one("studioid","tbl_date_dancers","profileid='".$row["profileid"]."' AND tourdateid='$tourdateid'")."'"))));
			$row["winnerstr"] = $row["winner"] == 0 ? "Runner-Up" : "Winner";
			$row["scholarship_code"] = db_one("scholarship_code","tbl_date_dancers","profileid='".$row["profileid"]."' AND tourdateid='$tourdateid'");
			$dancers[] = $row;
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Mailing Labels</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-family: 'OfficinaSansCTT', Tahoma, sans-serif;
			}
			.sl_table {
/* 				margin-left:30px; */
			}
			.sl_table tr td {
				padding: 0 0 0 0px;
				width: 340px;
				text-align: left;
			}

			.labelbg {
				background: url("mailing_label_bg.png") no-repeat top center;width:300px;height:149px;
				font-family: 'Blue Highway D Type', Arial, sans-serif;
				font-size:16px;
				padding-top:30px;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
		<?php
			$count = 0;
			for($i=0;$i<count($dancers);$i+=2) { ?>
		<div style="width:720px;padding-top:30px;">
			<table cellpadding="0" cellspacing="0" class="sl_table" <?php if($count+1 == 3) { print("style='page-break-after:always;padding-bottom:0;'"); }?>>
				<tr>
					<td style="vertical-align: top;">
						<?php if($dancers[$i]) { ?>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td style="text-align:left;vertical-align:top;">
										<span style="font-family:Regal Box;font-size:52px;color:#FF0000;">JUMP</span> <!-- text-shadow:0 1px 1px rgba(0, 0, 0, 1); -->
									</td>
									<td style="color:#555555;text-align:right;vertical-align:top;font-family:'Blue Highway D Type',Arial,sans-serif;">
										<?php print($dancers[$i]["scholarship_code"]); ?>
									</td>
								</tr>
							</table>

							<div style="font-family:'festus!';font-size:18px;margin-bottom:2px;line-height:16px;">the alternative convention</div>
							<span style="font-size:10px;line-height:9px;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601<br/>
								818.432.4395
							</span>
							<div class="labelbg">
								<table cellpadding="0" cellspacing="0">
									<tr>
<!-- 										<td style="width:35px;vertical-align:top;">&nbsp;</td> -->
										<td style="vertical-align:top;font-family:'Blue Highway D Type',Arial,sans-serif;">
											Congratulations <?php print($dancers[$i]["fname"]." ".$dancers[$i]["lname"]."!
											<br/>".
											$dancers[$i]["studioname"]."
											<br/>".
											$dancers[$i]["scholarshipname"]." ".$dancers[$i]["winnerstr"]);?>
										</td>
									</tr>
								</table>
							</div>
						<?php } ?>
					</td>
					<td style="vertical-align: top;padding-left:90px;">
						<?php if($dancers[$i+1]) { ?>
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td style="text-align:left;vertical-align:top;">
										<span style="font-family:Regal Box;font-size:52px;color:#FF0000;">JUMP</span> <!-- text-shadow:0 1px 1px rgba(0, 0, 0, 1); -->
									</td>
									<td style="color:#555555;text-align:right;vertical-align:top;font-family:'Blue Highway D Type',Arial,sans-serif;">
										<?php print($dancers[$i+1]["scholarship_code"]); ?>
									</td>
								</tr>
							</table>
							<div style="font-family:'festus!';font-size:18px;margin-bottom:2px;line-height:16px;">the alternative convention</div>
							<span style="font-size:10px;line-height:9px;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601<br/>
								818.432.4395
							</span>
							<div class="labelbg">
								<table cellpadding="0" cellspacing="0">
									<tr>
<!-- 										<td style="width:35px;vertical-align:top;">&nbsp;</td> -->
										<td style="vertical-align:top;font-family:'Blue Highway D Type',Arial,sans-serif;">
											Congratulations <?php print($dancers[$i+1]["fname"]." ".$dancers[$i+1]["lname"]."!
											<br/>".
											$dancers[$i+1]["studioname"]."
											<br/>".
											$dancers[$i+1]["scholarshipname"]." ".$dancers[$i+1]["winnerstr"]);?>
										</td>
									</tr>
								</table>
							</div>
						<?php } ?>
					</td>
				</tr>
			</table>
		</div>
		<?php  ++$count;
			} ?>
	</body>