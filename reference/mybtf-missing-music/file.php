<?php

	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$compgroups = array("vips","prelims","finals");

	if(!($tourdateid > 0))
		exit();

	$city = db_one("city","tbl_tour_dates","id='$tourdateid'");

	foreach($compgroups as $compgroup) {
		$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.studioid, tbl_date_routines.number_".$compgroup." AS number, ".$compgroup."_has_a AS has_a, tbl_routines.name AS routinename, tbl_studios.name AS studioname FROM tbl_date_routines LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_routines.studioid WHERE tbl_date_routines.$compgroup=1 AND tbl_date_routines.tourdateid='$tourdateid' AND uploaded=0 ORDER BY tbl_studios.name ASC, tbl_routines.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$row["dispnumber"] = $row["has_a"] == 1 ? $row["number"].".a" : $row["number"];
				$missings[$compgroup][] = $row;
			}
		}
	}

?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
	</head>
	<body>
		<div style="width:720px;">
			<div style="font-family:Unplug;font-size:20px;"><?php print($city); ?> MyBTF Missing Music</div>
			<table cellpadding="0" cellspacing="0" style="margin-top:15px;">
				<tr>
					<td class="thead" style="width: 250px;">Studio</td>
					<td class="thead" style="width: 250px;">Routine</td>
					<td class="thead" style="width: 100px">Number</td>
					<td class="thead" style="width: 115px;border-right: 1px solid #000000;">Comp. Group</td>
				</tr>
			<?php foreach($missings as $compgroup=>$routines) {
					foreach($routines as $routine) { ?>
				<tr>
					<td class="tbody"><?php print($routine["studioname"]); ?></td>
					<td class="tbody"><?php print($routine["routinename"]); ?></td>
					<td class="tbody"><?php print("#".$routine["dispnumber"]); ?></td>
					<td class="tbody" style="border-right: 1px solid #000000;"><?php print($compgroup); ?></td>
				</tr>
			<?php 	}
				}
			?>
			</table>
		</div>
	</body>
</html>