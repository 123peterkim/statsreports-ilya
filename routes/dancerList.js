var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var showAll = (req.query.showall === 1);
        
        util.getAllDateDancersList(tourDateId, showAll)
        .then(function(results) {
            
            var dancerListData = {
                dancers: []
            };

            _.forEach(results, function(dateDancer) {
                if(dateDancer.registrations_dancer) {
                    var person = dateDancer.registrations_dancer.dancer.person;
                    dancerListData.dancers.push({
                        fname: person.fname,
                        lname: person.lname
                    });
                }
            });

            res.render('reports/dancer-list', dancerListData);
        })
        .catch(function(err) {
            console.log('ERROR', err);
            res.send(err);
        })

    }
}