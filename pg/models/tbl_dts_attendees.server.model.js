"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_dts_attendees = sequelize.define('tbl_dts_attendees', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		dts_registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dts_registrations',
				key: 'id'
			}
		},
		dts_reg_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dts_reg_types',
				key: 'id'
			}
		},
		promo_code_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		fees: {
			type: DataTypes.JSONB
		},
		notes: {
			type: DataTypes.TEXT
		},
		canceled: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_dts_attendees;
};