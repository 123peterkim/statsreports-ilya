"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_songs = sequelize.define('tbl_songs', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		title: {
			type: DataTypes.INTEGER
		},
		artist: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_songs;
};