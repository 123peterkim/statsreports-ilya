"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_age_divisions = sequelize.define('tbl_age_divisions', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		level_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_levels',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
      references: {
        model: 'tbl_events',
        key: 'id'
      }
		},
		range: {
			type: DataTypes.INTEGER
		},
		minimum_age: {
			type: DataTypes.INTEGER
		},
		recommended_age: {
      type: DataTypes.INTEGER
    }
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_age_divisions.belongsTo(models.tbl_levels, {foreignKey: 'level_id', as: 'level'});
      tbl_age_divisions.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});
    }
	});
	return tbl_age_divisions;
};