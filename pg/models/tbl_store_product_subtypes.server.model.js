"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_product_subtypes = sequelize.define('tbl_store_product_subtypes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		product_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_product_types',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_product_subtypes;
};