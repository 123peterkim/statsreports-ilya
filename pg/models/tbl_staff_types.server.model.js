"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_staff_types = sequelize.define('tbl_staff_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_staff_types;
};