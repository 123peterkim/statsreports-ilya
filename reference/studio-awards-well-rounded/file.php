<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));


	// Get eligible studios
	$sql = "SELECT tbl_date_routines.studioid,
			   tbl_performance_divisions.name AS division,
			   tbl_studios.name AS studioname

			FROM tbl_date_routines

			LEFT JOIN tbl_performance_divisions
			ON tbl_date_routines.perfcategoryid = tbl_performance_divisions.id

			LEFT JOIN tbl_studios
			ON tbl_studios.id = tbl_date_routines.studioid

			WHERE tbl_date_routines.routinecategoryid > 2 AND
				  tbl_date_routines.tourdateid = $tourdateid

			ORDER BY tbl_studios.name ASC";

	$res = mysql_query($sql) or die(mysql_error());

	while($row = mysql_fetch_assoc($res)) {
		$key = $row['studioid'];
		$studios[$key]['studioname'] = $row['studioname'];

		if(!isset($studios[$key]['count']))
			$studios[$key]['count'] = 0;

		if(!is_array($studios[$key]['divisions']) || !in_array($row['division'], $studios[$key]['divisions']) ) {
			$studios[$key]['divisions'][] = $row['division'];
			$studios[$key]['count']++;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 11px;
			}

			.rtable {
				width:850px;
			}

			.rtable tbody tr td {
				font-size: 10pt;
				padding: 2px;
			}

			thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Well Rounded Studios</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print($venue); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:30px;margin-top:20px">
				<thead>
					<td style="width:200px;border-right:1px solid #000000;font-size:10pt">Studio</td>
					<td style="width:200px;border-right:1px solid #000000;font-size:10pt">Number of Divisions</td>
					<td style="width:200px;border-right:1px solid #000000;font-size:10pt">Performance Divisions</td>
				</thead>
				<tbody>
				<?php
				if($studios):
					foreach($studios as $studioid=>$studio):
						// Create list of performance divisions
						$divlist = implode(", ", $studio['divisions']);
				?>

						<tr>
							<td><?=$studio['studioname'];?></td>
							<td style="text-align:center;"><?=$studio['count'];?></td>
							<td><?=$divlist;?></td>
						</tr>
					<?php
					endforeach;
				endif;
				?>
				</tbody>
			</table>
		</div>
	</body>
</html>