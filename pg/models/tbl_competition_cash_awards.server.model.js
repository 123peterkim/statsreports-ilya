"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_competition_cash_awards = sequelize.define('tbl_competition_cash_awards', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		high_score_type: {
			type: DataTypes.STRING
		},
		place: {
			type: DataTypes.INTEGER
		},
		amount: {
			type: DataTypes.INTEGER
		},
		description: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_competition_cash_awards;
};