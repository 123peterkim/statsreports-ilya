"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_online_scoring_data = sequelize.define('tbl_online_scoring_data', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		online_scoring_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_online_scoring',
				key: 'id'
			}
		},
		staff_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_staff',
				key: 'id'
			}
		},
		note: {
			type: DataTypes.TEXT
		},
		score: {
			type: DataTypes.INTEGER
		},
		data: {
			type: DataTypes.JSONB
		},
		not_friendly: {
			type: DataTypes.BOOLEAN
		},
		i_choreographed: {
			type: DataTypes.BOOLEAN
		},
		position: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_online_scoring_data.belongsTo(models.tbl_staff, {foreignKey: 'staff_id', as: 'staff'});
			tbl_online_scoring_data.belongsTo(models.tbl_online_scoring, {foreignKey: 'online_scoring_id', as: 'online_scoring'})
		}
	});
	return tbl_online_scoring_data;
};