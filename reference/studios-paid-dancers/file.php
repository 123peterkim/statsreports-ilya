<?php
	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());

	$numppl = intval($_GET["numppl"]);

	$start = intval($_GET["start"]);
	$end = intval($_GET["end"]);

	$dispdate = get_tourdate_dispdate($tourdateid);

	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$studioid = $row["studioid"];
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["studioname"] = stripslashes($row["studioname"]);
			unset($row["contacts"]);
			$row["independent"] = $row["independent"] == 1 ? "Yes" : "No";
			$row["studiocode"] = strlen($row["studiocode"]) > 0 ? $row["studiocode"] : "-";

			$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid AND fee > 0");
			$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid AND fee > 0");
			$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid AND fee > 0");
			$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid AND fee > 0");
			$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid AND fee > 0");
			$row["nubie_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=7 AND tourdateid=$tourdateid AND fee > 0");
			$row["peewee_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=9 AND tourdateid=$tourdateid AND fee > 0");
			$row["sidekick_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=10 AND tourdateid=$tourdateid AND fee > 0");
			$row["sumdancers"] = $row["senior_count"] + $row["teen_count"] + $row["junior_count"] + $row["mini_count"] + $row["jumpstart_count"] + $row["sidekick_count"] + $row["nubie_count"] + $row["peewee_count"];

			if($row["sumdancers"] >= $start && $row["sumdancers"] <= $end)
				$studios[] = $row;
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Georgia;font-size:20px;"><?php print($city); ?> Studios <span style="font-family:Georgia;font-size:14px;">(<?php print($start."-".$end); ?> paying dancers)</span></div>
			<span style="font-family:Georgia;font-size:14px;"><?php print($venue); ?> / <?php print($dispdate); ?></span>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
				<tr>
					<td class="thead" style="width:235px;">Studio</td>
					<td class="thead" style="width:120px;">Contact</td>
					<td class="thead" style="width:55px;">Indep.</td>
					<td class="thead" style="width:90px;">Dancer Count</td>
					<td class="thead" style="border-right: 1px solid #000000;width:190px;">Breakdown</td>
				</tr>
			<?php
				if(count($studios) > 0) {
					foreach($studios as $studio) { ?>
						<tr>
							<td class="tbody"><?php print($studio["studioname"]." (".$studio["studiocode"].")"); ?></td>
							<td class="tbody"><?php print($studio["contact"]); ?></td>
							<td class="tbody"><?php print($studio["independent"]); ?></td>
							<td class="tbody"><?php print($studio["sumdancers"]); ?></td>
							<td class="tbody" style="border-right: 1px solid #000000;"><?php print("Sr(".$studio["senior_count"].") Tn(".$studio["teen_count"].") Jr(".$studio["junior_count"].") Mi(".$studio["mini_count"].") ");
							if($eventid == 7)
								print("Js(".$studio["jumpstart_count"].")");
							if($eventid == 8)
								print("Nu(".$studio["nubie_count"].")");
							if($eventid == 14)
								print("PW(".$studio["peewee_count"].")");
							if($eventid == 18)
								print("SK(".$studio["sidekick_count"].")");
						?>
							</td>
						</tr>
			<?php 		}
				} ?>
			</table>
		</div>
	</body>
</html>