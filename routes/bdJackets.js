var Excel = require('exceljs');
var _ = require('lodash');
var fs = require('fs');
var util = require('../reference/util/util');
var bdJacketData = require('../reference/bd-jacket-xls/data.json');

module.exports = {
    bdJacketSpreadsheet: function(req, res) {
        // query params
        var tourDateId = req.query.tourdateid;

        var wb = new Excel.Workbook();
        var ws = wb.addWorksheet('Worksheet');
        var dancers = bdJacketData.dancers;

        // TODO: setup auto width
        // https://stackoverflow.com/a/16762003
        ws.columns = [
            { header: 'First Name', key: 'fname', width: util.setWidthAuto(dancers, 'fname', 'First Name'), style: { font: { name: 'Arial' } } },
            { header: 'Last Name', key: 'lname', width: util.setWidthAuto(dancers, 'lname', 'Last Name'), style: { font: { name: 'Arial' } } },
            { header: 'Age', key: 'age', width: util.setWidthAuto(dancers, 'age', 'Age'), style: { font: { name: 'Arial' } } },
            { header: 'Reg Workshop Lvl', key: 'regwl', width: util.setWidthAuto(dancers, 'regwl', 'Reg Workshop Lvl'), style: { font: { name: 'Arial' } } },
            { header: 'Real Age Workshop Lvl', key: 'agewl', width: util.setWidthAuto(dancers, 'agewl', 'Real Age Workshop Lvl'), style: { font: { name: 'Arial' } } },
            { header: 'Studio', key: 'studio_name', width: util.setWidthAuto(dancers, 'studio_name', 'Studio'), style: { font: { name: 'Arial' } } },
            { header: 'Jacket Size', key: 'jacket_size', width: util.setWidthAuto(dancers, 'jacket_size', 'Jacket Size'), style: { font: { name: 'Arial' } } },
            { header: 'Jacket Name', key: 'jacket_name', width: util.setWidthAuto(dancers, 'jacket_name', 'Jacket Name'), style: { font: { name: 'Arial' } } }

        ];

        _.forEach(dancers, function(dancer) {            
            ws.addRow(dancer);
        });
        
        var filename = 'public/'+Date.now();

        wb.xlsx.writeFile(filename)
        .then(function() {
            res.download(filename, "TDA_Jackets.xlsx", function(err) {
                if(!err) fs.unlink(filename);
            });
        });
    }
}