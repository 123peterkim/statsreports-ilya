List of tech
-
1. HTML5
2. CSS3
4. PHP
5. Node.js
6. Express.js
7. SQL (MySQL & PostgreSQL)
8. Handlebars.js
9. Lodash
10. Sequelize
11. Async.js
12. Exceljs


To setup & start:
- 
1. Run npm install
* If you have issues with npm install, change your node version to 6.11.0 and npm version to 3.10.10
2. Run DEBUG=statsreports:* npm start
3. Use PostgreSQL client (like phpadmin or datagrip) and connect to the staging database
* All information you need for connection will be in the /pg/sequelize-connect.js file
4. Sequelize model for all tables are in /pg/models

You will be doing the scholarship-list report:
- 
1. Look for the report in /reference/scholarship-list and open it.
2. You will see the original PHP file in the folder.  This is the file you will be converting into JS using handlebars.  <b>IMPORTANT: When converting the PHP file, every line / logic must be converted</b>
3. You will also see a an empty JSON file.  You will not be using this file.
4. The new view with handlebars will need to go into /views/reports.
5. The logic for the report will need to go into /routes/index.js.
7. Refer the link in url.txt to see a live version of what the report looks like. 
8. Refer to /reference/util/hbs.js to add new handlebars helpers that you need (and you may use the ones I've already created).
9. Refer to /reference/util/util.js to add new javascript logic that you will be re-using.
10. Port SQL queries from PHP to node.js using Sequelize
* The tables in the old MySQL database and the new Postgres database are slightly different.  In the scholarship-list report, you will need to use the following databases: date_dancers (old table name is tbl_date_dancers), tbl_studios, tbl_dancers (old table name is tbl_profiles), tbl_persons
11. A lot of the other reports are already completed so you can reference them for help