"use strict";

module.exports = function(sequelize, DataTypes) {
	var staff_playlists = sequelize.define('staff_playlists', {
		staff_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_staff',
				key: 'id'
			},
			primaryKey: true
		},
		date_playlist_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_playlists',
				key: 'id'
			},
			primaryKey: true
		}
	}, {
		timestamps: false,
		associate: function(models) {
			}
	});
	return staff_playlists;
};