"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_workshop_schedule_time_slots = sequelize.define('date_workshop_schedule_time_slots', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    time: {
      type: DataTypes.DATE
    },
    duration: {
      type: DataTypes.INTEGER
    },
    tour_date_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_tour_dates',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function (models) {
      date_workshop_schedule_time_slots.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
      date_workshop_schedule_time_slots.hasMany(models.date_workshop_schedule_blocks, {foreignKey: 'date_workshop_schedule_time_slot_id', as: 'blocks'});
    }
  });
  return date_workshop_schedule_time_slots;
};