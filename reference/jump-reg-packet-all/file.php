<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$data = array();
	$studioids = array();
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$stateid = db_one("stateid","tbl_tour_dates","id=$tourdateid");
	$state = db_one("abbreviation","tbl_states","id=$stateid");
	$dispdate = get_tourdate_dispdate($tourdateid);

	if(intval($_GET["all"]) == 1) {
		$sql = "SELECT tbl_date_studios.studioid,tbl_studios.name AS studioname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$studioids[] = $row["studioid"];
			}
		}
	}
	else {
		$studioids[] = intval($_GET["studioid"]);
	}

	foreach($studioids as $studioid) {
		$datestudioid = intval(db_one("id","tbl_date_studios","tourdateid=$tourdateid AND studioid=$studioid"));
		$sa = array();
		//get all datestudioids <> studioid
		$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studiocode, tbl_date_studios.independent, tbl_date_studios.invoice_note, tbl_date_studios.studioid AS studioid, tbl_studios.name AS studioname, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_states.abbreviation, tbl_studios.zip, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid LEFT JOIN tbl_states ON tbl_states.name = tbl_studios.state WHERE tbl_date_studios.tourdateid=$tourdateid AND tbl_date_studios.id=$datestudioid";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
			//	$studioid = $row["studioid"];
				$contacts = json_decode($row["contacts"],true);
				$row["studioname"] = stripslashes($row["studioname"]);
				$row["studiocode"] = strlen($row["studiocode"]) > 0 ? $row["studiocode"] : "-";
				$row["contact_fname"] = $contacts[0]["fname"];
				$row["contact_lname"] = $contacts[0]["lname"];

				//workshop registrants
				$row["teacher_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=1 AND tourdateid=$tourdateid");
				$row["senior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=2 AND tourdateid=$tourdateid");
				$row["teen_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=3 AND tourdateid=$tourdateid");
				$row["junior_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=4 AND tourdateid=$tourdateid");
				$row["mini_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=5 AND tourdateid=$tourdateid");
				$row["jumpstart_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=6 AND tourdateid=$tourdateid");
				$row["observer_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=8 AND tourdateid=$tourdateid");
				$row["observer2_count"] = db_one("COUNT(id)","tbl_date_dancers","studioid=$studioid AND workshoplevelid=12 AND tourdateid=$tourdateid");

				$row["dancer_count"] = $row["senior_count"] + $row["teen_count"] + $row["junior_count"] + $row["mini_count"] + $row["jumpstart_count"] + $row["observer_count"] + $row["observer2_count"];

			//COMPETITION ROUTINES
				$row["competition_routine_count"] = db_one("COUNT(id)","tbl_date_routines","tourdateid=$tourdateid AND studioid=$studioid AND tbl_date_routines.finals = 1");
				$sql3 = "SELECT tbl_date_routines.routineid AS routineid, tbl_date_routines.id AS dateroutineid, tbl_routines.name AS routinename, tbl_age_divisions.name AS agedivisionname, tbl_age_divisions.range, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_performance_divisions.name AS performancedivisionname, tbl_date_routines.fee, tbl_date_routines.finals, tbl_date_routines.number_finals, tbl_date_routines.finals_time, tbl_date_routines.finals_has_a FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id = tbl_date_routines.routineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id = tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id = tbl_date_routines.routinecategoryid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id = tbl_date_routines.perfcategoryid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=$studioid ORDER BY tbl_routines.name ASC";

				$res3 = mysql_query($sql3) or die(mysql_error());
				$competitionroutines = Array();
				if(mysql_num_rows($res3) > 0) {
					while($row3 = mysql_fetch_assoc($res3)) {
						$competitionroutines[] = $row3;
					}

					foreach($competitionroutines as $cr) {
						if($cr["finals"] == 1) {
							$cr["finals_date"] = date("g:i A / l (F jS)",$cr["finals_time"]);
							$cr["finals_dispnumber"] = $cr["finals_has_a"] == 1 ? $cr["number_finals"].".a" : $cr["number_finals"];
							$sa["finals"][$cr["finals_time"]] = $cr;
						}
					}

					if(count($sa["finals"]) > 0)
						ksort($sa["finals"],SORT_NUMERIC);
				}

				$row["compawards"] = json_decode(db_one("awards","tbl_date_schedule_competition","tourdateid=$tourdateid"),true);
				$row["competitionroutines"] = $sa;


			//dancer shit (workshop)
				$ao = implode("', '",array(1,2,3,4,5,6,8,12));
				$wtmp = array();
				$sql2 = "SELECT tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.gender, tbl_profiles.lname, tbl_profiles.age, tbl_date_dancers.workshoplevelid, tbl_workshop_levels_$seasonid.name AS workshoplevelname, tbl_date_dancers.scholarship_code, tbl_date_dancers.fee, tbl_date_dancers.one_day FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid LEFT JOIN tbl_workshop_levels_$seasonid ON tbl_workshop_levels_$seasonid.id = tbl_date_dancers.workshoplevelid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.studioid = $studioid AND tbl_date_dancers.workshoplevelid IN ('$ao') ORDER BY FIELD (tbl_date_dancers.workshoplevelid,'$ao'), tbl_profiles.lname ASC";
				$res2 = mysql_query($sql2) or die(mysql_error());
				$workshopregistrants = Array();
				$bestdancers = Array();
				if(mysql_num_rows($res2) > 0) {
					while($row2 = mysql_fetch_assoc($res2)) {
						$row2["workshoplevelname"] = $row2["workshoplevelid"] == 12 ? "JUMPstart Observer" : $row2["workshoplevelname"];
						$workshopregistrants[] = $row2;
					}
					foreach($workshopregistrants as $awrk) {
						$wtmp[$awrk["workshoplevelname"]][] = $awrk;
					}
				}

				$row["workshopregistrants"] = $wtmp;

				$data[$studioid] = $row;
			}
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Reg. Packet</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}

			#sum_table tr td {
				font-size:11px;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
	</head>
	<body>
<?php
if(count($data) > 0) {
	foreach($data as $studio) { ?>
		<!-- PAGE 1 -->
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-size:12px;">
							<span style="font-family:Regal Box;font-size:60px;color:#FF0000;">JUMP</span><br/>
							<div style="font-family:'festus!';font-size:26px;margin:5px 0 10px;line-height:11px;">the alternative convention</div>
							<span style="font-style:italic;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601
							</span>
						</div>
					</td>
					<td style="text-align:right;vertical-align:top;">
						<div style="margin-top:5px;color:#4D73C4;font-size:35px;font-family:'Blue Highway D Type';">
							Registration Packet
						</div>
					</td>
				</tr>
			</table>


			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;width:460px;">
						<div style="text-align:justify;padding-top:30px;">
							<span style="font-weight:bold;font-size:14px;font-style:italic;">Dear <?php print($studio["contact_fname"]); ?>,</span>
							<br/><br/>
							<?php if($studio["independent"] == 0) { ?>
							Welcome to JUMP!  I am so glad you joined us this year! I hope you’re ready for an action packed weekend!
							<br/><br/>
							Enclosed, please find a list of all participating dancers from your studio, wristbands, and class scholarship numbers for all your Mini, Junior, Teen and Senior dancers. Please be sure your students wear their scholarship number during classes all day Saturday and Sunday only.
							<br/><br/>
							That’s about it for now!  If you have any questions, please feel free to ask myself or Caitlin - we'd be happy to help!
							<br/><br/>
							Have a great weekend!
							<?php    }
								  else { ?>
							Welcome to JUMP!  I am so glad you joined us this year.  I hope you’re ready for an action packed weekend!
							<br/><br/>
							Enclosed, please find your wristband(s) and scholarship number (if registered as a Mini, Jr. Teen, or Sr.). Your wristband must be worn at all times during the workshop classes on Saturday and Sunday only.
							<br/><br/>
							That's about it for now!  If you have any questions, please feel free to ask myself or Caitlin - we'd be happy to help!
							<br/><br/>
							Have a great weekend!
							<?php 	  } ?>
							<br/><br/>
							Rock On,
							<table cellpadding="0" cellspacing="0">
								<tr>
									<td colspan="2">
										<img src="gilsig.png" alt="" />
									</td>
								</tr>
								<tr>
									<td style="text-align:left;width: 192px;">Gil Stroming, President<br/>Break The Floor Productions</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>


		</div>
		<!-- END PAGE 1 -->

		<!-- WORKSHOP REGISTRANTS -->
		<?php if(count($studio["workshopregistrants"]) > 0) { ?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-size:12px;">
							<span style="font-family:Regal Box;font-size:60px;color:#FF0000;">JUMP</span><br/>
							<div style="font-family:'festus!';font-size:26px;margin:5px 0 10px;line-height:11px;">the alternative convention</div>
							<span style="font-style:italic;">
								5446 Satsuma Ave.<br/>
								North Hollywood, CA 91601
							</span>
						</div>
					</td>
					<td style="text-align:right;vertical-align:top;">
						<div style="margin-top:5px;color:#4D73C4;font-size:35px;font-family:'Blue Highway D Type';">
							Registration Packet
						</div>
					</td>
				</tr>
			</table>


			<div style="margin-top:20px;font-size:35px;color:#4D73C4;font-family:'Blue Highway D Type',sans-serif;font-weight:bold;"><?=$city;?></div>
			<div style="font-size:16px;font-family:'Blue Highway D Type',sans-serif;border-bottom: 1px dashed #555555;width:100%;padding-bottom:15px;"><?=$dispdate;?></div>
			<table cellpadding="0" cellspacing="0" style="margin-top:15px;width:100%;">
				<tr>
					<td style="width: 120px;color:#555555;text-align:left;vertical-align:top;">Director Name:</td>
					<td style="color:#AD9E00;font-weight:bold;width: 380px;vertical-align:top;"><?php print($studio["contact_fname"]." ".$studio["contact_lname"]);?></td>
				</tr>
				<tr>
					<td style="color:#555555;text-align:left;vertical-align:top;">Studio:</td>
					<td style="font-weight:bold;vertical-align:top;"><?php print(stripslashes($studio["studioname"]));?></td>
				</tr>
				<tr>
					<td style="color:#555555;text-align:left;vertical-align:top;">Studio Code:</td>
					<td style="font-weight:bold;vertical-align:top;"><?php print($studio["studiocode"]);?></td>
				</tr>
				<tr>
					<td style="color:#555555;text-align:left;vertical-align:top;">Total Registered:</td>
					<td style="font-weight:bold;vertical-align:top;"><?php print($studio["dancer_count"]);?></td>
				</tr>
				<tr>
					<td style="color:#555555;text-align:left;vertical-align:top;">Participant Levels:</td>
					<td style="vertical-align:top;font-size:11px;">Teachers(<?php print($studio["teacher_count"]); ?>)&nbsp;&nbsp;Seniors(<?php print($studio["senior_count"]); ?>)&nbsp;&nbsp;Teens(<?php print($studio["teen_count"]); ?>)&nbsp;&nbsp;Juniors(<?php print($studio["junior_count"]); ?>)&nbsp;&nbsp;Minis(<?php print($studio["mini_count"]); ?>)&nbsp;&nbsp;JUMPstarts(<?php print($studio["jumpstart_count"]); ?>)&nbsp;&nbsp;Observers(<?php print($studio["observer_count"]); ?>)&nbsp;&nbsp;JS Observers(<?php print($studio["observer2_count"]); ?>)</td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom: 1px dashed #555555;">&nbsp;</td>
				</tr>
			</table>

			<div style="font-weight:bold;font-family:'Blue Highway D Type',sans-serif;margin-top:20px;font-size:22px;">Workshop Dancers</div>
			<table cellpadding="0" cellspacing="0" style="margin-top:5px;">
				<tr>
					<td style="width: 120px;color:#555555;text-align:left;font-style:italic;vertical-align:top;">Dancers:</td>
					<td style="vertical-align:top;">
						<table cellpadding="0" cellspacing="0">
							<?php foreach($studio["workshopregistrants"] as $dwl=>$dancers) { ?>
								<tr><td style="font-size:11px;font-weight:bold;"><?php print(str_replace("2","",$dwl)."(s)");?></td></tr>
								<tr><td style="font-size:11px;">
									<?php
										if($dwl != "Observer") {
											  $updatestr = "";
											  foreach($dancers as $dancer) {
					  						  	 $updatestr .= ($dancer["fname"]." ".$dancer["lname"]);
					  						  	 if($dancer["workshoplevelid"] != 1 && $dancer["workshoplevelid"] != 6 && $dancer["workshoplevelid"] != 8 && $dancer["workshopleveid"] != 12 && $dancer["one_day"] != 1 && $dancer["scholarship_code"] > 0) {
														$updatestr .= " (#".$dancer["scholarship_code"].")";
					  						  	 }

					  						  	 $updatestr .= ", ";
							  				  }
							  				  print(substr($updatestr,0,strlen($updatestr)-2));
										}
										else {
											$obsd = array();
											$obs = get_studio_observers_real_order($tourdateid,$studio["studioid"]);
											foreach($obs as $ob) {
												$obsd[] = $ob["fname"]." ".$ob["lname"];
											}
											print(join(", ",$obsd));
										}
									?>
					  			</td></tr>
								<tr><td>&nbsp;</td></tr>
							<?php    } ?>
						</table>
					</td>
				</tr>
			</table>


		</div>
		<?php } ?>
		<!-- END PAGE 2 -->


		<!-- COMP ROUTINES -->
		<?php if(count($studio["competitionroutines"]) > 0) { ?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-size:12px;">
							<span style="font-family:Regal Box;font-size:60px;color:#FF0000;">JUMP</span><br/>
							<div style="font-family:'festus!';font-size:26px;margin:5px 0 10px;line-height:11px;">the alternative convention</div>
							<span style="font-style:italic;">
								5546 Satsuma Ave.<br/>
								North Hollywood, CA 91601
							</span>
						</div>
					</td>
					<td style="text-align:right;vertical-align:top;">
						<div style="margin-top:5px;color:#4D73C4;font-size:35px;font-family:'Blue Highway D Type';">
							Registration Packet
						</div>
					</td>
				</tr>
			</table>
			<div style="font-weight:bold;font-family:'Blue Highway D Type',sans-serif;margin-top:20px;font-size:22px;"><?=$studio["studioname"]." Competition Routines";?></div>
				<tr>
					<td style="vertical-align:top;padding-top:0px;">
						<table cellpadding="0" cellspacing="0" style="margin-top:20px;">
							<?php
								foreach($studio["competitionroutines"] as $compgroup=>$routines) {
									foreach($routines as $routine) {
										 ?>
										<tr>
											<td style="width: 20px;text-align:left;vertical-align:top;">&nbsp;</td>
											<td style="width: 40px;"><?php print("#".$routine["finals_dispnumber"]);?></td>
											<td style="width:240px;text-align:right;"><?php print($routine["finals_date"]);?></td>
											<td style="padding-left:15px;"><?php print(stripslashes($routine["routinename"]));?></td>
										</tr>

								<?php	}
								}
							?>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<!-- END PAGE 4 -->
	<?php
		}
	}
}
?>
	</body>
</html>