var Excel = require('exceljs');
var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');
var specialAwards = require('./specialAwards');
var fs = require('fs');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var path = req.path;
        var tourDateId = req.query.tourdateid || 746; // placeholder id;

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                { model: db.tbl_event_cities, as: 'event_city', include: [
                    { model: db.tbl_states, as: 'state' }
                ]},
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if(tourDate) {
                return {
                    city: tourDate.event_city.name,
                    venue: tourDate.venue.name,
                    start_date: tourDate.start_date,
                    eventid: tourDate.event_id,
                    dancers : []
                };
            }
            else {
                throw new Error('No results')
            }
        })
        .then(function(swData) {
            db.date_scholarships.findAll({
                where: { tour_date_id: 746 },
                include: [
                    { model: db.tbl_staff, as: 'staff' },
                    { model: db.tbl_scholarships, as: 'scholarship', where: { event_id: swData.eventid} },
                    { model: db.date_dancers, as: 'date_dancer', include: [
                        { model: db.tbl_studios, as: 'studio' }
                    ]},
                    { model: db.tbl_dancers, as: 'dancer', include: [
                        { model: db.tbl_persons, as: 'person' }
                    ]}
                ]
            })
            .then(function(results) {
                _.forEach(results, function(ds) {
                    var name = ds.staff ? ds.staff.fname + ' ' + ds.staff.lname : '';
                    swData.dancers.push({
                        fname: ds.dancer.person.fname,
                        lname: ds.dancer.person.lname,
                        studio_name: ds.date_dancer.studio.name,
                        scholarship_code: ds.date_dancer.scholarship_code,
                        schol_name: ds.scholarship.name,
                        winner: ds.winner === 1 ? 'Winner' : 'Runner-up',
                        code: ds.code,
                        faculty_name: name
                    });
                });
                var wb = new Excel.Workbook();
                var ws = wb.addWorksheet('Worksheet');

                ws.columns = [
                    { header: 'FIRST', key: 'fname', width: util.setWidthAuto(swData.dancers, 'fname', 'FIRSt'), style: { font: { name: 'Arial' } } },
                    { header: 'LAST', key: 'lname', width: util.setWidthAuto(swData.dancers, 'lname', 'LAST'), style: { font: { name: 'Arial' } } },
                    { header: 'STUDIO', key: 'studio_name', width: util.setWidthAuto(swData.dancers, 'studio_name', 'STUDIO'), style: { font: { name: 'Arial' } } },
                    { header: 'SCHOL. NUM', key: 'scholarship_code', width: util.setWidthAuto(swData.dancers, 'scholarship_code', 'SCHOL. NUM'), style: { font: { name: 'Arial' } } },
                    { header: 'TYPE', key: 'schol_name', width: util.setWidthAuto(swData.dancers, 'schol_name', 'TYPE'), style: { font: { name: 'Arial' } } },
                    { header: 'WINNER', key: 'winner', width: util.setWidthAuto(swData.dancers, 'winner', 'WINNER'), style: { font: { name: 'Arial' } } },
                    { header: 'CRITIQUE CODE', key: 'code', width: util.setWidthAuto(swData.dancers, 'code', 'CRITIQUE CODE'), style: { font: { name: 'Arial' } } },
                    { header: 'FACULTY', key: 'faculty_name', width: util.setWidthAuto(swData.dancers, 'faculty_name', 'FACULTY'), style: { font: { name: 'Arial' } } }
                ];

                ws.addRows(swData.dancers);

                var filename = 'public/' + Date.now();
                var downloadName = (swData.city + "_schol_winners_" + tourDateId + ".xlsx").toLowerCase();

                wb.xlsx.writeFile(filename)
                    .then(function () {
                        res.download(filename, downloadName, function (err) {
                            if (!err) fs.unlink(filename);
                        });
                    });
            });
        });
    }
}