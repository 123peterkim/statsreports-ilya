const moment = require('moment');
const util = require('../reference/util/util');
const Sequelize = require('../pg/sequelize');
const db = Sequelize.models;

/**
 * Seems there is no consistent error handling convention.
 * Am I missing something?
 */

function renderReport (req, res) {

  const tourDateId = parseInt(req.query.tourdateid);

  if (Number.isNaN(tourDateId)) {
    /**
     * https://stats.breakthefloor.com/reports/scholarship_list/?tourdateid=THIS_IS_WRONG_ID
     * does not perform this check, instead shows empty report.
     */
    let message = 'Query parameter "tourdateid" must be integer';
    let error = new Error(message);
    error.status = 400;
    // Do I throw so that app.js error handler catches it?
    throw error;
    // Or render manually for consistency?
    return res.status(400).render('error', { message, error });
  }

  /**
   * Must lazy-load, as Sequelize.sequelize
   * isn't initialized at startup.
   * Seems like not all controllers recognize this.
   */
  const sequelize = Sequelize.sequelize;

  Promise.all([
    db.tbl_tour_dates.findOne({
      attributes: ['start_date'],
      include: [
        {
          model: db.tbl_venues,
          attributes: ['name'],
          as: 'venue'
        },
        {
          model: db.tbl_event_cities,
          attributes: ['name'],
          as: 'event_city'
        }
      ],
      where: { id: tourDateId }
    }),
    db.date_dancers.findAll({
      attributes: ['scholarship_code'],
      include: [
        {
          model: db.tbl_studios,
          attributes: ['name'],
          as: 'studio'
        },
        {
          model: db.tbl_dancers,
          as: 'dancer',
          include: [
            {
              model: db.tbl_persons,
              attributes: ['fname', 'lname', 'birthdate'],
              as: 'person'
            }
          ]
        }
      ],
      where: {
        tour_date_id: tourDateId,
        scholarship_code: { [sequelize.Op.gt]: 0 }
      },
      order: sequelize.col('scholarship_code')
    })
  ]).then(function ([tourDate, dancerData]) {

    if (!tourDate) {
      /**
       * Cannot throw in async callback
       * without async error handling boilerplate.
       * This check is also not performed in the original code.
       */
      return res.status(404).render('error', {
        message: 'Tour date not found',
        error: { status: 404 }
      });
    }

    const levels = ['Mini', 'Junior', 'Teen', 'Senior'];

    /**
     * A faster method
     * O(levels.length + dancerData.length)
     */

    let dancers = {}, level, dancer;

    /**
     * Initialize manually, since object properties are
     * iterated through in the order in which they were defined.
     */
    for (level of levels) {
      dancers[level] = [];
    }

    for (dancer of dancerData) {
      dancer = {
        scholarship_code: dancer.scholarship_code,
        studio_name: dancer.studio.name,
        fname: dancer.dancer.person.fname,
        lname: dancer.dancer.person.lname,
        age: util.getAge(dancer.dancer.person.birthdate)
      }
      if (dancer.age < 11) {
        level = levels[0];
      } else if (dancer.age < 13) {
        level = levels[1];
      } else if (dancer.age < 16) {
        level = levels[2];
      } else {
        level = levels[3];
      }
      dancers[level].push(dancer);
    }

    /**
     * Original PHP algorithm was slow
     * O(dancerData.length + levels.length * dancerData.length)

    let dancers = [], anew = {}, dancer;

    for (dancer of dancerData) {
      dancer = {
        scholarship_code: dancer.scholarship_code,
        studio_name: dancer.studio.name,
        fname: dancer.dancer.person.fname,
        lname: dancer.dancer.person.lname,
        age: util.getAge(dancer.dancer.person.birthdate)
      }
      if (dancer.age < 11) {
        dancer.level = 'Mini';
      }
      if (dancer.age > 10 && dancer.age < 13) {
        dancer.level = 'Junior';
      }
      if (dancer.age > 12 && dancer.age < 16) {
        dancer.level = 'Teen';
      }
      if (dancer.age >= 16) {
        dancer.level = 'Senior';
      }
      dancers.push(dancer);
    }

    for (level of levels) {
      anew[level] = [];
      for (dancer of dancers) {
        if (dancer.level == level) {
          anew[level].push(dancer);
        }
      }
    }
    dancers = anew;

    */

    res.render('reports/scholarship-list', {
      city_name: tourDate.event_city.name,
      venue_name: tourDate.venue.name,
      start_date: moment(tourDate.start_date).format('MMMM D'),
      dancers
    });

  }, function (error) {
    /**
     * Cannot throw in async callback
     * without async error handling boilerplate.
     */
    res.status(500).render('error', {
      message: 'Internal Server Error',
      error: req.app.get('env') === 'development' ? error : { status: 500 }
    });
  });
}

module.exports = { renderReport };