"use strict";

module.exports = function(sequelize, DataTypes) {
	var contact_info_person = sequelize.define('contact_info_person', {
		contact_info_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_contact_info',
				key: 'id'
			},
			primaryKey: true
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			},
      primaryKey: true
		}
	}, {
		timestamps: false,
		associate: function(models) {
		}
	});
	return contact_info_person;
};