"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_online_critiques = sequelize.define('tbl_online_critiques', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		judge: {
			type: DataTypes.ARRAY(DataTypes.JSONB)
		},
		comp_group: {
			type: DataTypes.STRING
		},
		number: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_online_critiques.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id'});
		}
	});
	return tbl_online_critiques;
};