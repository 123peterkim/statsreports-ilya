var _ = require('lodash');
var path = require('path');
var Sequelize = require('../pg/sequelize');
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        var sequelize = Sequelize.sequelize;
        var db = Sequelize.models;
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var showAll = (req.query.all || 0) === 1;
        var compGroup = req.query.compgroup || 'finals';

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                {
                    model: db.tbl_event_cities, as: 'event_city', include: [
                        { model: db.tbl_states, as: 'state' }
                    ]
                },
                { model: db.tbl_events, as: 'event' },
                { model: db.tbl_venues, as: 'venue' }
            ]
        })
        .then(function (results) {
            var tourDate = results[0];
            if (tourDate) {
                return {
                    tour_date: tourDate,
                    studios: []
                };
            }
            else {
                throw new Error('No Tour Date with this id')
            }
        })
        .then(function(ossData) {
            var showAllOptions = {};
            if(showAll) {
                showAllOptions = {
                    studio_id: studioId
                };
            }
            db.date_studios_registrations.findAll({
                where: { tour_date_id: tourDateId },
                include: [
                    { model: db.date_studios, as: 'date_studio', where: showAllOptions, include: [
                        { model: db.tbl_studios, as: 'studio', include: [
                            { model: db.tbl_mybtf_users, as: 'primary_mybtf_user', include: [
                                { model: db.tbl_persons, as: 'person' }
                            ]},
                            { model: db.tbl_addresses, as: 'address' }
                        ]}
                    ]},
                    { model: db.tbl_registrations, as: 'registration', include: [
                        { model: db.tbl_payments, as: 'payments' }
                    ]}
                ]
            })
            .then(function(results) {
                var tasks = [];
                _.forEach(results, function(dsr) {
                    tasks.push(function(cb1) {
                        var person = dsr.date_studio.studio.primary_mybtf_user.person;
                        var studio = {
                            studio_id: dsr.date_studio.studio_id,
                            studio_name: dsr.date_studio.studio.name,
                            contact_full: person.fname + ' ' + person.lname,
                            address: dsr.date_studio.studio.address.address,
                            city: dsr.date_studio.studio.address.city,
                            studio_code: dsr.date_studio.studio.code,
                            zip: dsr.date_studio.studio.address.zip,
                            contact: person.fname,
                            workshop_fees: dsr.registration.fees.workshop,
                            competition_fees: dsr.registration.fees.competition,
                            total_fees: dsr.registration.fees.total,
                            fees_paid: 0,
                            credit: dsr.registration.credit.amount,
                            invoice_note: dsr.invoice_note,
                            comptetition_routines: []
                        }
                        _.forEach(dsr.registration.payments, function (p) {
                            studio.fees_paid += p.amount;
                        });

                        sql = "select routine.id, routine.name as routine_name, level2.name as age_division_name, \
                            case when has_a then number:: varchar|| '.a' else number:: varchar end dispnumber, has_a, \
                                minimum_age || '-' || minimum_age + t.range as range, cat.name as routine_category, \
                                tpd.name as performance_division_name, fee, extra_time, t2.name as award_name, \
                                total_score as finals_total_score, json_agg(tp.*) as routine_dancers \
                            from date_routines \
                            join date_routines_scoring on date_routines.id = date_routines_scoring.date_routine_id \
                            join tbl_competition_groups g ON date_routines_scoring.competition_group_id = g.id \
                            join tbl_competition_awards a ON date_routines_scoring.competition_award_id = a.id \
                            join tbl_award_types t2 ON a.award_type_id = t2.id \
                            JOIN tbl_routines routine ON date_routines.routine_id = routine.id \
                            join date_routines_dancers drd ON date_routines.id = drd.date_routine_id \
                            join date_dancers d3 ON drd.date_dancer_id = d3.id  \
                            join tbl_dancers d2 ON d3.dancer_id = d2.id \
                            join tbl_persons tp ON d2.person_id = tp.id \
                            join tbl_registrations_routines r ON date_routines.registrations_routine_id = r.id \
                            join tbl_routine_categories rc on r.routine_category_id = rc.id \
                            join tbl_categories cat on rc.category_id = cat.id \
                            join tbl_age_divisions t ON r.age_division_id = t.id \
                            join tbl_levels level2 ON t.level_id = level2.id \
                            join tbl_performance_divisions tpd ON r.performance_division_id = tpd.id \
                            where g.db_key = :compgroup \
                            and date_routines.tour_date_id = :tourdateid \
                            and date_routines.studio_id = :studioid \
                            GROUP BY routine.id, routine_name, \
                                finals_total_score, t2.name, age_division_name, dispnumber, has_a, minimum_age, cat.name, \
                                \"range\", performance_division_name, fee, award_name, finals_total_score, extra_time;";
                        sequelize.query(sql, {
                            type: sequelize.QueryTypes.SELECT,
                            replacements: {
                                tourdateid: tourDateId,
                                studioid: dsr.date_studio.studio_id,
                                compgroup: compGroup
                            }
                        })
                        .then(function(results) {
                            var subtasks = [];
                            _.forEach(results, function(compRoutine) {
                                subtasks.push(function(cb2) {
                                    compRoutine.judges = [];
                                    db.tbl_online_scoring.findAll({
                                        where: {
                                            tour_date_id: tourDateId,
                                            routine_id: compRoutine.id
                                        },
                                        inclde: [
                                            { model: db.tbl_online_scoring_data, as: 'online_scoring_data' },
                                            { model: db.tbl_competition_groups, as: 'competition_groups', where: {
                                                db_key: compGroup
                                            }}
                                        ]
                                    })
                                    .then(function(results) {
                                        // TODO finish this section
                                    });
                                });
                            });
                        });
                    });
                });
                async.parallel(tasks, function(err, results) {
                    res.send(ossData);
                });
            });
        });
    }
}