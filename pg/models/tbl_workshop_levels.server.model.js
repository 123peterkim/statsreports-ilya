"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_workshop_levels = sequelize.define('tbl_workshop_levels', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		level_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_levels',
				key: 'id'
			}
		},
		season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			}
		},
		discount: {
			type: DataTypes.REAL
		},
		full: {
			type: DataTypes.REAL
		},
		finale_discount: {
			type: DataTypes.REAL
		},
		finale_full: {
			type: DataTypes.REAL
		},
		one_day_full: {
			type: DataTypes.REAL
		},
		one_day_discount: {
			type: DataTypes.REAL
		},
    minimum_age: {
      type: DataTypes.INTEGER
    },
    range: {
      type: DataTypes.INTEGER
    },
		recommended_age: {
      type: DataTypes.INTEGER
    }
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_workshop_levels.belongsTo(models.tbl_levels, {foreignKey: 'level_id', as: 'level'});

		}
	});
	return tbl_workshop_levels;
};