"use strict";

module.exports = function(sequelize, DataTypes) {
	var dateMybtfExceptions = sequelize.define('date_mybtf_exceptions', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_tour_dates',
			  key: 'id'
			}
		},
		email: {
		  type: DataTypes.STRING
		},
		level: {
		  type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return dateMybtfExceptions;
};