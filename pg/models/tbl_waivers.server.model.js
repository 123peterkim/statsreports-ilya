"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_waivers = sequelize.define('tbl_waivers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_waivers.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id'});
		}
	});
	return tbl_waivers;
};