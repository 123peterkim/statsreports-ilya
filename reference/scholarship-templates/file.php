<?php
	include("../../../includes/util.php");

	// MINI: 8-10
	// JUNIOR: 11-12
	// TEEN: 13-15
	// SENIOR: 16-18
	$minis = Array();
	$juniors = Array();
	$teens = Array();
	$seniors = Array();

	// $default_rows kind of functions more like the minimum number of rows...
	// Actual number of rows auto adjusts to accomodate however many numbers the array contains
	$default_rows = 1;
	$cols = 19; // How many columns fit on a page.  Currently 23 with 40px wide divs.

	$tourdateid = intval($_GET["tourdateid"]);

	// Get dancer ids.
	$sql = "SELECT datedancerid FROM `tbl_date_scholarships` WHERE tourdateid=$tourdateid";
	$res = mysql_query($sql) or die(mysql_error());
	while($row = mysql_fetch_assoc($res)) {
		$dancerid = $row['datedancerid'];
		$sql2 = "SELECT age, scholarship_code FROM `tbl_date_dancers` WHERE id='$dancerid' AND scholarship_code > 0 AND age > 7 AND age < 19 ORDER BY scholarship_code ASC";
		$res2 = mysql_query($sql2) or die(mysql_error());

		while($row2 = mysql_fetch_assoc($res2)) {

			if($row2["age"] >= 8 && $row2["age"] <= 10) {
				$minis[] = $row2["scholarship_code"];
			} elseif($row2["age"] >= 10 && $row2["age"] <= 12) {
				$juniors[] = $row2["scholarship_code"];
			} elseif($row2["age"] >= 13 && $row2["age"] <= 15) {
				$teens[] = $row2["scholarship_code"];
			} else {
				$seniors[] = $row2["scholarship_code"];
			}

		}


	}
	sort($minis);
	sort($juniors);
	sort($teens);
	sort($seniors);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 13px;
				width:100px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 2px 0 2px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
			#dance_type_heads {
				margin-top: 30px;
				font-size: 16pt;
			}
			#dance_type_heads tr td {
				text-align: center;
				font-size: 14pt;
				border-top: 1px solid #000000;
			}
			.code_column {
				float:left;
				width:50px;
				font-size:12pt;
				text-align:center;
			}
			.codes_container {
				margin: 30px 0px 30px 0px;
				height: auto;
				line-height: 30px;
			}
			/*@media print { */
				#dance_type_heads{
					page-break-after: always;
				}

				#dance_type_heads td {
					height: 220px;
					vertical-align: top;
					border-right:1px solid black;
					width:200px;
				}

				#dance_type_heads tr td:nth-child(4) {
					border-right: none !important;
				}
		/* } */
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:952px;margin: 10px auto 0;">

			<!-- MINIS -->
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><div style="font-size:20pt;">Mini</div></td>
				</tr>
			</table>
			<div class="codes_container">
				<?php
				// Determine number of necessary rows
				$rows = $default_rows;
				$size = count($minis);
				while($rows*$cols < $size) {
					$rows++;
				}
				foreach($minis as $key=>$val) {
					if($key==0)
						print('<div class="code_column">');
					if($key%$rows==0 && $key!=0)
						print('</div><div class="code_column">');
					print($val);
					print('<br />');

					if($key==$size-1)
						print('</div>');
				}
				?>
			</div>
			<div style="clear:both;"></div>
			<table id="dance_type_heads"  cellpadding="0" cellspacing="0" style="width:100%;height:100%;">
				<tr>
					<td>Ballet</td>
					<td>Tap</td>
					<td>Hip Hop</td>
					<td>Jazz</td>
				</tr>
				<tr>
					<td>Musical Theatre</td>
					<td>Ballroom</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

			<!-- JUNIORS -->
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><div style="font-size:20pt;">Junior</div></td>
				</tr>
			</table>
			<div class="codes_container">
				<?php
				$rows = $default_rows;
				$size = count($juniors);
				while($rows*$cols < $size) {
					$rows++;
				}
				foreach($juniors as $key=>$val) {
					if($key==0)
						print('<div class="code_column">');
					if($key%$rows==0 && $key!=0)
						print('</div><div class="code_column">');
					print($val);
					print('<br />');

					if($key==$size-1)
						print('</div>');
				}
				?>
			</div>
			<div style="clear:both;"></div>
			<table id="dance_type_heads" cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>Ballet</td>
					<td>Tap</td>
					<td>Hip Hop</td>
					<td>Jazz</td>
				</tr>
				<tr>
					<td>Musical Theatre</td>
					<td>Ballroom</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

			<!-- TEENS -->
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><div style="font-size:20pt;">Teen</div></td>
				</tr>
			</table>
			<div class="codes_container">
				<?php
				$rows = $default_rows;
				$size = count($teens);
				while($rows*$cols < $size) {
					$rows++;
				}
				foreach($teens as $key=>$val) {
					if($key==0)
						print('<div class="code_column">');
					if($key%$rows==0 && $key!=0)
						print('</div><div class="code_column">');
					print($val);
					print('<br />');
					$remember = $key;

					if($key==$size-1)
						print('</div>');
				}
				?>
			</div>
			<div style="clear:both;"></div>
			<table id="dance_type_heads" cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>Ballet</td>
					<td>Tap</td>
					<td>Hip Hop</td>
					<td>Jazz</td>
				</tr>
				<tr>
					<td>Musical Theatre</td>
					<td>Ballroom</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

			<!-- SENIORS -->

			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><div style="font-size:20pt;">Senior</div></td>
				</tr>
			</table>
			<div class="codes_container">
				<?php
				$rows = $default_rows;
				$size = count($seniors);
				while($rows*$cols < $size) {
					$rows++;
				}
				foreach($seniors as $key=>$val) {
					if($key==0)
						print('<div class="code_column">');
					if($key%$rows==0 && $key!=0)
						print('</div><div class="code_column">');
					print($val);
					print('<br />');

					if($key==$size-1)
						print('</div>');
				}
				?>
			</div>
			<div style="clear:both;"></div>
			<table id="dance_type_heads" cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>Ballet</td>
					<td>Tap</td>
					<td>Hip Hop</td>
					<td>Jazz</td>
				</tr>
				<tr>
					<td>Musical Theatre</td>
					<td>Ballroom</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>

		</div>
	</body>
</html>