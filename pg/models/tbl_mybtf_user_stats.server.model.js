"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_mybtf_user_stats = sequelize.define('tbl_mybtf_user_stats', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		activation_code: {
			type: DataTypes.STRING
		},
		signup: {
			type: DataTypes.DATEONLY
		},
		activate: {
			type: DataTypes.DATEONLY
		},
		last_login: {
			type: DataTypes.DATEONLY
		},
		disable: {
			type: DataTypes.DATEONLY
		},
		login_count: {
			type: DataTypes.INTEGER
		},
		ip: {
			type: DataTypes.TEXT
		},
		dont_show: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_mybtf_user_stats;
};