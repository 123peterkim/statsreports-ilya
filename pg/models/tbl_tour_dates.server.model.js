"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_tour_dates = sequelize.define('tbl_tour_dates', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		venue_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_venues',
				key: 'id'
			}
		},
		hotel_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_hotels',
				key: 'id'
			},
			onDelete: 'SET NULL'
		},
		perf_div_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_perf_div_types',
				key: 'id'
			}
		},
		event_city_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_cities',
				key: 'id'
			}
		},
		start_date: {
			type: DataTypes.DATEONLY
		},
		end_date: {
			type: DataTypes.DATEONLY
		},
		cutoff_start_date: {
			type: DataTypes.DATEONLY
		},
		cutoff_end_date: {
			type: DataTypes.DATEONLY
		},
		room: {
			type: DataTypes.JSONB
		},
		notes: {
			type: DataTypes.TEXT
		},
		is_finals: {
			type: DataTypes.BOOLEAN
		},
		entry_limits: {
			type: DataTypes.TEXT
		},
		weather_link: {
			type: DataTypes.TEXT
		},
		hotel_alt: {
			type: DataTypes.TEXT
		},
		custom_routine_durations: {
			type: DataTypes.JSONB
		},
		default_category_order: {
			type: DataTypes.JSONB
		},
		routine_dist: {
			type: DataTypes.INTEGER
		},
		webcast_this_city: {
			type: DataTypes.INTEGER
		},
		hotel_notes: {
			type: DataTypes.TEXT
		},
		online_balance_payments_enabled: {
			type: DataTypes.INTEGER
		},
		event_date_status: {
			type: DataTypes.STRING
		},
		results_solo_display_count: {
			type: DataTypes.INTEGER
		},
		show_time: {
			type: DataTypes.STRING
		},
		use_online_scoring: {
			type: DataTypes.INTEGER
		},
		web: {
			type: DataTypes.JSONB
		},
		entry_limit: {
			type: DataTypes.JSONB
		},
		competition: {
			type: DataTypes.JSONB
		},
		tda_danceoff: {
			type: DataTypes.JSONB
		},
		mybtf: {
			type: DataTypes.JSONB
		},
		workshop_notes: {
			type: DataTypes.TEXT
		},
    competition_notes: {
      type: DataTypes.TEXT
    },
		webcast: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_tour_dates.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});
			tbl_tour_dates.belongsTo(models.tbl_seasons, {foreignKey: 'season_id', as: 'season'});
			tbl_tour_dates.belongsTo(models.tbl_event_cities, {foreignKey: 'event_city_id', as: 'event_city'});
			tbl_tour_dates.belongsTo(models.tbl_venues, {foreignKey: 'venue_id', as: 'venue'});
			tbl_tour_dates.belongsTo(models.tbl_hotels, {foreignKey: 'hotel_id', as: 'hotel'});
			tbl_tour_dates.belongsTo(models.tbl_perf_div_types, {foreignKey: 'perf_div_type_id', as: 'perf_div_type'});
      tbl_tour_dates.hasMany(models.date_mybtf_exceptions, {foreignKey: 'tour_date_id', as: 'exceptions'});

      tbl_tour_dates.belongsToMany(models.tbl_workshop_rooms, {as: 'workshop_rooms', through: models.tour_dates_workshop_rooms, foreignKey: 'tour_date_id'});
		}
	});
	return tbl_tour_dates;
};