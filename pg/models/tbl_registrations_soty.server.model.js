"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_soty = sequelize.define('tbl_registrations_soty', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		ts_ballet: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ts_jazz: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ts_mtspec: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ts_contemplyrical: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ts_hiphoptap: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mj_ballet: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mj_jazz: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mj_mtspec: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mj_contemplyrical: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mj_hiphoptap: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		has_soty: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_registrations_soty;
};