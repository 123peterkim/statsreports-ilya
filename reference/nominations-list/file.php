<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Nominations</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}
		</style>
	</head>
	<body>
		<div style="width:720px;">
			<img src="../tda_logo.jpg" alt="" style="width: 200px;" />
			<h1>Nominations List</h1>
			<div style="border-bottom: 1px dotted #333;font-size:20px;">Studio of the Year</div>
<!-- STUDIO OF THE YEAR -->
<?php
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.soty_ts_ballet_routineid, tbl_tda_award_nominations.soty_ts_jazz_routineid, tbl_tda_award_nominations.soty_ts_musicaltheater_routineid, tbl_tda_award_nominations.soty_ts_contemplyrical_routineid, tbl_tda_award_nominations.soty_ts_hiphoptap_routineid, tbl_tda_award_nominations.soty_mj_ballet_routineid, tbl_tda_award_nominations.soty_mj_jazz_routineid, tbl_tda_award_nominations.soty_mj_musicaltheater_routineid, tbl_tda_award_nominations.soty_mj_contemplyrical_routineid, tbl_tda_award_nominations.soty_mj_hiphoptap_routineid, tbl_studios.name AS studioname FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid WHERE tbl_tda_award_nominations.has_soty=1 AND tbl_tda_award_nominations.tourdateid=$tourdateid ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {

			$info = array();
			$info["ballet_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_ballet_routineid"]."'");
			$info["ballet_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_ballet_routineid"]."'");
			$info["jazz_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_jazz_routineid"]."'");
			$info["jazz_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_jazz_routineid"]."'");
			$info["musicaltheater_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_musicaltheater_routineid"]."'");
			$info["musicaltheater_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_musicaltheater_routineid"]."'");
			$info["contemplyrical_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_contemplyrical_routineid"]."'");
			$info["contemplyrical_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_contemplyrical_routineid"]."'");
			$info["hiphop_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_hiphoptap_routineid"]."'");
			$info["hiphop_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_hiphoptap_routineid"]."'");
			$info["studio_total"] = $info["ballet_mj"] + $info["ballet_ts"] + $info["jazz_mj"] + $info["jazz_ts"] + $info["musicaltheater_mj"] + $info["musicaltheater_ts"] + $info["contemplyrical_mj"] + $info["contemplyrical_ts"] + $info["hiphop_mj"] + $info["hiphop_ts"];

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_ballet_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_ballet_routineid"]."'");
			$info["dn_ballet_mj"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_ballet_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_ballet_routineid"]."'");
			$info["dn_ballet_ts"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_jazz_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_jazz_routineid"]."'");
			$info["dn_jazz_mj"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_jazz_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_jazz_routineid"]."'");
			$info["dn_jazz_ts"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_musicaltheater_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_musicaltheater_routineid"]."'");
			$info["dn_musicaltheater_mj"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_musicaltheater_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_musicaltheater_routineid"]."'");
			$info["dn_musicaltheater_ts"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_contemplyrical_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_contemplyrical_routineid"]."'");
			$info["dn_contemplyrical_mj"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_contemplyrical_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_contemplyrical_routineid"]."'");
			$info["dn_contemplyrical_ts"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_hiphoptap_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_hiphoptap_routineid"]."'");
			$info["dn_hiphop_mj"] = $hasa ? $number.".a" : $number;

			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_hiphoptap_routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_hiphoptap_routineid"]."'");
			$info["dn_hiphop_ts"] = $hasa ? $number.".a" : $number;




			$row["tdainfo"] = $info;
			$all[] = $row;
		}
//		print_r($all);exit();
		foreach($all as $arow) { ?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;"><?php print(stripslashes($arow["studioname"])); if($arow["tdainfo"]["studio_total"] > 0) print(" - ".$arow["tdainfo"]["studio_total"]); ?></div>
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="width:350px;">
						<div style="font-size:14px;font-style:italic;margin: 4px;text-decoration:underline;">Mini &amp; Junior</div>
						<div style="margin-left:30px;"> Ballet: <?php if($arow["soty_mj_ballet_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_mj_ballet_routineid"])); print("#".$arow["tdainfo"]["dn_ballet_mj"]." ");   if($arow["tdainfo"]["ballet_mj"] > 0) print(" [".$arow["tdainfo"]["ballet_mj"]."]"); ?></div>
						<div style="margin-left:30px;"> Jazz: <?php if($arow["soty_mj_jazz_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_mj_jazz_routineid"]));  print("#".$arow["tdainfo"]["dn_jazz_mj"]." "); if($arow["tdainfo"]["jazz_mj"] > 0) print(" [".$arow["tdainfo"]["jazz_mj"]."]");?></div>
						<div style="margin-left:30px;"> Musi. Thea. / Specialty: <?php if($arow["soty_mj_musicaltheater_routineid"] > 0)  print(db_one("name","tbl_routines","id=".$arow["soty_mj_musicaltheater_routineid"])); print("#".$arow["tdainfo"]["dn_musicaltheater_mj"]." "); if($arow["tdainfo"]["musicaltheater_mj"] > 0) print(" [".$arow["tdainfo"]["musicaltheater_mj"]."]");?></div>
						<div style="margin-left:30px;"> Contemp / Lyrical: <?php if($arow["soty_mj_contemplyrical_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_mj_contemplyrical_routineid"])); print("#".$arow["tdainfo"]["dn_contemplyrical_mj"]." ");  if($arow["tdainfo"]["contemplyrical_mj"] > 0) print(" [".$arow["tdainfo"]["contemplyrical_mj"]."]");?></div>
						<div style="margin-left:30px;"> Hip Hop / Tap: <?php if($arow["soty_mj_hiphoptap_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_mj_hiphoptap_routineid"]));  print("#".$arow["tdainfo"]["dn_hiphop_mj"]." ");  if($arow["tdainfo"]["hiphop_mj"] > 0) print(" [".$arow["tdainfo"]["hiphop_mj"]."]");?></div>
					</td>
					<td>
						<div style="font-size:14px;font-style:italic;margin: 4px;text-decoration:underline;">Teen &amp; Senior</div>
						<div style="margin-left:30px;"> Ballet: <?php if($arow["soty_ts_ballet_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_ts_ballet_routineid"])); print("#".$arow["tdainfo"]["dn_ballet_ts"]." "); if($arow["tdainfo"]["ballet_ts"] > 0) print(" [".$arow["tdainfo"]["ballet_ts"]."]"); ?></div>
						<div style="margin-left:30px;"> Jazz: <?php if($arow["soty_ts_jazz_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_ts_jazz_routineid"])); print("#".$arow["tdainfo"]["dn_jazz_ts"]." ");  if($arow["tdainfo"]["jazz_ts"] > 0) print(" [".$arow["tdainfo"]["jazz_ts"]."]"); ?></div>
						<div style="margin-left:30px;"> Musi. Thea. / Specialty: <?php if($arow["soty_ts_musicaltheater_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_ts_musicaltheater_routineid"])); print("#".$arow["tdainfo"]["dn_musicaltheater_ts"]." "); if($arow["tdainfo"]["musicaltheater_ts"] > 0) print(" [".$arow["tdainfo"]["musicaltheater_ts"]."]"); ?></div>
						<div style="margin-left:30px;"> Contemp / Lyrical: <?php if($arow["soty_ts_contemplyrical_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_ts_contemplyrical_routineid"]));  print("#".$arow["tdainfo"]["dn_contemplyrical_ts"]." ");  if($arow["tdainfo"]["contemplyrical_ts"] > 0) print(" [".$arow["tdainfo"]["contemplyrical_ts"]."]"); ?></div>
						<div style="margin-left:30px;"> Hip Hop / Tap: <?php if($arow["soty_ts_hiphoptap_routineid"] > 0) print(db_one("name","tbl_routines","id=".$arow["soty_ts_hiphoptap_routineid"])); print("#".$arow["tdainfo"]["dn_hiphop_ts"]." ");  if($arow["tdainfo"]["hiphop_ts"] > 0) print(" [".$arow["tdainfo"]["hiphop_ts"]."]"); ?></div>
					</td>
				</tr>
			</table>
		<?php }
	} ?>


			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Outstanding Tech. Ach. (Mini)</div>
<!-- OTA: M -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_m_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_m_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_m_ota_no=0 AND tbl_tda_award_nominations.sa_m_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Mini</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>


			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Outstanding Tech. Ach. (Junior)</div>
<!-- OTA: J -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_j_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_j_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_j_ota_no=0 AND tbl_tda_award_nominations.sa_j_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Mini</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>


			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Outstanding Tech. Ach. (Teen)</div>
<!-- OTA: T -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_t_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_t_ota_routineid WHERE
 tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_t_ota_no=0 AND tbl_tda_award_nominations.sa_t_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Teen</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>

<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Outstanding Tech. Ach. (Senior)</div>
<!-- OTA: T -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_s_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_s_ota_routineid WHERE
 tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_s_ota_no=0 AND tbl_tda_award_nominations.sa_s_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Teen</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>

			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Outstanding Ach. Costume Design</div>
<!-- OTA: CD -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_oacd_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_oacd_routineid WHERE
 tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_oacd_no=0 AND tbl_tda_award_nominations.sa_oacd_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>


			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Best Choreographer (Mini / Junior)</div>
<!-- OTA: BC MJ -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_mj_bc_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_mj_bc_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_mj_bc_no=0 AND tbl_tda_award_nominations.sa_mj_bc_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Mini / Junior</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>

			<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">Best Choreographer (Teen / Senior)</div>
<!-- OTA: BC TS -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_ts_bc_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_ts_bc_routineid WHERE
 tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_ts_bc_no=0 AND tbl_tda_award_nominations.sa_ts_bc_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<div style="font-size:18px;font-weight:bold;margin-top:10px;">Teen / Senior</div>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>

<div style="border-bottom: 1px dotted #333;font-size:20px;margin-top:20px;">People's Choice</div>
<!-- PEOPLES CHOICE -->
<?php
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_peopleschoice_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_peopleschoice_routineid WHERE
 tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_peopleschoice_no=0 AND tbl_tda_award_nominations.sa_peopleschoice_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$hasa = db_one("finals_has_a","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'") == 1 ? true : false;
			$number = db_one("number_finals","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$row["dispnumber"] = $hasa ? $number.".a" : $number;
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	?>
			<table cellpadding="0" cellspacing="0">
	<?php	foreach($all as $arow) { ?>
				<tr>
					<td>
						<div style="margin-left:30px;"> <?php print(str_replace("&amp;","&",$arow["studioname"]." - ".$arow["routinename"])); if($arow["finals_score"] > 0) print(" - <span style='font-weight:bold;'>".$arow["finals_score"]."</span>"); ?> <span style='color:#777777;'>#<?=$arow["dispnumber"];?></span></div>
					</td>
				</tr>
		<?php } ?>
			</table>

<?php	} ?>

		</div>
	</body>
</html>