"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_product_colors = sequelize.define('tbl_store_product_colors', {
		store_color_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_colors',
				key: 'id'
			}
		},
		store_product_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_products',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_product_colors;
};