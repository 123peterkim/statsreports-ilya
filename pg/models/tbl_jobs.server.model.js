"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_jobs = sequelize.define('tbl_jobs', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		title: {
			type: DataTypes.STRING
		},
		type: {
			type: DataTypes.STRING
		},
		description: {
			type: DataTypes.TEXT
		},
		views: {
			type: DataTypes.INTEGER
		},
		show: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_jobs;
};