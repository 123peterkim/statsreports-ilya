"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_event_registrations = sequelize.define('tbl_event_registrations', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		entered_by_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_admins',
				key: 'id'
			}
		},
		heard: {
			type: DataTypes.STRING
		},
		notes: {
			type: DataTypes.TEXT
		},
		dates: {
			type: DataTypes.JSONB
		},
		total_fees: {
			type: DataTypes.REAL
		},
		fees_paid: {
			type: DataTypes.REAL
		},
		balance_due: {
			type: DataTypes.REAL
		},
		pay_choice: {
			type: DataTypes.STRING
		},
		registration_date: {
			type: DataTypes.DATEONLY
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_event_registrations.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			tbl_event_registrations.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			tbl_event_registrations.belongsTo(models.tbl_persons, {foreignKey: 'person_id', as: 'person'});
			tbl_event_registrations.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
			tbl_event_registrations.belongsTo(models.tbl_mybtf_users, {foreignKey: 'mybtf_user_id', as: 'mybtf_user'});
			tbl_event_registrations.belongsTo(models.tbl_admins, {foreignKey: 'entered_by_id', as: 'entered_by'});
		}
	});
	return tbl_event_registrations;
};