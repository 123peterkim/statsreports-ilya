<?php
	include("../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('F d',mktime(0,0,0,$mm,$dd,$yy));

	$sql = "SELECT tbl_date_dancers.scholarship_code, tbl_studios.name AS studioname, tbl_profiles.fname, tbl_profiles.lname, tbl_date_dancers.age, tbl_date_dancers.workshoplevelid, tbl_workshop_levels_$seasonid.name as workshoplevelname FROM `tbl_date_dancers` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid LEFT JOIN tbl_workshop_levels_$seasonid ON tbl_workshop_levels_$seasonid.id = tbl_date_dancers.workshoplevelid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.scholarship_code > 0 ORDER BY tbl_profiles.lname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$dancers[] = $row;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 11px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;

			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;margin: 0 auto;">
			<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Scholarship Participants (by name)</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:30px;"><?php print($venue." - ".$start_date); ?></div>
			<table cellpadding="0" cellspacing="0" class="rtable">
				<tr>
					<td class="thead" style="width:90px;">Scholar. ID</td>
					<td class="thead" style="width:280px;">Studio</td>
					<td class="thead" style="width:220px;">Dancer</td>
					<td class="thead" style="width:90px;border-right:1px solid #000000;">Age</td>
				</tr>
		<?php
			for($i=0;$i<count($dancers);$i++) { ?>
				<tr>
					<td class="tbody"><?php print($dancers[$i]["scholarship_code"]); ?></td>
					<td class="tbody"><?php print($dancers[$i]["studioname"]); ?></td>
					<td class="tbody"><?php print($dancers[$i]["fname"]." ".$dancers[$i]["lname"]); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($dancers[$i]["age"]); ?></td>
				</tr>
		<?php  } ?>
			</table>
		</div>
	</body>
</html>