<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$safecity = strtolower(str_replace(array(" ",",","-"),"",$city));
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('F d, Y',mktime(0,0,0,$mm,$dd,$yy));
	$tmp = array();
	$sql = "SELECT tbl_date_studios.studioid, tbl_date_studios.studiocode, tbl_studios.phone, tbl_studios.phone2, tbl_studios.name AS studioname, tbl_studios.email, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_studios.zip, tbl_studios.countryid, tbl_date_studios.independent, tbl_date_studios.free_teacher_value, tbl_date_studios.total_fees, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.total_fees DESC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$contacts = json_decode($row["contacts"],true);
			$row["contact"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
			$row["country"] = db_one("name","tbl_countries","id='".$row["countryid"]."'");
			$row["state_abbr"] = db_one("abbreviation","tbl_states","name='".$row["state"]."' OR abbreviation='".$row["state"]."'");
			$row["sumdancers"] = db_one("COUNT(id)","tbl_date_dancers","tourdateid=$tourdateid AND studioid='".$row["studioid"]."' AND (workshoplevelid!=8 AND workshoplevelid!=1 AND workshoplevelid!=12)");
			$row["sumroutines"] = db_one("COUNT(id)","tbl_date_routines","tourdateid=$tourdateid AND studioid='".$row["studioid"]."'");
			$row["independent"] = $row["independent"] == "1" ? "Yes" : "No";
			$row["freeteachervalue"] = $row["free_teacher_value"];
			$row["dispfee"] = number_format(($row["total_fees"] - $row["free_teacher_value"]),2,'.','');

			$tmp[$row["studioid"]] = $row["dispfee"];
			$studios[] = $row;
		}
		$tmp2 = array();
		if(count($tmp) > 0) {
			arsort($tmp);
			foreach($tmp as $atk => $atk) {
				foreach($studios as $sk => $sv)
				if($atk == $sv["studioid"])
					$tmp2[] = $studios[$sk];
			}
			$studios = $tmp2;

			//create excel shit
			$workbook = new PHPExcel();
			$workbook->setActiveSheetIndex(0);

			//set global font & font size
			$workbook->getDefaultStyle()->getFont()->setName('Arial');

			//col widths
			$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$workbook->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

			$dataArray = array();
			$dataArray[] = array("Studio","Code","Indpt.","Contact","Dancers","Routines","Total Fees","Email","Phone1","Phone2","Address","City","State","ZIP","Country");
			foreach($studios as $studio) {
				$dataArray[] = array(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["studioname"]))),$studio["studiocode"],$studio["independent"],$studio["contact"],$studio["sumdancers"],$studio["sumroutines"],$studio["dispfee"],$studio["email"],$studio["phone"],$studio["phone2"],stripslashes($studio["address"]),$studio["city"],$studio["state_abbr"],$studio["zip"],$studio["country"]);
			}
			$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

			$outputFileType = 'Excel5';
			$mk = time();
			$somekindofrandomstr = "$safecity"."_hotelcomps_$tourdateid".substr($mk,6,5);
			$outputFileName = "../../temp/$somekindofrandomstr.xls";
			$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
			$objWriter->save($outputFileName);
			chmod($outputFileName,777);
			unset($workbook);
			unset($objWriter);

			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=".basename($outputFileName));
			header("Content-type: application/octet-stream");
			header("Content-Transfer-Encoding: binary");
			readfile($outputFileName);
		}
	}
?>