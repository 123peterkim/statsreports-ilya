"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_dts_reg_types = sequelize.define('tbl_dts_reg_types', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		fee: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_dts_reg_types;
};