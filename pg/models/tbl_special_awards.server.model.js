"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_special_awards = sequelize.define('tbl_special_awards', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		award_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_award_types',
				key: 'id'
			}
		},
		age_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_age_divisions',
				key: 'id'
			}
		},
		performance_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_performance_divisions',
				key: 'id'
			}
		},
		report_order: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_special_awards.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});
			tbl_special_awards.belongsTo(models.tbl_age_divisions, {foreignKey: 'age_division_id', as: 'age_division'});
			tbl_special_awards.belongsTo(models.tbl_award_types, {foreignKey: 'award_type_id', as: 'award_type'});
			tbl_special_awards.belongsTo(models.tbl_performance_divisions, {foreignKey: 'performance_division_id', as: 'performance_division'});
		}
	});
	return tbl_special_awards;
};