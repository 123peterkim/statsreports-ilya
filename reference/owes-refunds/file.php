<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);

	if($tourdateid > 0) {
		$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
		$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
		list($yy,$mm,$dd) = explode("-",$start_date_a);
		$start_date = date('F d',mktime(0,0,0,$mm,$dd,$yy));
		$end_date_a = db_one("end_date","tbl_tour_dates","id=$tourdateid");
		list($yy2,$mm2,$dd2) = explode("-",$end_date_a);
		$end_date = date('d',mktime(0,0,0,$mm2,$dd2,$yy2));
		$city = db_one("city","tbl_tour_dates","id=$tourdateid");

		$studios = array();

		$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.fees_paid, tbl_date_studios.credit, tbl_studios.contacts, tbl_date_studios.studiocode, tbl_studios.id AS studioid, tbl_studios.name AS studioname, tbl_date_studios.full_rates FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());

		if(mysql_num_rows($res) > 0) {

			while($row = mysql_fetch_assoc($res)) {
					$cnt = json_decode($row["contacts"],true);
					$row["contact"] = $cnt[0]["fname"]." ".$cnt[0]["lname"];
					unset($row["contacts"]);
					$atmp["info"] = $row;
					//$atmp["data"] = get_date_studio_stats($row["studioid"],$tourdateid);
					$atmp["info"]["total_fees"] = calc_total_studio_fees($row["studioid"],$tourdateid,true);
					$studios[] = $atmp;

			}

			if(count($studios) > 0) {
				$tmps = array();
				$tmps2 = array();
				$tmps3 = array();
				foreach($studios as $studio) {

					$studio["info"]["studioname"] = stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["info"]["studioname"])));
					$studio["info"]["studioname"] = $studio["info"]["independent"] == 1 ? $studio["info"]["studioname"]." (".$studio["info"]["contact"].")" : $studio["info"]["studioname"];
					$studio["info"]["free_teacher_value"] = number_format(db_one("free_teacher_value","tbl_date_studios","tourdateid='$tourdateid' AND studioid='".$studio["info"]["studioid"]."'"),2,'.','');
					$studio["info"]["credit"] = strlen($studio["info"]["credit"]) > 0 ? number_format(doubleval($studio["info"]["credit"]),2,'.','') : "0.00";
					$studio["info"]["fees_paid"] = strlen($studio["info"]["fees_paid"]) > 0 ? number_format($studio["info"]["fees_paid"],2,'.','') : "0.00";
					$studio["info"]["total_fees"] = $studio["info"]["total_fees"] - $studio["info"]["free_teacher_value"];
					$studio["info"]["balance_due"] = $studio["info"]["total_fees"] - $studio["info"]["fees_paid"] - $studio["info"]["credit"];

					if($studio["info"]["balance_due"] != 0) {

						$studio["data"]["balance_due"] = strpos($studio["data"]["balance_due"],"(") > -1 ? number_format((-1 * (str_replace(array("(",")"),"",$studio["data"]["balance_due"]))),2,'.','') : number_format($studio["data"]["balance_due"],2,'.','');
						$tmps[] = $studio;
						$tmps2[$studio["info"]["studioid"]] = $studio["data"]["balance_due"];
					}
				}
				$studios = $tmps;

//				arsort($tmps2);
				if(count($tmps2) > 0) {
					foreach($tmps2 as $tmpk => $tmpv) {
						foreach($tmps as $origk => $origv) {
							if($origv["info"]["studioid"] == $tmpk)
								$tmps3[] = $tmps[$origk];
						}
					}
				}

				$studios = $tmps3;
			}
		}
	}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Georgia;font-size:20px;"><?php print($city); ?> Studios - (Owes &amp; Balances)</div>
			<span style="font-family:Georgia;font-size:14px;"><?php print($venue); ?> / <?php print($start_date."-".$end_date." $yy2"); ?></span>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
				<tr>
					<td class="thead" style="width:265px;">Studio</td>
					<td class="thead" style="width:100px;">Total Fees</td>
					<td class="thead" style="width:100px;">Fees Paid</td>
					<td class="thead" style="width:90px;">Credit</td>
					<td class="thead" style="border-right: 1px solid #000000;width:95px;">Difference</td>
				</tr>
			<?php
				if(count($studios) > 0) {
					foreach($studios as $studio) { ?>
						<tr>
							<td class="tbody"><?php if($studio["info"]["independent"] == 1) print("[I]"); ?> <?php print($studio["info"]["studioname"]); if(strlen($studio["info"]["studiocode"]) > 0) print(" (".$studio["info"]["studiocode"].")"); ?></td>
							<td class="tbody"><?php print($studio["info"]["total_fees"]); ?></td>
							<td class="tbody"><?php print($studio["info"]["fees_paid"]); ?></td>
							<td class="tbody"><?php print($studio["info"]["credit"]); ?></td>
							<td class="tbody" style="border-right: 1px solid #000000;"><?php print($studio["info"]["balance_due"]); ?></td>
						</tr>
			<?php 		}
				} ?>
			</table>
		</div>
	</body>
</html>
