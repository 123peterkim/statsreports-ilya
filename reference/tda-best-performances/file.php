<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);
	$excelonly = $_GET["xls"] == 1 ? true: false;

	$data = array();
	$sql = "SELECT tbl_date_special_awards.id AS dateawardid, tbl_date_special_awards.dateroutineid, tbl_special_awards.name AS awardname, tbl_date_routines.routineid, tbl_routines.name AS routinename, tbl_date_routines.agedivisionid, tbl_date_routines.number_finals, tbl_date_routines.finals_has_a, tbl_studios.name AS studioname FROM `tbl_date_special_awards` LEFT JOIN tbl_special_awards ON tbl_special_awards.id=tbl_date_special_awards.awardid LEFT JOIN tbl_date_routines ON tbl_date_routines.id = tbl_date_special_awards.dateroutineid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_routines.studioid WHERE tbl_date_special_awards.tourdateid='$tourdateid' ORDER BY tbl_special_awards.report_order ASC";

	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {

		while($row = mysql_fetch_assoc($res)) {
			$row["dispnumber"] = $row["finals_has_a"] == 1 ? $row["number_finals"].".a" : $row["number_finals"];
			$row["studioname"] = stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$row["studioname"])));
			$data[$row["awardname"]][] = $row;
		}
	}

	if($excelonly) {
		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("Routine","Studio","Special Award");
		foreach($data as $awardname=>$winners) {
			foreach($winners as $winner) {
				$dataArray[] = array(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$winner["routinename"]))),stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$winner["studioname"]))),$winner["awardname"]);
			}
		}
		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "TDA_specialawardwinners_$tourdateid".substr($mk,6,5);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);
		exit();
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:705px;margin: 0 auto;">
			<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;">TDA <?php print($city); ?> Special Award Winners</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:30px;"><?php print($venue." - ".$dispdate); ?></div>

	<!-- SOTY -->
	<?php
		if(count($data) > 0) { ?>
		<?php
			foreach($data as $specialawardname=>$winners) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;"><?php print($specialawardname); ?></div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:30px;">
				<tr>
					<td class="thead" style="width:385px;">Routine Name</td>
					<td class="thead" style="width:385px;">Studio Name</td>
					<td class="thead" style="width:100px;border-right:1px solid #000000;">Finals #</td>
				</tr>
		<?php
				foreach($winners as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody"><?php print($arow["routinename"]); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;text-align:center;font-weight:bold;"><?php print($arow["dispnumber"]); ?></td>
				</tr>
			<?php
				} ?>
			</table>
		<?php
		 	}
		 } ?>
		</div>
	</body>
</html>