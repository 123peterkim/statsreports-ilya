"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_dts_registrations = sequelize.define('tbl_dts_registrations', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		admin_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_admins',
				key: 'id'
			}
		},
		heard: {
			type: DataTypes.STRING
		},
		notes: {
			type: DataTypes.TEXT
		},
		fees: {
			type: DataTypes.JSONB
		},
		balance_due: {
			type: DataTypes.REAL
		},
		boxsets: {
			type: DataTypes.JSONB
		},
		extra: {
			type: DataTypes.JSONB
		},
		edu: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_dts_registrations;
};