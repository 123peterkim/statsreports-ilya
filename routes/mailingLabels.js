var _ = require('lodash');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');
var util = require('../reference/util/util');

module.exports = {
    renderReport: function (req, res) {
        var tourDateId = req.query.tourdateid || 746;
        var DateStudios = db.date_studios
        /* 
        SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.contacts, tbl_studios.address, tbl_studios.city, tbl_studios.zip, tbl_studios.state, tbl_countries.name AS countryname 
        FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid LEFT JOIN tbl_countries ON tbl_countries.id = tbl_studios.countryid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC
        */

        DateStudios.findAll({
            where: {
                tour_date_id: tourDateId
            },
            include: [
                { model: db.tbl_studios, as: 'studio', include: [
                    { model: db.tbl_addresses, as: 'address', include: [
                        { model: db.tbl_countries, as: 'country' },
                        { model: db.tbl_states, as: 'state' }
                    ]},
                    { model: db.tbl_mybtf_users, as: 'users', include: [
                        { model: db.tbl_persons, as: 'person' }
                    ]}
                ]}
            ]
        })
        .then(function(results) {
            var data = {};
            data.contacts = [];
            _.forEach(results, function(dateStudio) {
                _.forEach(dateStudio.studio.users, function(user) {
                    data.contacts.push({
                        contact: user.person.fname+' '+user.person.lname,
                        studio_name: dateStudio.studio.name,
                        address: dateStudio.studio.address.address,
                        state: dateStudio.studio.address.state.abbr,
                        zip: dateStudio.studio.address.zip,
                        country_name: dateStudio.studio.address.country.name
                    });
                });
            });
            if (req.params.type == 1) {
                res.render('reports/mailing-labels-1', data);
            }
            else if (req.params.type == 2) {
                res.render('reports/mailing-labels-2', data);
            }
        })
        .catch(err => {
            console.log('ERR', err);
            res.status(500).json(err);
        });
    }
}