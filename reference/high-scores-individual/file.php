<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
	$compgroup = mysql_real_escape_string($_GET["compgroup"]);
	$save =  mysql_real_escape_string($_GET["save"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));

	$maxplaces = intval($_GET["max"]);

	$adorder = Array(5,6,7,10,11,1,2,3,4);
	$pdorder = Array(1,9,2,3,4,5,7,8,10);

	$awards = Array();
	for($i=0;$i<count($adorder);$i++) {
		$sql = "SELECT * FROM `tbl_age_divisions` WHERE id=".$adorder[$i];
		$res = mysql_query($sql) or die(mysql_error());
		while($row = mysql_fetch_assoc($res)) {
			$adnames = $row["name"];
			$adranges = $row["range"];
			$adname = $row["name"];
		}
		$adname = "$adnames ($adranges)";
		for($j=0;$j<count($pdorder);$j++) {
			$routines = array();
			$place = 0;
			$lastscore = 0;
			$lastdropped = 0;
			$tiebreaker = 0;
			$pdname = db_one("name","tbl_performance_divisions","id=".$pdorder[$j]);

			//MAKE SURE TO NOT INCLUDE SHOWCASE, ADJUDICATED ONLY OR PERFORMANCE
			$sql = "SELECT tbl_date_routines.routineid,tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_age_divisions.name AS agedivisionname, tbl_performance_divisions.name AS perfdivisionname, tbl_routines.name AS routinename, tbl_date_routines.".$compgroup."_total_score AS total_score, tbl_date_routines.".$compgroup."_dropped_score AS dropped_score, tbl_date_routines.".$compgroup."_awardid AS awardid, tbl_date_routines.".$compgroup."_dropped_score2 AS dropped_score2, tbl_studios.name AS studioname FROM `tbl_date_routines` LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id=tbl_date_routines.perfcategoryid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id=tbl_date_routines.agedivisionid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id=tbl_date_routines.routinecategoryid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_routines.studioid WHERE tbl_date_routines.routinecategoryid>2 AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.$compgroup=1 AND tbl_date_routines.perfcategoryid=".$pdorder[$j]." AND tbl_date_routines.agedivisionid=".$adorder[$i]." AND tbl_date_routines.routinetypeid=1 ORDER BY tbl_date_routines.".$compgroup."_total_score DESC, tbl_date_routines.".$compgroup."_dropped_score DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {

				if($save) {
					$sql5 = "UPDATE `tbl_date_routines` SET place_hsp=0 WHERE tourdateid=$tourdateid";
					$res5 = mysql_query($sql5) or die(mysql_error());
				}

				while($row = mysql_fetch_assoc($res)) {

					$row["place"] = "-";
					$row["adname"] = $adname;
					if($row["awardid"] > 0)
						$row["awardname"] = db_one("name","tbl_competition_awards","id=".$row["awardid"]);
					else
						$row["awardname"] = "-";

					if($row["dropped_score2"] > 0)
						$row["dispnumbers"] = "(".$row["total_score"]." : ".$row["dropped_score"]."/".$row["dropped_score2"].")";
					else
						$row["dispnumbers"] = "(".$row["total_score"]." : ".$row["dropped_score"].")";

					$row["dispname"] = $row["routinename"];

					if(db_one("SUM(".$compgroup."_total_score)","tbl_date_routines","tourdateid=$tourdateid") > 0) {
						if($maxplaces > 1) {
							if($row["total_score"] > 0 && $place < $maxplaces) {
								if($row["dropped_score2"] > 0)
									$tiebreaker = ceil(($row["dropped_score"] + $row["dropped_score2"])/2);
								else
									$tiebreaker = $row["dropped_score"];

								if($lastscore != $row["total_score"]) {
									++$place;
								}
								else {
									if($tiebreaker == $lastdropped) {

									}
									else {
										++$place;
									}
								}

								if($place == 1)
									$row["place"] = "1st";
								if($place == 2)
									$row["place"] = "2nd";
								if($place == 3)
									$row["place"] = "3rd";

								$lastscore = $row["total_score"];
								$lastdropped = $tiebreaker;

								$routines[count($routines)-1] = $row;
							}
						}
						if($maxplaces == 1) {
							if($row["total_score"] > 0 && $place <= $maxplaces) {
								if($row["dropped_score2"] > 0)
									$tiebreaker = ceil(($row["dropped_score"] + $row["dropped_score2"])/2);
								else
									$tiebreaker = $row["dropped_score"];

								if($lastscore != $row["total_score"]) {
									++$place;
								}
								else {
									if($tiebreaker == $lastdropped) {

									}
									else {
										++$place;
									}
								}

								if($place == 1) {
									$row["place"] = "1st";

									$lastscore = $row["total_score"];
									$lastdropped = $tiebreaker;

									$routines[count($routines)-1] = $row;
								}
							}
						}
						else {
						}
					}
					else {
						$routines[] = $row;
					}

				}
				if(count($routines) > 0)
					$awards[$adname][$pdname] = array_reverse($routines);


			}
		}

	}

	//print_r($awards);exit();

	//save if flagged true
	if($save) {
		if(count($awards) > 0) {

			foreach($awards as $ad=>$pd) {
				foreach($pd as $apd=>$routines) {
					foreach($routines as $routine) {

						$place = intval(substr($routine["place"],0,1));
						$sql = "UPDATE `tbl_date_routines` SET place_hsp='$place' WHERE routineid=".$routine["routineid"]." AND tourdateid=$tourdateid LIMIT 1";
						$res = mysql_query($sql) or die(mysql_error());
					}
				}
			}
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 7.9pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;">
							<?php print($city); ?> Placement Awards
							<br/>
							<span style="font-size:14pt;">(By Performance Division) Top <?=$maxplaces;?> Indiv.</span>
						</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print(ucfirst($compgroup)." Competition"); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<?php
				if(count($awards) > 0) {
					foreach($awards as $agedivision=>$perfdivisions) {
						if(count($perfdivisions) > 0) {
							foreach($perfdivisions as $pdname=>$routines) { ?>
								<div style="margin:10px 0 3px;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;">
									<?php print($agedivision." : ".$pdname); ?>
								</div>
								<table cellpadding="0" cellspacing="0" class="rtable" style="margin:0px 0 20px;">
									<tr>
										<td class="thead" style="width:65px;">Place</td>
										<td class="thead" style="width:165px;">Award</td>
										<td class="thead" style="width:285px;">Routine/Dancer</td>
										<td class="thead" style="width:230px;">Studio</td>
										<td class="thead" style="width:130px;border-right:1px solid #000000;">Category</td>
									</tr>
								<?php
									foreach($routines as $routine) { ?>
										<tr>
											<td class="tbody" style="text-align:center;"><?php print($routine["place"]); ?></td>
											<td class="tbody" style="text-align:center;"><?php print($routine["awardname"]." ".$routine["dispnumbers"]); ?></td>
											<td class="tbody"><?php print($routine["dispname"]); ?></td>
											<td class="tbody"><?php print($routine["studioname"]); ?></td>
											<td class="tbody" style="border-right:1px solid #000000;"><?php print($routine["agedivisionname"]." ".$routine["routinecategoryname"]); ?></td>
										</tr>
								<?php } ?>
								</table>
					<?php } ?>
				<?php	}
				}
			} ?>
		</div>
	</body>
</html>