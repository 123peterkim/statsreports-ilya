"use strict";

module.exports = function(sequelize, DataTypes) {
  var studios_teachers = sequelize.define('studios_teachers', {
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      },
      primaryKey: true
    },
    teacher_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_teachers',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {
      studios_teachers.belongsTo(models.tbl_teachers, {foreignKey: 'teacher_id', as: 'teacher'});
      studios_teachers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
    }
  });
  return studios_teachers;
};