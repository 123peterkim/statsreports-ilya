"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_routine_categories = sequelize.define('tbl_routine_categories', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		category_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_categories',
				key: 'id'
			}
		},
		season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			}
		},
		old_id: {
			type: DataTypes.INTEGER
		},
		min_dancers: {
			type: DataTypes.INTEGER
		},
		duration: {
			type: DataTypes.INTEGER
		},
		abbreviation: {
			type: DataTypes.STRING
		},
		fee_per_dancer: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {
      tbl_routine_categories.belongsTo(models.tbl_categories, {foreignKey: 'category_id', as: 'category'});

		}
	});
	return tbl_routine_categories;
};