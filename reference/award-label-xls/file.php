<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = intval(db_one("eventid","tbl_tour_dates","id=$tourdateid"));
	$cityname = strtolower(str_replace(array(" ","-",","),"",db_one("city","tbl_tour_dates","id=$tourdateid")));
	$routines = array();
	//get all studios
	$cgs = array("vips","prelims","finals");
	$routines = array();
	foreach($cgs as $cg) {
		$sql = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.routineid, tbl_date_routines.number_".$cg." AS number, tbl_date_routines.".$cg."_has_a AS has_a, tbl_date_studios.id AS datestudioid, tbl_date_studios.studiocode, tbl_date_studios.studioid, tbl_routines.name AS routinename, tbl_studios.name AS studioname FROM `tbl_date_routines` LEFT JOIN tbl_date_studios ON tbl_date_studios.studioid=tbl_date_routines.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_studios.tourdateid=$tourdateid AND tbl_date_routines.$cg=1 ORDER BY tbl_date_routines.number_$cg ASC, tbl_date_routines.".$cg."_has_a ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$row["dispnumber"] = $row["has_a"] == 1 ? $row["number"].".A" : $row["number"];
				$row["studioname"] = str_replace(",","",stripslashes(str_replace("&amp;","&",str_replace("&#44;","",$row["studioname"]))));
				$row["routinename"] = str_replace(",","",stripslashes(str_replace("&amp;","&",$row["routinename"])));
				$row["routinename"] = str_replace(",","",$row["routinename"]);
				$row["dcount"] = db_one("COUNT(id)","tbl_date_routine_dancers","tourdateid=$tourdateid AND routineid=".$row["routineid"]);
				$routines[] = $row;
			}
		}
	}

	//$filename = "awardlabels_$cityname.csv";

	if(count($routines) > 0) {
		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("Number","Routine Name","Studio Name","Studio Code","Dancer #");
		foreach($routines as $routine) {
			$dataArray[] = array($routine["dispnumber"],$routine["routinename"],$routine["studioname"],$routine["studiocode"],$routine["dcount"]);
		}
		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "awardslabels_$cityname".substr($mk,6,5);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);

/*
		echo("Number\tRoutine\tStudio\tStudio Code\tDancerCount\r");
		foreach($routines as $routine) {
			echo($routine["dispnumber"]."\t".$routine["routinename"]."\t".$routine["studioname"]."\t".$routine["studiocode"]."\t".$routine["dcount"]."\r");
		}
*/
	}

	exit();
?>