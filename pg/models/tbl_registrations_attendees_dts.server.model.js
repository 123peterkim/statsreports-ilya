"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_attendees_dts = sequelize.define('tbl_registrations_attendees_dts', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		dts_reg_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dts_reg_types',
				key: 'id'
			}
		},
		promo_code_ic: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_promo_codes',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		fee: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_registrations_attendees_dts;
};