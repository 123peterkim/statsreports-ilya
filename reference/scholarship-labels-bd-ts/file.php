<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);

	//select scholarship_code, dancer name, studio name.  order by studio name AND scholarship_code
	$sql = "SELECT tbl_tda_bestdancer_data.id, tbl_date_dancers.scholarship_code, tbl_date_dancers.id as datedancerid, tbl_date_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_studios.name as studioname FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_profiles ON tbl_profiles.id =tbl_date_dancers.profileid LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid WHERE tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_tda_bestdancer_data.iscompeting=1 AND tbl_date_dancers.tourdateid = $tourdateid AND (tbl_date_dancers.workshoplevelid=2 OR tbl_date_dancers.workshoplevelid=3) AND tbl_date_dancers.scholarship_code > 0 ORDER BY tbl_date_dancers.age ASC, tbl_profiles.lname ASC, tbl_profiles.fname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$dancers[] = $row;
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Teen/Senior Best Dancer Scholarship Labels</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-family: OfficinaSansCTT, Tahoma, sans-serif;
			}

			.sl_table tr td {
				height:290px !important;
				overflow:none;
				padding: 50px 0 0 0;
				width: 330px;
				text-align: center;
			}
		</style>
		<script type="text/javascript">
	//		window.print();
		</script>
	</head>
	<body>
		<?php
			for($i=0;$i<count($dancers);$i+=2) { ?>
		<div style="width:720px;height:955px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" class="sl_table">
				<?php
					for($j=0;$j<3;$j++) { ?>
				<tr>
					<td style="vertical-align: top;">
						<?php if($dancers[$i]) { ?>
							<div style="<?php if(strlen($dancers[$i]["scholarship_code"]) > 3) print("font-size:110pt"); else print("font-size:130pt"); ?>;"><?php print($dancers[$i]["scholarship_code"]); ?></div>
							<div style="font-size:15pt;font-style:italic;font-weight:bold;"><?php print($dancers[$i]["fname"]." ".$dancers[$i]["lname"]); ?></div>
							<div style="font-size:10pt;font-style:italic;"><?php print(stripslashes($dancers[$i]["studioname"])); ?></div>
						<?php } ?>
					</td>
					<td style="vertical-align: top;padding-left:50px;">
						<?php if($dancers[$i+1]) { ?>
							<div style="<?php if(strlen($dancers[$i+1]["scholarship_code"]) > 3) print("font-size:110pt"); else print("font-size:130pt"); ?>;"><?php print($dancers[$i+1]["scholarship_code"]); ?></div>
							<div style="font-size:15pt;font-style:italic;font-weight:bold;"><?php print($dancers[$i+1]["fname"]." ".$dancers[$i+1]["lname"]); ?></div>
							<div style="font-size:10pt;font-style:italic;"><?php print(stripslashes($dancers[$i+1]["studioname"])); ?></div>
						<?php } ?>
					</td>
				</tr>
				<?php } ?>
			</table>
		</div>
		<?php  } ?>
	</body>