<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$dcounts = json_decode(db_one("tda_danceoff_bestdancer_counts","tbl_tour_dates","id=$tourdateid"),true);
	$highestsolo = db_one("MAX(vips_total_score)","tbl_date_routines","tourdateid=$tourdateid");
	$maxball = db_one("MAX(ballet)","tbl_tda_bestdancer_data","tourdateid=$tourdateid");
	$maxjazz = db_one("MAX(jazz)","tbl_tda_bestdancer_data","tourdateid=$tourdateid");
	$highestclass = ($maxball + $maxjazz);
	$jcount = db_one("tda_danceoff_judge_count","tbl_tour_dates","id=$tourdateid");
	//get dateroutineid, routine #, has a, studio code, studio name, age division, routinecategory, performance division, time
	$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_tda_bestdancer_data.ballet, tbl_tda_bestdancer_data.jazz, tbl_tda_bestdancer_data.danceoff, tbl_tda_bestdancer_data.perc_solo, tbl_tda_bestdancer_data.perc_ballet, tbl_tda_bestdancer_data.groupid, tbl_tda_bestdancer_data.perc_danceoff, tbl_tda_bestdancer_data.danceoff_max, tbl_tda_bestdancer_data.round1_place, tbl_tda_bestdancer_data.round2_place, tbl_tda_bestdancer_data.round3_place, tbl_tda_bestdancer_data.perc_total, tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.routineid, tbl_date_routines.vips_awardid, tbl_competition_awards.name AS awardname, tbl_date_routines.number_vips AS number, tbl_date_routines.vips_has_a AS has_a, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender, tbl_date_studios.studiocode, tbl_date_routines.vips_score1, tbl_date_routines.vips_score2, tbl_date_routines.vips_score3, tbl_date_routines.vips_score4, tbl_date_routines.vips_score5, tbl_date_routines.vips_score6, tbl_date_routines.vips_total_score, tbl_date_routines.agedivisionid, tbl_routines.name AS routinename FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_routines ON tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_date_studios ON tbl_date_studios.studioid=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id=tbl_date_routines.vips_awardid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_tda_bestdancer_data.routineid WHERE tbl_tda_bestdancer_data.iscompeting=1 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_routines.number_vips ASC, tbl_date_routines.vips_has_a ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {

			$adname = db_one("name","tbl_age_divisions","id=".$row["agedivisionid"]);
			$dcpos = strtolower(substr($adname,0,1).substr($row["gender"],0,1));
			$row["docount"] = $dcounts[$dcpos];
			$row["highestdanceoff"] = db_one("MIN(danceoff)","tbl_tda_bestdancer_data","groupid='".$row["groupid"]."' AND tourdateid=$tourdateid");
			$row["vips_score1"] = $row["vips_score1"]." pts.";
			$row["vips_score2"] = $row["vips_score2"]." pts.";
			$row["vips_score3"] = $row["vips_score3"]." pts.";
			$row["vips_score4"] = $row["vips_score4"]." pts.";
			$row["vips_score5"] == "0" ? $row["vips_score5"] = "-" : $row["vips_score5"] = $row["vips_score5"]." pts.";
			$row["vips_score6"] == "0" ? $row["vips_score6"] = "-" : $row["vips_score5"] = $row["vips_score6"]." pts.";
			$row["dispnum"] = $row["has_a"] == "1" ? $row["number"].".a" : $row["number"];
			if($row["danceoff"] == 0)
				$row["danceoff"] = "N/A";
			$routines[] = $row;
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title><?php print($eventid == 14 ? "TDA" : "NSD"); ?> Tally Sheets</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.tr_table tr td{
				font-family: Verdana, Arial, sans-serif;
				color: #555555;
				font-size: 10px;
				padding-right:3px;
			}

			.rtable tr td {
				font-size: 10px;
				text-align: right;
			}

			.inner_table tr td{
				font-size: 11px;
				font-family: Verdana, Arial, sans-serif;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
	</head>
	<body>
	<?php
		if(count($routines) > 0) {
		foreach($routines as $routine) { ?>
		<div style="page-break-after:always;width:720px;height:950px;">
			<table cellpadding="0" cellspacing="0" style="width:100%;margin-top:10px;">
				<tr>
					<td style="vertical-align:top;">
					<?php
						if($eventid == 14) { ?>
						<img src="tda_logo.jpg" style="width:auto;height:121px;margin: 0 auto;display:block;" alt="" />
						<div style="font-family:'Trajan Pro';font-size:20pt;margin: 10px 0 0 0;text-align:center;">BEST DANCER TALLY SHEET</div>
					<?php }
						if($eventid == 18) { ?>
						<img src="../../24seven_logo.png" style="width:auto;height:135px;margin: 0 auto;display:block;" alt="" />
						<div style="font-size:16pt;margin: 10px 0 0 0;text-align:center;">NON-STOP DANCER TALLY SHEET</div>
					<?php } ?>

					</td>
				</tr>
			</table>
			<div style="text-align:center;margin-top:12px;font-size:20px;"><?php print(strtoupper($routine["fname"]." ".$routine["lname"]));?></div>
			<div style="text-align:center;margin-bottom:22px;font-size:14px;"><?php print(strtoupper($routine["routinename"]." (".$routine["studiocode"].")"));?></div>

			<div style="width:720px;height:455px;border:1px solid #000000;margin:15px 0 0 0;">
				<div style="width:100%;border-bottom:1px solid #000000;height:160px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:393px;vertical-align:top;font-family: 'Trajan Pro';font-size:20pt;font-style:italic;">SOLO&nbsp;
							</td>
							<td style="width:320px;vertical-align:middle;padding-left:4px;font-weight:bold;font-size:12pt;font-style:italic;text-align:right;">50% of score&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<table cellpadding="0" cellspacing="0" class="inner_table" style="width: 99%;">
									<tr>
										<td style="width:570px;padding-left:5px;">
											<table cellpadding="0" cellspacing="0" style="width: 100%;">
												<tr>
													<td style="width: 55px;font-style:italic;">Judge #1:</td><td style="width: 75px;font-style:italic;font-weight:bold;text-align:center;"><?php print($routine["vips_score1"]);?>&nbsp;&nbsp;&nbsp;</td>
													<td style="width: 55px;font-style:italic;">Judge #2:</td><td style="width: 75px;font-style:italic;font-weight:bold;text-align:center;"><?php print($routine["vips_score2"]);?>&nbsp;&nbsp;&nbsp;</td>
													<td style="width: 55px;font-style:italic;">Judge #3:</td><td style="width: 75px;font-style:italic;font-weight:bold;text-align:center;"><?php print($routine["vips_score3"]);?>&nbsp;&nbsp;&nbsp;</td>
													<td style="width: 55px;font-style:italic;">Judge #4:</td><td style="width: 75px;font-style:italic;font-weight:bold;text-align:center;"><?php print($routine["vips_score4"]);?>&nbsp;&nbsp;&nbsp;</td>
												</tr>
												<tr>
													<td style="font-style:italic;"><!--Judge #5:</td><td style="font-style:italic;font-weight:bold;text-align:center;"><?php print($routine["vips_score5"]);?>&nbsp;&nbsp;&nbsp;--></td>
													<td style="font-style:italic;"><!-- Judge #6:</td><td style="font-style:italic;font-weight:bold;text-align:center;"><?=$routine["vips_score6"];?>&nbsp;&nbsp; -->&nbsp;</td>
													<td>&nbsp;</td><td>&nbsp;</td>
													<td>&nbsp;</td><td>&nbsp;</td>
												</tr>
												<tr>
													<td colspan="8" style="font-style:italic;padding-top:9px;color:#777777;">
														Each judge can give a maximum of 100 points. Your lowest score is dropped, for a combined total of 300 possible points.</td>
												</tr>
											</table>
										</td>
										<td style="text-align:right;">
											<span style="font-size:10px;font-style:italic;">YOUR SOLO SCORE:</span>
											<br/>
											<span style="font-size:30px;font-family:Georgia;"><?php print($routine["vips_total_score"]);?></span>
											<div style="font-size:11px;font-style:italic;font-weight:bold;color:#555555;margin-top:5px;"><?php print($routine["awardname"]);?></div>
											<div style="margin-top:5px;font-size:11px;color:#777777;font-style:italic;">Highest Solo Score:<br/><span style="font-weight:bold;"><?php print($highestsolo);?></span>&nbsp;</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:100%;border-bottom:1px solid #000000;height:132px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:413px;vertical-align:top;font-family: 'Trajan Pro';font-size:20pt;font-style:italic;">AUDITION CLASS</td>
							<td style="width:300px;vertical-align:middle;padding-left:4px;font-weight:bold;font-size:12pt;font-style:italic;text-align:right;">20% of score&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<table cellpadding="0" cellspacing="0" class="inner_table" style="width: 99%;">
									<tr>
										<td style="width:570px;padding-left:5px;">
											<table cellpadding="0" cellspacing="0" style="width: 100%;">
												<tr>
													<td style="font-style:italic;padding-top:9px;color:#777777;">
														Three judges score each dancer from 1-100 in both the Ballet Audition Class and Jazz Audition Class for a total of 600 possible points.
													</td>
												</tr>
											</table>
										</td>
										<td style="text-align:right;">
											<span style="font-size:10px;font-style:italic;">YOUR CLASS SCORE:</span>
											<br/>
											<span style="font-size:30px;font-family:Georgia;"><?=($routine["ballet"] + $routine["jazz"]);?></span>
											<div style="margin-top:10px;font-size:11px;color:#777777;font-style:italic;">Highest Class Score:<br/><span style="font-weight:bold;"><?php print($highestclass);?>&nbsp;</span></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:100%;height:140px;">
					<table cellpadding="0" cellspacing="0" style="margin:6px 0 0 2px;">
						<tr><td style="width:593px;vertical-align:top;font-family: 'Trajan Pro';font-size:20pt;font-style:italic;">DANCE-OFF
							</td>
							<td style="width:120px;vertical-align:middle;padding-left:4px;font-weight:bold;font-size:12pt;font-style:italic;text-align:right;">30% of score&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								<table cellpadding="0" cellspacing="0" class="inner_table" style="width: 99%;">
									<tr>
										<td style="width:570px;padding-left:5px;">
											<table cellpadding="0" cellspacing="0" style="width: 100%;">
												<tr>
													<td style="font-style:italic;padding-top:9px;color:#777777;">
														The Top Ten <?php print($eventid == 14 ? "Best Dancers" : "Non-Stop Dancers"); ?> in each age category (after your solo &amp; audition class) compete against each other in the Dance-Off. Each judge ranks the dancers #1 through #10. Your score is the judges' combined rank.<br/>Example: 10 Judges & 10 dancers - the lowest (best) possible score is 10, and the highest is 100.
													</td>
												</tr>
											</table>
										</td>
										<td style="text-align:right;">
											<span style="font-size:10px;font-style:italic;">YOUR DANCE-OFF SCORE:</span>
											<br/>
											<span style="font-size:30px;font-family:Georgia;"><?php print($routine["danceoff"]);?></span>
											<div style="margin-top:10px;font-size:11px;color:#777777;font-style:italic;">Best Dance-Off Score:<br/><span style="font-weight:bold;"><?php print($jcount);?></span></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div style="width:720px;margin-top:15px;height:203px;">
				<table cellpadding="0" cellspacing="0" style="width:100%;">
					<tr>
						<td style="vertical-align:top;font-weight:bold;font-size:11px;width:500px;font-family:Verdana,Arial,sans-serif;padding-left:0px;">
<!-- 							<span style="font-style:italic;">OTHER SCORING INFO:</span> -->
						</td>
						<td style="vertical-align:top;font-weight:bold;font-size:11px;font-family:Verdana,Arial,sans-serif;text-align:right;">
							TOTAL SCORE:
							<br/>
							<span style="font-family:Georgia;font-size:32px;"><?php print($routine["perc_total"]);?>%</span>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:720px;margin-top:25px;border-top:1px solid #777777;padding-top:5px;">
				<table cellpadding="0" cellspacing="0" style="width: 100%;">
					<tr>
						<td style="color:#555555;font-family:Tahoma,Arial,sans-serif;font-size:10px;"><?php print($eventid == 14 ? "The Dance Awards" : "24SEVEN Nationals"); ?> &nbsp;&nbsp;/ <span style="font-weight:bold;">&nbsp;&nbsp;<?php print(date('Y'));?></span></td>
						<td style="color:#555555;font-family:Tahoma,Arial,sans-serif;font-size:10px;text-align:right;">
							<?php print($routine["dispnum"]." ".strtoupper($routine["routinename"]." (".$routine["studiocode"].")"));?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	<?php  }
	} ?>
	</body>
</html>