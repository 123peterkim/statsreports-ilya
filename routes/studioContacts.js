var Excel = require('exceljs');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');

module.exports = {
    studioContactSpreadsheet: function (req, res) {
        // url query params
        var tourDateId = req.query.tourdateid || 746;
        var TourDates = db.tbl_tour_dates;
        var DateStudios = db.date_studios;
        var wb = new Excel.Workbook();
        var ws = wb.addWorksheet('Worksheet');

        async.parallel([
            function(cb) {
                // SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.independent, tbl_studios.name AS studioname, tbl_studios.email, tbl_studios.address, tbl_studios.city, tbl_studios.state, tbl_studios.contacts, tbl_studios.phone, tbl_studios.fax, tbl_studios.zip, tbl_countries.name AS country FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid LEFT JOIN tbl_countries ON tbl_countries.id=tbl_studios.countryid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC
                DateStudios.findAll({
                    where: {
                        tour_date_id: tourDateId
                    },
                    include: [
                        { model: db.tbl_studios, as: 'studio', include: [
                            { model: db.tbl_mybtf_users, as: 'users', include: [
                                { model: db.tbl_persons, as: 'person' }
                            ]},
                            { model: db.tbl_addresses, as: 'address', include: [
                                { model: db.tbl_states, as: 'state' },
                                { model: db.tbl_countries, as: 'country' }
                            ]},
                            { model: db.tbl_contact_info, as: 'phone' },
                            { model: db.tbl_contact_info, as: 'email' }
                        ]},
                        { model: db.tbl_registrations, as: 'registrations' }
                    ]
                })
                .then(function(result) {
                    cb(null, result);
                })
                .catch(function(err) {
                    cb(err);
                });
            },
            function(cb) {
                // db_one("city","tbl_tour_dates","id=$tourdateid"))
                TourDates.findOne({
                    where: { id: tourDateId },
                    include: [ { model: db.tbl_event_cities, as: 'event_city' } ]
                })
                .then(function(tableDate) {
                    var city = "";
                    try {
                        city = tableDate.event_city.name;
                        cb(null, city);
                    }
                    catch (err) {
                        cb(err);
                    }
                })
                .catch(function(err) {
                    cb(err);
                });
            }
        ],
        function (err, results) {
            var studioContactData = [];
            var city = results[1]

            _.forEach(results[0], function (data) {
                var studio = data.studio;

                var registration = data.registrations[0];
                var person = studio.users[0] ? studio.users[0].person : {};
                var address = studio.address;
                var phone = studio.phone[0] || {};
                var email = studio.email[0] || {};
                var state = address.state || {};
                var country = address.country || {};

                try { email = studio.users[0].email; }
                catch (err) { email = ""; }

                studioContactData.push({
                    fname: person.fname,
                    lname: person.lname,
                    studio_name: studio.name,
                    independent: registration.independent ? 'Yes' : 'No',
                    full_name: person.fname + " " + person.lname,
                    address: address.address,
                    city: address.city,
                    state: state.name,
                    zip: address.zip,
                    phone: phone.value,
                    email: email.value,
                    country: country.name
                });
            });

            // TODO: setup auto width
            // https://stackoverflow.com/a/16762003
            ws.columns = [
                { header: "First Name", key: "fname", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Last Name", key: "lname", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Studio", key: "studio_name", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Independent?", key: "independent", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Full Name", key: "full_name", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Address", key: "address", width: 10, style: { font: { name: 'Arial' } } },
                { header: "City", key: "city", width: 10, style: { font: { name: 'Arial' } } },
                { header: "State", key: "state", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Zip", key: "zip", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Phone", key: "phone", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Email", key: "email", width: 10, style: { font: { name: 'Arial' } } },
                { header: "Country", key: "country", width: 10, style: { font: { name: 'Arial' } } }
            ];

            ws.addRows(studioContactData);

            var filename = 'public/' + Date.now();

            wb.xlsx.writeFile(filename)
                .then(function () {
                    res.download(filename, city + "_studio_contacts.xlsx", function (err) {
                        if (!err) fs.unlink(filename);
                    });
                });
        });

    }
}