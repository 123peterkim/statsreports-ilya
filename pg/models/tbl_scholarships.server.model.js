"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_scholarships = sequelize.define('tbl_scholarships', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		active: {
			type: DataTypes.INTEGER
		},
		report_order: {
			type: DataTypes.INTEGER
		},
		is_class: {
			type: DataTypes.INTEGER
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_scholarships;
};