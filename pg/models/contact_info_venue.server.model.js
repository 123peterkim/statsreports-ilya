"use strict";

module.exports = function(sequelize, DataTypes) {
  var contact_info_venue = sequelize.define('contact_info_venue', {
    contact_info_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_contact_info',
        key: 'id'
      },
      primaryKey: true
    },
    venue_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_venues',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {
    }
  });
  return contact_info_venue;
};