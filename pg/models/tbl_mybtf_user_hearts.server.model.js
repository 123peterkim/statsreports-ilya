"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_mybtf_user_hearts = sequelize.define('tbl_mybtf_user_hearts', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		store_heart_id: {
			type: DataTypes.STRING,
			references: {
				model: 'tbl_store_hearts',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.STRING,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_mybtf_user_hearts;
};