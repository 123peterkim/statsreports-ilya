"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_addresses = sequelize.define('tbl_addresses', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		city: {
			type: DataTypes.STRING
		},
		state_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_states',
				key: 'id'
			}
		},
		country_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_countries',
				key: 'id'
			}
		},
		address: {
			type: DataTypes.STRING
		},
		zip: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_addresses.belongsTo(models.tbl_states, {foreignKey: 'state_id', as: 'state'});
			tbl_addresses.belongsTo(models.tbl_countries, {foreignKey: 'country_id', as:'country'});
		}
	});
	return tbl_addresses;
};