"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_persons = sequelize.define('tbl_persons', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		address_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_addresses',
				key: 'id'
			}
		},
		gender_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_gender',
				key: 'id'
			}
		},
		person_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_person_types',
				key: 'id'
			}
		},
		fname: {
			type: DataTypes.STRING
		},
		lname: {
			type: DataTypes.STRING
		},
		birthdate: {
			type: DataTypes.DATEONLY
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_persons.belongsTo(models.tbl_addresses, {foreignKey: 'address_id', as: 'address'});
			tbl_persons.belongsTo(models.tbl_gender, {foreignKey: 'gender_id', as: 'gender'});
			tbl_persons.belongsTo(models.tbl_person_types, {foreignKey: 'person_type_id', as: 'person_type'});
			tbl_persons.belongsToMany(models.tbl_contact_info, {foreignKey: 'person_id', otherKey: 'contact_info_id', as: 'phone', through: models.contact_info_person});
      tbl_persons.belongsToMany(models.tbl_contact_info, {foreignKey: 'person_id', otherKey: 'contact_info_id', as: 'email', through: models.contact_info_person});
    }
	});
	return tbl_persons;
};