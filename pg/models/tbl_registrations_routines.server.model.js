"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_routines = sequelize.define('tbl_registrations_routines', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		performance_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_performance_divisions',
				key: 'id'
			}
		},
		routine_category_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routine_categories',
				key: 'id'
			}
		},
		age_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_age_divisions',
				key: 'id'
			}
		},
		routine_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_routines',
        key: 'id'
      }
    },
		extra_time: {
			type: DataTypes.INTEGER
		},
		fee: {
			type: DataTypes.REAL
		},
		award_type: {
			type: DataTypes.STRING
		},
		has_prop: {
			type: DataTypes.BOOLEAN
		},
		on_stage: {
			type: DataTypes.BOOLEAN
		},
		date_routine_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_routines',
        key: 'id'
      }
		},
    free_ballet: {
      type: DataTypes.BOOLEAN
    },
    vips: {
      type: DataTypes.BOOLEAN
    },
    prelims: {
      type: DataTypes.BOOLEAN
    },
    finals: {
      type: DataTypes.BOOLEAN
    },
		age_up: {
			type: DataTypes.BOOLEAN
		}
	}, {
		timestamps: false,
		associate: function(models) {
      tbl_registrations_routines.hasMany(models.registrations_routines_dancers, {foreignKey: 'registrations_routine_id', as: 'dancers'});
      tbl_registrations_routines.belongsTo(models.tbl_age_divisions, {foreignKey: 'age_division_id', as: 'age_division'});
      tbl_registrations_routines.belongsTo(models.tbl_routine_categories, {foreignKey: 'routine_category_id', as: 'routine_category'});
      tbl_registrations_routines.belongsTo(models.tbl_performance_divisions, {foreignKey: 'performance_division_id', as: 'performance_division'});
      tbl_registrations_routines.belongsTo(models.tbl_routines, {foreignKey: 'routine_id', as: 'routine'});
      tbl_registrations_routines.belongsTo(models.date_routines, {foreignKey: 'date_routine_id', as: 'date_routine'});
    }
	});
	return tbl_registrations_routines;
};