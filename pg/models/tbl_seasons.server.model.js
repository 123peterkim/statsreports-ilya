"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_seasons = sequelize.define('tbl_seasons', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		start_year: {
			type: DataTypes.INTEGER
		},
		end_year: {
			type: DataTypes.INTEGER
		},
		event_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_types',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_seasons.belongsTo(models.tbl_event_types, {foreignKey: 'event_type_id', as: 'event_type'});

		}
	});
	return tbl_seasons;
};