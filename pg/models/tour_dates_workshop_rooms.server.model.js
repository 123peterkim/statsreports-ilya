"use strict";

module.exports = function(sequelize, DataTypes) {
	var tour_dates_workshop_rooms = sequelize.define('tour_dates_workshop_rooms', {
		workshop_room_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_workshop_rooms',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		age_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_age_divisions',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tour_dates_workshop_rooms.belongsTo(models.tbl_age_divisions, {foreignKey: 'age_division_id', as: 'age_division'});
		}
	});
	return tour_dates_workshop_rooms;
};