var _ = require('lodash');
var path = require('path');
var db = require('../pg/sequelize').models;
var sequelize = require('../pg/sequelize').sequelize;
var async = require('async');
var util = require('../reference/util/util');
var Const = require('../reference/util/const');

module.exports = {
    renderReport: function (req, res) {
        // query params
        var tourDateId = req.query.tourdateid || 746; // placeholder id;
        var studioId = req.query.studioid || 18; // placeholder id;
        var registrationId = req.query.registrationid || 87;

        db.tbl_tour_dates.findAll({
            where: { id: tourDateId },
            limit: 1,
            include: [
                {model: db.tbl_event_cities, as: 'event_city', include: [
                    {model: db.tbl_states, as: 'state'}
                ]},
                { model: db.tbl_events, as: 'event' }
            ]
        })
        .then(function(results) {
            var tourDate = results[0];
            var jumpRegData = {};
            jumpRegData.city = tourDate.event_city.name;
            jumpRegData.state = tourDate.event_city.state.abbr;
            jumpRegData.age_as_of_year = tourDate.event.age_as_of_year;
            jumpRegData.current_year = new Date().getFullYear();
            jumpRegData.studios = [];
        })

        d.show_scholarship_code =
            d.workshop_level_id != 1
            && d.workshop_level_id != 6
            && d.workshop_level_id != 8
            && d.workshop_level_id != 12
            && d.one_day != 1
            && d.scholarship_code > 0;

        res.render('reports/jump-reg-packet', jumpRegPacketData);
    }
}