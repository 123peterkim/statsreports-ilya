module.exports = {
    // WORKSHOP LEVELS
    MINI:       "Mini",
    TEEN:       "Teen",
    OBSERVER:   "Observer",
    PEEWEE:     "PeeWee",
    TEACHER:    "Teacher",
    ROOKIE:     "Rookie",
    SENIOR:     "Senior",
    SIDEKICK:   "Sidekick",
    JUMPSTART:  "JUMPstart",
    NUBIE:      "Nubie",
    JUNIOR:     "Junior",
    OBSERVER2:  "Observer2",
    FINALS:     "finals",
    PRELIMS:    "prelims",
    VIPS:       "vips"
}