"use strict";

module.exports = function(sequelize, DataTypes) {
	var payments_registrations = sequelize.define('payments_registrations', {
		payment_id: {
			type: DataTypes.INTEGER,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
		}
	});
	return payments_registrations;
};
