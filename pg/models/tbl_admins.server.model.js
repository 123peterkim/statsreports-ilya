"use strict";

var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
	var tbl_admins = sequelize.define('tbl_admins', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		password: {
			type: DataTypes.STRING
		},
    salt: DataTypes.STRING,
    last_login: {
			type: DataTypes.DATE
		},
		ip: {
			type: DataTypes.STRING
		},
		name: {
			type: DataTypes.STRING
		},
		theme: {
			type: DataTypes.STRING
		},
		financials: {
			type: DataTypes.INTEGER
		},
    roles: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      isArray: true
    },
    access_level: {
      type: DataTypes.INTEGER
    }
  }, {
		timestamps: false,
		associate: function(models) {

		}
	});

  tbl_admins.prototype.makeSalt = function () {
    return crypto.randomBytes(16).toString('base64');
  };
  tbl_admins.prototype.authenticate = function (plainText) {
    var encryptedPassword = this.encryptPassword(plainText, this.dataValues.salt);
    return encryptedPassword === this.dataValues.password;
  };
  tbl_admins.prototype.oldAuthenticate = function (plainText) {
    return plainText === this.dataValues.password;
  };
  tbl_admins.prototype.encryptPassword = function (password, salt) {
    if (!password || !salt)
      return '';
    salt = new Buffer(salt, 'base64');
    return crypto.pbkdf2Sync(password, salt, 10000, 64).toString('base64');
  };
	return tbl_admins;
};