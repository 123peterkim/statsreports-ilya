"use strict";

module.exports = function (sequelize, DataTypes) {
	var date_dancers = sequelize.define('date_dancers', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		dancer_id: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'tbl_dancers',
			  key: 'id'
			}
		},
		custom_fee: {
		  type: DataTypes.BOOLEAN
		},
		has_photo: {
		  type: DataTypes.INTEGER
		},
		vip: {
		  type: DataTypes.INTEGER
		},
		independent: {
		  type: DataTypes.INTEGER
		},
		scholarship_code: {
		  type: DataTypes.INTEGER
		},
		registrations_dancer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations_dancers',
        key: 'id'
      }
		}
	}, {
		timestamps: false,
		associate: function (models) {
			date_dancers.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
			date_dancers.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			date_dancers.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			date_dancers.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
			date_dancers.hasOne(models.date_scholarships, {foreignKey: 'date_dancer_id', as: 'date_scholarship'});
      date_dancers.belongsTo(models.tbl_registrations_dancers, {foreignKey: 'registrations_dancer_id', as: 'registrations_dancer'});
    }
	});
	return date_dancers;
};