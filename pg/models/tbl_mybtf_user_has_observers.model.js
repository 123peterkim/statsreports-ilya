"use strict";

module.exports = function(sequelize, DataTypes) {
  var tbl_mybtf_user_has_observers = sequelize.define('tbl_mybtf_user_has_observers', {
    observer_id: {
      type: DataTypes.STRING,
      references: {
        model: 'tbl_observers',
        key: 'id'
      },
      primaryKey: true
    },
    mybtf_user_id: {
      type: DataTypes.STRING,
      references: {
        model: 'tbl_mybtf_users',
        key: 'id'
      },
      primaryKey: true
    }
  }, {
    timestamps: false,
    associate: function(models) {

    }
  });
  return tbl_mybtf_user_has_observers;
};