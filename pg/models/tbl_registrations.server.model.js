"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations = sequelize.define('tbl_registrations', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		entered_by_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_admins',
				key: 'id'
			}
		},
		dts_registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dts_registrations',
				key: 'id'
			}
		},
		event_registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_event_registrations',
				key: 'id'
			}
		},
		independent: {
			type: DataTypes.BOOLEAN
		},
		waiver: {
			type: DataTypes.BOOLEAN
		},
		left_off: {
			type: DataTypes.STRING
		},
		heard: {
			type: DataTypes.STRING
		},
		details: {
			type: DataTypes.STRING
		},
		viewed: {
			type: DataTypes.INTEGER
		},
		deleted: {
			type: DataTypes.INTEGER
		},
		fees: {
			type: DataTypes.JSONB
		},
		free_teacher: {
			type: DataTypes.JSONB
		},
		credit: {
			type: DataTypes.JSONB
		},
		additional_observers: {
			type: DataTypes.JSONB
		},
		confirmed: {
			type: DataTypes.BOOLEAN
		},
		completed: {
			type: DataTypes.BOOLEAN
		},
		dates: {
			type: DataTypes.JSONB
		},
    discounts: {
      type: DataTypes.JSONB
    },
    is_competing: {
      type: DataTypes.BOOLEAN
    },
		comp_room: {
			type: DataTypes.STRING
		},
		reviewed: {
			type: DataTypes.BOOLEAN
		},
    paying_by_check: {
      type: DataTypes.BOOLEAN
    },
		date_studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_studios',
        key: 'id'
      }
		},
    full_rates: {
      type: DataTypes.BOOLEAN
    },
		review_notes: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_registrations.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			tbl_registrations.belongsTo(models.tbl_mybtf_users, {foreignKey: 'mybtf_user_id', as: 'mybtf_user'});
			tbl_registrations.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
			tbl_registrations.belongsTo(models.tbl_admins, {foreignKey: 'entered_by_id', as: 'admin'});
			tbl_registrations.belongsTo(models.tbl_dts_registrations, {foreignKey: 'dts_registration_id', as: 'dts_registration'});
			tbl_registrations.belongsTo(models.tbl_event_registrations, {foreignKey: 'event_registration_id', as: 'event_registration'});
      tbl_registrations.belongsTo(models.date_studios, {foreignKey: 'date_studio_id', as: 'date_studio'});
      tbl_registrations.hasMany(models.tbl_registrations_dancers, {foreignKey: 'registration_id', as: 'dancers'});
      tbl_registrations.hasMany(models.tbl_registrations_routines, {foreignKey: 'registration_id', as: 'routines'});
      tbl_registrations.hasMany(models.tbl_registrations_teachers, {foreignKey: 'registration_id', as: 'teachers'});
      tbl_registrations.hasMany(models.tbl_registrations_observers, {foreignKey: 'registration_id', as: 'observers'});
      tbl_registrations.belongsToMany(models.tbl_payments, {as: 'payments', through: models.payments_registrations, foreignKey: 'registration_id', otherKey: 'payment_id'});
    }
	});
	return tbl_registrations;
};