"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_workshop_schedule_blocks = sequelize.define('date_workshop_schedule_blocks', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    date_workshop_schedule_time_slot_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_workshop_schedule_time_slots',
        key: 'id'
      }
    },
    date_workshop_schedule_room_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_workshop_schedule_rooms',
        key: 'id'
      }
    },
    date_workshop_schedule_block_type_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_workshop_schedule_block_types',
        key: 'id'
      }
    },
    text: {
      type: DataTypes.STRING
    },
    span: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: false,
    associate: function (models) {
      date_workshop_schedule_blocks.belongsTo(models.date_workshop_schedule_time_slots, {foreignKey: 'date_workshop_schedule_time_slot_id', as: 'time_slot'});
      date_workshop_schedule_blocks.belongsTo(models.date_workshop_schedule_rooms, {foreignKey: 'date_workshop_schedule_room_id', as: 'room'});
      date_workshop_schedule_blocks.belongsTo(models.date_workshop_schedule_block_types, {foreignKey: 'date_workshop_schedule_block_type_id', as: 'type'});
    }
  });
  return date_workshop_schedule_blocks;
};