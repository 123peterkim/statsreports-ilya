<?php
	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$citydata = db_get("city,venue_name","tbl_tour_dates","id=$tourdateid");
	$compgroups = array("vips","prelims","finals");
	$studios = array();
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid AND tbl_date_studios.studiocode !='' ORDER BY tbl_date_studios.independent ASC, tbl_date_studios.studiocode ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			//get all studio routines
			$row["name"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["name"])));
			$ad["studios"] = $row;
			$ad["routines"] = get_date_critique_sheet($tourdateid,$row["studioid"]);
			$total = 0;
			foreach($compgroups as $compgroup) {
				if(count($ad["routines"][$compgroup]) > 0) {
					$total += count($ad["routines"][$compgroup]);
					ksort($ad["routines"][$compgroup]);
				}
			}
			$ad["total"] = $total;
			$studios[] = $ad;
		}
	}
//	print_r($studios);
//	print(count($studios));exit();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
	//		window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Judge's Critique Sheet</title>
		<style type="text/css">
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.sometable tr td {
				font-size:11px;
			}
		</style>
	</head>
	<body>
		<div style="width: 952px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-family:Unplug;font-size:20pt;"><?php print($citydata[0]["city"]); ?> Judges Critique DVD Studio Sheet</div>
						<div style="font-size:16px;font-family:Unplug;"><?php print($citydata[0]["venue_name"]); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;font-size:14px;font-family:Unplug;">
						<?php print($title_date); ?>
					</td>
				</tr>
			</table>

			<?php foreach($studios as $studio) { ?>
			<table cellpadding="0" cellspacing="0" class="sometable" style="width:100%;margin-top:20px;">
				<tr>
					<td colspan="3"><span style="font-size:16px;font-style:italic;font-family:Unplug;color:#0079BF;"><?php print($studio["studios"]["name"]." ("); if(strlen($studio["studios"]["studiocode"]) > 0) print($studio["studios"]["studiocode"]); else print("-"); print(")");?></span> - <?=$studio["studios"]["studioid"];?></td>
				</tr>
				<tr>
					<td colspan="3"><span style="font-size:14px;font-weight:bold;font-family:Unplug;color:#000000;"><?php print($studio["total"]." dances competing");?></span></td>
				</tr>
				<?php foreach($compgroups as $compgroup) { ?>
				<?php  if(count($studio["routines"][$compgroup]) > 0) { ?>
					<tr>
						<td colspan="3" style="padding: 10px 0 3px;"><span style="font-size:11px;font-style:italic;">

						<?php
							if($eventid==14 && $compgroup=="vips")
								print("Best Dancers");
							if($compgroup == "vips" && $eventid!=14)
								print("VIPs");
							if($compgroup == "finals")
								print("Finals");
							if($compgroup == "prelims")
								print("Prelims");
						?>

						</span></td>
					</tr>
						<?php foreach($studio["routines"][$compgroup] as $routine) { ?>
						<tr>
							<td style="width:60px;text-align:center;"><?php print("#".$routine["dispnum"]);?></td>
							<td style="width:80px;text-align:center;"><?php print(date('g:i A',$routine["mktime"]));?></td>
							<td style="font-weight:bold;"><?php print($routine["routinename"]);?></td>
						</tr>
						<?php } //each routine in compgroup?>
					<?php } //if compgroup routines exist ?>
				<?php } //each compgroup ?>
			</table>
			<?php } ?>
		</div>
	</body>
</html>