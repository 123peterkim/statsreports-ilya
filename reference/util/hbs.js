var moment = require('moment');

hbs = require('express-hbs'),
  _ = require('lodash');

hbs.registerHelper('dynamicKey', function (object, key) {
  return object[key];
});

hbs.registerHelper('observerType', function (type, youngsters) {
  if (type === 'Observer') {
    return type;
  }

  if (type === 'Observer2') {
    return youngsters.slice(0, -1) + ' Observer'
  }
});

hbs.registerHelper('singular', function (object) {
  return object.slice(0, -1);
});

hbs.registerHelper('singularOrPlural', function (singular, plural, count) {
  if (count === 1) {
    return singular;
  } else {
    return plural;
  }
});

hbs.registerHelper('ifCond', function (v1, operator, v2, options) {

  switch (operator) {
    case '==':
      return (v1 == v2) ? options.fn(this) : options.inverse(this);
    case '===':
      return (v1 === v2) ? options.fn(this) : options.inverse(this);
    case '!=':
      return (v1 != v2) ? options.fn(this) : options.inverse(this);
    case '!==':
      return (v1 !== v2) ? options.fn(this) : options.inverse(this);
    case '<':
      return (v1 < v2) ? options.fn(this) : options.inverse(this);
    case '<=':
      return (v1 <= v2) ? options.fn(this) : options.inverse(this);
    case '>':
      return (v1 > v2) ? options.fn(this) : options.inverse(this);
    case '>=':
      return (v1 >= v2) ? options.fn(this) : options.inverse(this);
    case '&&':
      return (v1 && v2) ? options.fn(this) : options.inverse(this);
    case '||':
      return (v1 || v2) ? options.fn(this) : options.inverse(this);
    case '%' :
      return (v1 % v2) == 0 ? options.fn(this) : options.inverse(this);
    default:
      return options.inverse(this);
  }
});

hbs.registerHelper('operator', function (v1, operator, v2) {
  switch (operator) {
    case '+':
      return v1 + v2;
    case '-':
      return v1 - v2;
    case '%':
      return v1 % v2;
    case '/ ':
      return v1 / v2;
    case '* ':
      return v1 / v2;
    default:
      return '-';
  }
});

hbs.registerHelper('joinRoutineDancers', function (dancers) {
  var array = [];
  _.forEach(dancers, function (dancer) {
    array.push(dancer.dancer.person.fname + ' ' + dancer.dancer.person.lname);
  });
  return array.join(', ');
});

hbs.registerHelper('boolean', function (value, ifTrue, ifFalse) {
  if (value === true || value === 1 || value === '1' || value === 'true') {
    return ifTrue;
  } else {
    return ifFalse;
  }
});

hbs.registerHelper('link', function (report, url) {
  return new hbs.SafeString(
    "<a href='" + url + "'>" + report + "</a>"
  );
});

/**
 * Format a given number with decimal places given as parameter.
 * (this method can be improved to format currency automatically.)
 * @param {Number} number The number to be formated
 * @param {Number} decPlaces The amount of decimal places
 * @return {String} The number formated
 */
hbs.registerHelper('numberFormat', function(number, decPlaces) {
  decPlaces = Number.parseInt(decPlaces);
  let res = new Intl.NumberFormat('es-EN', {minimumFractionDigits: decPlaces, maximumFractionDigits: decPlaces}).format(number);
  return (number < 0) ?  '(' + res.substring(1) + ')' :  res;
});

/**
 * Format a date in the given format string. Ex.: "YYYY" = 2017.
 * See moment.js docs for more formats available (http://momentjs.com/
 * @param {String} date The date as a String in the format "YYYY-MM-DD", or empty to format current date
 * @param {String} format How the user wants to show the date
 * @return {String} The date formated
 */
hbs.registerHelper('formatDate', function(date, format) {
  if(date) {
    return moment(date, 'YYYY-MM-DD').format(format);
  }
  else {
    return moment().format(format);
  }
});

/**
 * Format a timestamp in a given format. The timestamp must be 
 * in the ISO 8601/SQL standard format, which is "1997-12-17 07:37:16"
 * for timestamp without timezone, or "1997-12-17 07:37:16-08" with
 * timezone offset.
 * See moment.js docs for more formats available (http://momentjs.com/
 * Timestamp Postgres reference (https://www.postgresql.org/docs/9.1/static/datatype-datetime.html)
 * @param {String} date The date as a String in the format "YYYY-MM-DD", or empty to format current date
 * @param {String} format How the user wants to show the date
 * @return {String} The date formated
 */
hbs.registerHelper('formatDateTime', function(timestamp, format) {
  return moment(timestamp).format(format);
});

/**
 * Replaces a substring for the new substring in a text
 * 
 * @param {String} substring the old piece to be replaced
 * @param {String} newSubstring the new piece to replace the old one
 * @param {String} text The whole text with the occurring substrings
 * @return {String} The new text
 */
hbs.registerHelper('strReplace', function(substring, newSubstring, text) {
  if(text) return text.replace(new RegExp(substring, 'g'), newSubstring);
  else return '';
});

hbs.registerHelper('inc', function(value){
  return parseInt(value) + 1;
});

/**
 * This helper allows to go through arrays inside an object
 * where the keys set is given as a parameter
 * Ex.:
 * {
 *  keys: ['a', 'b', 'c']
 *  object: {
 *    a: [{x: 1}, {x: 2}, {x: 3}]
 *    b: [{x: 4}, {x: 5}, {x: 6}]
 *    c: [{x: 7}, {x: 8}, {x: 9}]
 *  }
 * }
 * {{#eachKeyAndValue keys object}}
 *    X: {{x}},
 * {{/eachKeyAndValue}}
 * 
 * @param {Array} keys the keys array
 * @param {Object} object the object with the values array
 */
hbs.registerHelper('eachKeyValue', function(keys, object, options) {
  var result = "";
  var data = { key: "", first: true, index: 0 };
  
  _.forEach(keys, function(key) {
    data.key = key;

    _.forEach(object[key], function(item) {
      data.index++;
      result += options.fn(item, { data: data });
      data.first = false;
    });

    data.first = true;
  });

  return result;
});

hbs.registerHelper('ifEven', function(number, options) {
  return number%2==0? options.fn(this) : options.inverse(this);
});

hbs.registerHelper('toUpperCase', function(str){
  return typeof str === 'string'? ( str || '' ).toUpperCase(): new String(str).toUpperCase();
});

/*
  Helper which names 'times' this is like for loop
*/
hbs.registerHelper('times', function(n, block) {
    var accum = '';
    for(var i = 0; i < n; ++i)
        accum += block.fn();
    return accum;
});

hbs.registerHelper('times_with_value', function(n, block) {
    var accum = '';
    for(var i = 0; i < n; ++i)
        accum += block.fn(i);
    return accum;
});

hbs.registerHelper('lookup', function(obj, field) {
  return obj[field];
});


hbs.registerHelper('toLowerCase', function(str){
  return typeof str === 'string'? ( str || '' ).toLowerCase(): new String(str).toLowerCase();
});

hbs.registerHelper('contains', function(collection, item, options) {
  // string check
  if (typeof collection === 'string') {
    if (collection.search(item) >= 0) {
      return options.fn(this);
    }
    else {
      return options.inverse(this);
    }
  }
  // "collection" check (objects & arrays)
  for (var prop in collection) {
    if (collection.hasOwnProperty(prop)) {
      if (collection[prop] == item) return options.fn(this);
    }
  }
  return options.inverse(this);
});

hbs.registerHelper('notContains', function(collection, item, options) {
  // string check
  if (typeof collection === 'string') {
    if (collection.search(item) >= 0) {
      return options.inverse(this);
    }
    else {
      return options.fn(this);
    }
  }
  // "collection" check (objects & arrays)
  for (var prop in collection) {
    if (collection.hasOwnProperty(prop)) {
      if (collection[prop] == item) return options.inverse(this);
    }
  }
  return options.fn(this);
});

hbs.registerHelper('var', function(name, value, context){
  this[name] = value;
});

hbs.registerHelper('capitalizeFirstLetter', function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
});

/**
 * A set of logic helper that are helpful when building nested conditional 
 * oprators. These helpers are not block helpers, they only return a boolean value
 * Reference: https://stackoverflow.com/a/31632215
 */
hbs.registerHelper({
    eq: function (v1, v2) {
        return v1 === v2;
    },
    ne: function (v1, v2) {
        return v1 !== v2;
    },
    lt: function (v1, v2) {
        return v1 < v2;
    },
    gt: function (v1, v2) {
        return v1 > v2;
    },
    lte: function (v1, v2) {
        return v1 <= v2;
    },
    gte: function (v1, v2) {
        return v1 >= v2;
    },
    and: function (v1, v2) {
        return v1 && v2;
    },
    or: function (v1, v2) {
        return v1 || v2;
    },
    not: function(v) {
      return !v;
    },
    // arithmetic operators
    mod: function(v1, v2) {
      return v1%v2;
    },
    mdz: function(v1, v2) {
      return (v1%v2)===0;
    },
    sum: function(v1, v2) {
      return v1+v2;
    },
    sub: function(v1, v2) {
      return v1-v2;
    },
    mult: function(v1, v2) {
      return v1*v2;
    },
    div: function(v1, v2) {
      return v1/v2;
    }
});