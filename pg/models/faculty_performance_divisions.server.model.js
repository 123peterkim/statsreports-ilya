"use strict";

module.exports = function (sequelize, DataTypes) {
	var facultyPerformanceDivisions = sequelize.define('faculty_performance_divisions', {
		faculty_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_faculty',
				key: 'id'
			}
		},
		performance_division_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_performance_divisions',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function (models) {

		}
	});
	return facultyPerformanceDivisions;
};