"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_registrations_soty = sequelize.define('tbl_registrations_soty', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		registration_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations',
				key: 'id'
			}
		},
		ota_mj_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_ts_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_cd_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		bc_mj_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		bc_ts_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		peopleschoice_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_m_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_j_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_t_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		ota_s_regroutineid: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_registrations_routines',
				key: 'id'
			}
		},
		mybtf_user_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			}
		},
		bc_mj_cho: {
			type: DataTypes.STRING
		},
		bc_ts_cho: {
			type: DataTypes.STRING
		},
		ota_cd_cd: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_registrations_soty;
};