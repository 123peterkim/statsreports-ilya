<?php

	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$compgroup = isset($_GET["compgroup"]) ? mysql_real_escape_string($_GET["compgroup"]) : "finals" ;

	if(!($tourdateid > 0)) {
		exit();
	}

	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$city = db_one("city","tbl_tour_dates","id='$tourdateid'");
	$dispdate = get_tourdate_dispdate($tourdateid);

	if(intval($_GET["all"]) == 1) {
		$sql = "SELECT tbl_date_studios.studioid,tbl_date_studios.studiocode, tbl_studios.name AS studioname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$studios[] = $row;
			}
		}
	}
	else {
		$sql = "SELECT tbl_date_studios.studioid,tbl_studios.name AS studioname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid AND tbl_date_studios.studioid=".intval($_GET["studioid"])." ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$studios[] = $row;
			}
		}
	}

	foreach($studios as $skey=>$studio) {

		//get studio's routines
		$sql = "SELECT tbl_date_routines.number_$compgroup AS number, tbl_date_routines.".$compgroup."_has_a AS has_a, tbl_date_routines.routineid, tbl_date_routines.".$compgroup."_awardid, tbl_date_routines.".$compgroup."_total_score AS total_score, tbl_competition_awards.name AS awardname FROM tbl_date_routines LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id=tbl_date_routines.".$compgroup."_awardid WHERE tbl_date_routines.studioid=".$studio["studioid"]." AND tbl_date_routines.tourdateid='$tourdateid' AND (tbl_date_routines.".$compgroup."_awardid > 0 OR tbl_date_routines.perfcategoryid = 6) ORDER BY tbl_date_routines.number_$compgroup ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {

				$routine["base"] = $row;
				$routine["info"] = get_date_routine_info($tourdateid,$row["routineid"]);
 				$routine["info"]["dispnumber"] = $row["has_a"] == 1 ? $row["number"].".a" : $row["number"];

				$sql2 = "SELECT * FROM tbl_online_scoring WHERE tourdateid=$tourdateid AND compgroup='$compgroup' AND routineid='".$row["routineid"]."'";
				$res2 = mysql_query($sql2) or die(mysql_error());
				if(mysql_num_rows($res2) > 0) {
					while($row2 = mysql_fetch_assoc($res2)) {

						$j1name = $row2["facultyid1"] > 0 ? ltrim(rtrim(db_one("fname","tbl_staff","id='".$row2["facultyid1"]."'")." ".db_one("lname","tbl_staff","id='".$row2["facultyid1"]."'"))) : "";
						$j2name = $row2["facultyid2"] > 0 ? ltrim(rtrim(db_one("fname","tbl_staff","id='".$row2["facultyid2"]."'")." ".db_one("lname","tbl_staff","id='".$row2["facultyid2"]."'"))) : "";
						$j3name = $row2["facultyid3"] > 0 ? ltrim(rtrim(db_one("fname","tbl_staff","id='".$row2["facultyid3"]."'")." ".db_one("lname","tbl_staff","id='".$row2["facultyid3"]."'"))) : "";
						$j4name = $row2["facultyid4"] > 0 ? ltrim(rtrim(db_one("fname","tbl_staff","id='".$row2["facultyid4"]."'")." ".db_one("lname","tbl_staff","id='".$row2["facultyid4"]."'"))) : "";

						$j1data = json_decode($row2["data1"],true);
						$j2data = json_decode($row2["data2"],true);
						$j3data = json_decode($row2["data3"],true);
						$j4data = json_decode($row2["data4"],true);

						$j1att = array();
						$j2att = array();
						$j3att = array();
						$j4att = array();

						$judges[0] = array("id"=>$row2["facultyid1"],"position"=>1,"judge"=>$j1name,"data"=>$j1data);
						$judges[1] = array("id"=>$row2["facultyid2"],"position"=>2,"judge"=>$j2name,"data"=>$j2data);
						$judges[2] = array("id"=>$row2["facultyid3"],"position"=>3,"judge"=>$j3name,"data"=>$j3data);
						$judges[3] = array("id"=>$row2["facultyid4"],"position"=>4,"judge"=>$j4name,"data"=>$j4data);

						foreach($judges as $jk=>$judge) {
							if($judge["id"] == 0)
								unset($judges[$jk]);

							if($judge["data"]["score"] == 0 && $judge["data"]["i_choreographed"] == 0 && $routine["base"]["perfcategoryid"] != 6)
								unset($judges[$jk]);
						}

						if($routine["base"]["perfcategoryid"] == 6) {
							$routine["base"]["total_score"] = "N/A";
							$routine["base"]["awardname"] = "Showcase Only";
						}

						//ensure judge listed order is same order as during competition (#1-4, left from right)
						$tmp = array();
						if(count($judges) > 0) {
							foreach($judges as $judge) {
								$tmp[$judge["position"]] = $judge;
							}
						}

						ksort($tmp);
						$routine["jdata"] = $tmp;
					}
				}
				$studios[$skey]["routines"][] = $routine;
			}
		} else {
			unset($studios[$skey]);
		}
	}
//	print_r($studios);exit();

?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>Online Score Sheets</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}

			#boxes_holder .judge_box {
				background-color: #EEEEEE;
				border: 2px solid #DDDDDD;
				min-height: 100px;
				margin-top: 10px;
				padding: 10px;
			}
		</style>
	</head>
	<body>
<?php
if(count($studios) > 0) {
	foreach($studios as $studio) {
		foreach($studio["routines"] as $routine) {
			?>

		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
		<?php if($eventid == 7) { ?>
					<td style="text-align:center;vertical-align:top;width:325px;">
						<img src="../../logos/<?=$eventid;?>.png" style="width:325px;height:auto;display:block;margin: 0 auto 5px;" />
		<? } ?>
		<?php if($eventid == 8) { ?>
					<td style="text-align:center;vertical-align:top;width:325px;">
						<img src="../../logos/<?=$eventid;?>.png" style="width:325px;height:auto;display:block;margin: 0 auto 5px;" />
		<? } ?>
		<?php if($eventid == 14) { ?>
					<td style="text-align:center;vertical-align:top;width:270px;">
						<img src="../../logos/<?=$eventid;?>.png" style="width:165px;height:auto;display:block;margin: 0 auto 5px;" />
		<? } ?>
		<?php if($eventid == 18) { ?>
					<td style="text-align:center;vertical-align:top;width:270px;">
						<img src="../../logos/<?=$eventid;?>.png" style="width:165px;height:auto;display:block;margin: 0 auto 5px;" />
		<? } ?>
						<div style="font-weight:bold;font-size:18px;text-transform:uppercase;"><?=$city;?></div>
						<div style="font-size:16px;"><?=$dispdate;?></div>
					</td>
					<td style="vertical-align:top;padding-top:15px;">
						<div style="font-size:20px;font-weight:bold;text-align:right;padding-bottom:5px;text-transform:uppercase;">
							#<?=$routine["info"]["dispnumber"];?> - <?=stripslashes($routine["info"]["routinename"]);?>
						</div>
						<div style="text-align: right;font-size:16px;">
							<?=stripslashes($routine["info"]["studioname"])." <span style='color:#555555;font-size:12px;'>[".$routine["info"]["studiocode"]."]</span>";?>
							<br/>
							<?=$routine["info"]["agedivision"]." &bull; ".$routine["info"]["perfdivision"]." ".$routine["info"]["routinecategory"];?>
						</div>
					</td>
				</tr>
			</table>
			<div id="boxes_holder" style="margin-top:10px;">
<?php
	if(count($routine["jdata"]) > 0) {
		foreach($routine["jdata"] as $judgekey=>$judge) {
		?>
				<div class="judge_box">
					<table cellpadding="0" cellspacing="0" style="width:100%;">
						<tr>
							<td style="width:560px;vertical-align:top;">
								<div style="font-size:16px; font-weight: bold;margin-bottom:5px;border-bottom:2px dashed #BBBBBB;padding-bottom:10px;">
									Judge #<?=$judgekey;?>&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;&nbsp;<?=$judge["judge"];?>
								</div>

						<?php if(count($judge["data"]["attributes"]["good"]) > 0) { ?>
								<div style="padding: 5px;">
									<span style="font-weight:bold;">Great Job:</span> <?=join(", ",$judge["data"]["attributes"]["good"]);?>
								</div>
						<?php } ?>
						<?php if(count($judge["data"]["attributes"]["bad"]) > 0) { ?>
								<div style="padding: 5px;">
									<span style="font-weight:bold;">Needs Work:</span> <?=join(", ",$judge["data"]["attributes"]["bad"]);?>
								</div>
						<?php } ?>
						<?php if(strlen($judge["data"]["note"]) > 2) { ?>
								<div style="padding: 5px;">
									<span style="font-weight:bold;">Judge's Note:</span> <?=ltrim(rtrim(stripslashes(str_replace("\\n","",$judge["data"]["note"]))));?>
								</div>
						<?php } ?>
						<?php if($judge["data"]["not_friendly"] == 1) { ?>
								<div style="padding: 5px;font-weight:bold;">
									&bull; I didn't think this routine was family friendly.
								</div>
						<?php } ?>
						<?php if($judge["data"]["i_choreographed"] == 1) { ?>
								<div style="padding: 5px;">
									&bull; I choreographed this routine.
								</div>
						<?php } ?>
							</td>

							<td style="text-align:center;vertical-align:top;">
								<div style="font-size:24px;">Score:</div>
								<div style="font-size:28px;font-weight:bold;"><?=$judge["data"]["score"];?></div>
							</td>
						</tr>
					</table>
				</div>
	<?php 	}
		} ?>
				<div id="bottom_box" style="margin-top:20px;text-align:right;font-size:24px;background-color:#EEEEEE;padding:15px 10px;border:1px dashed #999999;">
					<span style="font-weight:bold;">TOTAL SCORE:</span>&nbsp;&nbsp;&nbsp;<?=$routine["base"]["total_score"];?>&nbsp;&nbsp;&bull;&nbsp;&nbsp;<?=$routine["base"]["awardname"];?>
				</div>
			</div>
		</div>

<?php
		}
	}
} ?>
	</body>
</html>
