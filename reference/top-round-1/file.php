<?php
	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id='$tourdateid'");
	$adids = array(1,2,3,4);
	$genders = array("female","male");
	$allshit = array();

	foreach($adids as $adid) {
		$adname = db_one("name","tbl_age_divisions","id=$adid");
		foreach($genders as $gender) {

			$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_tda_bestdancer_data.ballet, tbl_tda_bestdancer_data.jazz, tbl_tda_bestdancer_data.danceoff, tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.routineid, tbl_studios.name AS studioname, tbl_date_routines.number_vips AS number, tbl_date_routines.vips_has_a AS has_a, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender, tbl_date_studios.studiocode, tbl_date_routines.vips_score1, tbl_date_routines.vips_score2, tbl_date_routines.vips_score3, tbl_date_routines.vips_score4, tbl_date_routines.vips_score5, tbl_date_routines.vips_score6, tbl_date_routines.vips_total_score, tbl_date_routines.vips_dropped_score, tbl_date_routines.vips_dropped_score2, tbl_routines.name AS routinename, tbl_competition_awards.name AS awardname FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_routines ON tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_date_studios ON tbl_date_studios.studioid=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_routines ON tbl_routines.id = tbl_tda_bestdancer_data.routineid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id=tbl_date_routines.vips_awardid WHERE tbl_tda_bestdancer_data.iscompeting=1 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.agedivisionid=$adid AND tbl_profiles.gender='$gender' AND tbl_date_studios.tourdateid=$tourdateid AND tbl_date_routines.routineid=tbl_tda_bestdancer_data.routineid ORDER BY tbl_date_routines.vips_total_score DESC, vips_dropped_score DESC, vips_dropped_score2 DESC";
			$res = mysql_query($sql) or die(mysql_error());
			if(mysql_num_rows($res) > 0) {
				while($row = mysql_fetch_assoc($res)) {
					if($row["vips_dropped_score2"] > 0)
						$row["awardstr"] = $row["awardname"]." (".$row["vips_total_score"]." : ".$row["vips_dropped_score2"]."/".$row["vips_dropped_score"].")";
					else
						$row["awardstr"] = $row["awardname"]." (".$row["vips_total_score"]." : ".$row["vips_dropped_score"].")";

					$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);

					$allshit[$adname][ucfirst($gender)][] = $row;
				}
			}
		}//each gender
	}// each ad


	//calculate some shit
	foreach($allshit as $agediv=>$cggenders) {
		foreach($cggenders as $cggender=>$routines) {
			$place = 1;
			$lasttotal = 0;
			$lastdropped = 0;
			foreach($routines as $rkey=>$routine) {

				//place
				if($routine["vips_total_score"] != $lasttotal) {
					$allshit[$agediv][$cggender][$rkey]["place"] = $place;
					$lasttotal = $routine["vips_total_score"];
					$lastdropped = $routine["vips_dropped_score"];
					++$place;
				}
				else {
					//if dropped scores match give same score
					if($routine["vips_dropped_score"] == $lastdropped) {
						$allshit[$agediv][$cggender][$rkey]["place"] = $place-1;
						$lasttotal = $routine["vips_total_score"];
						$lastdropped = $routine["vips_dropped_score"];
					}
					//otherwise 2nd place gets next place#
					else {
						$allshit[$agediv][$cggender][$rkey]["place"] = $place;
						++$place;
						$lasttotal = $routine["vips_total_score"];
						$lastdropped = $routine["vips_dropped_score"];
					}
				}

				//percent
				$percentage = ($routine["vips_total_score"] / 300);
				$percentage = number_format(((.5 * $percentage)*100),2,'.','');
				$allshit[$agediv][$cggender][$rkey]["perc"] = $percentage;
				$sql = "UPDATE `tbl_tda_bestdancer_data` SET perc_solo='$percentage',round1_place='".$allshit[$agediv][$cggender][$rkey]["place"]."' WHERE id='".$routine["tdabdid"]."' LIMIT 1";
				$res = mysql_query($sql) or die(mysql_error());

			}
		}
	}

//	print_r($allshit);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>TDA Round 1</title>
		<style type="text/css">
			@page land {size: landscape;}
			.landscape {page: land;}

			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.sched_table {
/*  				width: 100%; */
				margin-top:8px;
			}

			.sched_table tr td {
				border-left: 1px solid #000000;
				font-size: 8pt;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
			}

			.tline {
				padding: 1px 0;
				text-align: left;
			}
		</style>
	</head>
	<body>
		<div style="width: 952px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td style="vertical-align:top;">
						<div style="font-family:'Trajan Pro';font-size:20pt;"><?php print($eventid == 14) ? "TDA Best Dancers" : "24SEVEN Non-Stop Dancers"; ?> : Round 1</div>
						<div style="font-size:16px;font-family:Unplug;"><?php print($citydata[0]["venue_name"]); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;font-size:14px;font-family:Unplug;">
						<?php print($title_date); ?>
					</td>
				</tr>
			</table>
		<?php
			foreach($allshit as $agediv=>$cggenders) {
				foreach($cggenders as $cggender=>$routines) {
		?>

			<div style="color:#AA0000;font-size:17px;font-style:italic;margin-top:15px;"><?php print($agediv." &bull; ".$cggender);?></div>
			<table cellpadding="0" cellspacing="0" class="sched_table">
				<tr>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:80px;">Place</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:200px;">Total Points</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:100px;">Percent</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:330px;">Studio</td>
					<td class="thead" style="border-top:1px solid #000000;border-bottom:1px solid #000000;width:250px;border-right:1px solid #000000;">Dancer</td>
				</tr>

				<?	foreach($routines as $routine) { ?>

				<tr>
					<td class="tline" style="border-bottom:1px solid #000000;text-align:center;"><?php print($routine["place"]);?></td>
					<td class="tline" style="border-bottom:1px solid #000000;text-align: center;"><?php print($routine["awardstr"]);?></td>
					<td class="tline" style="border-bottom:1px solid #000000;padding-left:2px;text-align:center;"><?php print($routine["perc"]."%");?></td>
					<td class="tline" style="border-bottom:1px solid #000000;padding-left:2px;"><?php print($routine["studioname"]);?></td>
					<td class="tline" style="border-bottom:1px solid #000000;border-right:1px solid #000000;padding-left:2px;"><?php print($routine["fname"]." ".$routine["lname"]." - ".$routine["tdabdid"]);?></td>
				</tr>

				<?php	} ?>
			</table>
		<?php		}
			}
		?>

		</div>
	</body>
</html>