<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);

	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.soty_ts_ballet_routineid, tbl_tda_award_nominations.soty_ts_jazz_routineid, tbl_tda_award_nominations.soty_ts_musicaltheater_routineid, tbl_tda_award_nominations.soty_ts_contemplyrical_routineid, tbl_tda_award_nominations.soty_ts_hiphoptap_routineid, tbl_tda_award_nominations.soty_mj_ballet_routineid, tbl_tda_award_nominations.soty_mj_jazz_routineid, tbl_tda_award_nominations.soty_mj_musicaltheater_routineid, tbl_tda_award_nominations.soty_mj_contemplyrical_routineid, tbl_tda_award_nominations.soty_mj_hiphoptap_routineid, tbl_studios.name AS studioname FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid WHERE tbl_tda_award_nominations.has_soty=1 AND tbl_tda_award_nominations.tourdateid=$tourdateid ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$all = array();
	$soty = array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$info = array();
			$info["ballet_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_ballet_routineid"]."'");
			$info["ballet_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_ballet_routineid"]."'");
			$info["jazz_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_jazz_routineid"]."'");
			$info["jazz_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_jazz_routineid"]."'");
			$info["musicaltheater_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_musicaltheater_routineid"]."'");
			$info["musicaltheater_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_musicaltheater_routineid"]."'");
			$info["contemplyrical_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_contemplyrical_routineid"]."'");
			$info["contemplyrical_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_contemplyrical_routineid"]."'");
			$info["hiphop_mj"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_mj_hiphoptap_routineid"]."'");
			$info["hiphop_ts"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["soty_ts_hiphoptap_routineid"]."'");
			$row["studio_total"] = $info["ballet_mj"] + $info["ballet_ts"] + $info["jazz_mj"] + $info["jazz_ts"] + $info["musicaltheater_mj"] + $info["musicaltheater_ts"] + $info["contemplyrical_mj"] + $info["contemplyrical_ts"] + $info["hiphop_mj"] + $info["hiphop_ts"];
			$all[] = $row;
		}
		$atmp = array();
		$allnew = array();
		foreach($all as $al) {
			$atmp[$al["studio_total"]][] = $al;
			ksort($atmp);
		}
		foreach($atmp as $ascore=>$astudios) {
			foreach($astudios as $astudio)
				$allnew[] = $astudio;
		}
		$soty = array_reverse($allnew);
	}

/* OTA M */

	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_m_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_m_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_m_ota_no=0 AND tbl_tda_award_nominations.sa_m_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$otamj = array();
	$all = array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$otam = array_reverse($allnew);

/* OTA J */

	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_j_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_j_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_j_ota_no=0 AND tbl_tda_award_nominations.sa_j_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$otamj = array();
	$all = array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$otaj = array_reverse($allnew);

/* OTA T */
	$all = array();
	$otats = array();
	$allnew = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_t_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_t_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_t_ota_no=0 AND tbl_tda_award_nominations.sa_t_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$otat = array_reverse($allnew);

/* OTA S */

	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_s_ota_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_s_ota_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_s_ota_no=0 AND tbl_tda_award_nominations.sa_s_ota_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$otamj = array();
	$all = array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$otas = array_reverse($allnew);


/* OA CD */
	$all = array();
	$oacd = array();
	$allnew = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_oacd_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_oacd_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_oacd_no=0 AND tbl_tda_award_nominations.sa_oacd_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$oacd = array_reverse($allnew);


/* BC MJ */
	$all = array();
	$bcmj = array();
	$allnew = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_mj_bc_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_mj_bc_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_mj_bc_no=0 AND tbl_tda_award_nominations.sa_mj_bc_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$bcmj = array_reverse($allnew);

/* BC TS */
	$all = array();
	$bcts = array();
	$allnew = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_ts_bc_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_ts_bc_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_ts_bc_no=0 AND tbl_tda_award_nominations.sa_ts_bc_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$bcts = array_reverse($allnew);

	/* PEOPLES CHOICE */
	$all = array();
	$sql = "SELECT tbl_tda_award_nominations.id, tbl_tda_award_nominations.studioid, tbl_tda_award_nominations.sa_peopleschoice_routineid AS routineid, tbl_studios.name AS studioname, tbl_routines.name AS routinename FROM `tbl_tda_award_nominations` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_award_nominations.studioid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_tda_award_nominations.sa_peopleschoice_routineid WHERE tbl_tda_award_nominations.tourdateid=$tourdateid AND tbl_tda_award_nominations.sa_peopleschoice_no=0 AND tbl_tda_award_nominations.sa_peopleschoice_routineid > 0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$otamj = array();
	$all = array();
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["finals_score"] = db_one("finals_total_score","tbl_date_routines","tourdateid='$tourdateid' AND routineid='".$row["routineid"]."'");
			$all[] = $row;
		}
	}
	$atmp = array();
	$allnew = array();
	foreach($all as $al) {
		$atmp[$al["finals_score"]][] = $al;
		ksort($atmp);
	}
	foreach($atmp as $ascore=>$astudios) {
		foreach($astudios as $astudio)
			$allnew[] = $astudio;
	}
	$peopleschoice = array_reverse($allnew);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:705px;margin: 0 auto;">
			<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;">TDA <?php print($city); ?> Nominations List [Sorted]</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:30px;"><?php print($dispdate); ?></div>

	<!-- SOTY -->
	<?php
		if(count($soty) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Studio of the Year</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:580px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Nominated Routine Total</td>
				</tr>
		<?php
			foreach($soty as $studio) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($studio["studio_total"]); ?></td>
				</tr>
		<?php
		 	} ?>
			</table>
	<?php
		} ?>

	<!-- OTA M -->
	<?php
		if(count($otam) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Outstanding Technical Achievement (Mini)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($otam as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>

	<!-- OTA J -->
	<?php
		if(count($otaj) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Outstanding Technical Achievement (Junior)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($otaj as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>


	<!-- OTA T -->
	<?php
		if(count($otat) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Outstanding Technical Achievement (Teen)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($otat as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>

	<!-- OTA S -->
	<?php
		if(count($otas) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Outstanding Technical Achievement (Senior)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($otas as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>


	<!-- OA CD -->
	<?php
		if(count($oacd) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Outstanding Achievement in Costume Design</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($oacd as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>


	<!-- BC MJ -->
	<?php
		if(count($bcmj) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Best Choreographer (Mini/Junior)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($bcmj as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>


	<!-- BC TS -->
	<?php
		if(count($bcts) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">Best Choreographer (Teen/Senior)</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($bcts as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>

<!-- OTA MJ -->
	<?php
		if(count($peopleschoice) > 0) { ?>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;">People's Choice Award</div>
			<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:25px;">
				<tr>
					<td class="thead" style="width:260px;">Routine</td>
					<td class="thead" style="width:260px;">Studio</td>
					<td class="thead" style="width:290px;border-right:1px solid #000000;">Routine Score</td>
				</tr>
		<?php
			foreach($peopleschoice as $arow) { ?>
				<tr>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["routinename"])))); ?></td>
					<td class="tbody"><?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$arow["studioname"])))); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($arow["finals_score"]); ?></td>
				</tr>
		<?php
			} ?>
			</table>
	<?php
		} ?>

		</div>
	</body>
</html>