"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_performance_divisions = sequelize.define('tbl_performance_divisions', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_performance_divisions;
};