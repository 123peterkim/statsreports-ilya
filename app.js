var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var winston = require('./pg/winston');

// setup env variables
try {
  require('./config')();
} catch (err) {
  winston.warn('./config/index.js File Missing');
  winston.warn("You haven't setup the environment variables file yet. ");
}
var index = require('./routes/index');
var users = require('./routes/users');

var sequelize = require('./pg/sequelize-connect');
var hbs = require('express-hbs');
var hbsHelpers = require('./reference/util/hbs');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'view.html');

app.engine('view.html', hbs.express4({
  extname: 'view.html'
}));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
