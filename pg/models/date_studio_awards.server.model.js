"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_studio_awards = sequelize.define('date_studio_awards', {
		date_studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_studios',
				key: 'id'
			},
			primaryKey: true
		},
		studio_award_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studio_awards',
				key: 'id'
			},
			primaryKey: true
		},
		winner: {
		  type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_studio_awards.belongsTo(models.date_studios, {foreignKey: 'date_studio_id', as: 'date_studio'});
			date_studio_awards.belongsTo(models.tbl_studio_awards, {foreignKey: 'studio_award_id', as: 'studio_award'});

		}
	});
	return date_studio_awards;
};