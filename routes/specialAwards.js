var Excel = require('exceljs');
var _ = require('lodash');
var fs = require('fs');
var util = require('./../reference/util/util');

module.exports = {
    specialAwardsSpreadsheet: function (req, res, specialAwardsData) {
        // query params
        var tourDateId = req.query.tourdateid || 746;

        var wb = new Excel.Workbook();
        var ws = wb.addWorksheet('Worksheet');
        var data = [];
        _.forEach(specialAwardsData.awards, function (groups, key) {
            _.forEach(groups, function (award) {
                data.push({ routine_name: award.routine_name, studio_name: award.studio_name, special_awards: key});
            });
        });
        
        ws.columns = [
            { header: 'Routine', key: 'routine_name', width: util.setWidthAuto(data, 'routine_name', 'Routine'), style: { font: {name: 'Arial' } } },
            { header: 'Studio', key: 'studio_name', width: util.setWidthAuto(data, 'studio_name', 'Studio'), style: { font: { name: 'Arial' } } },
            { header: 'Special Award', key: 'special_awards', width: util.setWidthAuto(data, 'special_awards', 'Special Award'), style: { font: { name: 'Arial' } } },
        ];

        ws.addRows(data);

        var filename = 'public/' + Date.now();

        wb.xlsx.writeFile(filename)
            .then(function () {
                res.download(filename, "JUMP_specialawardwinners_"+tourDateId+".xlsx", function (err) {
                    if (!err) fs.unlink(filename);
                });
            });
    }
}