"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_categories = sequelize.define('tbl_categories', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		limit: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_categories;
};