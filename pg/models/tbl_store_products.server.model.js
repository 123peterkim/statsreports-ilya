"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_products = sequelize.define('tbl_store_products', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		product_subtype_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_product_subtypes',
				key: 'id'
			}
		},
		name: {
			type: DataTypes.STRING
		},
		description: {
			type: DataTypes.TEXT
		},
		price: {
			type: DataTypes.REAL
		},
		shipping: {
			type: DataTypes.REAL
		},
		featured: {
			type: DataTypes.INTEGER
		},
		time_added: {
			type: DataTypes.DATE
		},
		in_stock: {
			type: DataTypes.INTEGER
		},
		show_on_site: {
			type: DataTypes.INTEGER
		},
		on_sale: {
			type: DataTypes.INTEGER
		},
		sale_price: {
			type: DataTypes.REAL
		},
		weight: {
			type: DataTypes.REAL
		},
		trending: {
			type: DataTypes.INTEGER
		},
		short_description: {
			type: DataTypes.TEXT
		},
		sort: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_products;
};