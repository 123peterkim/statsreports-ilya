<?php

	if(!$hideutil)
		include_once("includes/util.php");

	require_once '/data/tools/awssdk/aws-autoloader.php';
	use Aws\Ses\SesClient;

	$regid = $regid > 0 ? $regid : intval($_GET["r"]);
	$userid = intval($_SESSION["User"]["userid"]);
	$fs = mysql_real_escape_string($_GET["fs"]) === "true" ? true : false;
	$observerfee = 0;
	$alldata = array();
	$contact = array();

	if($fs)
		$sql = "SELECT * FROM `tbl_user_registrations` WHERE id='$regid' LIMIT 1";
	else
		$sql = "SELECT * FROM `tbl_user_registrations` WHERE id='$regid' AND userid='$userid' LIMIT 1";

	$res = mysql_query($sql,constant("conn2")) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$alldata = $row;
			$tourdateid = $row["tourdateid"];

			$isfinals = db_one("is_finals","tbl_tour_dates","id='$tourdateid'");

			if($isfinals && !$sendemail) {
				header("Location: reg_nat_print.php?r=$regid&fs=true");
				exit();
			}


			$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
			$regdata = json_decode($row["regdata"],true);
			$contact = $regdata["Contact"];
			$dancers = $regdata["Dancers"];
			$routines = $regdata["Routines"];
			$tourdate = get_tourdate_info($tourdateid);
			$eventid = intval($regdata["Tourdate"]["eventid"]);
			$tourdate["eventname"] = stripslashes(db_one("name","events","id='$eventid'"));
			$ftc = $row["ftc"];
			$ftv = $row["ftv"];
			$free_observer_qty2 = 0;
			if($regdata["Observer_qty"] > 0)
				$observerfee = $regdata["Observer_val"];
			if($regdata["Observer_qty2"] > 0)
				$observerfee2 = $regdata["Observer_val2"];
			if(count($dancers) > 0) {
				foreach($dancers as $dancer) {
					$counts[$dancer["workshoplevelid"]] += 1;

					if($dancer["workshoplevelid"] == 6 || $dancer["workshoplevelid"] == 7 || $dancer["workshoplevelid"] == 9 || $dancer["workshoplevelid"] == 10)
						++$free_observer_qty2;
				}
			}

			$youngstr = "";
			switch($eventid) {
				case 7:
					$youngstr = "JUMPstarts";
					break;
				case 8:
					$youngstr = "Nubies";
					break;
				case 14:
					$youngstr = "PeeWees";
					break;
				case 18:
					$youngstr = "Sidekicks";
					break;
			}
		}
	}
	if(!isset($regdata))
		exit();

	if($sendemail)
		ob_start();

	if(isset($_GET["dev"])) {
		print_r($regdata);exit();
	}

?><!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
			body {
				font-family: Verdana, Arial, sans-serif;
				font-size: 11px;
				padding: 0;
				margin: 0;
			}

			table {

			}

			table tr th {
				padding: 0px 0px 2px 0;
				border-bottom: 1px dotted #000000;
				font-size: 11px;
				font-style: italic;
			}

			table tr td {
				padding: 0 0 2px 0;
				font-size: 11px;
			}

			.heading {
				font-style: italic;
				font-weight: bold;
				font-family: Georgia,serif;
			}

		</style>
	</head>
	<body>
		<div style="width: 650px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td><img src="http://www.mybreakthefloor.com/images/logos/email/<?php print($contact["e"]); ?>.jpg" alt="" /></td>
					<td style="text-align:right;"><!-- <img src="//www.mybreakthefloor.com/images/register/registrationconfirmation2.jpg" alt="" /> --></td>
				</tr>
				<tr><td colspan="2" style="padding-top:15px;">Dear <?php print(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$contact["fname"])))); ?>,<br/><br/>Thank you for registering for the <span style="font-weight:bold;"><?php print($alldata["eventname"]); ?> Tour!</span> I am so glad you will be joining us!</td></tr>
				<tr>
					<td colspan="2" style="font-weight:bold;">
				<?php print("<br/>".$tourdate["city"].", ".$tourdate["state"]."<br/>".$tourdate["dispdate"]."<br/>".$tourdate["venue_name"]);?>
				<?php print("<br/><br/>".stripslashes($contact["studioname"])." Registration Summary");?>
					</td>
				</tr>
			</table>



			<!-- WORKSHOP DANCERS -->
			<div style="margin-top:15px;">
				<div class="heading">WORKSHOP REGISTRANTS (<?php print(count($dancers) + $regdata["Observer_qty"]); ?>)</div>
			<?php if($contact["e"] == 7) { //JUMP ?>
				<div>Teachers (<?php print(isset($counts["1"]) ? $counts["1"] : "0"); ?>), Seniors (<?php print(isset($counts["2"]) ? $counts["2"] : "0"); ?>), Teens (<?php print(isset($counts["3"]) ? $counts["3"] : "0"); ?>), Juniors (<?php print(isset($counts["4"]) ? $counts["4"] : "0"); ?>), Minis (<?php print(isset($counts["5"]) ? $counts["5"] : "0"); ?>), JUMPstarts (<?php print(isset($counts["6"]) ? $counts["6"] : "0"); ?>), Observers (<?php print(intval($regdata["Observer_qty"]));?>), <?=$youngstr;?> Observers (<?php print(intval($regdata["Observer_qty2"]) + $free_observer_qty2);?>)</div>
			<?php } ?>
			<?php if($contact["e"] == 8) { //NUVO ?>
				<div>Teachers (<?php print(isset($counts["1"]) ? $counts["1"] : "0"); ?>), Seniors (<?php print(isset($counts["2"]) ? $counts["2"] : "0"); ?>), Teens (<?php print(isset($counts["3"]) ? $counts["3"] : "0"); ?>), Juniors (<?php print(isset($counts["4"]) ? $counts["4"] : "0"); ?>), Minis (<?php print(isset($counts["5"]) ? $counts["5"] : "0"); ?>), Nubies (<?php print(isset($counts["7"]) ? $counts["7"] : "0"); ?>), Observers (<?php print(intval($regdata["Observer_qty"]));?>), <?=$youngstr;?> Observers (<?php print(intval($regdata["Observer_qty2"]) + $free_observer_qty2);?>)</div>
			<?php } ?>
			<?php if($contact["e"] == 18) { //24SEVEN ?>
				<div>Teachers (<?php print(isset($counts["1"]) ? $counts["1"] : "0"); ?>), Seniors (<?php print(isset($counts["2"]) ? $counts["2"] : "0"); ?>), Teens (<?php print(isset($counts["3"]) ? $counts["3"] : "0"); ?>), Juniors (<?php print(isset($counts["4"]) ? $counts["4"] : "0"); ?>), Minis (<?php print(isset($counts["5"]) ? $counts["5"] : "0"); ?>), SideKicks (<?php print(isset($counts["10"]) ? $counts["10"] : "0"); ?>), Observers (<?php print(intval($regdata["Observer_qty"]));?>), <?=$youngstr;?> Observers (<?php print(intval($regdata["Observer_qty2"]) + $free_observer_qty2);?>)</div>
			<?php } ?>
				<div style="margin:10px 0;font-weight:bold;font-style:italic;color:#FF0000;">ALL AGES ARE AS OF JANUARY 1, <?php print(db_one("ageasofyear","events","id='".$regdata["Contact"]["e"]."'")); ?></div>
			</div>
			<table cellpadding="0" cellspacing="0">
				<thead>
					<th style="text-align:left;width:170px;">Name</th><th style="width:100px;text-align:center;">Birthdate</th><th style="width:90px;text-align:center;">Age</th><th style="width:110px;text-align:left;">Workshop Level</th><th style="text-align:right;width:170px;">Fee</th>
				</thead>
				<tbody>
				<?php
					if(count($dancers) > 0) {
						foreach($dancers as $dancer) {
				?>
					<tr>
						<td><?php print($dancer["dname"]);?></td>
						<td style="text-align:center;"><?php print($dancer["birthdate"]);?></td>
						<td style="text-align:center;"><?php print($dancer["age"]);?></td>
						<td style="font-weight:bold;"><?php print($dancer["workshoplevelname"]);?></td>
						<td style="text-align:right;"><?php

							if($dancer["hasscholarship"] == 1)
								print("<span style='font-size:10px;font-style:italic;'>(Scholarship)  </span>&nbsp;&nbsp;");
							else {
								if($dancer["oneday"] == 1)
									print("<span style='font-size:10px;font-style:italic;'>(One Day) </span>&nbsp;&nbsp;");
								if($dancer["attended_reg"] == 1)
									print("<span style='font-size:10px;font-style:italic;'>(-50%)  </span>&nbsp;&nbsp;");
							}

							print("$".$dancer["workshop_fee"]);?>
						</td>
					</tr>
				<?php 	}
					}	?>
				<?php
					if($regdata["Observer_qty"] > 0) { ?>
					<tr>
						<td style="font-style:italic;"><?php print("Observer x ".$regdata["Observer_qty"]);?></td>
						<td style="text-align:center;">-</td>
						<td style="text-align:center;">-</td>
						<td style="font-weight:bold;">Observer</td>
						<td style="text-align:right;"><?php print("$".number_format($observerfee,2));?></td>
					</tr>
				<?	} ?>
				<?php
					if($regdata["Observer_qty2"] > 0 || $free_observer_qty2 > 0) { ?>
					<tr>
						<td style="font-style:italic;"><?php print($youngstr." Observer x ".($regdata["Observer_qty2"] + $free_observer_qty2));?></td>
						<td style="text-align:center;">-</td>
						<td style="text-align:center;">-</td>
						<td style="font-weight:bold;"><?=$youngstr;?> Observer</td>
						<td style="text-align:right;"><?php print("$".number_format($observerfee2,2));?></td>
					</tr>
				<?	} ?>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td colspan="5" style="text-align:right;">
				<?php
					if($ftc > 0) { ?>
							<div class="heading">Subtotal</div>
							<div>$<?php print(number_format($alldata["workshop_fee"],2)); ?><br/><br/></div>
							<div><?php $multi = $ftc == 1 ? "Teacher" : "Teachers"; print("$ftc Free $multi -$".$ftv."</div>"); ?><br/></div>
				<?php } ?>
							<div class="heading">Workshop Subtotal</div>
							<div>$<?php print(number_format($alldata["workshop_fee"] - $ftv,2)); ?></div>
						</td>
					</tr>
				</tbody>
			</table>


			<!-- COMPETITION ROUTINES -->
			<div style="margin:15px 0 5px;">
				<div class="heading">COMPETITION ROUTINES (<?php print(count($routines)); ?>)</div>
			</div>
			<table cellpadding="0" cellspacing="0">
				<thead>
					<th style="text-align:left;width:240px;">Routine Name</th><th style="text-align:left;width:105px;">Age Division</th><th style="text-align:left;width:105px;">Category</th><th style="text-align:left;width:105px;">Perf. Division</th><th style="text-align:right;width:85px;">Fee</th>
				</thead>
				<tbody>
				<?php
					if(count($routines) > 0) {
						foreach($routines as $routine) {
				?>
					<tr>
						<td><?php print(stripslashes($routine["routinename"]));?></td>
						<td><?php print(db_one("name","tbl_age_divisions","id=".$routine["agedivisionid"]));?></td>
						<td><?php print(db_one("name","tbl_routine_categories_$seasonid","id=".$routine["routinecategoryid"]));?></td>
						<td><?php print(db_one("name","tbl_performance_divisions","id=".$routine["perfdivisionid"]));?></td>
						<td style="text-align:right;">
							<?php print("$".$routine["fee"]);?>
						</td>
					</tr>

				<?php
					if(count($routine["dancers"]) > 0) {
						$dstr = "";
						foreach($routine["dancers"] as $rd)
							$dstr .= $rd["fname"]." ".$rd["lname"].", ";
				?>
						<tr><td colspan="5" style="padding-bottom:10px;font-style:italic;vertical-align:top;">
							<?php print(substr($dstr,0,strlen($dstr)-2)); ?>
							<?php if($routine["extra_time"] > 0) { ?>
								<div style="font-style:italic;font-size:11px;font-weight:bold;">Extra Time - <?php print($routine["extra_time"]);?> min(s)</div>
							<?php } ?>
						</td></tr>
				<?php } ?>

				<?php 	}
					}
					else { ?>
						<tr><td colspan="5">You did not register any routines.</td></tr>
					<?php }	?>
					<tr><td colspan="5">&nbsp;</td></tr>
					<tr>
						<td colspan="5" style="text-align:right;"><div class="heading">Competition Subtotal</div><div>$<?php if(strlen($alldata["competition_fee"]) > 0) print($alldata["competition_fee"]); else print("0.00"); ?></div></td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>

					<tr>
						<td colspan="5" style="text-align:right;">
							<div class="heading" style="color:#000000;">Registration Subtotal</div>
							<div>$<?php print(number_format($alldata["competition_fee"] + $alldata["workshop_fee"] - $ftv,2)); ?></div>
						</td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>

				<?php
					if($alldata["creditamt"] > 0) { ?>
					<tr>
						<td colspan="5" style="text-align:right;"><div class="heading" style="color:#FF0000;">Credit</div><div>$<?php print($alldata["creditamt"]." - ".$alldata["creditfrom"]); ?></div></td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
				<?php }	?>
					<tr>
						<td colspan="5" style="text-align:right;"><div class="heading" style="color:#FF0000;">Grand Total</div><div><?php print("$".number_format($alldata["total_fees"]-str_replace(",","",$alldata["creditamt"]),2,'.','')); ?></div></td>
					</tr>
					<tr><td colspan="5">&nbsp;</td></tr>
				</tbody>
			</table>

			<div style="font-style:italic;">
		<?php
			//jump
			if($contact["e"] == 7) { ?>
				* This registration confirmation is based upon approval of your credit card or receipt of your check by Break The Floor Productions. It is also based on availability. If the event is full, BTFP will contact you ASAP and your credit card will not be charged or your check will not be cashed.
				<br/><br/>
				* IF YOU ARE PAYING BY CHECK, PLEASE REMEMBER: You are not registered until we receive the check and confirm the registration. Also, the early registration discount is not determined by the date of online registration, it is determined by the date JUMP receives the check. JUMP does not accept checks within 30 days of an event.
				<br/><br/>
				* Changes to your registration cannot be made online.  Please email all changes to info@jumptour.com.
				<br/><br/>
				* If you have any questions please feel free to contact us at any time. We will be in touch periodically, by email, to give you updates and scheduling information. Thanks so much for choosing to participate! I know you are going to have a great time. See you soon!
		<?php } ?>
		<?php
			//nuvo
			if($contact["e"] == 8) { ?>
				* This registration confirmation is based upon approval of your credit card or receipt of your check by NUVO Dance Convention. It is also based on availability. If the event is full, NUVO will contact you ASAP and your credit card will not be charged or your check will not be cashed.
				<br/><br/>			 -->
				* IF YOU ARE PAYING BY CHECK, PLEASE REMEMBER: You are not registered until we receive the check and confirm the registration. Also, the early registration discount is not determined by the date of online registration, it is determined by the date NUVO receives the check. NUVO does not accept checks within 30 days of an event.
				<br/><br/>
				* Changes to your registration cannot be made online.  Please email all changes to stephanie@gonuvo.com.
				<br/><br/>
				* If you have any questions please feel free to contact us at any time. We will be in touch periodically, by email, to give you updates and scheduling information. Thanks so much for choosing to participate! I know you are going to have a great time. See you soon!
		<?php } ?>
		<?php
			//24 seven
			if($contact["e"] == 18) { ?>
				* This registration confirmation is based upon approval of your credit card or receipt of your check by 24 SEVEN. It is also based on availability. If the event is full, 24 SEVEN will contact you ASAP and your credit card will not be charged or your check will not be cashed.
				<br/><br/>
				* IF YOU ARE PAYING BY CHECK, PLEASE REMEMBER: You are not registered until we receive the check and confirm the registration. Also, the early registration discount is not determined by the date of online registration, it is determined by the date 24 Seven receives the check. 24 Seven does not accept checks within 30 days of an event.
				<br/><br/>
				* Changes to your registration cannot be made online.  Please email all changes to info@24sevendance.com.
				<br/><br/>
				* If you have any questions please feel free to contact us at any time. We will be in touch periodically, by email, to give you updates and scheduling information. Thanks so much for choosing to participate! I know you are going to have a great time. See you soon!
		<?php } ?>
			</div>

			<div style="border-top:1px solid #CCCCCC;margin-top:20px;">
				<table cellpadding="0" cellspacing="0" style="margin-top:5px;">
				<?php
					if($contact["e"] == 7) { ?>
					<tr>
						<td style="color:#FF0000;width:216px;border-right:1px solid #CCCCCC;">5446 Satsuma Ave<br/>North Hollywood, CA 91601</td>
						<td style="color:#FF0000;width:216px;text-align:center;border-right:1px solid #CCCCCC;">Tel: (818) 432-4395<!-- <br/>Fax: (212) 397-3636 --></td>
						<td style="color:#FF0000;width:216px;text-align:right;">info@jumptour.com<br/>www.jumptour.com</td>
					</tr>
				<?php } ?>
				<?php
					if($contact["e"] == 8) { ?>
					<tr>
						<td style="color:#FF0000;width:216px;border-right:1px solid #CCCCCC;">5446 Satsuma Ave<br/>North Hollywood, CA 91601</td>
						<td style="color:#FF0000;width:216px;text-align:center;border-right:1px solid #CCCCCC;">Tel: (818) 432-3894<br/>Fax: (888) 436-9971</td>
						<td style="color:#FF0000;width:216px;text-align:right;">go@gonuvo.com<br/>www.gonuvo.com</td>
					</tr>
				<?php } ?>
				<?php
					if($contact["e"] == 18) { ?>
					<tr>
						<td style="color:#FF0000;width:216px;border-right:1px solid #CCCCCC;">5446 Satsuma Ave<br/>North Hollywood, CA 91601</td>
						<td style="color:#FF0000;width:216px;text-align:center;border-right:1px solid #CCCCCC;">Tel: (818) 432-6085<br/>Fax: (888) 774-9502</td>
						<td style="color:#FF0000;width:216px;text-align:right;">info@24sevendance.com<br/>www.24sevendance.com</td>
					</tr>
				<?php } ?>
				</table>
			</div>

		</div>
	</body>
</html>