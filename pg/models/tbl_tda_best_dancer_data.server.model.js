"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_tda_best_dancer_data = sequelize.define('tbl_tda_best_dancer_data', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tda_best_dancer_data',
				key: 'id'
			}
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		},
		routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		choreographer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		is_competing: {
			type: DataTypes.INTEGER
		},
		has_photo: {
			type: DataTypes.INTEGER
		},
		ballet: {
			type: DataTypes.INTEGER
		},
		danceoff: {
			type: DataTypes.JSONB
		},
		group_id: {
			type: DataTypes.INTEGER
		},
		perc: {
			type: DataTypes.JSONB
		},
		place: {
			type: DataTypes.JSONB
		},
		jazz: {
			type: DataTypes.INTEGER
		},
		jacket: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_tda_best_dancer_data.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			tbl_tda_best_dancer_data.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
			tbl_tda_best_dancer_data.belongsTo(models.tbl_routines, {foreignKey: 'routine_id', as: 'routine'});
			tbl_tda_best_dancer_data.belongsTo(models.tbl_persons, {foreignKey: 'choreographer_id', as: 'person'});
			tbl_tda_best_dancer_data.belongsTo(models.tbl_studios, {foreignKey: 'studio_id', as: 'studio'});
		}
	});
	return tbl_tda_best_dancer_data;
};