"use strict";

module.exports = function(sequelize, DataTypes) {
  var studios_users = sequelize.define('studios_users', {
    studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_studios',
        key: 'id'
      }
    },
    mybtf_user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_mybtf_users',
        key: 'id'
      }
    }
  }, {
    timestamps: false,
    associate: function(models) {

    }
  });
  return studios_users;
};