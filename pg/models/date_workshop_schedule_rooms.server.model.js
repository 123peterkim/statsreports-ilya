"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_workshop_schedule_rooms = sequelize.define('date_workshop_schedule_rooms', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    tour_date_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_tour_dates',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.STRING
    },
    position: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: false,
    associate: function (models) {
      date_workshop_schedule_rooms.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
    }
  });
  return date_workshop_schedule_rooms;
};