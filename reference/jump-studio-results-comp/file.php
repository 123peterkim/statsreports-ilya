<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$dispdate = get_tourdate_dispdate($tourdateid);
	$componly = isset($_GET["componly"]) ? true : false;

	if($tourdateid > 0) {
		$seasonid = db_one("seasonid","tbl_tour_dates","id='$tourdateid'");
		$cityname = stripslashes(db_one("city","tbl_tour_dates","id=$tourdateid"));
		$stateid = db_one("stateid","tbl_tour_dates","id='$tourdateid'");
		$state = db_one("abbreviation","tbl_states","id='$stateid'");

		//GET STUDIOS
		$studios = array();
		$sql = "SELECT tbl_date_studios.studioid, tbl_studios.contacts, tbl_date_studios.independent, tbl_date_studios.studiocode, tbl_studios.name AS studioname FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC";
		$res = mysql_query($sql) or die(mysql_error());
		if(mysql_num_rows($res) > 0) {
			while($row = mysql_fetch_assoc($res)) {
				$row["studioname"] = stripslashes(str_replace("&amp;","&",$row["studioname"]));
				$contacts = json_decode($row["contacts"],true);
				$row["contact"] = $contacts[0]["fname"];
				$row["contact_full"] = $contacts[0]["fname"]." ".$contacts[0]["lname"];
				//piece of fucking shit adjudicated awards
				//(just kidding, they're not so bad)
				$adjudicated = array();
				$sql2 = "SELECT tbl_date_routines.routineid, tbl_routines.name AS routinename, tbl_date_routines.finals_total_score, tbl_competition_awards.name AS awardsname FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_competition_awards ON tbl_competition_awards.id=tbl_date_routines.finals_awardid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$row["studioid"]." AND tbl_date_routines.canceled!=1 ORDER BY tbl_routines.name ASC";
				$res2 = mysql_query($sql2) or die(mysql_error());
				if(mysql_num_rows($res2) > 0) {
					while($row2 = mysql_fetch_assoc($res2)) {
						$adjudicated[] = $row2;
					}
					$row["adjudicated"] = $adjudicated;
				}

				//piece of fucking shit high scores by age
				//seriously, fuck scores.
				$hsa = array();
				$sql3 = "SELECT tbl_date_routines.routineid, tbl_routines.name AS routinename, tbl_routine_categories_$seasonid.name AS routinecategoryname, tbl_date_routines.place_hsa, tbl_age_divisions.name AS agedivisionname, tbl_age_divisions.range AS agedivisionrange FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_routine_categories_$seasonid ON tbl_routine_categories_$seasonid.id=tbl_date_routines.routinecategoryid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id=tbl_date_routines.agedivisionid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.place_hsa > 0 AND tbl_date_routines.studioid=".$row["studioid"]." AND tbl_date_routines.canceled!=1 ORDER BY tbl_routines.name ASC";
				$res3 = mysql_query($sql3) or die(mysql_error());
				if(mysql_num_rows($res3) > 0) {
					while($row3 = mysql_fetch_assoc($res3)) {
						if($row3["place_hsa"] == 1) {
							$row3["placestr"] = "1st";
							if($row3["routinecategoryname"] == "Solo")
								$row["has1st_solo"] = true;
							if($row3["routinecategoryname"] == "Duo/Trio")
								$row["has1st_duotrio"] = true;
						}
						if($row3["place_hsa"] == 2)
							$row3["placestr"] = "2nd";
						if($row3["place_hsa"] == 3)
							$row3["placestr"] = "3rd";

						if($row3["place_hsa"] < 4)
							$hsa[] = $row3;
					}

					$row["hsa"] = $hsa;
				}



				//piece of fucking shit high scores by perf div
				//worse than hsa. fucking Matt, douche.  again, kidding.  he's just a production manager. not hurtin' anybody
				$perfdivtype = db_one("perfdivtype","tbl_tour_dates","id=$tourdateid");

				switch($perfdivtype) {
					case 1:
							// Combined Top 3
							$maxplaces = 3;
							$limit = "LIMIT $maxplaces"; //NEW 5.13.2014
							$hsporder = Array(
											 "0"=>Array(
											 			 "adid1"=>1,
											 			 "adid2"=>2,
											 			 "name"=>"Mini/Junior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "1"=>Array(
											 			 "adid1"=>3,
											 			 "adid2"=>4,
											 			 "name"=>"Teen/Senior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   )
											 );
							break;
					case 2:
							// Combined Top 1
							$maxplaces = 1;
							$hsporder = Array(
											 "0"=>Array(
											 			 "adid1"=>1,
											 			 "adid2"=>2,
											 			 "name"=>"Mini/Junior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "1"=>Array(
											 			 "adid1"=>3,
											 			 "adid2"=>4,
											 			 "name"=>"Teen/Senior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   )
											 );
							break;
					case 3:
							// Individual Top 3
							$maxplaces = 3;
							$limit = "LIMIT $maxplaces";  //NEW 5.13.2014
							$hsporder = Array(
											 "1"=>Array(
											 			 "adid"=>1,
											 			 "name"=>"Mini",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "2"=>Array(
											 			 "adid"=>2,
											 			 "name"=>"Junior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "3"=>Array(
											 			 "adid"=>3,
											 			 "name"=>"Teen",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "4"=>Array(
											 			 "adid"=>4,
											 			 "name"=>"Senior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 );
							break;
					case 4:
							// Individual Top 1
							$maxplaces = 1;
							$limit = "LIMIT $maxplaces";  //NEW 5.13.2014
							$hsporder = Array(
											 "1"=>Array(
											 			 "adid"=>1,
											 			 "name"=>"Mini",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "2"=>Array(
											 			 "adid"=>2,
											 			 "name"=>"Junior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "3"=>Array(
											 			 "adid"=>3,
											 			 "name"=>"Teen",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 "4"=>Array(
											 			 "adid"=>4,
											 			 "name"=>"Senior",
											 			 "order"=>array(1,9,2,3,4,5,7,8,10)
											 		   ),
											 );
							break;
				}
				$studioid = $row["studioid"];
				foreach($hsporder as $hspkey=>$agedivgroup) {
					$agedivname = $agedivgroup["name"];
					if($perfdivtype==1 || $perfdivtype==2) {
						$adid1 = $agedivgroup["adid1"];
					 	$adid2 = $agedivgroup["adid2"];
					 	$injectthis = "(tbl_date_routines.agedivisionid=$adid1 OR tbl_date_routines.agedivisionid=$adid2)";
					 } else {
					 	$adid = $agedivgroup["adid"];
						$injectthis = "tbl_date_routines.agedivisionid=$adid";
					 }
				 	$perfdivisionname = "";
				 	foreach($agedivgroup["order"] as $order) {
					 		$perfdivisionname = db_one("name","tbl_performance_divisions","id=$order");

					 		$sqlaa4 = "SELECT tbl_date_routines.id AS dateroutineid, tbl_date_routines.place_hsp AS place, tbl_routines.name AS routinename, tbl_studios.name AS studioname
									 FROM `tbl_date_routines`
									 LEFT JOIN tbl_routines
									 ON tbl_routines.id=tbl_date_routines.routineid
									 LEFT JOIN tbl_studios
									 ON tbl_studios.id=tbl_date_routines.studioid
									 WHERE tbl_date_routines.tourdateid=$tourdateid
									 AND tbl_date_routines.place_hsp > 0
									 AND $injectthis
									 AND tbl_date_routines.perfcategoryid=$order
									 AND tbl_date_routines.studioid=$studioid
									 ORDER BY tbl_date_routines.place_hsp ASC
									 $limit";

							/* 5.13.2014 -- removed LIMIT $maxplaces and changed to $limit, so ties can appear in combined top 1 */


							$resaa4 = mysql_query($sqlaa4) or die(mysql_error());
							//print('num rows: '.mysql_num_rows($res4));
							if(mysql_num_rows($resaa4) > 0) {
								$place = 1;
								while($rowaa4 = mysql_fetch_assoc($resaa4)) { // goes in ascending order, so it will always go in order 1 -> 2 -> 3

									 switch($place) {
										 case 1:
										 	$rowaa4["place"] = "1st";
										 	break;
										 case 2:
										 	$rowaa4["place"] = "2nd";
										 	break;
										 case 3:
										 	$rowaa4["place"] = "3rd";
										 	break;
									 }

									$row["hsp"][$agedivname."|".$perfdivisionname][$place] = $rowaa4;
									$place++;

								}
								//print_r($hspawards[$agedivname."|".$perfdivisionname]);
								//print("<br/>");

							}




					}
				}

				/*
				$hsp = array();
				$sql4 = "SELECT tbl_date_routines.routineid, tbl_date_routines.agedivisionid, tbl_routines.name AS routinename, tbl_performance_divisions.name AS perfdivisionname, tbl_date_routines.place_hsp FROM `tbl_date_routines` LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_performance_divisions ON tbl_performance_divisions.id=tbl_date_routines.perfcategoryid WHERE tbl_date_routines.tourdateid=$tourdateid AND tbl_date_routines.place_hsp = 1 AND tbl_date_routines.studioid=".$row["studioid"]." AND tbl_date_routines.canceled!=1 ORDER BY tbl_routines.name ASC";
				$res4 = mysql_query($sql4) or die(mysql_error());
				if(mysql_num_rows($res4) > 0) {
					while($row4 = mysql_fetch_assoc($res4)) {
						if($row4["agedivisionid"] == 1 || $row4["agedivisionid"] == 2)
							$row4["agedivisionstr"] = "Mini/Junior";
						if($row4["agedivisionid"] == 3 || $row4["agedivisionid"] == 4)
							$row4["agedivisionstr"] = "Teen/Senior";
						if($row4["agedivisionid"] == 8) //open
							$row4["agedivisionstr"] = "Open";

						if($row4["place_hsp"] == 1)
							$row4["placestr"] = "1st";
						$hsp[] = $row4;
					}
					$row["hsp"] = $hsp;
				}
				*/

if(!$componly) {

				//ass-raping scholarships [NOT CLASS]
				$scholarships = array();
				$sql5 = "SELECT tbl_date_scholarships.id AS datescholarshipid, tbl_date_scholarships.winner, tbl_date_dancers.studioid, tbl_staff.fname AS facultyfname, tbl_staff.lname AS facultylname, tbl_scholarships.name AS scholarshipname, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_scholarships` LEFT JOIN tbl_date_dancers ON tbl_date_dancers.id=tbl_date_scholarships.datedancerid LEFT JOIN tbl_scholarships ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_scholarships.profileid LEFT JOIN tbl_staff ON tbl_staff.id=tbl_date_scholarships.facultyid WHERE tbl_date_scholarships.tourdateid=$tourdateid AND tbl_date_dancers.studioid=".$row["studioid"]." AND tbl_scholarships.isclass=0  ORDER BY tbl_scholarships.report_order ASC, tbl_date_scholarships.winner DESC, tbl_profiles.lname ASC, tbl_profiles.fname ASC";
				$res5 = mysql_query($sql5) or die(mysql_error());
				if(mysql_num_rows($res5) > 0) {
					while($row5 = mysql_fetch_assoc($res5)) {
						if($row5["winner"] == 0) {
							$row["vip_hasrunnerup"] = true;
							$row5["winnerstr"] = "Runner-Up";
						}
						else {
							$row["vip_haswinner"] = true;
							$row5["winnerstr"] = "Winner";
						}
						$scholarships[] = $row5;
					}
					$row["scholarships"] = $scholarships;
				}


				//titty-fucking scholarships [CLASS]
				$scholarships = array();
				$sql5a = "SELECT tbl_date_scholarships.id AS datescholarshipid, tbl_date_scholarships.winner, tbl_date_dancers.studioid, tbl_staff.fname AS facultyfname, tbl_staff.lname AS facultylname, tbl_scholarships.name AS scholarshipname, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_scholarships` LEFT JOIN tbl_date_dancers ON tbl_date_dancers.id=tbl_date_scholarships.datedancerid LEFT JOIN tbl_scholarships ON tbl_scholarships.id=tbl_date_scholarships.scholarshipid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_scholarships.profileid LEFT JOIN tbl_staff ON tbl_staff.id=tbl_date_scholarships.facultyid WHERE tbl_date_scholarships.tourdateid=$tourdateid AND tbl_date_dancers.studioid=".$row["studioid"]." AND tbl_scholarships.isclass=1  ORDER BY tbl_scholarships.report_order ASC, tbl_date_scholarships.winner DESC, tbl_profiles.lname ASC, tbl_profiles.fname ASC";
				$res5a = mysql_query($sql5a) or die(mysql_error());
				if(mysql_num_rows($res5a) > 0) {
					while($row5a = mysql_fetch_assoc($res5a)) {
						$scholarships[] = $row5a;
					}
					$row["class_scholarships"] = $scholarships;
				}

				//nerts!  best of jumps (all age divs together), best in studio, entertainment, & choreo award
				$boj = array();
				$bis = array();
				$ent = array();
				$cho = array();
				$sql6 = "SELECT tbl_date_special_awards.id AS specialawardid, tbl_date_special_awards.awardid, tbl_routines.name AS routinename, tbl_special_awards.name AS awardname, tbl_age_divisions.name AS agedivisionname, tbl_age_divisions.range AS agedivisionrange FROM `tbl_date_special_awards` LEFT JOIN tbl_date_routines ON tbl_date_routines.id=tbl_date_special_awards.dateroutineid LEFT JOIN tbl_age_divisions ON tbl_age_divisions.id=tbl_date_routines.agedivisionid LEFT JOIN tbl_routines ON tbl_routines.id=tbl_date_routines.routineid LEFT JOIN tbl_special_awards ON tbl_special_awards.id=tbl_date_special_awards.awardid WHERE tbl_date_special_awards.tourdateid=$tourdateid AND tbl_date_routines.studioid=".$row["studioid"]." ORDER BY tbl_routines.name ASC";
				$res6 = mysql_query($sql6) or die(mysql_error());
				if(mysql_num_rows($res6) > 0) {
					while($row6 = mysql_fetch_assoc($res6)) {
						if($row6["awardid"] == 100)
							$ent[] = $row6;
						if($row6["awardid"] == 101)
							$cho[] = $row6;
						if($row6["awardid"] == 5)
							$bis[] = $row6;
						if($row6["awardid"] == 110 || $row6["awardid"] == 4 || $row6["awardid"] == 3 || $row6["awardid"] == 2 || $row6["awardid"] == 1)
							$boj[] = $row6;
					}
					if(count($boj) > 0)
						$row["boj"] = $boj;
					if(count($bis) > 0)
						$row["bis"] = $bis;
					if(count($cho) > 0)
						$row["choreo"] = $cho;
					if(count($ent) > 0)
						$row["enter"] = $ent;
				}

				//well goddamned rounded studio award [studioawardid 3]
				$sql7 = "SELECT id FROM `tbl_date_studio_awards` WHERE awardtypeid=3 AND tourdateid=$tourdateid AND studioid='".$row["studioid"]."' AND winner=1";
				$res7 = mysql_query($sql7) or die(mysql_error());
				if(mysql_num_rows($res7) > 0) {
					while($row7 = mysql_fetch_assoc($res7)) {
						$row["wellrounded"] = 1;
					}
				}

} // comp only

				if(isset($row["boj"]) || isset($row["bis"]) || isset($row["choreo"]) || isset($row["enter"]) || isset($row["scholarships"]) || isset($row["hsp"]) || isset($row["hsa"]) || isset($row["adjudicated"]) || isset($row["wellrounded"]))
					$studios[] = $row;
			}



		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Studio Results</title>
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,700|Nothing+You+Could+Do:400' rel='stylesheet' type='text/css'>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}
			.awardheading {
				font-family: 'Oswald';
				font-size: 26px;
				color: #23a496;
			}

			.listtable {
				margin-bottom: 15px;
			}
				.listtable .tdfirst {
					width: 30px;
				}
				.listtable .tdsecond {
					width: 350px;
					font-weight: bold;
				}
				.listtable .tdthird {
					width: 160px;
				}
				.listtable .tdfourth {
					width: 180px;
				}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
	</head>
	<body>
	<?php
		foreach($studios as $studio) { ?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
			<!--	<tr>
					<td style="vertical-align:top;">
						<div style="font-size:12px;">
							<span style="font-family:Regal Box;font-size:70px;color:#FF0000;">JUMP</span><br/>
							<div style="font-family:'festus!';font-size:29px;margin:5px 0 10px;line-height:11px;">the alternative convention</div>
						</div>
					</td>
					<td style="text-align:right;vertical-align:top;">
						<div style="margin-top:5px;color:#333333;font-size:11px;font-style:italic;font-family:Verdana,sans-serif;">
							BREAK THE FLOOR PRODUCTIONS<br/>5446 Satsuma Ave<br/>North Hollywood, CA 91601<br/><br/>
							P: (818) 432-4395<br/>
							F: (888) 873-0795<br/><br/>
							www.breakthefloor.com<br/>info@jumptour.com
						</div>
					</td>
				</tr>
			-->
				<tr>
					<td colspan="2">
						<div style="background:url('../../jump_report_header.jpg') no-repeat top center;margin-bottom:10px;width:720px;height:119px;background-size:cover;">
							<table cellpadding="0" cellspacing="0" style="float:right;margin:0px 20px 0 0;">
								<tr>
									<td style="font-family:'Oswald',sans-serif;font-size:35px;color:#23a496;">STUDIO RESULTS</td>
								</tr>
								<tr>
									<td>
										<div style="color:#FFFFFF;text-align:right;">
											JUMP DANCE CONVENTION
											<br/>
											(818) 432-4395
											<br/>
											info@jumptour.com
										</div>
									</td>
							</table>
						</div>
					</td>
				</tr>

				<tr>
					<td colspan="2" style="text-align:center;">
						<span style="font-family:'festus!';font-size:50px;color:#FF0000;line-height:60px;">Congratulations!</span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div style="color:#000000;font-size:40px;font-family:'Nothing You Could Do';">
							<?php
								print($cityname.", ".$state);
							?>
						</div>
						<div style="color:#000;font-size:20px;font-family:'Oswald',sans-serif">
							<?=$dispdate;?>
						</div>
						<div style="border-bottom: 1px dashed #999999;font-family:'Oswald';margin-bottom:5px;padding-bottom:5px;font-size:24px;color:#000000;font-style:normal;">
							<?php
								print("<span style='font-weight:bold;'>".$studio["studioname"])."</span>&nbsp;&nbsp;&nbsp;<span style='font-weight:normal;'>(".$studio["studiocode"].")</span>";
							?>
						</div>
					</td>
				</tr>
			</table>

	<?php	if(count($studio["bis"]) > 0) { ?>
			<div class="awardheading">Best In Studio</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
				<tr><td colspan="4" style="padding:4px 0;">&bull; Each routine receives free Finals Competition entry to The Dance Awards 2015.</td></tr>
		<?php foreach($studio["bis"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print($award["agedivisionname"]." &bull; ".$award["agedivisionrange"]); ?></td>
					<td class="tdfourth">&nbsp;</td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>

	<?php	if(count($studio["boj"]) > 0) { ?>
			<div class="awardheading">Best of JUMP</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
				<tr><td colspan="4" style="padding:4px 0;">&bull; Each routine receives a $250 JUMP Competition Voucher for the 2015-2016 season.</td></tr>
				<tr><td colspan="4" style="padding:4px 0;">&bull; Each routine receives free Finals Competition entry to The Dance Awards 2015.</td></tr>
		<?php foreach($studio["boj"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print($award["agedivisionname"]." &bull; ".$award["agedivisionrange"]); ?></td>
					<td class="tdfourth">&nbsp;</td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>

	<?php	if($studio["wellrounded"] > 0) { ?>
			<div class="awardheading">Well-Rounded Studio Award</div>
			&bull; Your studio receives a $250 JUMP Competition Voucher for the 2015-2016 season.
	<?php } ?>

	<?php	if(count($studio["hsa"]) > 0) { ?>
			<div class="awardheading">High Score (by age division)</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
	<?php
		if($studio["has1st_solo"]) { ?>
				<tr><td colspan="4" style="padding:4px 0;">&bull; 1st Place Soloists receive free Finals Competiton entry to The Dance Awards 2015.</td></tr>
	<?php }
		if($studio["has1st_duotrio"]) { ?>
				<tr><td colspan="4" style="padding:4px 0;">&bull; 1st Place Duo/Trios receive free Finals Competiton entry to The Dance Awards 2015.</td></tr>
	<?php } ?>
		<?php foreach($studio["hsa"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print($award["placestr"]." Place"); ?></td>
					<td class="tdfourth"><?php print($award["agedivisionname"]." ".$award["agedivisionrange"]." &bull; ".$award["routinecategoryname"]); ?></td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>




<?php	if(count($studio["hsp"]) > 0) { ?>
		<div class="awardheading">High Score (by performance division)</div>

	<?php
		foreach($studio["hsp"] as $key=>$value) {
			$names = explode("|",$key);
			$agediva = explode(" (",$names[0]);
			$agediv = str_replace("/","",$agediva[0]);
			$routcat = str_replace(array("-"," ","/"),"",$names[1]);

	?>
		<!-- HighScore PERF Div -->
		<?php
			if(count($value) > 0) {
		?>
			<table class="listtable" cellpadding="0" cellspacing="0">
		<?php	foreach($value as $dancer) {
		?>
					<tr>
						<td class="tdfirst"><?php print($dancer["place"]); ?></td>
						<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$dancer["routinename"]))); ?></td>
						<td class="tdthird"><?php print($dancer["studioname"]); ?></td>
						<td class="tdfourth"><?=$agediv." &bull; ".$routcat;?></td>
					</tr>
		<? 	   } ?>
			</table>
		<?  } ?>
	<?	} ?>
	<? } ?>


	<?php	if(count($studio["adjudicated"]) > 0) { ?>
			<div class="awardheading">Adjudicated Awards</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
		<?php foreach($studio["adjudicated"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print("Score: ".$award["finals_total_score"]); ?></td>
					<td class="tdfourth"><?php print("Award: ".$award["awardsname"]); ?></td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>

	<?php	if(count($studio["enter"]) > 0) { ?>
			<div class="awardheading">Entertainment Award</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
		<?php foreach($studio["enter"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print($award["agedivisionname"]." &bull; ".$award["agedivisionrange"]); ?></td>
					<td class="tdfourth">&nbsp;</td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>

	<?php	if(count($studio["choreo"]) > 0) { ?>
			<div class="awardheading">Choreography Award</div>
			<table class="listtable" cellpadding="0" cellspacing="0">
		<?php foreach($studio["choreo"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print(stripslashes(str_replace("&amp;","&",$award["routinename"]))); ?></td>
					<td class="tdthird"><?php print($award["agedivisionname"]." &bull; ".$award["agedivisionrange"]); ?></td>
					<td class="tdfourth">&nbsp;</td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>



	<?php	if(count($studio["scholarships"]) > 0) { ?>
			<div class="awardheading">VIP Scholarships <span style="font-family:Arial,sans-serif;font-style:italic;font-size:10px;color:#FF0000;">Please do not share these results with your dancers until after the Closing Show!</span></div>
			<table class="listtable" cellpadding="0" cellspacing="0">
			<?php if($studio["vip_haswinner"]) { ?>
				<tr><td colspan="4" style="padding:4px 0;">&bull; VIP Winners receive (1) JUMP Tour Scholarship, (1) Workshop Scholarship to The Dance Awards 2015, and (1) $400 DancerPalooza Intensive Scholarship.</td></tr>
			<?php }
				if($studio["vip_hasrunnerup"]) { ?>
				<tr><td colspan="4" style="padding:4px 0;">&bull; VIP Runners-Up receive (1) Workshop Scholarship to The Dance Awards 2015 and (1) $250 DancerPalooza Intensive Scholarship.</td></tr>
			<?php } ?>
		<?php foreach($studio["scholarships"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print($award["fname"]." ".$award["lname"]); ?></td>
					<td class="tdthird"><?php print(str_replace("Class","",$award["scholarshipname"])); ?></td>
					<td class="tdfourth"><?php print($award["winnerstr"]); ?></td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>

	<?php	if(count($studio["class_scholarships"]) > 0) { ?>
			<div class="awardheading">Class Scholarships <span style="font-family:Arial,sans-serif;font-style:italic;font-size:10px;color:#FF0000;">Please do not share these results with your dancers until after the Closing Show!</span></div>
			<table class="listtable" cellpadding="0" cellspacing="0">
				<tr><td colspan="4" style="padding:4px 0;">&bull; Class Scholarship Winners receive (1) $250 DancerPalooza Intensive Scholarship.</td></tr>
		<?php foreach($studio["class_scholarships"] as $award) { ?>
				<tr>
					<td class="tdfirst">&nbsp;</td>
					<td class="tdsecond"><?php print($award["fname"]." ".$award["lname"]); ?></td>
					<td class="tdthird"><?php print($award["scholarshipname"]); ?></td>
					<td class="tdfourth">&nbsp;</td>
				</tr>
		<?php } ?>
			</table>
	<?php } ?>


		</div>
	<?php } //each studio ?>
	</body>
</html>
<?php exit(); ?>