"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_mybtf_user_has_dancers = sequelize.define('tbl_mybtf_user_has_dancers', {
		dancer_id: {
			type: DataTypes.STRING,
			references: {
			  model: 'tbl_dancers',
			  key: 'id'
			},
			primaryKey: true
		},
		mybtf_user_id: {
			type: DataTypes.STRING,
			references: {
				model: 'tbl_mybtf_users',
				key: 'id'
			},
      primaryKey: true
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_mybtf_user_has_dancers;
};