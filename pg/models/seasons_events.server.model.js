"use strict";

module.exports = function(sequelize, DataTypes) {
	var seasons_events = sequelize.define('seasons_events', {
		season_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_seasons',
				key: 'id'
			},
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			},
			primaryKey: true
		}
	}, {
		timestamps: false,
		associate: function(models) {
			seasons_events.belongsTo(models.tbl_seasons, {foreignKey: 'season_id', as: 'season'});
			seasons_events.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});
		}
	});
	return seasons_events;
};