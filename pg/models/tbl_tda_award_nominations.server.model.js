"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_tda_award_nominations = sequelize.define('tbl_tda_award_nominations', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		studio_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_studios',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		has_soty: {
			type: DataTypes.INTEGER
		},
		sa_ts_ota_no: {
			type: DataTypes.INTEGER
		},
		sa_mj_ota_no: {
			type: DataTypes.INTEGER
		},
		sa_oacd_no: {
			type: DataTypes.INTEGER
		},
		sa_ts_bc_no: {
			type: DataTypes.INTEGER
		},
		sa_mj_bc_no: {
			type: DataTypes.INTEGER
		},
		sa_peopleschoice_no: {
			type: DataTypes.INTEGER
		},
		sa_s_ota_no: {
			type: DataTypes.INTEGER
		},
		sa_t_ota_no: {
			type: DataTypes.INTEGER
		},
		sa_m_ota_no: {
			type: DataTypes.INTEGER
		},
		sa_j_ota_no: {
			type: DataTypes.INTEGER
		},
		soty_ts_ballet_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_ts_jazz_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_ts_musicaltheater_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_ts_contemplyrical_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_ts_hiphoptap_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_mj_ballet_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_mj_jazz_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_mj_musicaltheater_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_mj_contemplyrical_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		soty_mj_hiphoptap_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_ts_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_mj_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_oacd_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_oacd_designer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		sa_ts_bc_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_ts_bc_choreographer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		sa_mj_bc_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_mj_bc_choreographer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		sa_peopleschoice_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_s_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_t_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_m_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		},
		sa_j_ota_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_routines',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_ts_ballet_routine_id', as: 'soty_ts_ballet_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_ts_jazz_routine_id', as: 'soty_ts_jazz_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_ts_musicaltheater_routine_id', as: 'soty_ts_musicaltheater_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_ts_contemplyrical_routine_id', as: 'soty_ts_contemplyrical_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_ts_hiphoptap_routine_id', as: 'soty_ts_hiphoptap_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_mj_ballet_routine_id', as: 'soty_mj_ballet_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_mj_jazz_routine_id', as: 'soty_mj_jazz_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_mj_musicaltheater_routine_id', as: 'soty_mj_musicaltheater_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_mj_contemplyrical_routine_id', as: 'soty_mj_contemplyrical_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'soty_mj_hiphoptap_routine_id', as: 'soty_mj_hiphoptap_routine'});

			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_ts_ota_routine_id', as: 'sa_ts_ota_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_mj_ota_routine_id', as: 'sa_mj_ota_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_oacd_routine_id', as: 'sa_oacd_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_ts_bc_routine_id', as: 'sa_ts_bc_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_mj_bc_routine_id', as: 'sa_mj_bc_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_peopleschoice_routine_id', as: 'sa_peopleschoice_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_s_ota_routine_id', as: 'sa_s_ota_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_t_ota_routine_id', as: 'sa_t_ota_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_m_ota_routine_id', as: 'sa_m_ota_routine'});
			tbl_tda_award_nominations.belongsTo(models.tbl_routines, {foreignKey: 'sa_j_ota_routine_id', as: 'sa_j_ota_routine'});

			tbl_tda_award_nominations.belongsTo(models.tbl_persons, {foreignKey: 'sa_oacd_designer_id', as: 'sa_oacd_designer'});
			tbl_tda_award_nominations.belongsTo(models.tbl_persons, {foreignKey: 'sa_ts_bc_choreographer_id', as: 'sa_ts_bc_choreographer'});
			tbl_tda_award_nominations.belongsTo(models.tbl_persons, {foreignKey: 'sa_mj_bc_choreographer_id', as: 'sa_mj_bc_choreographer'});

		}
	});
	return tbl_tda_award_nominations;
};