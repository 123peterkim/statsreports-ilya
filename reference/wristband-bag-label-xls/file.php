<?php
	include("../../../includes/util.php");
	include("../../../includes/phpexcel/Classes/PHPExcel.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/Writer/Excel2007.php");
	include("../../../includes/phpexcel/Classes/PHPExcel/IOFactory.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = intval(db_one("eventid","tbl_tour_dates","id=$tourdateid"));
	$cityname = strtolower(str_replace(array(" ","-",","),"",db_one("city","tbl_tour_dates","id=$tourdateid")));

	//get all studios
	//non-independents	(why?)
	$sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.email, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid AND tbl_date_studios.independent=0 ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = str_replace(",","",stripslashes(str_replace("&amp;","&",$row["studioname"])));
			$contacts = json_decode($row["contacts"],true);
			$nc = array();
			if(count($contacts) > 0) {
				foreach($contacts as $contact) {
					if(strlen($contact["fname"]) > 0)
						$nc[] = $contact["fname"]." ".$contact["lname"];
				}
			}
			$row["contacts"] = implode(", ",$nc);
			$studios[] = $row;
		}
	}

//	print_r($studios);exit();
	if(count($studios) > 0) {
		//create excel shit
		$workbook = new PHPExcel();
		$workbook->setActiveSheetIndex(0);

		//set global font & font size
		$workbook->getDefaultStyle()->getFont()->setName('Arial');

		//col widths
		$workbook->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$workbook->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

		$dataArray = array();
		$dataArray[] = array("Studio","Contacts","Email");
		foreach($studios as $studio) {
			$dataArray[] = array(stripslashes(str_replace("&amp;","&",str_replace("&#44;",",",$studio["studioname"]))),$studio["contacts"],$studio["email"]);
		}
//		print_r($dataArray);exit();
		$workbook->getActiveSheet()->fromArray($dataArray,NULL,'A1');

		$outputFileType = 'Excel5';
		$mk = time();
		$somekindofrandomstr = "$cityname"."_wristbands".substr($mk,6,5);
		if(file_exists($outputFileName))
			unlink($outputFileName);
		$outputFileName = "../../temp/$somekindofrandomstr.xls";
		$objWriter = PHPExcel_IOFactory::createWriter($workbook, $outputFileType);
		$objWriter->save($outputFileName);
		chmod($outputFileName,777);
		unset($workbook);
		unset($objWriter);

		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".basename($outputFileName));
		header("Content-type: application/octet-stream");
		header("Content-Transfer-Encoding: binary");
		readfile($outputFileName);
	}

	exit();
?>