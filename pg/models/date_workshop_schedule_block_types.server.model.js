"use strict";

module.exports = function (sequelize, DataTypes) {
  var date_workshop_schedule_block_types = sequelize.define('date_workshop_schedule_block_types', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false,
    associate: function (models) {
    }
  });
  return date_workshop_schedule_block_types;
};