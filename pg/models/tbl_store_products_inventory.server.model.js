"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_products_inventory = sequelize.define('tbl_store_products_inventory', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		store_product_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_products',
				key: 'id'
			}
		},
		store_color_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_colors',
				key: 'id'
			}
		},
		store_size_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_store_sizes',
				key: 'id'
			}
		},
		quantity: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_products_inventory;
};