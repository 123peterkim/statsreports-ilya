"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_faculty = sequelize.define('tbl_faculty', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		person_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_persons',
				key: 'id'
			}
		},
		bio: {
			type: DataTypes.TEXT
		},
		director: {
			type: DataTypes.STRING
		},
		list_order: {
			type: DataTypes.INTEGER
		},
		links: {
			type: DataTypes.JSONB
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_faculty.belongsTo(models.tbl_persons, { foreignKey: 'person_id', as: 'person'});
		}
	});
	return tbl_faculty;
};