"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_competition_awards = sequelize.define('tbl_competition_awards', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		award_type_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_award_types',
				key: 'id'
			}
		},
		event_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_events',
				key: 'id'
			}
		},
		lowest: {
			type: DataTypes.INTEGER
		},
		highest: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
			tbl_competition_awards.belongsTo(models.tbl_award_types, {foreignKey: 'award_type_id', as: 'award_type'});
		}
	});
	return tbl_competition_awards;
};