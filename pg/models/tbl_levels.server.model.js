"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_levels = sequelize.define('tbl_levels', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING
		},
		event_id: {
			type: DataTypes.INTEGER,
      references: {
        model: 'tbl_events',
        key: 'id'
      }
		},
		plural: {
			type: DataTypes.STRING
		},
		order: {
			type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {
      tbl_levels.belongsTo(models.tbl_events, {foreignKey: 'event_id', as: 'event'});

		}
	});
	return tbl_levels;
};