"use strict";

module.exports = function(sequelize, DataTypes) {
  var date_studios_registrations = sequelize.define('date_studios_registrations', {
    date_studio_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'date_studios',
        key: 'id'
      },
      primaryKey: true
    },
    registration_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_registrations',
        key: 'id'
      },
      primaryKey: true
    },
    tour_date_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'tbl_tour_dates',
        key: 'id'
      }
    },
    invoice_note: {
      type: DataTypes.STRING
    },
    studio_code: {
      type: DataTypes.STRING
    },
    emailer_count: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: false,
    associate: function(models) {
      date_studios_registrations.belongsTo(models.date_studios, {foreignKey: 'date_studio_id', as: 'date_studio'});
      date_studios_registrations.belongsTo(models.tbl_registrations, {foreignKey: 'registration_id', as: 'registration'});
      date_studios_registrations.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
    }
  });
  return date_studios_registrations;
};