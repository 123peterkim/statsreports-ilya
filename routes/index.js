var express = require('express');
var router = express.Router();
var _ = require('lodash');
var Excel = require('exceljs');
var fs = require('fs-js');
var path = require('path');
var Sequelize = require(path.resolve('./pg/sequelize'));
var async = require('async');

var constants = require('./../reference/util/const');
var Util = require('./../reference/util/util');
var studioEmails = require('./studioEmails');
var awardLabel = require('./awardLabel');

// JSON files
var tourConfirmation = require('./../reference/tour-confirmation/data');
var tourReconfirmationData = require('../reference/tour-reconfirmation/data.json');
var programInfoData = require('../reference/program-info/data.json');
var hotelCompsData = require('../reference/hotel-comps/data.json');
var hotelCompsXlsData = require('../reference/hotel-comps-xls/data.json');
var wristbandBagLabelXlsData = require('../reference/wristband-bag-label-xls/data.json');
var onlineScoreSheetesData = require('../reference/online-score-sheets/data.json');
var critiqueSheetData = require('../reference/critique-sheet/data.json');
var missingMusicData = require('../reference/mybtf-missing-music/data.json');
var mailingLabelsData = require('../reference/mailing-labels/data.json');
var jumpScoreSheetsData = require('../reference/jump-score-sheets/data.json');
var jumpStudioListData = require('../reference/jump-studio-list/data.json');
var jumpRegPacketData = require('../reference/jump-reg-packet/data.json');
var jumpStudioResults = require('../reference/jump-studio-results/data.json');
var owesRefundsData = require('../reference/owes-refunds/data.json');
var outstandingItemsData = require('../reference/outstanding-items/data.json');
var scholarshipLabelsData = require('../reference/scholarship-labels/data.json');
var scholarshipLabelsNewData = require('../reference/scholarship-labels-new/data.json');
var studioPaidDancersData = require('../reference/studios-paid-dancers/data.json');
var bestDancerListData = require('../reference/best-dancer-list/data.json');
var scholarshipLabelsBdAllData = require('../reference/scholarship-labels-bd-all/data.json');
var scholarshipLabelsBdMjData = require('../reference/scholarship-labels-bd-mj/data.json');
var scholarshipLabelsBdTsData = require('../reference/scholarship-labels-bd-ts/data.json');
var scholarshipListByNameData = require('../reference/scholarship-list-by-name/data.json');
var petitesData = require('../reference/petites/data.json');
var petitesXlsData = require('../reference/petites-xls/data.json');
var scholarshipListByNameData = require('../reference/scholarship-list-by-name/data.json');
var vipPhotoData = require('../reference/vip-photo-browser/data.json');
var nonCompetingBDList = require('../reference/non-competing-bd-list/data.json');
var nominationsListData = require('../reference/nominations-list/data.json');
var nominationsListSortedData = require('../reference/nominations-list-sorted/data.json');
var dancerListData = require('../reference/dancer-list/data.json');
var jumpWorkshopScheduleData = require('../reference/jump-workshop-schedule/data.json');
var adjudicatedAwardsData = require('../reference/adjudicated-awards/data.json');
var bestOfJumpData = require('../reference/best-of-jump/data.json')
var jumpCompetitionScheduleData = require('../reference/jump-competition-schedule/data.json');
var jumpCompetitionScheduleVideoData = require('../reference/jump-competition-schedule-video/data.json');
var bestPerformancesData = require('../reference/best-performances/data.json');
var hightScoresAgeData = require('../reference/high-scores-age/data.json');
var jumpCompetitionSchedule2Data = require('../reference/jump-competition-schedule-2room-2/data.json');
var highScoresAgeData = require('../reference/high-scores-age/data.json');
var highScoresCombinedData = require('../reference/high-scores-combined/data.json');
var highScoresIndividualData = require('../reference/high-scores-individual/data.json');
var tdaHighScoresAgeData = require('../reference/tda-high-scores-age/data.json');
var tdaHighScoresIndividualData = require('../reference/tda-high-scores-individual/data.json');
var auditionClassData = require('../reference/audition-class/data.json');
var jumpSpecialAwardsData = require('../reference/jump-special-awards/data.json');
var bestDancerTallySheetsData = require('../reference/best-dancer-tally-sheets/data.json');
var bestDancerTallySheetsSampleData = require('../reference/best-dancer-tally-sheets-sample/data.json');
var topRound1Data = require('../reference/top-round-1/data.json');
var topRound2Data = require('../reference/top-round-2/data.json');
var topRound3Data = require('../reference/top-round-3/data.json');
var jumpBestInStudioCandidatesData = require('../reference/jump-best-in-studio-candidates/data.json');
var tdaBestPerformacesData = require('../reference/tda-best-performances/data.json');
var jumpScholarshipShippingLabelsData = require('../reference/jump-scholarship-shipping-labels/data.json');
var jumpScholarshipShippingLabelsWithLogosData = require('../reference/jump-scholarship-shipping-labels-with-logos/data.json');
var jumpSeasonVipsXlsData = require('../reference/jump-season-vips-xls/data.json');
var scholarshipWinnersData = require('../reference/scholarship-winners/data.json');
var scholarshipTemplatesData = require('../reference/scholarship-templates/data.json');
var studioAwardsData = require('../reference/studio-awards/data.json');
var studioAwardsWellRoundedData = require('../reference/studio-awards-well-rounded/data.json');
var scholarshipListData = require('../reference/scholarship-list/data.json');

// EXCEL Reports Controllers
var studioEmails = require('./studioEmails');
var awardLabel = require('./awardLabel');
var studioContacts = require('./studioContacts');
var bdJackets = require('./bdJackets');
var specialAwards = require('./specialAwards');
var scholarshipWinnersXLS = require('./scholarshipWinnersXLS');
var critiqueSheet = require('./critiqueSheet');
var mailingLabels = require('./mailingLabels');
var tourReconfirmation = require('./tourReconfirmation');
var jumpRegPacket = require('./jumpRegPacket');
var bestDancerList = require('./bestDancerList');
var jumpStudioResults = require('./jumpStudioResults');
var owesRefunds = require('./owesRefunds');
var studioPaidDancers = require('./studioPaidDancers');
var dancerList = require('./dancerList');
var dancersOnly = require('./dancersOnly');
var jumpSpecialAwards = require('./jumpSpecialAwards');
var scholarshipWinners = require('./scholarshipWinners');
var onlineScoreSheets = require('./onlineScoreSheets');
var scholarshipList = require('./scholarshipList');

/* GET home page. */
router.get('/', function(req, res, next) {
  var options = {
    reports: []
  };

  // add new reports here which will be displayed on the homepage
  options.reports.push({report: 'tour-confirmation', url: '/tour-confirmation/?registration_id=86'});
  options.reports.push({ report: 'tour-reconfirmation', url: '/tour-reconfirmation?tourdateid=746&studioid=3197'});
  options.reports.push({report: 'scholarship-list', url: '/scholarship-list/?tourdateid=746'});
  options.reports.push({report: 'program-info', url: '/program-info/?tourdateid=746'});
  options.reports.push({ report: 'studio-emails', url: '/studio-emails?tourdateid=746' });
  options.reports.push({report: 'award-label', url: '/award-label'});
  options.reports.push({report: 'studio-contacts', url: '/studio-contacts'});
  options.reports.push({report: 'critique-sheet', url: '/critique-sheet'});
  options.reports.push({report: 'hotel-comps', url: '/hotel-comps/?tourdateid=746&studioid=18'});
  options.reports.push({report: 'hotel-comps-xls', url: '/hotel-comps-xls/?tourdateid=746&studioid=18'});
  options.reports.push({report: 'wristband-bag-label-xls', url: '/wristband-bag-label-xls/?tourdateid=746'});
  options.reports.push({report: 'online-score-sheets', url: '/online-score-sheets'});
  options.reports.push({report: 'mybtf-missing-music', url: '/mybtf-missing-music'});
  options.reports.push({report: 'mailing-labels 1', url: '/mailing-labels/1'});
  options.reports.push({report: 'mailing-labels 2', url: '/mailing-labels/2'});
  options.reports.push({report: 'jump-score-sheets', url: '/jump-score-sheets'});
  options.reports.push({report: 'jump-score-sheets-blank', url: '/jump-score-sheets-blank'});
  options.reports.push({report: 'jump-studio-list', url: '/jump-studio-list/?tourdateid=746'});
  options.reports.push({report: 'jump-reg-packet', url: '/jump-reg-packet'});
  options.reports.push({report: 'jump-reg-packet-all', url: '/jump-reg-jump-reg-packet-all'});
  options.reports.push({report: 'jump-studio-results', url: '/jump-studio-results?tourdateid=746'});
  options.reports.push({report: 'jump-studio-results-comp', url: '/jump-studio-results?componly=true'});
  options.reports.push({report: 'owes-refunds', url: '/owes-refunds'});
  options.reports.push({report: 'outstanding-items', url: '/outstanding-items'});
  options.reports.push({report: 'outstanding-items-jacket', url: '/outstanding-items/jacket'});
  options.reports.push({report: 'outstanding-items-photo', url: '/outstanding-items/photo'});
  options.reports.push({report: 'scholarship-labels', url: '/scholarship-labels/?tourdateid=746'});
  options.reports.push({report: 'scholarship-labels-new', url: '/scholarship-labels-new/?tourdateid=746&eventid=7'});
  options.reports.push({report: 'studio-paid-dancers', url: '/studio-paid-dancers'});
  options.reports.push({report: 'bd-jackets-xls', url: '/bd-jackets-xls'});
  options.reports.push({report: 'best-dancer-list', url: '/best-dancer-list'});
  options.reports.push({report: 'scholarship-labels-bd-all', url: '/scholarship-labels-bd-all/?tourdateid=743'});
  options.reports.push({report: 'scholarship-labels-bd-mj', url: '/scholarship-labels-bd-mj/?tourdateid=743'});
  options.reports.push({report: 'scholarship-labels-bd-ts', url: '/scholarship-labels-bd-ts/?tourdateid=743'});
  options.reports.push({report: 'scholarship-list-by-name', url: '/scholarship-list-by-name'});
  options.reports.push({report: 'petites', url: '/petites'});
  options.reports.push({report: 'petites-xls', url: '/petites-xls'});
  options.reports.push({report: 'vip-photo-browser', url: '/vip-photo-browser'});
  options.reports.push({report: 'non-competing-bd-list', url: '/non-competing-bd-list'});
  options.reports.push({report: 'nominations-list', url: '/nominations-list'});
  options.reports.push({report: 'nominations-list-sorted', url: '/nominations-list-sorted'});
  options.reports.push({report: 'dancer-list', url: '/dancer-list'});
  options.reports.push({report: 'dancers-only', url: '/dancers-only'});
  options.reports.push({report: 'jump-workshop-schedule', url: '/jump-workshop-schedule'});
  options.reports.push({report: 'adjudicated-awards', url: '/adjudicated-awards'});
  options.reports.push({report: 'best-of-jump', url: '/best-of-jump'});
  options.reports.push({report: 'jump-competition-schedule', url: '/jump-competition-schedule'});
  options.reports.push({report: 'jump-competition-schedule-video', url: '/jump-competition-schedule-video'});
  options.reports.push({report: 'best-performances', url: '/best-performances'});
  options.reports.push({report: 'high-scores-age', url: '/high-scores-age'});
  options.reports.push({report: 'high-scores-combined', url: '/high-scores-combined'});
  options.reports.push({report: 'high-scores-individual', url: '/high-scores-individual'});
  options.reports.push({report: 'tda-high-scores-age', url: '/tda-high-scores-age'});
  options.reports.push({report: 'tda-high-scores-individual', url: '/tda-high-scores-individual'});
  options.reports.push({report: 'audition-class', url: '/audition-class'});
  options.reports.push({report: 'judges-running-order', url: '/judges-running-order'});
  options.reports.push({report: 'jump-competition-schedule-2room-1', url: '/jump-competition-schedule-2room-1'});
  options.reports.push({report: 'jump-special-awards', url: '/jump-special-awards'});
  options.reports.push({report: 'jump-special-awards XLS', url: '/jump-special-awards?xls=1'});
  options.reports.push({report: 'jump-competition-schedule-2room-2', url: '/jump-competition-schedule-2room-2'});
  options.reports.push({report: 'best-dancer-tally-sheets', url: '/best-dancer-tally-sheets'});
  options.reports.push({report: 'best-dancer-tally-sheets-sample', url: '/best-dancer-tally-sheets-sample'});
  options.reports.push({report: 'top-round-1', url: '/top-round-1'});
  options.reports.push({report: 'top-round-2', url: '/top-round-2'});
  options.reports.push({report: 'top-round-3', url: '/top-round-3'});
  options.reports.push({report: 'jump-best-in-studio-candidates', url: '/jump-best-in-studio-candidates'});
  options.reports.push({report: 'tda-best-performances', url: '/tda-best-performances'});
  options.reports.push({report: 'jump-scholarship-shipping-labels', url: '/jump-scholarship-shipping-labels'});
  options.reports.push({report: 'jump-scholarship-shipping-labels-with-logos', url: '/jump-scholarship-shipping-labels-with-logos'});
  options.reports.push({report: 'jump-season-vips-xls', url: '/jump-season-vips-xls'});
  options.reports.push({report: 'scholarship-winners', url: '/scholarship-winners'});
  options.reports.push({report: 'scholarship-winners-xls', url: '/scholarship-winners-xls'});
  options.reports.push({report: 'scholarship-winners-lname', url: '/scholarship-winners-lname'});
  options.reports.push({report: 'scholarship-templates', url: '/scholarship-templates'});
  options.reports.push({report: 'studio-awards', url: '/studio-awards'});
  options.reports.push({report: 'studio-awards-well-rounded', url: '/studio-awards-well-rounded'});
  options.reports.push({report: 'scholarship-list', url: '/scholarship-list?tourdateid=746'});

  res.render('index', options);
});

// add new routes here, like this
router.get('/tour-confirmation', function (req, res) {
  var sequelize = Sequelize.sequelize;
  var db = Sequelize.models;

  db.tbl_registrations.findAll({
    where: {
      id: req.query.registration_id
    },
    limit: 1,
    include: [
      {model: db.tbl_payments, as: 'payments'},
      {
        model: db.tbl_tour_dates,
        as: 'tour_date',
        attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
        include: [
          {
            model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name'], include: [
              {model: db.tbl_states, as: 'state', attributes: ['id', 'abbr']}
            ]
          },
          {model: db.tbl_events, as: 'event', attributes: ['id', 'name', 'age_as_of_year']},
          {model: db.tbl_seasons, as: 'season'},
          {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
        ]
      },
      {
        model: db.tbl_mybtf_users, as: 'mybtf_user', include: [
          {
            model: db.tbl_persons, as: 'person', include: [
              {
                model: db.tbl_addresses, as: 'address', include: [
                  {model: db.tbl_states, as: 'state'},
                  {model: db.tbl_countries, as: 'country'}
                ]
              },
              {model: db.tbl_person_types, as: 'person_type'},
              {
                model: db.tbl_contact_info, as: 'phone', include: [
                  {model: db.tbl_contact_types, as: 'contact_type'}
                ]
              },
              {
                model: db.tbl_contact_info, as: 'email', include: [
                  {model: db.tbl_contact_types, as: 'contact_type'}
                ]
              }
            ]
          }
        ]
      },
      {
        model: db.tbl_registrations_dancers, as: 'dancers', include: [
          {
            model: db.tbl_dancers, as: 'dancer', include: [
              {
                model: db.tbl_persons, as: 'person', include: [
                  {model: db.tbl_gender, as: 'gender'}
                ]
              }
            ]
          },
          {
            model: db.tbl_workshop_levels, as: 'workshop_level', include: [
              {model: db.tbl_levels, as: 'level'}
            ]
          }
        ]
      },
      {
        model: db.tbl_registrations_teachers, as: 'teachers', include: [
          {
            model: db.tbl_teachers, as: 'teacher', include: [
              {
                model: db.tbl_persons, as: 'person', include: [
                  {model: db.tbl_gender, as: 'gender'}
                ]
              }
            ]
          }
        ]
      },
      {
        model: db.tbl_registrations_observers, as: 'observers', include: [
          {
            model: db.tbl_observers, as: 'observer', include: [
              {
                model: db.tbl_persons, as: 'person', include: [
                  {model: db.tbl_gender, as: 'gender'}
                ]
              }
            ]
          },
          {
            model: db.tbl_workshop_levels, as: 'workshop_level', include: [
              {model: db.tbl_levels, as: 'level'}
            ]
          }
        ]
      },
      {
        model: db.tbl_registrations_routines, as: 'routines', include: [
          {model: db.tbl_routines, as: 'routine'},
          {
            model: db.tbl_performance_divisions,
            as: 'performance_division'
          },
          {
            model: db.tbl_routine_categories,
            as: 'routine_category',
            attributes: ['category_id', 'fee_per_dancer'],
            include: [
              {model: db.tbl_categories, as: 'category', attributes: ['name']}
            ]
          },
          {
            model: db.tbl_age_divisions,
            as: 'age_division',
            attributes: ['level_id', 'range', 'minimum_age', 'recommended_age'],
            include: [
              {model: db.tbl_levels, as: 'level', attributes: ['name']}
            ]
          },
          {
            model: db.registrations_routines_dancers, as: 'dancers', include: [
              {
                model: db.tbl_dancers, as: 'dancer', include: [
                  {
                    model: db.tbl_persons, as: 'person', include: [
                      {model: db.tbl_gender, as: 'gender'}
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name']}
    ]
  }).then(function (data) {
    var tourConfirmation = data[0].toJSON();

    tourConfirmation.tour_date.display_date = Util.tourDateDisplayDate(tourConfirmation.tour_date);
    var youngsters;
    switch (tourConfirmation.tour_date.event.id) {
      case 7:
        youngsters = 'JUMPstarts';
        break;
      case 8:
        youngsters = 'Nubies';
        break;
      case 18:
        youngsters = 'Sidekicks';
        break;
      case 14:
        youngsters = 'PeeWees';
        break;
      case 28:
        youngsters = 'Rookies';
        break;
      default:
        youngsters = '2';
        break;
    }
    var count = {};
    count.total = tourConfirmation.dancers.length + tourConfirmation.teachers.length + tourConfirmation.observers.length + tourConfirmation.additional_observers.one + tourConfirmation.additional_observers.two;
    count.Teachers = tourConfirmation.teachers.length;
    count[youngsters] = 0;
    count.Minis = 0;
    count.Juniors = 0;
    count.Teens = 0;
    count.Seniors = 0;
    count.Observer = 0;
    count.Observer2 = 0;
    count.routines = tourConfirmation.routines.length;
    _.forEach(tourConfirmation.teachers, function (teacher) {
      var birthdate = teacher.teacher.person.birthdate.split('-');
      birthdate.push(birthdate.splice(0, 1));
      birthdate = birthdate.join('/');
      teacher.teacher.person.birthdate = birthdate;
    });
    _.forEach(tourConfirmation.dancers, function (dancer) {
      count[dancer.workshop_level.level.plural] += 1;
      var birthdate = dancer.dancer.person.birthdate.split('-');
      birthdate.push(birthdate.splice(0, 1));
      birthdate = birthdate.join('/');
      dancer.dancer.person.birthdate = birthdate;
    });
    _.forEach(tourConfirmation.observers, function (observer) {
      count[observer.workshop_level.level.name] += 1;
    });
    count.Observer += tourConfirmation.additional_observers.one;
    count.Observer2 += tourConfirmation.additional_observers.two;

    tourConfirmation.dancers = _.orderBy(tourConfirmation.dancers, [function (user) {
      return user.workshop_level.minimum_age;
    }, function (user) {
      return user.dancer.person.lname.toLowerCase()
    }, function (user) {
      return user.dancer.person.fname.toLowerCase()
    }], ['desc', 'asc', 'asc']);


    res.render('reports/tour-confirmation', {
      registration: tourConfirmation,
      youngsters: youngsters,
      count: count
    });
  }).catch(function (err) {
    res.status(400).send({
      message: "There was an error: " + err
    })
  });
});

router.get('/tour-reconfirmation', tourReconfirmation.renderReport);

router.get('/program-info', function(req, res){
  /*if($tourdateid==113)
    $sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.city, tbl_studios.state FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid='$tourdateid' AND tbl_studios.name!='[N/A]' ORDER BY tbl_studios.name ASC";
  else
    $sql = "SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.city, tbl_studios.state FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid='$tourdateid' AND tbl_date_studios.independent=0 ORDER BY tbl_studios.name ASC";
  */
  if(req.query.tourdateid === 113){
    db.date_studios.findAll({where: {
      tour_date_id: req.query.tourdateid
    },
    order: [[{model: db.tbl_studios, as: 'studio'}, 'name', 'ASC']],
    include: [
      {
        model: db.tbl_registrations_routines, as: 'routines', include: [
          {model: db.tbl_routines, as: 'routine'},
          {
            model: db.tbl_performance_divisions,
            as: 'performance_division'
          },
          {
            model: db.tbl_routine_categories,
            as: 'routine_category',
            attributes: ['category_id', 'fee_per_dancer'],
            include: [
              {model: db.tbl_categories, as: 'category', attributes: ['name']}
            ]
          },
          {
            model: db.tbl_age_divisions,
            as: 'age_division',
            attributes: ['level_id', 'range', 'minimum_age', 'recommended_age'],
            include: [
              {model: db.tbl_levels, as: 'level', attributes: ['name']}
            ]
          },
          {
            model: db.registrations_routines_dancers, as: 'dancers', include: [
              {
                model: db.tbl_dancers, as: 'dancer', include: [
                  {
                    model: db.tbl_persons, as: 'person', include: [
                      {model: db.tbl_gender, as: 'gender'}
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {model: db.tbl_studios, as: 'studio', where: {name: { $ne : '[N/A]'} }, attributes: ['id', 'name', 'code'], include: [
        {
          model: db.tbl_addresses, as: 'address', include: [
            {
              model: db.tbl_states, as: 'state'
            }
          ]
        }
      ]}
    ]
    }).then(function (data) {
      //
      //  need to get hmtime
      //  come back to it later on after competition schedule is done
      //
      programInfoData = JSON.parse(JSON.stringify(data));
      res.render('reports/program-info',
        {
          programInfoData: programInfoData
        });
    }).catch(function (err) {
    res.status(400).send({
      message: "There was an error: " + err
    })
  });
  }
  else{
    db.tbl_registrations.findAll({where: {
      tour_date_id: req.query.tourdateid,
      independent: false
    },
    order: [[{model: db.tbl_studios, as: 'studio'}, 'name', 'ASC']],
    include: [
      {
        model: db.tbl_registrations_routines, as: 'routines', include: [
          {model: db.tbl_routines, as: 'routine'},
          {
            model: db.tbl_performance_divisions,
            as: 'performance_division'
          },
          {
            model: db.tbl_routine_categories,
            as: 'routine_category',
            attributes: ['category_id', 'fee_per_dancer'],
            include: [
              {model: db.tbl_categories, as: 'category', attributes: ['name']}
            ]
          },
          {
            model: db.tbl_age_divisions,
            as: 'age_division',
            attributes: ['level_id', 'range', 'minimum_age', 'recommended_age'],
            include: [
              {model: db.tbl_levels, as: 'level', attributes: ['name']}
            ]
          },
          {
            model: db.registrations_routines_dancers, as: 'dancers', include: [
              {
                model: db.tbl_dancers, as: 'dancer', include: [
                  {
                    model: db.tbl_persons, as: 'person', include: [
                      {model: db.tbl_gender, as: 'gender'}
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], include: [
        {
          model: db.tbl_addresses, as: 'address', include: [
            {
              model: db.tbl_states, as: 'state'
            }
          ]
        }
      ]}
    ]
    }).then(function (data) {
      programInfoData = JSON.parse(JSON.stringify(data));
      //
      //  need to get hmtime
      //  come back to it later on after competition schedule is done
      //
      res.render('reports/program-info',
        {
          programInfoData: programInfoData
        });
    }).catch(function (err) {
    res.status(400).send({
      message: "There was an error: " + err
    })
  });
  }
});

router.get('/critique-sheet', critiqueSheet.route);

router.get('/mybtf-missing-music', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/missing-music', missingMusicData);
});

router.get('/mailing-labels/:type', mailingLabels.renderReport);

router.get('/studio-emails', studioEmails.studioEmailsSpreadsheet);
router.get('/award-label', awardLabel.awardLabelSpreadsheet);
router.get('/studio-contacts', studioContacts.studioContactSpreadsheet);

router.get('/hotel-comps', function(req, res) {
  //SELECT tbl_date_studios.studioid, tbl_date_studios.studiocode, tbl_studios.name AS studioname,
  // tbl_date_studios.independent, tbl_date_studios.free_teacher_value, tbl_date_studios.total_fees, tbl_studios.contacts FROM `tbl_date_studios`
  // LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid
  // ORDER BY tbl_date_studios.total_fees DESC
  var tasks = {
    studiosInfo : function (callback) {
      db.date_studios.findAll({where: {
        tour_date_id: req.query.tourdateid
      },
      order: [[{model: db.tbl_registrations, as: 'registrations'}, 'fees', 'DESC']],
      include: [
          {
            model: db.tbl_tour_dates,
            as: 'tour_date',
            attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
            include: [
              {
                model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name']
              },
              {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
            ]
          },
          {
            model: db.tbl_registrations, as: 'registrations',  attributes:['independent', 'free_teacher', 'fees'], include: [
              {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], include: [
                { model: db.tbl_mybtf_users, as: 'users', include: [
                        { model: db.tbl_persons, as: 'person' }
                ]},
              ]},
              {
                model: db.tbl_registrations_routines, as: 'routines', include: [
                  {model: db.tbl_routines, as: 'routine'},
                  {
                    model: db.tbl_performance_divisions,
                    as: 'performance_division'
                  },
                  {
                    model: db.tbl_routine_categories,
                    as: 'routine_category',
                    attributes: ['category_id', 'fee_per_dancer'],
                    include: [
                      {model: db.tbl_categories, as: 'category', attributes: ['name']}
                    ]
                  },
                  {
                    model: db.tbl_age_divisions,
                    as: 'age_division',
                    attributes: ['level_id', 'range', 'minimum_age', 'recommended_age'],
                    include: [
                      {model: db.tbl_levels, as: 'level', attributes: ['name']}
                    ]
                  },
                  {
                    model: db.registrations_routines_dancers, as: 'dancers', include: [
                      {
                        model: db.tbl_dancers, as: 'dancer', include: [
                          {
                            model: db.tbl_persons, as: 'person', include: [
                              {model: db.tbl_gender, as: 'gender'}
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
      ]
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    },
    routnesCount : function(callback){
      db.date_routines.count({
        where : {
          tour_date_id : req.query.tourdateid,
          studio_id : req.query.studioid
        }
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    },
    dancersCount : function(callback){
      db.date_dancers.count({
        where : {
          tour_date_id : req.query.tourdateid,
          studio_id : req.query.studioid
        }
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    }
  };
    async.parallel(tasks, function (err, results) {
      hotelCompsData = JSON.parse(JSON.stringify(results.studiosInfo));
      var countOfStudios = 0;
      _.forEach(hotelCompsData, function(items){
        _.forEach(items.registrations, function(registration){
          //count of studios
          countOfStudios += 1;
          //count dispfee for each studios
          registration.dispfee = parseFloat(registration.fees.total-registration.free_teacher.value).toFixed(2);
          //get dancers count where studioid= req.query.studioid and tourdateid = req.query.studioid
          //get routines count where studioid= req.query.studioid and tourdateid = req.query.studioid
          registration.sum_dancers = JSON.parse(JSON.stringify(results.dancersCount));
          registration.sum_routines = JSON.parse(JSON.stringify(results.routnesCount));
          registration.contact_full = registration.studio.users[0].person.fname + " " + registration.studio.users[0].person.lname;
        });
      });
      if (err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        })
      }
      res.render('reports/hotel-comps',
          {
            city:hotelCompsData[0].tour_date.event_city.name,
            venue:hotelCompsData[0].tour_date.venue.name,
            disp_date:Util.tourDateDisplayDate(hotelCompsData[0].tour_date),
            hotelCompsData:hotelCompsData,
            count : { countOfStudios }
          });
    });
  });

router.get('/hotel-comps-xls', function(req, res){
    //SELECT tbl_date_studios.studioid, tbl_date_studios.studiocode, tbl_studios.name AS studioname,
  // tbl_date_studios.independent, tbl_date_studios.free_teacher_value, tbl_date_studios.total_fees, tbl_studios.contacts FROM `tbl_date_studios`
  // LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid
  // ORDER BY tbl_date_studios.total_fees DESC
    var tasks = {
    studiosInfo : function (callback) {
      db.date_studios.findAll({where: {
        tour_date_id: req.query.tourdateid
      },
      order: [[{model: db.tbl_registrations, as: 'registrations'}, 'fees', 'DESC']],
      include: [
          {
            model: db.tbl_tour_dates,
            as: 'tour_date',
            attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
            include: [
              {
                model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name']
              },
              {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
            ]
          },
          {
            model: db.tbl_registrations, as: 'registrations',  attributes:['independent', 'free_teacher', 'fees'], include: [
              {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], include: [
                { model: db.tbl_mybtf_users, as: 'users', attributes: ['id', 'email'], include: [
                  { model: db.tbl_persons, as: 'person', include: [
                    {
                      model: db.tbl_contact_info, as: 'phone', include: [
                        {model: db.tbl_contact_types, as: 'contact_type'}
                      ]
                    },
                    {
                      model: db.tbl_contact_info, as: 'email', include: [
                        {model: db.tbl_contact_types, as: 'contact_type'}
                      ]
                    },
                    {
                      model: db.tbl_addresses, as: 'address', include: [
                        {model: db.tbl_states, as: 'state'},
                        {model: db.tbl_countries, as: 'country'}
                      ]
                    }
                    ]
                  }
                ]},
              ]},
              {
                model: db.tbl_registrations_routines, as: 'routines', include: [
                  {model: db.tbl_routines, as: 'routine'},
                  {
                    model: db.tbl_performance_divisions,
                    as: 'performance_division'
                  },
                  {
                    model: db.tbl_routine_categories,
                    as: 'routine_category',
                    attributes: ['category_id', 'fee_per_dancer'],
                    include: [
                      {model: db.tbl_categories, as: 'category', attributes: ['name']}
                    ]
                  },
                  {
                    model: db.tbl_age_divisions,
                    as: 'age_division',
                    attributes: ['level_id', 'range', 'minimum_age', 'recommended_age'],
                    include: [
                      {model: db.tbl_levels, as: 'level', attributes: ['name']}
                    ]
                  },
                  {
                    model: db.registrations_routines_dancers, as: 'dancers', include: [
                      {
                        model: db.tbl_dancers, as: 'dancer', include: [
                          {
                            model: db.tbl_persons, as: 'person', include: [
                              {model: db.tbl_gender, as: 'gender'}
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
      ]
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    },
    routnesCount : function(callback){
      db.date_routines.count({
        where : {
          tour_date_id : req.query.tourdateid,
          studio_id : req.query.studioid
        }
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    },
    dancersCount : function(callback){
      db.date_dancers.count({
        where : {
          tour_date_id : req.query.tourdateid,
          studio_id : req.query.studioid
        }
      }).then(function (data) {
        callback(null, data);
      }).catch(function (err) {
        callback(err);
      })
    }
  };
  async.parallel(tasks, function (err, results) {
    //get data
      hotelCompsXlsData = JSON.parse(JSON.stringify(results.studiosInfo));
      _.forEach(hotelCompsXlsData, function(items){
        _.forEach(items.registrations, function(registration){
          //count dispfee for each studios
          registration.dispfee = parseFloat(registration.fees.total-registration.free_teacher.value).toFixed(2);
          //get dancers count where studioid= req.query.studioid and tourdateid = req.query.studioid
          //get routines count where studioid= req.query.studioid and tourdateid = req.query.studioid
          registration.sum_dancers = JSON.parse(JSON.stringify(results.dancersCount));
          registration.sum_routines = JSON.parse(JSON.stringify(results.routnesCount));
          registration.contact_full = registration.studio.users[0].person.fname + " " + registration.studio.users[0].person.lname;
          registration.studio.email = registration.studio.users[0].email;
        });
      });
      var safecity = hotelCompsXlsData[0].tour_date.event_city.name.toLowerCase();
      if (err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        })
      }
      var workbook = new Excel.Workbook();
      workbook.views = [
        {
          firstSheet: 0, activeTab: 1
        }
      ];
      var worksheet = workbook.addWorksheet('Worksheet');
      worksheet.columns = [
        { header: 'Studio', id: 'Studio', width : 20, style: { font: {name: 'Arial' } } },
        { header: 'Code', id: 'Code', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Indpt.', id: 'Indpt', width : 6, style: { font: {name: 'Arial' } } },
        { header: 'Contact', id: 'Contact', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Dancers', id: 'Dancers', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Routines', id: 'Routines', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Email', id: 'Email', width : 15, style: { font: {name: 'Arial' } } },
        { header: 'Total Fees', id: 'Total Fees', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Phone1', id: 'Phone1', width : 15, style: { font: {name: 'Arial' } } },
        { header: 'Phone2', id: 'Phone2', width : 15, style: { font: {name: 'Arial' } } },
        { header: 'Address', id: 'Address', width : 20, style: { font: {name: 'Arial' } } },
        { header: 'City', id: 'City', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'State', id: 'State', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'ZIP', id: 'ZIP', width : 10, style: { font: {name: 'Arial' } } },
        { header: 'Country', id: 'Country', width : 10, style: { font: {name: 'Arial' } } }
      ];
      _.forEach(hotelCompsXlsData, function(items){
        _.forEach(items.registrations, function(registration){
          worksheet.addRow([registration.studio.name, registration.studio.code, registration.independent ? "Yes" : "No", registration.contact_full, registration.sum_dancers, registration.sum_routines, registration.studio.email, registration.dispfee, registration.studio.users[0].person.phone[0].value, registration.studio.users[0].person.phone[1].value, registration.studio.users[0].person.address.address, registration.studio.users[0].person.address.city, registration.studio.users[0].person.address.state.abbr, registration.studio.users[0].person.address.zip, registration.studio.users[0].person.address.country.name]);
        });
      });
      //$mk = time();
      //substr($mk,6,5)
      //start
      var time = new Date();
      time = time.getTime().toString();
      time = time.substring(6,3);
      //end
      var filename = __dirname + '/'+safecity + '_hotelcomps_' + req.query.tourdateid + time +'.xls';
      workbook.xlsx.writeFile(filename)
            .then(function() {
                res.download(filename, safecity + '_hotelcomps_' + req.query.tourdateid + time +'.xls', function(err) {
                    if(!err) fs.unlink(filename);
                });
            });
      });
});

router.get('/wristband-bag-label-xls', function(req, res){
  //SELECT tbl_date_studios.id AS datestudioid, tbl_studios.name AS studioname, tbl_studios.email, tbl_studios.contacts FROM `tbl_date_studios`
  //LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid
  //AND tbl_date_studios.independent=0 ORDER BY tbl_studios.name ASC
  db.date_studios.findAll({where: {
      tour_date_id: req.query.tourdateid
    },
    include: [
      {
        model: db.tbl_tour_dates,
        as: 'tour_date',
        attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
        include: [
          {
            model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name']
          },
          {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
        ]
      },
      {
        model: db.tbl_registrations, as: 'registrations',  attributes:['independent', 'free_teacher', 'fees'], where:{ independent: false }, include: [
          {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], include: [
            { model: db.tbl_mybtf_users, as: 'users', attributes: ['id', 'email'], include: [
              { model: db.tbl_persons, as: 'person'}
            ]},
          ]}
        ]
      },
  ]
  }).then(function(data){
    wristbandBagLabelXlsData = JSON.parse(JSON.stringify(data));
    //get city safw
    var safecity = wristbandBagLabelXlsData[0].tour_date.event_city.name.toLowerCase();
    _.forEach(wristbandBagLabelXlsData, function(items){
      _.forEach(items.registrations, function(registration){
        //logic for create contact full name
        var arrLength = registration.studio.users.length;
        registration.contact_full = "";
        _.forEach(registration.studio.users, function(contact){
          if(arrLength > 1){
            registration.contact_full = contact.person.fname + " " + contact.person.lname + ", ";
          }
          else{
            registration.contact_full = contact.person.fname + " " + contact.person.lname;
          }
          arrLength--;
        })
        registration.studio.email = registration.studio.users[0].email;
      });
    });
    var workbook = new Excel.Workbook();
    workbook.views = [
      {
        firstSheet: 0, activeTab: 1
      }
    ];

    var worksheet = workbook.addWorksheet('Worksheet');

    worksheet.columns = [
      { header: 'Studio', id: 'name', width : 20, style: { font: {name: 'Arial' } } },
      { header: 'Contacts', id: 'contact', width : 15, style: { font: {name: 'Arial' } } },
      { header: 'Email', id: 'email', width : 25, style: { font: {name: 'Arial' } } }
    ];
    _.forEach(wristbandBagLabelXlsData, function(items){
      _.forEach(items.registrations, function(registration){
        worksheet.addRow([registration.studio.name, registration.contact_full, registration.studio.email]);
      });
    });
    var time = new Date();
    time = time.getTime().toString();
    time = time.substring(6,3);
    var filename = __dirname + '/'+safecity + '_wristbands' + req.query.tourdateid + time +'.xls';
    workbook.xlsx.writeFile(filename)
          .then(function() {
              res.download(filename, safecity + '_wristbands' + req.query.tourdateid + time +'.xls', function(err) {
                  if(!err) fs.unlink(filename);
              });
          });
  }).catch(function(err){
    res.status(400).send({
      message: "There was an error: " + err
    })
  });
});

router.get('/online-score-sheets', onlineScoreSheets.renderReport);

router.get('/jump-reg-packet', jumpRegPacket.renderReport);

router.get('/jump-reg-packet-all', function (req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  _.forEach(jumpRegPacketData.studios, function (studio) {
    _.forEach(studio.workshop_registrants, function (dancers) {
      _.forEach(dancers, function (d) {
        d.show_scholarship_code =
          d.workshop_level_id != 1
          && d.workshop_level_id != 6
          && d.workshop_level_id != 8
          && d.workshop_level_id != 12
          && d.one_day != 1
          && d.scholarship_code > 0;
      });
    })
  })
  res.render('reports/jump-reg-packet-all', jumpRegPacketData);
});

router.get('/jump-studio-results', jumpStudioResults.renderReport);
  // query params
//   var tourDateId = req.query.tourdateid;
//   var comp = req.query.componly;

//   if (comp) {
//     res.render('reports/jump-studio-results-comp', jumpStudioResults);
//   }
//   else {
//     res.render('reports/jump-studio-results', jumpStudioResults);
//   }
// });

router.get('/outstanding-items', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/outstanding-items', outstandingItemsData);
});

router.get('/outstanding-items/jacket', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/outstanding-items-jacket', outstandingItemsData);
});

router.get('/outstanding-items/photo', function (req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/outstanding-items-photo', outstandingItemsData);
});

router.get('/jump-score-sheets', function(req, res){
  jumpScoreSheetsData = JSON.parse(JSON.stringify(jumpScoreSheetsData));
  var tourdateid = req.query.tourdateid;
  var city = jumpScoreSheetsData.tour_date.event_city.name;
  var routines = jumpScoreSheetsData.routines;
  var countOfRoutines = routines.length;
  res.render('reports/jump-score-sheets',
    {
      routines : routines,
      city : city,
      count : { countOfRoutines }
    });
});

router.get('/studio-paid-dancers', studioPaidDancers.renderReport);

router.get('/jump-score-sheets-blank', function(req, res){
  //I miss this code because it doesn`t use at the html template
  //
  //<?php
  //include("../../../includes/util.php");
  //$tourdateid = intval($_GET["tourdateid"]);
  //$compgroup = mysql_real_escape_string($_GET["compgroup"]);
  //$compgroup = "finals";
  //$city = db_one("city","tbl_tour_dates","id=$tourdateid");
  //?>

  res.render('reports/jump-score-sheets-blank');
});

router.get('/jump-studio-list', function(req, res){
  //SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode,
  //tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios`
  //LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid
  //WHERE tourdateid=$tourdateid AND tbl_date_studios.independent=0 AND tbl_studios.name!='[N/A]'
  //ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC
  var sequelize = Sequelize.sequelize;
  var db = Sequelize.models;
var tasks = {
    tour_date_info : function(callback){
      db.tbl_tour_dates.findAll({where :{
        id : req.query.tourdateid
      },
      limit: 1,
      include : [
        {model : db.tbl_event_cities, as: 'event_city', attributes: ['name']},
        {model : db.tbl_venues, as: 'venue', attributes: ['name']}
      ]
      }).then(function (data){
        callback(null, data);
      }).catch(function (err){
        callback(err);
      });
    },
    //NON-IND'T
    non_independent : function (callback) {
    db.date_studios.findAll({where: {
      tour_date_id: req.query.tourdateid
    },
    order: [[{model: db.tbl_registrations, as: 'registrations'}, 'independent', 'ASC']],
    include: [
      {
        model: db.tbl_registrations, as: 'registrations',  attributes:['id', 'independent', 'free_teacher', 'fees'], include: [
          {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], include: [
            { model: db.tbl_mybtf_users, as: 'users', attributes: ['id', 'email'], include: [
              { model: db.tbl_persons, as: 'person'}
            ]},
          ]}
        ]
      },
    ]
    }).then(function (data) {
      data = JSON.parse(JSON.stringify(data));
        //SELECT COUNT(dd.id), trd.workshop_level_id, tbl_levels.name FROM date_dancers as dd
        //LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id
        //left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id
        //left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;
        _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            sequelize.query('SELECT COUNT(date_routines.id) FROM date_routines LEFT JOIN tbl_studios ON tbl_studios.id = date_routines.studio_id AND tour_date_id ='+ req.query.tourdateid +';').then(function(routine_data){
                  registration.studio.routine_count = routine_data[0][0].count ? routine_data[0][0].count : 0;
                });
            });
          });
        sequelize.query('SELECT COUNT(dd.id), tbl_levels.name FROM date_dancers as dd LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;').then(function(count_data){
          count_data = JSON.parse(JSON.stringify(count_data));
          _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            registration.studio.count = {};
            registration.studio.packet_count = null;
            registration.studio.code = registration.studio.code != null ? registration.studio.code : "-";
            registration.studio.independent = registration.independent == 1 ? "Yes" : "No";
            registration.studio.contact_full = registration.studio.users[0].person.fname + " " + registration.studio.users[0].person.lname;

            _.forEach(count_data[0], function (object) {
              registration.studio.count[object.name] = object.count;
              registration.studio.packet_count += parseInt(object.count);
            });
              registration.studio.packet_count += 1;
            });
          });
          callback(null, data);
    }).catch(function (err) {
        callback(err);
    });
  });
},
  //IND'T NA
  //SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent, tbl_date_studios.studiocode,
  //tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios`
  //LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.independent=1
  //AND tourdateid=$tourdateid AND tbl_studios.name='[N/A]' ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC
  independent_na : function (callback) {
      db.date_studios.findAll({where: {
      tour_date_id: req.query.tourdateid
    },
    order: [[{model: db.tbl_registrations, as: 'registrations'}, 'independent', 'ASC']],
    include: [
      {
        model: db.tbl_tour_dates,
        as: 'tour_date',
        attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
        include: [
          {
            model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name']
          },
          {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
        ]
      },
      {
        model: db.tbl_registrations, as: 'registrations',  attributes:['independent', 'free_teacher', 'fees'], where:{ independent: true }, include: [
          {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], where: { name : '[N/A]' }, include: [
            { model: db.tbl_mybtf_users, as: 'users', attributes: ['id', 'email'], include: [
              { model: db.tbl_persons, as: 'person'}
            ]},
          ]}
        ]
      },
    ]
    }).then(function (data) {
            data = JSON.parse(JSON.stringify(data));
        //SELECT COUNT(dd.id), trd.workshop_level_id, tbl_levels.name FROM date_dancers as dd
        //LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id
        //left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id
        //left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;
        _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            sequelize.query('SELECT COUNT(date_routines.id) FROM date_routines LEFT JOIN tbl_studios ON tbl_studios.id = date_routines.studio_id AND tour_date_id ='+ req.query.tourdateid +';').then(function(routine_data){
                  registration.studio.routine_count = routine_data[0][0].count ? routine_data[0][0].count : 0;
                });
            });
          });
        sequelize.query('SELECT COUNT(dd.id), tbl_levels.name FROM date_dancers as dd LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;').then(function(count_data){
          count_data = JSON.parse(JSON.stringify(count_data));
          _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            registration.studio.count = {};
            registration.studio.packet_count = null;
            registration.studio.code = registration.studio.code != null ? registration.studio.code : "-";
            registration.studio.independent = registration.independent == 1 ? "Yes" : "No";
            registration.studio.contact_full = registration.studio.users[0].person.fname + " " + registration.studio.users[0].person.lname;

            _.forEach(count_data[0], function (object) {
              registration.studio.count[object.name] = object.count;
              registration.studio.packet_count += parseInt(object.count);
            });
            });
          });
          callback(null, data);
          }
    ).catch(function (err) {
        callback(err);
    })
  }
  )},
  //IND'T NON-NA
  //SELECT tbl_date_studios.id AS datestudioid, tbl_date_studios.studioid, tbl_date_studios.independent,
  //tbl_date_studios.studiocode, tbl_studios.name AS studioname, tbl_studios.contacts FROM `tbl_date_studios`
  //LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tourdateid=$tourdateid
  //AND tbl_date_studios.independent=1 AND tbl_studios.name!='[N/A]' ORDER BY tbl_date_studios.independent ASC, tbl_studios.name ASC
  independent_non_na : function (callback) {
      db.date_studios.findAll({where: {
        tour_date_id: req.query.tourdateid
      },
      order: [[{model: db.tbl_registrations, as: 'registrations'}, 'independent', 'ASC']],
      include: [
        {
          model: db.tbl_tour_dates,
          as: 'tour_date',
          attributes: ['id', 'event_id', 'event_city_id', 'season_id', 'start_date', 'end_date'],
          include: [
            {
              model: db.tbl_event_cities, as: 'event_city', attributes: ['id', 'name']
            },
            {model: db.tbl_venues, as: 'venue', attributes: ['id', 'name']}
          ]
        },
        {
          model: db.tbl_registrations, as: 'registrations',  attributes:['independent', 'free_teacher', 'fees'], where:{ independent: true }, include: [
            {model: db.tbl_studios, as: 'studio', attributes: ['id', 'name', 'code'], where: { name : { $ne : '[N/A]'} }, include: [
              { model: db.tbl_mybtf_users, as: 'users', attributes: ['id', 'email'], include: [
                { model: db.tbl_persons, as: 'person'}
              ]},
            ]}
          ]
        },
      ]
      }).then(function (data) {
              data = JSON.parse(JSON.stringify(data));
        //SELECT COUNT(dd.id), trd.workshop_level_id, tbl_levels.name FROM date_dancers as dd
        //LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id
        //left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id
        //left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;
        _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            sequelize.query('SELECT COUNT(date_routines.id) FROM date_routines LEFT JOIN tbl_studios ON tbl_studios.id = date_routines.studio_id AND tour_date_id ='+ req.query.tourdateid +';').then(function(routine_data){
                  registration.studio.routine_count = routine_data[0][0].count ? routine_data[0][0].count : 0;
                });
            });
          });
        sequelize.query('SELECT COUNT(dd.id), tbl_levels.name FROM date_dancers as dd LEFT JOIN tbl_registrations_dancers as trd ON dd.registrations_dancer_id = trd.id left join tbl_workshop_levels on tbl_workshop_levels.id=trd.workshop_level_id left join tbl_levels on tbl_levels.id=tbl_workshop_levels.level_id GROUP BY trd.workshop_level_id, tbl_levels.name;').then(function(count_data){
          count_data = JSON.parse(JSON.stringify(count_data));
          _.forEach(data, function(items){
            _.forEach(items.registrations, function(registration){
            registration.studio.count = {};
            registration.studio.packet_count = null;
            registration.studio.code = registration.studio.code != null ? registration.studio.code : "-";
            registration.studio.independent = registration.independent == 1 ? "Yes" : "No";
            registration.studio.contact_full = registration.studio.users[0].person.fname + " " + registration.studio.users[0].person.lname;

            _.forEach(count_data[0], function (object) {
              registration.studio.count[object.name] = object.count;
              registration.studio.packet_count += parseInt(object.count);
            });
            });
          });
          callback(null, data);
          }
      ).catch(function (err) {
          callback(err);
      });
  }
)}
};
  async.parallel(tasks, function (err, results) {
    var jumpStudioTourDateInfo = JSON.parse(JSON.stringify(results.tour_date_info));
    var jumpStudioData1 = JSON.parse(JSON.stringify(results.non_independent));
    var jumpStudioData2 = JSON.parse(JSON.stringify(results.independent_non_na));
    var jumpStudioData3 = JSON.parse(JSON.stringify(results.independent_na));
    var tourdateid = req.query.tourdateid;
    var city = jumpStudioTourDateInfo[0].event_city.name;
    var venue = jumpStudioTourDateInfo[0].venue.name;
    var display_date = Util.tourDateDisplayDate(jumpStudioTourDateInfo[0]);
    var studios = [];
    var tpc = 0; //total packet count
    _.forEach(jumpStudioData1, function(items){
      _.forEach(items.registrations, function(registration){
        tpc += registration.studio.packet_count;
        studios.push(registration);
      });
    });
    _.forEach(jumpStudioData2, function(items){
      _.forEach(items.registrations, function(registration){
        tpc += registration.studio.packet_count;
        studios.push(registration);
      });
    });
    _.forEach(jumpStudioData3, function(items){
      _.forEach(items.registrations, function(registration){
        tpc += registration.studio.packet_count;
        studios.push(registration);
      });
    });
    var total = 0, tsec = 0, tmin = 0, thour = 0, trem = 0;
    if(tpc > 0){
      tsec = tpc * 43;
      tmin = Math.ceil(tsec / 60);
      thour = Math.floor(tmin / 60);
      trem = Util.str_pad(tmin-(thour * 60), 2, "0", "STR_PAD_LEFT");
      total = thour + ":" + trem;
    }
    if (err) {
        res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        })
      }
    res.render('reports/jump-studio-list',
      {
        data : {
                  city,
                  venue,
                  display_date,
                  tpc,
                  total
               },
        studios : studios
      });
  });
  });


router.get('/owes-refunds', owesRefunds.renderReport);

router.get('/scholarship-labels', function(req, res){
  //SELECT tbl_date_dancers.scholarship_code, tbl_date_dancers.id as datedancerid,
  //tbl_date_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_studios.name as studioname
  //FROM `tbl_date_dancers` LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid
  //LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid WHERE tbl_date_dancers.tourdateid = $tourdateid
  //AND tbl_date_dancers.scholarship_code > 0 ORDER BY tbl_studios.name, tbl_date_dancers.scholarship_code
  db.date_dancers.findAll({where:{
    tour_date_id : req.query.tourdateid,
    scholarship_code : { $gt : 0 }
  },
  order: [[{model: db.tbl_studios, as: 'studio'}, 'name', 'ASC'], ['scholarship_code', 'ASC']],
  include : [
      {
        model: db.tbl_dancers, as: 'dancer', include: [
          { model: db.tbl_persons, as: 'person'}
        ]
      },
      {
        model: db.tbl_studios, as: 'studio', attributes: ['name']
      }
  ]
  }).then(function(data){
    scholarshipLabelsData = JSON.parse(JSON.stringify(data));
    _.forEach(scholarshipLabelsData, function(items){
      items.scholarship_code = items.scholarship_code.toString();
    });
    var dancers = scholarshipLabelsData;
    res.render('reports/scholarship-labels', { dancers });
  }).catch(function(err){
    res.status(400).send({
      message: "There was an error: " + err
    })
  });
});

router.get('/scholarship-labels-new', function(req, res){
  //SELECT tbl_date_dancers.scholarship_code,
  //    tbl_date_dancers.age,
  //    tbl_date_dancers.id as datedancerid,
  //    tbl_date_dancers.profileid, tbl_profiles.fname,
  //    tbl_profiles.lname,
  //    tbl_studios.name as studioname
  //    FROM `tbl_date_dancers`
  //    LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid
  //    LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid WHERE tbl_date_dancers.tourdateid = $tourdateid
  //    AND tbl_date_dancers.scholarship_code > 0
  //    ORDER BY tbl_studios.name, tbl_date_dancers.scholarship_code
  db.date_dancers.findAll({where:{
    tour_date_id : req.query.tourdateid,
    scholarship_code : { $gt : 0 }
  },
  order: [[{model: db.tbl_studios, as: 'studio'}, 'name', 'ASC'], ['scholarship_code', 'ASC']],
  include : [
      {
        model: db.tbl_dancers, as: 'dancer', include: [
          { model: db.tbl_persons, as: 'person'}
        ]
      },
      {
        model: db.tbl_studios, as: 'studio', attributes: ['name']
      }
  ]
  }).then(function(data){
    scholarshipLabelsNewData = JSON.parse(JSON.stringify(data));
    var tourdateid = req.query.tourdateid;
    var eventid = parseInt(req.query.eventid);
    var tour = null;
    if(eventid === 7) tour = "jump";
    if(eventid === 8) tour = "nuvo";
    if(eventid === 18) tour = "24";
    _.forEach(scholarshipLabelsNewData, function(dancer){
      dancer.scholarship_code = dancer.scholarship_code.toString();
      dancer.dancer.age = Util.getAge(Util.formatBirthdate(dancer.dancer.person.birthdate));
        if(dancer.dancer.age <= 10){ dancer.dancer.age_div = 'Mini'; }
        if(dancer.dancer.age >= 11 && dancer.dancer.age <= 12){ dancer.dancer.age_div = 'Junior'; }
        if(dancer.dancer.age >= 13 && dancer.dancer.age <= 15){ dancer.dancer.age_div = 'Teen'; }
        if(dancer.dancer.age >= 16) { dancer.dancer.age_div = 'Senior'; }
          if(dancer.dancer.person.fname.length <= 8){ dancer.dancer.person.f_length = 'short'; }
          if(dancer.dancer.person.fname.length > 8 && dancer.dancer.person.fname.length <= 12){ dancer.dancer.person.f_length = 'long'; }
          if(dancer.dancer.person.fname.length > 12){ dancer.dancer.person.f_length = 'very-long'; }
    });
    var pageWidth = 11;
    var pageHeight = 8.5;
    var pageMargin = 0.375;

    res.render('reports/scholarship-labels-new',
      {
        data : {tour, tourdateid, eventid},
        dancers : scholarshipLabelsNewData
      });
  }).catch(function(err){
      res.status(400).send({
        message: "There was an error: " + err
      })
    });
});

router.get('/scholarship-labels-new/custom', function(req, res){
  var tourdateid = req.query.tourdateid;
  var eventid = parseInt(req.query.eventid);
  var tour = null;
    if(eventid === 7) tour = "jump";
    if(eventid === 8) tour = "nuvo";
    if(eventid === 18) tour = "24";
  var dancers = [];
  dancers = [{
    scholarship_code: req.query.num,
    dancer : {
      age: req.query.age,
      studio : {name: req.query.studio},
      person : {
        fname: req.query.fname,
        lname: req.query.lname
      }
    }
  }];
  dancers = JSON.parse(JSON.stringify(dancers));
  _.forEach(dancers, function(dancer){
      if(dancer.dancer.age <= 10){ dancer.dancer.age_div = 'Mini'; }
      if(dancer.dancer.age >= 11 && dancer.dancer.age <= 12){ dancer.dancer.age_div = 'Junior'; }
      if(dancer.dancer.age >= 13 && dancer.dancer.age <= 15){ dancer.dancer.age_div = 'Teen'; }
      if(dancer.dancer.age >= 16) { dancer.dancer.age_div = 'Senior'; }
        if(dancer.dancer.person.fname.length <= 8){ dancer.dancer.person.f_length = 'short'; }
        if(dancer.dancer.person.fname.length > 8 && dancer.dancer.person.fname.length <= 12){ dancer.dancer.person.f_length = 'long'; }
        if(dancer.dancer.person.fname.length > 12){ dancer.dancer.person.f_length = 'very-long'; }
  });
  var pageWidth = 11;
  var pageHeight = 8.5;
  var pageMargin = 0.375;

  res.render('reports/scholarship-labels-new',
    {
      data : {tour, tourdateid, eventid},
      dancers : dancers
    });
  });

router.get('/studio-emails', studioEmails.studioEmailsSpreadsheet);
router.get('/bd-jackets-xls', bdJackets.bdJacketSpreadsheet);

router.get('/best-dancer-list', bestDancerList.renderReport);

router.get('/scholarship-labels-bd-all', function(req, res){
  scholarshipLabelsBdAllData = JSON.parse(JSON.stringify(scholarshipLabelsBdAllData));
  var tourdateid = req.query.tourdateid;
  var dancers = scholarshipLabelsBdAllData.dancers;
  var city = scholarshipLabelsBdAllData.tour_date.event_city.name;
  res.render('reports/scholarship-labels-bd-all',
  {
    dancers : dancers,
    city : city
  });
});

router.get('/scholarship-labels-bd-mj', function(req, res){
  scholarshipLabelsBdMjData = JSON.parse(JSON.stringify(scholarshipLabelsBdMjData));
  var tourdateid = req.query.tourdateid;
  var dancers = scholarshipLabelsBdMjData.dancers;
  res.render('reports/scholarship-labels-bd-mj',
  {
    dancers : dancers
  });
});

router.get('/scholarship-labels-bd-ts', function(req, res){
  scholarshipLabelsBdTsData = JSON.parse(JSON.stringify(scholarshipLabelsBdTsData));
  var tourdateid = req.query.tourdateid;
  var dancers = scholarshipLabelsBdTsData.dancers;
  res.render('reports/scholarship-labels-bd-ts',
  {
    dancers : dancers
  });
});

router.get('/scholarship-list-by-name', function(req, res){
  scholarshipListByNameData = JSON.parse(JSON.stringify(scholarshipListByNameData));
  var tourdateid = req.query.tourdateid;
  var city = scholarshipListByNameData.tour_date.event_city.name;
  var venue = scholarshipListByNameData.tour_date.venue.name;
  var start_date = scholarshipListByNameData.tour_date.start_date;
  res.render('reports/scholarship-list-by-name',
    {
      data : { city, venue, start_date},
      dancers : scholarshipListByNameData.dancers
    });
});

router.get('/petites', function(req, res){
  petitesData = JSON.parse(JSON.stringify(petitesData));
  var tourdateid = req.query.tourdateid;
  var disp_date = Util.tourDateDisplayDate(petitesData.tour_date);
  var city = petitesData.tour_date.event_city.name;
  var venue = petitesData.tour_date.venue.name;
  var eventid = null;
  var astr2 = null;
  if(tourdateid > 0){
    eventid = 7;
    if(eventid == 7){
      astr2 = "JUMPstart";
    }
    if(eventid == 8){
      astr2 = "Nubie";
    }
    if(eventid == 18){
      astr2 = "Sidekick";
    }
    if(eventid == 14){
      astr2 = "PeeWee";
    }
    if(eventid == 28){
      astr2 = "Rookie";
    }
  }
  res.render('reports/petites',
    {
      dancers : petitesData.dancers,
      data : { city, venue, disp_date, astr2 }
    });
});

router.get('/petites-xls', function(req, res){
  petitesXlsData = JSON.parse(JSON.stringify(petitesXlsData));
  var tourdateid = req.query.tourdateid;
  var city = petitesXlsData.tour_date.event_city.name;
  var eventid = null;
  var astr2 = null;
  if(tourdateid > 0){
    eventid = 7;
    if(eventid == 7){
      astr2 = "JUMPstart";
    }
    if(eventid == 8){
      astr2 = "Nubie";
    }
    if(eventid == 18){
      astr2 = "Sidekick";
    }
    if(eventid == 14){
      astr2 = "PeeWee";
    }
    if(eventid == 28){
      astr2 = "Rookie";
    }
  }
  var workbook = new Excel.Workbook();
  workbook.views = [
    {
      firstSheet: 0, activeTab: 1
    }
  ];

  var worksheet = workbook.addWorksheet('Worksheet');
  worksheet.columns = [
    { header: 'FIRST', id: 'FIRST', width : 12, style: { font: {name: 'Arial' } } },
    { header: 'LAST', id: 'LAST', width : 12, style: { font: {name: 'Arial' } } },
    { header: 'AGE', id: 'AGE', width : 6, style: { font: {name: 'Arial' } } },
    { header: 'DAYS #', id: 'DAYS', width : 8, style: { font: {name: 'Arial' } } },
    { header: 'STUDIO', id: 'STUDIO', width : Util.setWidthAuto(petitesXlsData.dancers, 'studio_name'), style: { font: {name: 'Arial' } } }
  ];
  _.forEach(petitesXlsData.dancers, function(dancer){
      worksheet.addRow([dancer.person.fname, dancer.person.lname, dancer.age, dancer.one_day == 1 ? 1 : 2, dancer.studio_name]);
  });
  var time = new Date();
  time = time.getTime().toString();
  time = time.substring(6,2);
  var filename = __dirname + '/'+astr2.toLowerCase() + '_' + city.toLowerCase() + time +'.xls';
  workbook.xlsx.writeFile(filename)
        .then(function() {
            res.download(filename, astr2.toLowerCase() + '_' + city.toLowerCase() + time +'.xls', function(err) {
                if(!err) fs.unlink(filename);
            });
        });
  });

router.get('/vip-photo-browser', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/vip-photo-browser', vipPhotoData);
});

router.get('/non-competing-bd-list', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/non-competing-bd-list', nonCompetingBDList);
});

router.get('/nominations-list', function(req, res){
  nominationsListData = JSON.parse(JSON.stringify(nominationsListData));
  var tourdateid = req.query.tourdateid;

  res.render('reports/nominations-list', {
    award_nominations : nominationsListData.award_nominations,
    mini : nominationsListData.mini,
    junior : nominationsListData.junior,
    teen : nominationsListData.teen,
    senior : nominationsListData.senior,
    costume_design : nominationsListData.costume_design,
    choreographer_mj : nominationsListData.choreographer_mj,
    choreographer_ts : nominationsListData.choreographer_ts,
    people_choice : nominationsListData.people_choice
  });
});

router.get('/nominations-list-sorted', function(req, res) {
  nominationsListSortedData = JSON.parse(JSON.stringify(nominationsListSortedData));
  var tourdateid = req.query.tourdateid;
  var city = nominationsListSortedData.tour_date.event_city.name;
  var disp_date = Util.tourDateDisplayDate(nominationsListSortedData.tour_date);
  res.render('reports/nominations-list-sorted', {
    data : { city, disp_date },
    award_nominations : nominationsListSortedData.award_nominations,
    mini : nominationsListSortedData.mini,
    junior : nominationsListSortedData.junior,
    teen : nominationsListSortedData.teen,
    senior : nominationsListSortedData.senior,
    costume_design : nominationsListSortedData.costume_design,
    choreographer_mj : nominationsListSortedData.choreographer_mj,
    choreographer_ts : nominationsListSortedData.choreographer_ts,
    people_choice : nominationsListSortedData.people_choice
  });
});

router.get('/dancer-list', dancerList.renderReport);

router.get('/dancers-only', dancersOnly.renderReport);

router.get('/jump-workshop-schedule', function(req, res){
  jumpWorkshopScheduleData = JSON.parse(JSON.stringify(jumpWorkshopScheduleData));
  var cityId = parseInt(req.query.tourdateid);
  if(Number.isInteger(cityId) && cityId > 0){
    jumpWorkshopScheduleData.title = jumpWorkshopScheduleData.tour_date.event_city.name+" Workshop Schedule";
  }
  jumpWorkshopScheduleData.maxrows = 48;

  var page = 1;
  var numrows = 0;
  var currentday = null;
  res.render('reports/jump-workshop-schedule', jumpWorkshopScheduleData);
});

router.get('/adjudicated-awards', function(req, res){
  adjudicatedAwardsData = JSON.parse(JSON.stringify(adjudicatedAwardsData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  var showscores = req.query.showscores == "true" ? true : false;
  var city = adjudicatedAwardsData.tour_date.event_city.name;
  var dispdate = Util.tourDateDisplayDate(adjudicatedAwardsData.tour_date);
  res.render('reports/adjudicated-awards', {
    data : { compgroup, city, dispdate },
    showscores : showscores,
    routines : adjudicatedAwardsData.routines
  });
});

router.get('/best-of-jump', function(req, res){
  bestOfJumpData = JSON.parse(JSON.stringify(bestOfJumpData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  compgroup = "finals";
  var save =  req.query.save;
  var city = bestOfJumpData.tour_date.event_city.name;
  var start_date = bestOfJumpData.tour_date.start_date;
  res.render('reports/best-of-jump', {
    data : { city, start_date, compgroup },
    data_routines : bestOfJumpData.routines
  });
});

router.get('/jump-competition-schedule', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;
  var compGroup = req.query.comp_group;

  res.render('reports/jump-competition-schedule', jumpCompetitionScheduleData);
});

router.get('/jump-competition-schedule-video', function(req, res){
  jumpCompetitionScheduleVideoData = JSON.parse(JSON.stringify(jumpCompetitionScheduleVideoData));
  var tourdateid = req.query.tourdateid;
  var comp_group = req.query.comp_group;
  var disp_comp_group;
  if(comp_group === "finals")
    jumpCompetitionScheduleVideoData.disp_comp_group = "Finals";
  if(comp_group === "vips")
    jumpCompetitionScheduleVideoData.disp_comp_group = "VIP";
  if(comp_group === "prelims")
    jumpCompetitionScheduleVideoData.disp_comp_group = "Prelims";
  res.render('reports/jump-competition-schedule-video', jumpCompetitionScheduleVideoData);
});

router.get('/best-performances', function(req, res){
  bestPerformancesData = JSON.parse(JSON.stringify(bestPerformancesData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  compgroup = "finals";
  var save =  req.query.save;
  var city = bestPerformancesData.tour_date.event_city.name;
  var start_date = bestPerformancesData.tour_date.start_date;
  res.render('reports/best-performances', {
    data : { city, start_date, compgroup },
    data_routines : bestPerformancesData.routines
  });
});

router.get('/high-scores-age', function(req, res){
  highScoresAgeData = JSON.parse(JSON.stringify(highScoresAgeData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  compgroup = "finals";
  var save =  req.query.save;
  var city = highScoresAgeData.tour_date.event_city.name;
  var start_date = highScoresAgeData.tour_date.start_date;
  res.render('reports/high-scores-age', {
    data : { city, start_date, compgroup },
    awards : highScoresAgeData.awards
  });
});

router.get('/jump-competition-schedule-2room-1', function (req, res) {
  // query params
  var tourDateId = req.query.tourdateid;
  var compGroup = req.query.comp_group;

  res.render('reports/jump-competition-schedule-2room-1', jumpCompetitionScheduleData);
});

router.get('/jump-competition-schedule-2room-2', function (req, res) {
  // query params
  var tourDateId = req.query.tourdateid;
  var compGroup = req.query.comp_group;

  res.render('reports/jump-competition-schedule-2room-2', jumpCompetitionSchedule2Data);
});

router.get('/judges-running-order', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;
  var compGroup = req.query.comp_group;

  res.render('reports/judges-running-order', jumpCompetitionScheduleData);
});

router.get('/high-scores-combined', function(req, res){
  highScoresCombinedData = JSON.parse(JSON.stringify(highScoresCombinedData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  var save = req.query.save;
  var maxplaces = req.query.max;
  var city = highScoresAgeData.tour_date.event_city.name;
  var start_date = highScoresAgeData.tour_date.start_date;
  res.render('reports/high-scores-combined', {
    data : { city, start_date, compgroup, maxplaces },
    awards : highScoresCombinedData.awards
  });
});

router.get('/high-scores-individual', function(req, res){
  highScoresIndividualData = JSON.parse(JSON.stringify(highScoresIndividualData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  var save = req.query.save;
  var maxplaces = req.query.max;
  var city = highScoresIndividualData.tour_date.event_city.name;
  var start_date = highScoresIndividualData.tour_date.start_date;
  res.render('reports/high-scores-individual', {
    data : { city, start_date, compgroup, maxplaces },
    awards : highScoresIndividualData.awards
  });
});

router.get('/tda-high-scores-age', function(req, res){
  tdaHighScoresAgeData = JSON.parse(JSON.stringify(tdaHighScoresAgeData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  compgroup = "finals";
  var save =  req.query.save;
  var city = tdaHighScoresAgeData.tour_date.event_city.name;
  var start_date = tdaHighScoresAgeData.tour_date.start_date;
  var maxplaces = 3;
  res.render('reports/tda-high-scores-age', {
    data : { city, start_date, compgroup },
    awards : tdaHighScoresAgeData.awards
  });
});

router.get('/tda-high-scores-individual', function(req, res){
  tdaHighScoresIndividualData = JSON.parse(JSON.stringify(tdaHighScoresIndividualData));
  var tourdateid = req.query.tourdateid;
  var compgroup = req.query.compgroup;
  var save = req.query.save;
  var maxplaces = req.query.max;
  var city = tdaHighScoresIndividualData.tour_date.event_city.name;
  var start_date = tdaHighScoresIndividualData.tour_date.start_date;
  res.render('reports/tda-high-scores-individual', {
    data : { city, start_date, compgroup, maxplaces },
    awards : tdaHighScoresIndividualData.awards
  });
});

router.get('/audition-class', function(req, res){
  auditionClassData = JSON.parse(JSON.stringify(auditionClassData));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  res.render('reports/audition-class', {
    data : { eventid },
    auditionClassData : auditionClassData
  });
});

router.get('/best-dancer-tally-sheets', function(req, res){
  bestDancerTallySheetsData = JSON.parse(JSON.stringify(bestDancerTallySheetsData));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  var highestsolo = 300;
  var highestclass = 589;
  var jcount = 6;
  res.render('reports/best-dancer-tally-sheets',{
    data : { eventid, highestsolo, highestclass, jcount },
    routines : bestDancerTallySheetsData.routines
  });
});

/**
 * THIS REPORT RETURNS A XLS FILE OF A HANDLEBARS PAGE DEPENDING ON THE
 * xls=1 PARAMETER.
 */
router.get('/jump-special-awards', jumpSpecialAwards.renderReport);

router.get('/best-dancer-tally-sheets-sample', function(req, res){
  bestDancerTallySheetsSampleData = JSON.parse(JSON.stringify(bestDancerTallySheetsSampleData));
  var tourdateid = req.query.tourdateid;
  var eventid = null;
  res.render('reports/best-dancer-tally-sheets-sample',{
    data : { eventid }
  });
});

router.get('/top-round-1', function(req, res){
  topRound1Data = JSON.parse(JSON.stringify(topRound1Data));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  var venue_name = null;
  var title_date = null;
  res.render('reports/top-round-1',{
    data : { eventid, venue_name, title_date },
    topRound1Data : topRound1Data
  });
});

router.get('/top-round-2', function(req, res){
  topRound2Data = JSON.parse(JSON.stringify(topRound2Data));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  var venue_name = null;
  var title_date = null;
  res.render('reports/top-round-2',{
    data : { eventid, venue_name, title_date },
    topRound2Data : topRound2Data
  });
});

router.get('/top-round-3', function(req, res){
  topRound3Data = JSON.parse(JSON.stringify(topRound3Data));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  var venue_name = null;
  var title_date = null;
  res.render('reports/top-round-3',{
    data : { eventid, venue_name, title_date },
    topRound3Data : topRound3Data
  });
});

router.get('/jump-best-in-studio-candidates', function(req, res){
  jumpBestInStudioCandidatesData = JSON.parse(JSON.stringify(jumpBestInStudioCandidatesData));
  var tourdateid = req.query.tourdateid;
  var compgroup = "finals";
  var city = jumpBestInStudioCandidatesData.tour_date.event_city.name;
  var start_date = jumpBestInStudioCandidatesData.tour_date.start_date;

  res.render('reports/jump-best-in-studio-candidates',{
    data : { compgroup, city, start_date },
    routines : jumpBestInStudioCandidatesData.routines
  });
});

router.get('/tda-best-performances', function(req, res){
  tdaBestPerformacesData = JSON.parse(JSON.stringify(tdaBestPerformacesData));
  var tourdateid = req.query.tourdateid;
  var city = tdaBestPerformacesData.tour_date.event_city.name;
  var venue = tdaBestPerformacesData.tour_date.venue.name;
  var dispdate = Util.tourDateDisplayDate(tdaBestPerformacesData.tour_date);
  var excelonly = req.query.xls == 1 ? true : false;
  if(excelonly){
    var workbook = new Excel.Workbook();
    workbook.views = [
      {
        firstSheet: 0, activeTab: 1
      }
    ];

    var worksheet = workbook.addWorksheet('Worksheet');
    var maxwidthRoutine = 0, maxwidthStudio = 0;
    _.forEach(tdaBestPerformacesData.awards, function(winners){
      _.forEach(winners.winners, function(arow){
          if(maxwidthRoutine < arow.routinename.length){
            maxwidthRoutine = arow.routinename.length;
          }
          if(maxwidthStudio < arow.studioname.length){
            maxwidthStudio = arow.studioname.length;
          }
      });
    });
    worksheet.columns = [
      { header: 'Routine', id: 'Special Award', width :  maxwidthRoutine+4, style: { font: {name: 'Arial' } } },
      { header: 'Studio', id: 'Special Award', width : maxwidthStudio+4, style: { font: {name: 'Arial' } } },
      { header: 'Special Award', id: 'Special Award', width : Util.setWidthAuto(tdaBestPerformacesData.awards, 'specialawardname'), style: { font: {name: 'Arial' } } }
    ];
    _.forEach(tdaBestPerformacesData.awards, function(winners){
      _.forEach(winners.winners, function(arow){
          worksheet.addRow([arow.routinename, arow.studioname, winners.specialawardname]);
      });
    });
    var time = new Date();
    time = time.getTime().toString();
    time = time.substring(6,2);
    var filename = __dirname + '/TDA_specialawardwinners_' + tourdateid + time +'.xls';
    workbook.xlsx.writeFile(filename)
          .then(function() {
              res.download(filename, '/TDA_specialawardwinners_' + tourdateid + time +'.xls', function(err) {
                  if(!err) fs.unlink(filename);
              });
          });
  }
  else{
    res.render('reports/tda-best-performances',{
      data : { city, venue, dispdate },
      award_data : tdaBestPerformacesData.awards
    });
  }
});

router.get('/jump-scholarship-shipping-labels', function(req, res){
  jumpScholarshipShippingLabelsData = JSON.parse(JSON.stringify(jumpScholarshipShippingLabelsData));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  res.render('reports/jump-scholarship-shipping-labels',{
    dancers : jumpScholarshipShippingLabelsData.dancers
  });
});

router.get('/jump-scholarship-shipping-labels-with-logos', function(req, res){
  jumpScholarshipShippingLabelsWithLogosData = JSON.parse(JSON.stringify(jumpScholarshipShippingLabelsWithLogosData));
  var tourdateid = req.query.tourdateid;
  var eventid = 14;
  res.render('reports/jump-scholarship-shipping-labels-with-logos',{
    dancers : jumpScholarshipShippingLabelsWithLogosData.dancers
  });
});

router.get('/jump-season-vips-xls', function(req, res){
  jumpSeasonVipsXlsData = JSON.parse(JSON.stringify(jumpSeasonVipsXlsData));
  var tourdateid = req.query.tourdateid;
  var seasonid = 5;
  var eventid = 7;
  var safeevent = "jump";
  var workbook = new Excel.Workbook();
  workbook.views = [
    {
      firstSheet: 0, activeTab: 1
    }
  ];

  var worksheet = workbook.addWorksheet('Worksheet');
  worksheet.columns = [
    { header: 'First Name', id: 'first_name', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'first_name') + 10, style: { font: {name: 'Arial' } } },
    { header: 'Last Name', id: 'last_name', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'last_name'), style: { font: {name: 'Arial' } } },
    { header: 'Tour City', id: 'tour_city', width : 10, style: { font: {name: 'Arial' } } },
    { header: 'Workshop Lvl', id: 'workshop_lvl', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'workshop_lvl')+10, style: { font: {name: 'Arial' } } },
    { header: 'Studio', id: 'studio', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'studio_name'), style: { font: {name: 'Arial' } } },
    { header: 'Scholarshp Type', id: 'scholarshp type', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'scholarshp_type'), style: { font: {name: 'Arial' } } },
    { header: 'Winner', id: 'winner', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'winner')+5, style: { font: {name: 'Arial' } } },
    { header: 'City', id: 'city', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'city'), style: { font: {name: 'Arial' } } },
    { header: 'State', id: 'state', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'state')+5, style: { font: {name: 'Arial' } } },
    { header: 'Zip', id: 'zip', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'zip'), style: { font: {name: 'Arial' } } },
    { header: 'Country', id: 'country', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'country'), style: { font: {name: 'Arial' } } },
    { header: 'Phone', id: 'phone', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'phone'), style: { font: {name: 'Arial' } } },
    { header: 'Email', id: 'email', width : Util.setWidthAuto(jumpSeasonVipsXlsData.studios, 'email'), style: { font: {name: 'Arial' } } },
    { header: 'Studio E-Mail', id: 'studio e-mail', width : 45, style: { font: {name: 'Arial' } } }
  ];
  _.forEach(jumpSeasonVipsXlsData.studios, function(studio){
    _.forEach(studio.studio_email, function(item){
      console.log(item);
      if(item.length > 1){
        studio.studio_email_full = item.email;
      }
      else{
        studio.studio_email_full = null + item.email + '; ';
      }
    });
  });

  _.forEach(jumpSeasonVipsXlsData.studios, function(studio){
      worksheet.addRow([studio.first_name, studio.last_name, studio.tour_city, studio.workshop_lvl, studio.studio_name, studio.scholarshp_type, studio.winner ? "Yes" : "No", studio.city, studio.state, studio.zip, studio.country, studio.phone, studio.email, studio.studio_email_full]);
  });

  var time = new Date();
  time = time.getTime().toString();
  time = time.substring(6,3);
  var filename;
  var path;
  if(eventid === 7){
    filename = __dirname + '/'+safeevent + '_VIP_Season_ToDate_' + seasonid + time +'.xls';
    path = safeevent + '_VIP_Season_ToDate_' + seasonid + time +'.xls';
  }
  if(eventid === 8){
    filename = __dirname + '/'+safeevent + '_BO_Season_ToDate_' + seasonid + time +'.xls';
    path = safeevent + '_BO_Season_ToDate_' + seasonid + time +'.xls';
  }
  if(eventid === 18){
    filename = __dirname + '/'+safeevent + '_NSD_Season_ToDate_' + seasonid + time +'.xls';
    path = safeevent + '_NSD_Season_ToDate_' + seasonid + time +'.xls';
  }

  workbook.xlsx.writeFile(filename)
        .then(function() {
            res.download(filename, path, function(err) {
                if(!err) fs.unlink(filename);
            });
        });
});

router.get('/scholarship-winners', scholarshipWinners.renderReport);

router.get('/scholarship-winners-xls', scholarshipWinnersXLS.renderReport);

router.get('/scholarship-winners-lname', scholarshipWinners.renderReport);

router.get('/scholarship-templates', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  var miniRows = 1, juniorRows = 1, teensRows = 1, seniorsRows = 1;
  var cols = 19; // How many columns fit on a page.  Currently 23 with 40px wide divs.
  var miniSize = scholarshipTemplatesData.minis.length;
  var juniorSize = scholarshipTemplatesData.juniors.length;
  var teensSize = scholarshipTemplatesData.teens.length;
  var seniorsSize = scholarshipTemplatesData.seniors.length;

  while(miniRows*cols < miniSize) miniRows++;
  while(juniorRows*cols < juniorSize) juniorRows++;
  while(teensRows*cols < teensSize) teensRows++;
  while(seniorsRows*cols < seniorsSize) seniorsRows++;

  scholarshipTemplatesData.minis_rows = miniRows;
  scholarshipTemplatesData.juniors_rows = juniorRows;
  scholarshipTemplatesData.teens_rows = teensRows;
  scholarshipTemplatesData.seniors_rows = seniorsRows;
  scholarshipTemplatesData.minis_size = miniSize;
  scholarshipTemplatesData.juniors_size = juniorSize;
  scholarshipTemplatesData.teens_size = teensSize;
  scholarshipTemplatesData.seniors_size = seniorsSize;

  res.render('reports/scholarship-templates', scholarshipTemplatesData);
});

router.get('/studio-awards', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/studio-awards', studioAwardsData);
});

router.get('/studio-awards-well-rounded', function(req, res) {
  // query params
  var tourDateId = req.query.tourdateid;

  res.render('reports/studio-awards-well-rounded', studioAwardsWellRoundedData);
});

router.get('/scholarship-list', scholarshipList.renderReport);

module.exports = router;
