<!-- THIS REPORT ISN'T SHOWING ANY DATA -->
<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$eventid = db_one("eventid","tbl_tour_dates","id=$tourdateid");
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('n/d/Y',mktime(0,0,0,$mm,$dd,$yy));


//get award types
	$sa_data = array();
	$sql = "SELECT id,awardname FROM `tbl_studio_awards` WHERE eventid=$eventid";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$awards[] = $row;
		}
		foreach($awards as $award) {
			$sql2 = "SELECT tbl_date_studio_awards.id, tbl_date_studio_awards.winner, tbl_studios.name AS studioname FROM `tbl_date_studio_awards` LEFT JOIN tbl_studios ON tbl_studios.id=tbl_date_studio_awards.studioid WHERE tbl_date_studio_awards.awardtypeid=".$award["id"]." AND tbl_date_studio_awards.tourdateid=$tourdateid";
			$res2 = mysql_query($sql2) or die(mysql_error());
			if(mysql_num_rows($res2) > 0) {
				while($row2 = mysql_fetch_assoc($res2)) {
					$sa_data[$award["awardname"]][] = $row2;
				}
			}
		}
	}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 11px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:850px;margin: 0 auto;">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td style="vertical-align:top;width:772px;">
						<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Studio Awards</div>
						<div style="font-family:Unplug;font-size:12pt;"><?php print($venue); ?></div>
					</td>
					<td style="vertical-align:top;text-align:right;padding-top:25px;"><div style="font-family:Unplug;font-size:12pt;"><?php print($start_date); ?></div></td>
				</tr>
			</table>
			<?php
				if(count($sa_data) > 0) {
					foreach($sa_data as $key=>$group) {
						if(count($group) > 0) {?>
					<div style="margin:25px 0 7px;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;">
							<?=$key;?>
					</div>
					<table cellpadding="0" cellspacing="0" class="rtable" style="margin-bottom:30px;">
						<tr>
							<td class="thead" style="width:850px;border-right:1px solid #000000;">Studio</td>
						</tr>
					<?php

							foreach($group as $line) { ?>
						<tr>
							<td class="tbody" style="border-right:1px solid #000000;"><?php print(str_replace("&amp;","&",$line["studioname"])); ?></td>
						</tr>
					<?php } ?>
					</table>
			<?php  	}
				}
			} ?>
		</div>
	</body>
</html>