<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('F d, Y',mktime(0,0,0,$mm,$dd,$yy));

	$sql = "SELECT tbl_studios.name AS studioname, tbl_date_studios.total_fees, tbl_date_studios.studioid, tbl_date_studios.id AS datestudioid, tbl_date_studios.fees_paid, tbl_studios.email, tbl_studios.phone, tbl_studios.phone2, tbl_studios.fax, tbl_studios.contacts FROM `tbl_date_studios` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_studios.studioid WHERE tbl_date_studios.tourdateid=$tourdateid ORDER BY tbl_studios.name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
//			print_r($row);
			$row["studioname"] = stripslashes(str_replace("&#44;",",",str_replace("&amp;","&",$row["studioname"])));

			//check for missing bd jacket name
			$sql14 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_tda_bestdancer_data.profileid, tbl_date_dancers.studioid FROM `tbl_profiles` LEFT JOIN tbl_tda_bestdancer_data ON tbl_tda_bestdancer_data.profileid = tbl_profiles.id LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_profiles.id WHERE (tbl_tda_bestdancer_data.jacketname = '' OR tbl_tda_bestdancer_data.jacketname = 'Name on Jacket') AND tbl_date_dancers.studioid = ".$row["studioid"]." AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC";
			$res14 = mysql_query($sql14) or die(mysql_error());
			if(mysql_num_rows($res14) > 0) {
				while($row14 = mysql_fetch_assoc($res14)) {
					$row["nobdjacketname"][] = $row14;
					++$oicount;
				}
			}

			//check for missing bd jacket size
			$sql15 = "SELECT tbl_profiles.fname, tbl_profiles.lname, tbl_tda_bestdancer_data.profileid, tbl_date_dancers.studioid FROM `tbl_profiles` LEFT JOIN tbl_tda_bestdancer_data ON tbl_tda_bestdancer_data.profileid = tbl_profiles.id LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid = tbl_profiles.id WHERE tbl_tda_bestdancer_data.jacketsize = '' AND tbl_date_dancers.studioid = ".$row["studioid"]." AND tbl_tda_bestdancer_data.iscompeting > 0 AND tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid ORDER BY tbl_profiles.lname ASC";
			$res15 = mysql_query($sql15) or die(mysql_error());
			if(mysql_num_rows($res15) > 0) {
				while($row15 = mysql_fetch_assoc($res15)) {
					$row["nobdjacketsize"][] = $row15;
					++$oicount;
				}
			}

			//if either exist for row, add to studios array
			if(count($row["nobdjacketname"]) > 0 || count($row["nobdjacketsize"]) > 0 || count($row["nobdphoto"]) > 0 || count($row["noagediv"]) > 0 || count($row["waivers"]) > 0 || strlen($row["balance"]) > 0 || count($row["tbas"]) > 0 || count($row["noperfcat"]) > 0) {
				$row["contacts"] = json_decode($row["contacts"],true);
				$studios[] = $row;
			}

		}
	}

	//print_r($studios);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}
		</style>
		<script type="text/javascript">
			//window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Unplug;font-size:20pt;margin: 5px 0 0 0;"><?php print($city); ?> Studio Outstand. Items</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:5px;"><?php print($venue." / ".$start_date); ?></div>
		<?php
			if(count($studios) > 0) {
				foreach($studios as $studio) { ?>
					<div style="margin:10px 0;font-family:Unplug;font-size:16pt;font-style:italic;color:#3366FF;">
					<?php print($studio["studioname"]); ?>
					</div>
					<div style="font-style:italic;font-family:Verdana,Arial,sans-serif;font-size:12px;padding-bottom:25px;border-bottom:1px dotted #000000;">
						The following items need to be fixed to complete this studio's registration:
						<br/>
						<br/>
					<?php if(count($studio["contacts"]) > 0) {
							for($i=0;$i<count($studio["contacts"]);$i++) {
								if(strlen($studio["contacts"][$i]["fname"]) > 0) {
									print("Contact ".($i+1).": ".$studio["contacts"][$i]["fname"]." ".$studio["contacts"][$i]["lname"]);
									if(strlen($studio["contacts"][$i]["type"]) > 0)
										print(" (".$studio["contacts"][$i]["type"].")");
									print("<br/>");
								}
							}
						  } ?>
						<br/>
						Phone 1: <?php print(strlen($studio["phone"]) > 0 ? $studio["phone"] : "-"); ?>
						<br/>
						Phone 2: <?php print(strlen($studio["phone2"]) > 0 ? $studio["phone2"] : "-"); ?>
						<br/>
						Fax: <?php print(strlen($studio["fax"]) > 0 ? $studio["fax"] : "-"); ?>
						<br/>
						Email: <?php print(strlen($studio["email"]) > 0 ? $studio["email"] : "-"); ?>
						<br/>
						<div style="font-style:normal;line-height:18px;padding-left:10px;margin-top:5px;">
					<?php
						$pos = 1;

						  if(count($studio["nobdjacketname"]) > 0) {
						  	for($i=0;$i<count($studio["nobdjacketname"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Best Dancer ".strtoupper($studio["nobdjacketname"][$i]["fname"]." ".$studio["nobdjacketname"][$i]["lname"])." has not selected a JACKET NAME<br/>");
						  		++$pos;
						  	}
						  }
						  if(count($studio["nobdjacketsize"]) > 0) {
						  	for($i=0;$i<count($studio["nobdjacketsize"]);$i++) {
						  		print($pos.".&nbsp;&nbsp;&nbsp;Best Dancer ".strtoupper($studio["nobdjacketsize"][$i]["fname"]." ".$studio["nobdjacketsize"][$i]["lname"])." has not selected a JACKET SIZE<br/>");
						  		++$pos;
						  	}
						  }

						 ?>
						  </div>
					</div>
		<?php	}
			} ?>
		</div>
	</body>