"use strict";

module.exports = function(sequelize, DataTypes) {
	var dateSpecialAwards = sequelize.define('date_special_awards', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		special_award_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_special_awards',
				key: 'id'
			}
		},
		date_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_routines',
				key: 'id'
			}
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		}
	}, {
		timestamps: false,
		associate: function(models) {
			dateSpecialAwards.belongsTo(models.tbl_special_awards, {foreignKey: 'special_award_id', as: 'special_award'});
			dateSpecialAwards.belongsTo(models.date_routines, {foreignKey: 'date_routine_id', as: 'date_routine'});
		}
	});
	return dateSpecialAwards;
};