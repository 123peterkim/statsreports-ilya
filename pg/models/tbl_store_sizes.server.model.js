"use strict";

module.exports = function(sequelize, DataTypes) {
	var tbl_store_sizes = sequelize.define('tbl_store_sizes', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		size: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return tbl_store_sizes;
};