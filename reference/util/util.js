"use strict";
var db = require('../../pg/sequelize').models;
var sequelize = require('../../pg/sequelize').sequelize;
var async = require('async');
var moment = require('moment');
var Const = require('../util/const');

var path = require('path'),
  _ = require('lodash');

var Util = {
  tourDateDisplayDate: function (tourdate) {
    if (!tourdate.start_date || !tourdate.end_date) {
      return 'TBA';
    }
    var displayDate;
    var startDate;
    var endDate;
    if (typeof tourdate.start_date === 'string') {
      if (!tourdate.start_date.includes('Z')) {
        var startDateArr = tourdate.start_date.replace(/-/g, '/').split('/');
        startDate = new Date(+startDateArr[0], +startDateArr[1] - 1, +startDateArr[2], 0, 0, 0, 0);
        var endDateArr = tourdate.end_date.replace(/-/g, '/').split('/');
        endDate = new Date(+endDateArr[0], +endDateArr[1] - 1, +endDateArr[2], 0, 0, 0, 0);
      } else {
        startDate = new Date(tourdate.start_date);
        endDate = new Date(tourdate.end_date);
      }
    } else {
      startDate = new Date(tourdate.start_date.getFullYear(), tourdate.start_date.getMonth(), tourdate.start_date.getDate(), 0, 0, 0, 0);
      endDate = new Date(tourdate.end_date.getFullYear(), tourdate.end_date.getMonth(), tourdate.end_date.getDate(), 0, 0, 0, 0);
    }

    var startDateObj = {};
    var endDateObj = {};
    var monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    var key = [
      {
        type: 'month',
        format: function (month) {
          return month.getMonth();
        }
      },
      {
        type: 'day',
        format: function (day) {
          return day.getDate();
        }
      },
      {
        type: 'year',
        format: function (year) {
          return year.getFullYear();
        }
      }
    ];
    _.forEach(key, function (type) {
      if (type.type !== 'month') {
        startDateObj[type.type] = type.format(startDate);
        endDateObj[type.type] = type.format(endDate);
      } else {
        startDateObj[type.type] = monthNames[type.format(startDate)];
        endDateObj[type.type] = monthNames[type.format(endDate)];
      }
    });

    if (startDate === endDate) {
      displayDate = startDateObj.month + ' ' + startDateObj.day + ', ' + startDateObj.year;
    } else {
      if (startDateObj.month !== endDateObj.month) {
        displayDate = startDateObj.month + ' ' + startDateObj.day + '-' + endDateObj.month + ' ' + endDateObj.day + ', ' + endDateObj.year;
      } else {
        displayDate = startDateObj.month + ' ' + startDateObj.day + '-' + endDateObj.day + ', ' + endDateObj.year;
      }
    }
    return displayDate;
  },
  /**
   * This function receives an array of objects and return the column width
   * based on the largest value identified by the context parameter. It also
   * considers the length of the column name
   * @param {Array} arr the array of objects
   * @param {String} context the identifier of the column in the object.
   * @param {String} columnName the display name of the column.
   * @returns {Number} The column width according to the largest item, including the column name.
   */
  setWidthAuto: function (arr, context, columnName) {
    var max = (columnName || '').length + 4;
    _.forEach(arr, function (item) {
      _.forEach(item, function (value, key) {
        if (key == context) {
          if (value != null && max < value.length) {
            max = value.toString().length + 4;
          }
        }
      });
    });
    return max != 0 ? max : 10;
  },
  str_pad: function (input, padLength, padString, padType) {
    var half = '';
    var padToGo;
    var _strPadRepeater = function (s, len) {
      var collect = '';
      while (collect.length < len) {
        collect += s;
      }
      collect = collect.substr(0, len);
      return collect;
    };
    input += '';
    padString = padString !== undefined ? padString : ' ';
    if (padType !== 'STR_PAD_LEFT' && padType !== 'STR_PAD_RIGHT' && padType !== 'STR_PAD_BOTH') {
      padType = 'STR_PAD_RIGHT';
    }
    if ((padToGo = padLength - input.length) > 0) {
      if (padType === 'STR_PAD_LEFT') {
        input = _strPadRepeater(padString, padToGo) + input;
      } else if (padType === 'STR_PAD_RIGHT') {
        input = input + _strPadRepeater(padString, padToGo);
      } else if (padType === 'STR_PAD_BOTH') {
        half = _strPadRepeater(padString, Math.ceil(padToGo / 2));
        input = half + input + half;
        input = input.substr(0, padLength);
      }
    }
    return input;
  },
  formatBirthdate: function (birthdate) {
    if (typeof birthdate === 'string') {
      if (!birthdate.includes('Z')) {
        var dateArr = birthdate.replace(/-/g, '/').split('/');
        birthdate = new Date(+dateArr[0], +dateArr[1] - 1, +dateArr[2], 0, 0, 0, 0);
      } else {
        birthdate = new Date(birthdate);
      }
    } else {
      birthdate = new Date(birthdate.getFullYear(), birthdate.getMonth(), birthdate.getDate(), 0, 0, 0, 0);
    }
    return birthdate;
  },
  getAge: function (birthdate, dateToCalculateWith) {
    if (!birthdate) {
      return null;
    }
    var dateToCalculate;
    if (typeof birthdate === 'string') {
      if (!birthdate.includes('Z')) {
        var dateArr = birthdate.replace(/-/g, '/').split('/');
        birthdate = new Date(+dateArr[0], +dateArr[1] - 1, +dateArr[2], 0, 0, 0, 0);
      } else {
        birthdate = new Date(birthdate);
      }
    } else {
      birthdate = new Date(birthdate.getFullYear(), birthdate.getMonth(), birthdate.getDate(), 0, 0, 0, 0);
    }
    if (!dateToCalculateWith) {
      dateToCalculate = Date.now();
    } else {
      dateToCalculate = new Date(dateToCalculateWith, 0, 1, 0, 0, 0, 0)
    }
    var ageDifMs = dateToCalculate - birthdate.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    var age = ageDate.getUTCFullYear() - 1970;
    return (age > 0) ? age : 0;
  },

  /**
   * WAITING FOR DATE_ROUTINE_SCORING IMPLEMENTATION
   * This method returns the critiquesheet data for each routine
   * @param {Number} tourDateId
   * @param {Number} studioId
   * @returns {Promise} A promise for that resolves with the expected data
   */
  getDateCritiqueSheet: function(tourDateId, studioId) {
    
    if(tourDateId > 0 && studioId > 0) {
      data = [];
      fdata = [];
      // SQL: SELECT * FROM `tbl_date_routines` WHERE tourdateid=$tourdateid AND studioid=$studioid
      return db.date_routines.findAll({
        where: {
          tour_date_id: tourDateId,
          studio_id: studioId
        },
        include: [
          { model: db.tbl_routines, as: 'routine'},
          { model: db.date_routines_scoring, as: 'scores', include: [
            { model: db.tbl_competition_groups, as: 'competition_group' }
          ]}
        ]
      })
      .then(function(results) { 
        var compGroups = {};

        _.forEach(results, function(dateRoutine) {
          var score = dateRoutine.routine.scores[0];
          var key = score.competition_group.db_key;
          var routine = {
            routine_name: dateRoutine.routine.name,
            mktime: score.time,
            dispnum: score.has_a
                      ? score.number + '.a' 
                      : score.number
          };
          if(key in compGroups) {
            compGroups[key].push(routine);
          }
          else {
            compGroups[key] = [routine];
          }
        });

        return compGroups;
      });
    }
  },

  checkWaiver: function(fname, lname, birth_date, profileid, returnWaiverId, saveNew=false) {
    var waiverId = 0;
  },

  /**
   * Returns a nicely-formated tour_date date string
   * ex.: 
   *  January 4-6, 2013
   *  February 28 - March 2, 2013
   *  May 28, 2013
   * 
   * @param {Number} tourDateId 
   * @returns {Promise<String>} a promise resolved with the tourdate date string
   */
  getTourdateDispdate(tourDateId) {
    if(tourDateId) {
      return db.tbl_tour_dates.findAll({
        attributes: ['start_date', 'end_date'],
        where: { id: tourDateId },
        limit: 1
      })
      .then(function(results) {
        var td = results[0];
        var startDate, endDate;
        if(td.start_date) {
          if(moment(td.start_date).isSame(td.ed_date)) {
            return moment(td.start_date).format('MMMM D, Y');
          }
          else {
            startDate = moment(td.start_date).format('MMMM D');
            if (moment(td.start_date).isSame(td.end_date, 'month')) {
              return startDate + moment(td.end_date).format('-D, Y');  
            }
            else {
              return startDate + moment(td.end_date).format(' - MMMM D, Y');
            }
          }
        }

        return '';
      });
    }
  },

  /**
   * This method produces an array of dancer with workshop = "observer2"
   * It return a promise which is the result of the query.
   * 
   * @param {Number} tourDateId 
   * @param {Number} studioId 
   * @returns {Promise<array>}
   */
  getStudioObserverRealOrder(tourDateId, studioId) {
    if(tourDateId && studioId) {
      // SELECT tbl_date_dancers.id, tbl_date_dancers.fee, tbl_date_dancers.profileid, tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.birth_date, tbl_date_dancers.age 
      // FROM `tbl_date_dancers` 
      // LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid 
      // WHERE tbl_date_dancers.tourdateid=$tourdateid 
      // AND tbl_date_dancers.studioid=$studioid 
      // AND tbl_date_dancers.workshoplevelid=8 
      // ORDER BY tbl_profiles.lname ASC
      return db.date_dancers.findAll({
        where: {
          id: tourDateId,
          studio_id: studioId,
        },
        include: [
          { model: db.tbl_registrations_dancers, as: 'registrations_dancer', include: [
              { model: db.tbl_dancers, as: 'dancer', include: [
                  { model: db.tbl_persons, as: 'person' }
              ]},
              { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
                  { model: db.tbl_levels, as: 'level' }
              ]}
          ]}
        ]
      })
      .then(function(results) {
        var obs = [];
        _.forEach(results, function(dateDancer) {
          _.forEach(dateDancer.registrations_dancer, function(regDancer) {
            if(!dancer.person.birthdate) dancer.person.birthdate = '-';
            obs.push({
              workshop_level_name: Const.OBSERVER,
              fname: regDancer.dancer.person.fname,
              lname: regDancer.dancer.person.lname,
              birthdate: regDancer.dancer.person.birthdate,
              age: Util.getAge(regDancer.dancer.person.birthdate),
              has_scholarship: regDancer.has_scholarship,
              one_day: regDancer.one_day,
              fee: regDancer.fees.workshop
            });
          });
        });

        if(obs.length > 0) {
          var nobs = [];
         _.sortBy(obs, o => o.lname);
        }

        return obs
      });
    }
  },

  /**
   * This method get a list of all date dancers from the tour_date_id.
   * It returns a promise with the result of a query in the date_dancers table
   * including the objects:
   * date_dancers
   *  - registrations_dancer
   *   - dancer
   *    - person
   *  - workshop_level
   *   - level
   * 
   * @param {any} tourDateId 
   * @param {boolean} [showTeachObs=false] 
   * @param {boolean} [bdonly=false] 
   * @returns {Promise} query results
   */
  getAllDateDancersList(tourDateId, showTeachObs=false, bdonly=false) {
    var queryOptions = {};
    if(bdonly) {
      /*SELECT tbl_date_dancers.id AS datedancerid, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname 
        FROM `tbl_date_dancers` 
        LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_date_dancers.profileid 
        WHERE tbl_date_dancers.tourdateid='$tourdateid' 
        AND tbl_date_dancers.workshoplevelid!=1 
        AND tbl_date_dancers.workshoplevelid!=8 
        AND tbl_date_dancers.vip=1 
        ORDER BY tbl_profiles.lname ASC*/
      queryOptions = {
        where: {
          tour_date_id: tourDateId
        },
        include: [
          { model: db.tbl_registrations_dancers, as: 'registrations_dancer', include: [
            { model: db.tbl_dancers, as: 'dancer', include: [
              { model: db.tbl_persons, as: 'person' }
            ]},
            { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
              { model: db.tbl_levels, as: 'level', where: {
                name: { $ne: Const.TEACHER },
                name: { $ne: Const.OBSERVER },
                vip: 1
              }}
            ]}
          ]}
        ],
        order: [
          [
            {model: db.tbl_registrations_dancers, as: 'registrations_dancer'},
            {model: db.tbl_dancers, as: 'dancer'},
            {model: db.tbl_persons, as: 'person'}, 'lname', 'ASC'
          ]
        ]
      }
    }
    else if(showTeachObs) {
      queryOptions = {
        where: {
          tour_date_id: tourDateId
        },
        include: [
          { model: db.tbl_registrations_dancers, as: 'registrations_dancer', include: [
            { model: db.tbl_dancers, as: 'dancer', include: [
              { model: db.tbl_persons, as: 'person' }
            ]},
            { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
              { model: db.tbl_levels, as: 'level' }
            ]}
          ]}
        ],
        order: [
          [
            {model: db.tbl_registrations_dancers, as: 'registrations_dancer'},
            {model: db.tbl_dancers, as: 'dancer'},
            {model: db.tbl_persons, as: 'person'}, 'lname', 'ASC'
          ]
        ]
      }
    }
    else {
      queryOptions = {
        where: {
          tour_date_id: tourDateId
        },
        include: [
          { model: db.tbl_registrations_dancers, as: 'registrations_dancer', include: [
            { model: db.tbl_dancers, as: 'dancer', include: [
              { model: db.tbl_persons, as: 'person' }
            ]},
            { model: db.tbl_workshop_levels, as: 'workshop_level', include: [
              { model: db.tbl_levels, as: 'level', where: {
                name: { $ne: Const.TEACHER },
                name: { $ne: Const.OBSERVER }
              }}
            ]}
          ]}
        ],
        order: [
          [
            {model: db.tbl_registrations_dancers, as: 'registrations_dancer'},
            {model: db.tbl_dancers, as: 'dancer'},
            {model: db.tbl_persons, as: 'person'}, 'lname', 'ASC'
          ]
        ]
      }
    }

    return db.date_dancers.findAll(queryOptions);
  },

  getCompetitionScheduleInOrder(tourDateId, compGroup, withDancers=false, room=0) {
    var queryOptions = {
      where: {
        
      }
    }
  }
};

module.exports = Util;