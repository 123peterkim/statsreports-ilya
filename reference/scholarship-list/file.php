<?php
	include("../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$start_date_a = db_one("start_date","tbl_tour_dates","id=$tourdateid");
	list($yy,$mm,$dd) = explode("-",$start_date_a);
	$start_date = date('F d',mktime(0,0,0,$mm,$dd,$yy));

	$sql = "SELECT tbl_date_dancers.scholarship_code, tbl_date_dancers.age, tbl_studios.name AS studioname, tbl_profiles.id AS profileid, tbl_profiles.fname, tbl_profiles.lname FROM `tbl_date_dancers` LEFT JOIN tbl_studios ON tbl_studios.id = tbl_date_dancers.studioid LEFT JOIN tbl_profiles ON tbl_profiles.id = tbl_date_dancers.profileid WHERE tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.scholarship_code > 0 ORDER BY tbl_date_dancers.scholarship_code ASC";
//	print($sql);exit();
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			if($row["age"] < 11)
				$row["workshoplevelname"] = "Mini";
			if($row["age"] > 10 && $row["age"] < 13)
				$row["workshoplevelname"] = "Junior";
			if($row["age"] > 12 && $row["age"] < 16)
				$row["workshoplevelname"] = "Teen";
			if($row["age"] >= 16)
				$row["workshoplevelname"] = "Senior";
			$dancers[] = $row;
		}

		$aa = array("Mini","Junior","Teen","Senior");
		$anew = array();
		foreach($aa as $ana) {
			foreach($dancers as $dkey=>$dancer) {
				if($dancer["workshoplevelname"] == $ana) {
					$anew[$ana][] = $dancer;
					unset($dancers[$dkey]);
				}
			}
		}
	}

//	print_r($anew);exit();
//	$dancers = $anew;
	$dancers = $anew;
	$current_wl = "Mini";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td{
				font-size: 10px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 1px 0 1px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>

		<div style="width:705px;margin: 0 auto;">
			<div style="font-family:Unplug;font-size:20pt;margin: 25px 0 0 0;"><?php print($city); ?> Scholarship Participants</div>
			<div style="font-family:Unplug;font-size:12pt;margin-bottom:30px;"><?php print($venue." - ".$start_date); ?></div>
			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;"><?php print($current_wl." (".count($dancers["Mini"]).")"); ?></div>
			<table cellpadding="0" cellspacing="0" class="rtable">
				<tr>
					<td class="thead" style="width:90px;">Scholar. ID</td>
					<td class="thead" style="width:280px;">Studio</td>
					<td class="thead" style="width:220px;">Dancer</td>
					<td class="thead" style="width:90px;border-right:1px solid #000000;">Age</td>
				</tr>
		<?php
		if(count($dancers) > 0) {
			foreach($dancers as $wlkey=>$wldancers) {
				foreach($wldancers as $dancer) {
					if($dancer["workshoplevelname"] != $current_wl) { $current_wl = $dancer["workshoplevelname"]; ?>
			</table>

			<div style="font-family:Crackhouse;font-size:18pt;color:#FF0000;page-break-before:always;margin-top:10px;"><?php print($current_wl." (".count($wldancers).")"); ?></div>
			<table cellpadding="0" cellspacing="0" class="rtable">
				<tr>
					<td class="thead" style="width:90px;">Scholar. ID</td>
					<td class="thead" style="width:280px;">Studio</td>
					<td class="thead" style="width:220px;">Dancer</td>
					<td class="thead" style="width:90px;border-right:1px solid #000000;">Age</td>
				</tr>
				<?php } ?>
				<tr>
					<td class="tbody"><?php print($dancer["scholarship_code"]); ?></td>
					<td class="tbody"><?php print($dancer["studioname"]); ?></td>
					<td class="tbody"><?php print($dancer["fname"]." ".$dancer["lname"]); ?></td>
					<td class="tbody" style="border-right:1px solid #000000;"><?php print($dancer["age"]); ?></td>
				</tr>
		<?php  	}
			}
		}
		else print("<td colspan='4'>You gotta assign scholarship codes first.</td>"); ?>
			</table>
		</div>
	</body>
</html>