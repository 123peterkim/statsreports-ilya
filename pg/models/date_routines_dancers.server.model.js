"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_routines_dancers = sequelize.define('date_routines_dancers', {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
      autoIncrement: true
		},
		date_routine_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_routines',
				key: 'id'
			}
		},
		date_dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_dancers',
				key: 'id'
			}
		},
		registrations_routines_dancer_id: {
      type: DataTypes.INTEGER,
      references: {
        model: 'registrations_routines_dancers',
        key: 'id'
      }
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_routines_dancers.belongsTo(models.date_dancers, {foreignKey: 'date_dancer_id', as: 'date_dancer'});
			date_routines_dancers.belongsTo(models.date_routines, {foreignKey: 'date_routine_id', as: 'date_routine'});
      date_routines_dancers.belongsTo(models.registrations_routines_dancers, {foreignKey: 'registrations_routines_dancer_id', as: 'registrations_routines_dancer'});
		}
	});
	return date_routines_dancers;
};