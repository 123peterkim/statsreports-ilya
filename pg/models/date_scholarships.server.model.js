"use strict";

module.exports = function(sequelize, DataTypes) {
	var date_scholarships = sequelize.define('date_scholarships', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true
		},
		tour_date_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_tour_dates',
				key: 'id'
			}
		},
		dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_dancers',
				key: 'id'
			}
		},
		date_dancer_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'date_dancers',
				key: 'id'
			}
		},
		staff_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_staff',
				key: 'id'
			}
		},
		scholarship_id: {
			type: DataTypes.INTEGER,
			references: {
				model: 'tbl_scholarships',
				key: 'id'
			}
		},
		winner: {
			type: DataTypes.BOOLEAN
		},
		code: {
			type: DataTypes.STRING
		}
	}, {
		timestamps: false,
		associate: function(models) {
			date_scholarships.belongsTo(models.tbl_tour_dates, {foreignKey: 'tour_date_id', as: 'tour_date'});
			date_scholarships.belongsTo(models.tbl_dancers, {foreignKey: 'dancer_id', as: 'dancer'});
			date_scholarships.belongsTo(models.date_dancers, {foreignKey: 'date_dancer_id', as: 'date_dancer'});
			date_scholarships.belongsTo(models.tbl_staff, {foreignKey: 'staff_id', as: 'staff'});
			date_scholarships.belongsTo(models.tbl_scholarships, {foreignKey: 'scholarship_id', as: 'scholarship'});

		}
	});
	return date_scholarships;
};