"use strict";

module.exports = function(sequelize, DataTypes) {
	var dateScheduleWorkshopRooms = sequelize.define('date_schedule_workshop_rooms', {
		number: {
		  type: DataTypes.INTEGER,
			references: {
			  model: 'date_schedule_workshops',
			  key: 'id'
			}
		},
		name: {
		  type: DataTypes.STRING
		},
		bold: {
		  type: DataTypes.INTEGER
		},
		highlight: {
		  type: DataTypes.INTEGER
		}
	}, {
		timestamps: false,
		associate: function(models) {

		}
	});
	return dateScheduleWorkshopRooms;
};