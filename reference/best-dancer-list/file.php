<?php
	include("../../../includes/util.php");

	$tourdateid = intval($_GET["tourdateid"]);
	$city = db_one("city","tbl_tour_dates","id=$tourdateid");
	$venue = db_one("venue_name","tbl_tour_dates","id=$tourdateid");
	$dispdate = get_tourdate_dispdate($tourdateid);
	$wlorder = array("Senior","Teen","Junior","Mini");
	$bdancers = array();
	$dancers = array();
	$sql = "SELECT tbl_tda_bestdancer_data.profileid, tbl_tda_bestdancer_data.iscompeting, tbl_date_dancers.age, tbl_profiles.lname, tbl_profiles.fname, tbl_profiles.gender, tbl_studios.name AS studioname FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid WHERE tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid='$tourdateid' ORDER BY tbl_profiles.gender ASC, tbl_profiles.lname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$count = 0;
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = stripslashes($row["studioname"]);
			//goddamn ages
			$anage = intval($row["age"]);
			if($anage < 11)
				$row["realwl"] = "Mini";
			if($anage > 10 && $age < 13)
				$row["realwl"] = "Junior";
			if($anage > 12 && $age < 16)
				$row["realwl"] = "Teen";
			if($anage > 15)
				$row["realwl"] = "Senior";

			$bdancers[] = $row;
		}

		//sort by workshop level
		foreach($wlorder as $wlkey=>$awl) {
			foreach($bdancers as $dkey=>$dancer) {
				if($dancer["realwl"] == $awl) {
					++$count;
					$cdancers[$awl][] = $dancer;
					unset($bdancers[$wlkey][$key]);
				}
			}
		}

		//sort by gender FUCKING FUCK
		foreach($cdancers as $wlkey=>$wldancers) {
			foreach($wldancers as $wldancer)
				$dancers[$wlkey][$wldancer["gender"]][] = $wldancer;
		}
	}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<style>
			html { margin: 0; padding: 0;}

			body {
				font-family: Tahoma, Arial, Helvetica, sans-serif;
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
			}

			.rtable tr td {
				font-size: 12px;
			}

			.thead {
				padding: 1px 0;
				background-color:#DDDDDD;
				text-align: center;
				font-weight: bold;
				font-wize:12px;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
				border-top: 1px solid #000000;
			}

			.tbody {
				padding: 5px 0px 5px 2px;
				text-align: left;
				border-left: 1px solid #000000;
				border-bottom: 1px solid #000000;
			}
		</style>
		<script type="text/javascript">
		//	window.print();
		</script>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	</head>
	<body>
		<div style="width:705px;">
			<div style="font-family:Georgia;font-size:20px;"><?php print($city); ?> Best Dancer List</div>
			<span style="font-family:Georgia;font-size:14px;"><?php print($dispdate); ?></span>
			<table cellpadding="0" cellspacing="0" style="margin: 10px 0;">
				<tr>
					<td><div style="width: 25px;height:12px;background-color:#FFDDDD;border:1px dotted #333333;"></div></td>
					<td><div style="font-size:11px;">&nbsp;&nbsp; - not competing</div></td>
				</tr>
			</table>
		<?php
			if(count($dancers) > 0) {
				foreach($dancers as $workshoplevelname=>$wlgenders) { ?>
					<div style="page-break-after:always;">
					<? foreach($wlgenders as $wlgender=>$genderdancers) { ?>
						<div style="color:#FF0000;font-weight:bold;margin-top:10px;"><?php print($workshoplevelname." - ".$wlgender);?></div>
						<table cellpadding="0" cellspacing="0" class="rtable" style="margin-top:3px;">
							<tr>
								<td class="thead" style="width:100px;border-left:none;">Check-In</td>
								<td class="thead" style="width:270px;">Dancer</td>
								<td class="thead" style="width:250px;">Studio</td>
								<td class="thead" style="border-right: 1px solid #000000;width:70px;">Age</td>
							</tr>
						<?php	foreach($genderdancers as $dancer) { ?>
							<tr>
								<td class="tbody" style="<?php if($dancer["iscompeting"] != 1) print("background-color:#FFDDDD !important;"); ?>border-left:none;">&nbsp;</td>
								<td class="tbody" style="<?php if($dancer["iscompeting"] != 1) print("background-color:#FFDDDD !important;"); ?>"><?php print(stripslashes($dancer["fname"]." ".$dancer["lname"])); ?></td>
								<td class="tbody" style="<?php if($dancer["iscompeting"] != 1) print("background-color:#FFDDDD !important;"); ?>"><?php print(stripslashes($dancer["studioname"])); ?></td>
								<td class="tbody" style="<?php if($dancer["iscompeting"] != 1) print("background-color:#FFDDDD !important;"); ?>text-align:center;padding-right:3px;border-right: 1px solid #000000;"><?php print($dancer["age"]);?></td>
							</tr>
						<?php } ?>
						</table>
					<?php } ?>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	</body>
</html>