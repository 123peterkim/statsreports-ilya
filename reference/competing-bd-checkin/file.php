<?php
	include("../../../includes/util.php");
	$tourdateid = intval($_GET["tourdateid"]);

	$allshit = array();

	$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_date_dancers.age, tbl_tda_bestdancer_data.profileid, tbl_studios.name AS studioname,  tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender, tbl_date_dancers.scholarship_code FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.age < 13 AND tbl_tda_bestdancer_data.iscompeting=1 ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);

			$allshit["mijr"][] = $row;
		}
	}
	$sql = "SELECT tbl_tda_bestdancer_data.id AS tdabdid, tbl_date_dancers.age, tbl_tda_bestdancer_data.profileid, tbl_studios.name AS studioname,  tbl_profiles.fname, tbl_profiles.lname, tbl_profiles.gender, tbl_date_dancers.scholarship_code FROM `tbl_tda_bestdancer_data` LEFT JOIN tbl_profiles ON tbl_profiles.id=tbl_tda_bestdancer_data.profileid LEFT JOIN tbl_studios ON tbl_studios.id=tbl_tda_bestdancer_data.studioid LEFT JOIN tbl_date_dancers ON tbl_date_dancers.profileid=tbl_tda_bestdancer_data.profileid WHERE tbl_tda_bestdancer_data.tourdateid=$tourdateid AND tbl_date_dancers.tourdateid=$tourdateid AND tbl_date_dancers.age > 12 AND tbl_tda_bestdancer_data.iscompeting=1 ORDER BY tbl_profiles.lname ASC, tbl_profiles.fname ASC";
	$res = mysql_query($sql) or die(mysql_error());
	if(mysql_num_rows($res) > 0) {
		while($row = mysql_fetch_assoc($res)) {
			$row["studioname"] = str_replace("&amp;","&",$row["studioname"]);
			$allshit["tnsr"][] = $row;
		}
	}
//	print_r($allshit);exit();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Best Dancer Check-In</title>
		<style type="text/css">
			html { margin: 0; padding: 0;}

			body {
				text-align: left;
				margin: 0; padding: 0;
				color: #000000;
				font-size: 12px;
				font-family: Tahoma, Arial, sans-serif;
			}
			.dtable thead tr th {
				background-color: #000000;
				color: #FFFFFF;
				font-family: 'Trajan Pro';
				padding: 6px 0 2px;
				text-align: center;
				font-size: 15px;
				font-weight: normal;
			}
			.dtable tbody tr td {
				border-left: 1px solid #CCCCCC;
				border-bottom: 1px solid #CCCCCC;
				padding: 8px 0;
			}
		</style>
	</head>
	<body>
	<?php
		foreach($allshit as $adname=>$dancers) {
	?>
		<div style="width:720px;page-break-after:always;">
			<table cellpadding="0" cellspacing="0" style="width:100%;">
				<tr>
					<td>
						<img src="tda_logo.jpg" alt="" style="width: 200px;" />
					</td>
					<td style="text-align:right;">
						<div style="font-family:'Trajan Pro';font-size:27px;">BEST DANCER CHECK-IN</div>
						<div style="font-family:'Trajan Pro';font-size:20px;"><?php if($adname=="mijr") print("Mini / Junior"); else print("Teen / Senior"); ?></div>
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" style="width:100%;margin-top:15px;" class="dtable">
				<thead>
					<tr>
						<th style="width: 70px;">AUD. #</th><th style="width:160px;">DANCER</th><th style="width:280px;">STUDIO</th><th style="width:50px;">AGE</th><th>CHECKED IN</th>
					</tr>
				</thead>
				<tbody>
				<?php
					foreach($dancers as $dancer) { ?>
					<tr>
						<td style="text-align:center;"><?php print($dancer["scholarship_code"]);?></td>
						<td style="padding-left:3px;"><?php print($dancer["fname"]." ".$dancer["lname"]);?></td>
						<td style="padding-left:3px;"><?php print($dancer["studioname"]);?></td>
						<td style="padding-left:3px;text-align:center;"><?php print($dancer["age"]);?></td>
						<td style="border-right: 1px solid #CCCCCC;">&nbsp;</td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
	<?php
	  } ?>
	</body>
</html>